<?php
    return [
        'discount-min-price' => 'Do not use this discount code. The orders given below.',
        'discount-max-price' => 'Do not use this discount code. The orders exceeded.',
        'discount-not-found' => 'This discount code can not be found in the system.',
        'discount-empty' => 'Discount code out',
        'discount-used' => 'This discount code is already taken.',
        'discount-expire' => 'Discount code expires',
        'discount-correct' => 'The discount code is valid',
    ];
?>