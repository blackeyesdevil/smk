@extends('frontend.layout.main-layout')

@section('title', ' - Thank You')

@section('css')
    {!! Html::style('css/frontend/thank-you.css') !!}
@endsection

@section('content')
    <div id="content">
      <div><img id="correct" src="{{ URL::asset('images/correct.png')}}"></img></div>
      <div><h3 style="text-align: center;">Thank you!</h3></div>

    <!-- <div id="order">
        <div class="" id="head">
          <span class="col-xs-6">Your Order</span>
          <span class="col-xs-6" id="thank-order-id">Order ID: AA00001</span>

        </div>
        <div class="product-item">
          <span class="product-item col-xs-6">BRIX BOX</span>
          <span class="product-item col-xs-6">580 THB</span>
          <span class="product-option col-xs-12">Ice Cream Chocolate Flavors</span>
          <span class="product-option col-xs-12">BRIX BOX</span>
        </div>
        <div class="product-total">
          <span class="col-xs-6"></span>
          <span class="col-xs-6">Total : 580 THB</span>
        </div> -->
        <div class="text-center"><button class="finish-btn btn"  style="margin-top: 20px;">Go to Home</button></div>
    <!-- </div> -->
    </div>
@endsection

@section('script')
<script>
$('.finish-btn').on('click',function(){
  window.location=base_url;
});
$('#content-checkout').hide();
$('#headconfirm').hide();
$('#yes').hide();
$('#no').hide();
$('#personalize-otp').hide();
$('#personalize').hide();
$('#no').hide();
</script>
@endsection
