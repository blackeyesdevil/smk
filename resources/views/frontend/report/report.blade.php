<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
{!! Html::style('https://fonts.googleapis.com/css?family=Open+Sans:400,300,700') !!}
{!! Html::style('fonts/foundation-icons/foundation-icons.css') !!}
{!! Html::style('css/frontend/cashier-table2.css') !!}
{!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
<script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
{!! Html::script('assets/global/scripts/jquery.min.js') !!}
{!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
</head>

<body>
    <div class="container-fluid">
        <div id="btn-type" class="col-sm-12">
            <a href="{{ url()->to('/report/print-eod/{gen_token}') }}"><div class="btn-type">END OF DAY</div></a>
            <a href="{{ url()->to('/report/print-z-out/{gen_token}') }}"><div class="btn-type">Z-OUT</div></a>
        </div>
    </div>
<!-- {!! Html::script('js/frontend/cashier-table2.js') !!} -->
{!! Html::script('js/frontend/cashier-table3.js') !!}
<script>
    var api_url = '{{ URL::to("api") }}';
</script>
</body>
</html>
