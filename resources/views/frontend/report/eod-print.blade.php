<!DOCTYPE html>
<html>
<head>
    {!! Html::style('css/frontend/report-print.css') !!}
</head>
<body>
    <div id="print">
        <div class="company-info">
            <div class="company-logo text-center"><img src="{{ url()->asset('images/logo-b.png') }}"></div><br>
            <div class="text-center">
                <b>End of Day Report</b><br>
                @if($till != ' ')
                    <b>For Till {{$till}} </b><br>
                @endif
                As of 01 October 2016<br>
                Report was run on {{ date('d F Y H:i') }}
            </div><br>
            <hr class="style2">
            <div><b>Product Sales by category</b></div>
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td class="left"><b>Category</b></td>
                    <td align="right"><b>Gross Sales &nbsp;</b></td>
                    <td align="right"><b>Discount&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                    <td align="right"><b>Net Sales</b></td>
                </tr>
                <tr>
                    <td class="left">Appetizer</b></td>
                    <td align="right">19,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">1,550.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">17,450.00</td>
                </tr>
                <tr>
                    <td class="left">Soup</b></td>
                    <td align="right">19,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">1,550.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">17,450.00</td>
                </tr>
                <tr>
                    <td class="left">Salad</b></td>
                    <td align="right">19,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">1,550.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">17,450.00</td>
                </tr>
                <tr>
                    <td class="left">Dessert</b></td>
                    <td align="right">19,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">1,550.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">17,450.00</td>
                </tr>
                <tr>
                    <td class="left">Cocktail</b></td>
                    <td align="right">19,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">1,550.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">17,450.00</td>
                </tr>
                <tr>
                    <td class="left">Drinks</b></td>
                    <td align="right">19,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">1,550.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">17,450.00</td>
                </tr>
                <tr>
                    <td class="left">Cake</b></td>
                    <td align="right">19,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">1,550.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">17,450.00</td>
                </tr>
                <tr>
                    <td class="left">Bakery</b></td>
                    <td align="right">19,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">1,550.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">17,450.00</td>
                </tr>
                <tr>
                    <td class="left">Main Course</b></td>
                    <td align="right">19,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">1,550.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">17,450.00</td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Gross Sales</b></td>
                    <td align="right" colspan="2"><b>152,000.00</b></td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Discount</b></td>
                    <td align="right" colspan="2"><b>12,000.00</b></td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Net Saless</b></td>
                    <td align="right" colspan="2"><b>139,600.00</b></td>
                </tr>
                <tr>
                    <td colspan="4"><hr class="style2"></td>
                </tr>
                <tr>
                    <td colspan="4"><div><b>Product Sales by Shop</b></div></td>
                </tr>
                <tr>
                    <td class="left"><b>Shop</b></td>
                    <td align="right"><b>Gross Sales &nbsp;</b></td>
                    <td align="right"><b>Discount&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                    <td align="right"><b>Net Sales</b></td>
                </tr>
                <tr>
                    <td class="left">Cafe Chilli</b></td>
                    <td align="right">38,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">3,100.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">34,900.00</td>
                </tr>
                <tr>
                    <td class="left">Kuppadeli</b></td>
                    <td align="right">38,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">3,100.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">34,900.00</td>
                </tr>
                <tr>
                    <td class="left">Brix Dessert</b></td>
                    <td align="right">38,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">3,100.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">34,900.00</td>
                </tr>
                <tr>
                    <td class="left">NARA Thai Cu</b></td>
                    <td align="right">38,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">3,100.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">34,900.00</td>
                </tr>
                <tr>
                    <td class="left">Yuzu Sushi</b></td>
                    <td align="right">38,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">3,100.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">34,900.00</td>
                </tr>
                <tr>
                    <td class="left">Man Fu Yuan</b></td>
                    <td align="right">38,000.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">3,100.00&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="right">34,900.00</td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Gross Sales</b></td>
                    <td align="right" colspan="2"><b>228,000.00</b></td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Discount</b></td>
                    <td align="right" colspan="2"><b>18,600.00</b></td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Net Saless</b></td>
                    <td align="right" colspan="2"><b>209,400.00</b></td>
                </tr>
                <tr>
                    <td colspan="4"><hr class="style2"></td>
                </tr>
                <tr>
                    <td colspan="4"><div><b>Order Summary</b></div></td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Net Sales before TAX</b></td>
                    <td align="right" colspan="2"><b>130,467.29</b></td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Service Charges</b></td>
                    <td align="right" colspan="2"><b>11,860.66</b></td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Net Product Sales</b></td>
                    <td align="right" colspan="2"><b>118,606.62</b></td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Sales Tax</b></td>
                    <td align="right" colspan="2"><b>9,132.71</b></td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Ticket Sales</b></td>
                    <td align="right" colspan="2"><b>139,600.00</b></td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Open (Unpaid) Tickets</b></td>
                    <td align="right" colspan="2"><b>0.00</b></td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Closed (tendered) Tickets</b></td>
                    <td align="right" colspan="2"><b>139,600.00</b></td>
                </tr>
                <tr>
                    <td colspan="4"><hr class="style2"></td>
                </tr>
                <tr>
                    <td colspan="4"><div><b>Payment Group by Tender</b></div></td>
                </tr>
                <tr>
                    <td class="left" colspan="2">Cash</td>
                    <td align="right" colspan="2">6,500.00</td>
                </tr>
                <tr>
                    <td class="left" colspan="2">Siam Gift Card</td>
                    <td align="right" colspan="2">6,300.00</td>
                </tr>
                <tr>
                    <td class="left" colspan="2">Credit Card</td>
                    <td align="right" colspan="2">126,800.00</td>
                </tr>
                <tr>
                    <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Payment Received</b></td>
                    <td align="right" colspan="2"><b>139,600.00</b></td>
                </tr>
            </table>
            <hr class="style2">
        </div>
    </div>
</body>
</html>