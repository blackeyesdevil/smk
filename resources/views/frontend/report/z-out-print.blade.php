<!DOCTYPE html>
<html>
<head>
    {!! Html::style('css/frontend/report-print.css') !!}
</head>
<body>
    <div id="print">
        <div class="company-info">
            <div class="company-logo text-center"><img src="{{ url()->asset('images/logo-b.png') }}"></div><br>
            <div class="text-center">
                <b>Z-Out Reset Report</b><br>
                <b>For Till 101 Sequence 233</b><br>
                As of 01 October 2016<br>
                Report was run on {{ date('d F Y H:i') }} 
            </div><br>
            <hr class="style2">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="left" colspan="2"><b>Tender Summary</b></td>
                        <td align="right"><b>Amount</b></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2">Debit</b></td>
                        <td align="right">2,000.00</td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2">Siam Gift Card</b></td>
                        <td align="right">2,000.00</td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2">Credit Card Card</b></td>
                        <td align="right">2,000.00</td>
                    </tr>
                    </tr>
                    <tr>
                        <td class="left" colspan="2">Cash</td>
                        <td align="right">2,000.00</td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Tendering Total:</b></td>
                        <td align="right"><b></b></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Credit Card Tips:</b></td>
                        <td align="right"><b></b></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total Tips & Auto Gratuities:</b></td>
                        <td align="right"><b></b></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Net Cash:</b></td>
                        <td align="right"><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="3"><hr class="style2"></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2"><b>Sales Taxes</b></td>
                        <td align="right"><b>Amount</b></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2">7% VAt</b></td>
                        <td align="right">9,132.71</td>
                    </tr>
                    <tr>
                        <td colspan="3"><hr class="style2"></td>
                    </tr>
                    <tr>
                        <td class="left"><b>Category</b></td>
                        <td align="right"><b>Quantity&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                        <td align="right"><b>Amount</b></td>
                    </tr>
                    <tr>
                        <td class="left">Appetizer</b></td>
                        <td align="right">200&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="right">17,450.00</td>
                    </tr>
                    <tr>
                        <td class="left">Soup</b></td>
                        <td align="right">200&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="right">17,450.00</td>
                    </tr>
                    <tr>
                        <td class="left">Salad</b></td>
                        <td align="right">200&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="right">17,450.00</td>
                    </tr>
                    <tr>
                        <td class="left">Main Course</b></td>
                        <td align="right">200&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="right">17,450.00</td>
                    </tr>
                    <tr>
                        <td class="left">Side Dishes</b></td>
                        <td align="right">200&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="right">17,450.00</td>
                    </tr>
                    <tr>
                        <td class="left">Bakery</b></td>
                        <td align="right">200&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="right">17,450.00</td>
                    </tr>
                    <tr>
                        <td class="left">Dessert</b></td>
                        <td align="right">200&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="right">17,450.00</td>
                    </tr>
                    <tr>
                        <td class="left">Drinks</b></td>
                        <td align="right">200&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="right">17,450.00</td>
                    </tr>
                    <tr>
                        <td class="left"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total</b></td>
                        <td align="right"><b>1,600&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                        <td align="right"><b>139,600.00</b></td>
                    </tr>
                    <tr>
                        <td colspan="3"><hr class="style2"></td>
                    </tr>
                    <tr>
                        <td class="left"><b>Void / Return</b></td>
                        <td align="right"><b>Quantity&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                        <td align="right"><b>Amount</b></td>
                    </tr>
                    <tr>
                        <td class="left">Appetizer</b></td>
                        <td align="right">2&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="right">400.00</td>
                    </tr>
                    <tr>
                        <td class="left">Soup</b></td>
                        <td align="right">1&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td align="right">150.00</td>
                    </tr>
                    <tr>
                        <td class="left"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total</b></td>
                        <td align="right"><b>3&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                        <td align="right"><b>550.00</b></td>
                    </tr>
                    <tr>
                        <td colspan="3"><hr class="style2"></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction Count:</b></td>
                        <td align="right"><b>39</b></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Credit Card Count:</b></td>
                        <td align="right"><b>40</b></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Debit Card Count:</b></td>
                        <td align="right"><b>20</b></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Guest<!-- /Check --> Count:</b></td>
                        <td align="right"><b>400</b></td>
                    </tr>
                    <tr>
                        <td colspan="3"><hr class="style2"></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Taxable Total</b></td>
                        <td align="right"><b>139,600.00</b></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Nontaxable Total</b></td>
                        <td align="right"><b>0.00</b></td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;Tax Total</b></td>
                        <td align="right"><b>9,132.71</b></td>
                    </tr>
                    <!-- <tr>
                        <td class="left"><b>&nbsp;&nbsp;&nbsp;&nbsp;TOTAL</b></td>
                        <td align="right"><b>2630.62</b></td>
                    </tr> -->
                </table>
            <hr class="style2">
            <div><b>&nbsp;&nbsp;&nbsp;&nbsp;Server: {{ $cashier_name_en }}</b></div><br>
        </div>
    </div>
</body>
</html>