<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
{!! Html::style('https://fonts.googleapis.com/css?family=Open+Sans:400,300,700') !!}
{!! Html::style('fonts/foundation-icons/foundation-icons.css') !!}
{!! Html::style('css/frontend/grab-go.css') !!}
{!! Html::style('css/frontend/report-print.css') !!}
{!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
<script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
{!! Html::script('assets/global/scripts/jquery.min.js') !!}
{!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
</head>

<body>
    <!-- <div id="cashier-table"> -->
        <div class="container-fluid">
            <div id="btn-type" class="col-sm-12">
                <div class="btn-type" style="background:#C19F79;"><a href="{{url()->to('cashier/grab-go')}}">GRAB & GO</a></div>
                <div class="btn-type" ><a href="{{url()->to('cashier/cashier-table')}}">CASHIER</a></div>
                <div class="btn-type" style="width: 300px;"><a href="{{url()->to('cashier/customer-service')}}">CUSTOMER-SERVICE</a></div>
            </div>
                <div id="table" class= "col-sm-6">
                    <div id="table-order" class="col-sm-12">
                        <table id="eiei">
                            <thead>
                                <tr>
                                    <th class="col-sm-2">Code</th>
                          	        <th class="col-sm-4">Item</th>
                          	        <th class="col-sm-1">Qty</th>
                          	        <th class="col-sm-2">Price</th>
                                    <th class="col-sm-2">Total</th>
                                    <th class="col-sm-1">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <div id="table-payment" class="col-sm-12">
                        <div class="line-table">
                            <div class="barcode">

                            </div>
                            <div class="btn-payment">
                                <input type="text" name="barcode" value="" id="barcode" placeholder="Barcode input & Qty" autofocus>
                                <div class="btn-discount ">Viz card</div>
                                <div class="btn-discount ">Discount Code</div>
                                <div class="btn-discount">Total</div>
                            </div>
                        </div>

                        <table>
                            <thead>
                                <tr>
                                    <th class="col-sm-4">Payment By</th>
                                    <th class="col-sm-4">Amount</th>
                                    <th class="col-sm-4">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <div id="paid-left" class="col-sm-12">
                        <div class="paid-text">
                            <div id="paid-text-p" class="col-sm-6">Paid :<span id="paid-text-p-input">-</span></div>
                            <div id="paid-text-l" class="col-sm-6">Left :<span id="paid-text-l-input">-</span></div>
                        </div>
                    </div>
                </div>
                <div id="pay" class="col-sm-6">
                    <div id="head" class="col-sm-12">
                        <div class="head-pay"><span id="head-pay-text">0.00</span> THB</div>
                        <div class="change-pay">Change :<span id="change-pay-text">00.00</span> THB</div>
                        <div id="button-etc">

                            <table class="col-sm-12" id="payment-method">
                                <tr>
                                    <td class="col-sm-4">Cash</td>
                                    <td class="col-sm-4">Credit Card</td>
                                    <td class="col-sm-4">Siam Gift Card</td>
                                </tr>
                                <tr>
                                    <td class="col-sm-4" style="background-color:#FFF;"></td>
                                    <td class="col-sm-4">Rabbit Card</td>
                                    <td class="col-sm-4">Other</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="bottom" class="col-sm-12">
                        <div class="input-pay col-sm-12">
                            <div id="pay-text">0</div>
                        </div>
                        <div id="bank" class="col-sm-4">
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b1000.jpg')}}" alt="" id="b1000"/></div>
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b500.jpg')}}" alt="" id="b500"/></div>
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b100.jpg')}}" alt="" id="b100"/></div>
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b50.jpg')}}" alt="" id="b50"/></div>
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b20.jpg')}}" alt="" id="b20"/></div>
                        </div>
                        <div id="btn-number" class="col-sm-8">
                            <table>
                                <tr>
                                    <td class="col-sm-3">7</td><td class="col-sm-3">8</td><td class="col-sm-3">9</td><td class="col-sm-3">C</td>
                                </tr>
                                <tr>
                                    <td class="col-sm-3">4</td><td class="col-sm-3">5</td><td class="col-sm-3">6</td><td class="col-sm-3" id="del">
                                        <image src="{{URL::asset('images/cashier/del.svg')}}" alt="" fill="red"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-sm-3">1</td><td class="col-sm-3">2</td><td class="col-sm-3">3</td><td rowspan="2"class="col-sm-3 enter" style="font-size:20px;">ENTER</td>
                                </tr>
                                <tr>
                                    <td class="col-sm-3">00</td><td class="col-sm-3">0</td><td class="col-sm-3">.</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
        </div>
</div>
<script>
    var orders= [];
    var api_url = '{{ URL::to("api") }}';
    console.log(api_url+'/cashier');

</script>
{!! Html::script('js/frontend/grab-go.js') !!}

</body>
</html>
