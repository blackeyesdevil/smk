@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
    {!! Html::style('css/frontend/home.css') !!}
    {!! Html::style('css/frontend/product-list.css') !!}
    {!! Html::style('css/frontend/checkout.css') !!}
    {!! Html::style('css/frontend/my-room.css') !!}
    {!! Html::style('css/frontend/personalize.css') !!}
    {!! Html::style('css/frontend/promotion.css') !!}
    {!! Html::style('css/frontend/special-set.css') !!}
    {!! Html::style('css/frontend/mykitchen-choose-table.css') !!}
    {!! Html::style('css/frontend/order-history.css') !!}
    {!! Html::style('css/frontend/collection-list.css') !!}

@endsection

@section('content')
    <div id="content">
        <div class="container">

        </div>
    </div>

@endsection

@section('script')
    <script>
        var lang = $('meta[name=lang]').attr('content');

        function filterLang(element, containerDIV){
            var anotherLang = (lang=='en')?'th':'en';

            obj = $(element);

            obj.find('.' + anotherLang).removeClass('show').addClass('hide');
            obj.find('.' + lang).removeClass('hide').addClass('show');

            containerDIV.append(obj);
        }
    </script>
    {!! Html::script('js/frontend/sidebar.js') !!}
    {!! Html::script('js/frontend/main.js') !!}
    {!! Html::script('js/frontend/product-list.js') !!}
    {!! Html::script('js/frontend/collection-layout.js') !!}
    {!! Html::script('js/frontend/cart.js') !!}
    {!! Html::script('js/frontend/checkout.js') !!}
    {!! Html::script('js/frontend/my-room.js') !!}
    {!! Html::script('js/frontend/order-history.js') !!}
@endsection
