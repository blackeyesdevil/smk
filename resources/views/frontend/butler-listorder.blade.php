@extends('frontend.layout.butler-layout')

@section('title', ' - Home')

@section('css')
{!! Html::style('css/frontend/butler-listorder.css') !!}
{!! Html::style('css/frontend/kitchen.css') !!}
@endsection

@section('content')

    <div id="content">
        <div class="container">
            <div class="back">
                <img src="{{ URL::asset('images/butler/back.png') }}">
                <?php
                   echo "<a href=\"javascript:history.go(-1)\">BACK</a>";
                ?>
            </div>
            <div class="head-text">
                Order Status
            </div>
            <div class="input-table">
                TABLE NO.
                <!-- <img src="{{ URL::asset('images/butler/search.png') }}"> -->
                {{ Form::open(array('url' => '/butler-listorder', 'method' => 'post')) }}
                <div class="form-group">
                    <div class="input-group input-group-md">
                        <div class="icon-addon addon-md col-xs-9">
                            <input type="text" name="tb_search" placeholder="" class="form-control">
                        </div>
                        <span class="input-group-btn col-xs-3">
                            <!-- <a href="{{ URL::to('butler-listorder/')}}"> -->
                              <button type="submit"class="btn btn-default" type="button">Search!</button>
                            <!-- </a> -->
                        </span>
                    </div>
                </div>
                {{ Form::close() }}


            </div>
            <div class="order-head">
                <div class="order-text col-xs-6">
                    ORDER ID
                </div>
                <div class="order-status col-xs-6">
                    STATUS
                </div>
            </div>
            @if( count($orders) > 0)
                @foreach($orders as $order)


                    @if($order['status'] == 0)
                    <a href="{{ URL::route('butler-listorder-detail', array($order['orders_id'],$order['orders_no']) ) }}">
                      <div class="order-item-process col-xs-12">
                    @elseif( $order['status'] == 1)
                    <a href="{{ URL::route('butler-listorder-detail', array($order['orders_id'],$order['orders_no']) ) }}">
                      <div class="order-item-process col-xs-12">
                    @elseif( $order['status'] == 2)
                    <a>
                      <div class="order-item-cancel col-xs-12">
                    @elseif( $order['status'] == 99)
                    <a>
                      <div class="order-item-complete col-xs-12">
                    @endif

                          <div class="order-id col-xs-6">
                              <!-- #AA00015 -->
                              <div class="table-no">Table No. {{$order['table_no']}}</div>
                              <div class="orders-no">Orders No. #{{ substr($order['orders_no'],6)}}</div>

                          </div>
                          <div class="order-status-in col-xs-6">
                              <div class="status">
                                @if($order['status'] == 0)
                                  Processing
                                @elseif( $order['status'] == 1)
                                  Processing
                                @elseif( $order['status'] == 2)
                                  Canceled
                                @elseif( $order['status'] == 99)
                                  Completed
                                @endif
                              </div>
                              <?php
                              $time_ago = MainFunction::get_time_ago(strtotime($order->created_at));
                              ?>
                              <div class="date">
                                  {{$time_ago}}
                              </div>
                          </div>
                      </div>
                    </a>
                @endforeach
            @else
                <p class="text-center">No Order.</p>
            @endif
            <!-- <div class="order-item-complete col-xs-12">
                <div class="order-id col-xs-6">
                    #AA00014
                </div>
                <div class="order-status-in col-xs-6">
                    <div class="status">
                        Completed
                    </div>
                    <div class="date">
                        14/09/2016 14:47
                    </div>
                </div>
            </div> -->
            <!-- <div class="order-item-complete col-xs-12">
                <div class="order-id col-xs-6">
                    #AA00011
                </div>
                <div class="order-status-in col-xs-6">
                    <div class="status">
                        Completed
                    </div>
                    <div class="date">
                        14/09/2016 14:47
                    </div>
                </div>
            </div> -->

        </div>
    </div>
@endsection

@section('script')

@endsection
