<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{ Config::get('constants.APP_NAME') }}@yield('title')</title>

        <!-- Responsive Window -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="token" content="{{ csrf_token() }}">
        <meta name="lang" content="{{ $lang }}">

        <!-- Custom fonts -->
        {!! Html::style('https://fonts.googleapis.com/css?family=Open+Sans:400,300,700') !!}
        {!! Html::style('fonts/foundation-icons/foundation-icons.css') !!}

        <!-- CSS Files -->
        {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('css/frontend/main.css') !!}
        @yield('css')
    </head>

    <body>
        @include('frontend.include.header')

        <div id="ui-main">
            @yield('content')
        </div>

        <div id="popup-div">
            <div id="cover-box"></div>
            <div id="cover-box-popup"></div>
            <div id="loading">
                <img src="{{ URL::asset('images/loading.gif') }}">
            </div>

            <div id="sidebar">
                <div class="close-btn-cart">X</div>

                <div class="sidebar-footer">
                    <div class="table-number">
                        <div class="topic">TABLE NUMBER</div>
                        <div class="value">12</div>
                    </div>

                    <div class="options">
                        <button id="view-order-history-btn" class="order-history btn">View Order History</button>
                    </div>
                </div>

                <div class="headline">MY ORDER</div>
                <div class="content">
                  <div class="order-detail-list">
                      @for ($i=0; $i<=3; $i++)
                      <div class="order-detail-item">
                          <div class="row">
                              <div class="product-img col-xs-2">
                                  <img src="{{ URl::asset('images/no-pic.png') }}">
                              </div>

                              <div class="product-info col-xs-6">
                                  <div class="product-name">BRIX BOX</div>
                                  <div class="product-option-list">
                                      <div class="product-option-item">
                                          <div class="topic col-xs-6">Ice Cream Flavor:</div>
                                          <div class="value col-xs-6">Chocolate</div>
                                      </div>

                                      <div class="product-option-item">
                                          <div class="topic col-xs-6">Topping:</div>
                                          <div class="value col-xs-6">
                                              Whip Cream (+20)<br>
                                              Banana (+15)
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="qty col-xs-3">
                                  <span class="control-btn">-</span>
                                  <span class="qty-number">1</span>
                                  <span class="control-btn">+</span>
                              </div>

                              <div class="remove col-xs-1">
                                  <button class="remove-btn">
                                      <span>X</span>
                                  </button>
                              </div>
                          </div>

                          <div class="subtotal-price">
                              290 THB
                          </div>
                      </div>
                      @endfor
                  </div>

                  <div class="total-price">
                      TOTAL <span class="value">4,000.00 THB</span>
                  </div>
                  <div class="text-des text-center" style="margin-top: 50px">

                  </div>

                  <div class="footer">
                      <button class="checkout-btn btn">CHECK OUT</button>
                  </div>
                </div>
            </div>

            <div id="product-option">
                <div class="product-item" style="background-image: url('http://www.brixdessertbar.com/wp-content/uploads/2016/02/BRIX-14_12_1515533-300x204.jpg')">
                    <div class="close-btn">X</div>
                    <div class="add-favorite-btn">
                      <i class="glyphicon glyphicon-heart-empty"></i>
                    </div>
                    <div class="gradient-div"></div>
                    <div class="product-info"></div>
                </div>

                <div class="option-div">
                    <div class="cautions row">
                        <div class="caution-item col-xs-4">
                            <img src="{{ URL::asset('images/dairy-free-icon.jpg') }}" alt="Dairy Free">
                            Dairy Free
                        </div>

                        <div class="caution-item col-xs-4">
                            <img src="{{ URL::asset('images/egg-free-icon.jpg') }}" alt="Egg Free">
                            Egg Free
                        </div>

                        <div class="caution-item col-xs-4">
                            <img src="{{ URL::asset('images/vegan-icon.jpg') }}" alt="Vegan">
                            Vegan
                        </div>
                    </div>
                    <div class="options row">
                        <!-- <form action="" method="post">
                        </form> -->
                    </div>

                    <div class="text-center">
                        <button class="btn-add-cart btn">DINE IN</button>
                        <button class="btn-add-cart btn">TAKE AWAY</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="content-checkout">
            <div class="close-checkout-btn">X</div>
            <div id="checkout-box" class="col-sm-8">
                <div class="order-detail col-xs-12">
                    <div class="head-text">
                        MY ORDER
                    </div>
                    <!-- <div class="order-id col-xs-6">
                        Order ID : AA0001
                    </div> -->
                    <div class="product-order-list">
                      <div class="product-orders">
                          <div class="product-name col-xs-9">
                              BRIX BOX
                          </div>
                          <div class="product-price col-xs-3">
                              290 THB
                          </div>
                      </div>
                      <div class="product-option col-xs-9">
                          <div class="product-option-item">
                              Ice cream Chocolate flavors
                          </div>
                          <div class="product-option-item">
                              Whimp Cream
                          </div>
                      </div>
                    </div>


                    <div class="checkout-total-price row"></div>
                    <div class="checkout-total-discount-price row"></div>
                    <div class="checkout-total-discount row"></div>

                </div>
                <!-- <div id="tax-receipt" class="col-xs-12">
                    <div class="head-text col-xs-12">
                        Tax Receipt
                    </div>
                    <div class="checkbox col-xs-12">
                      <label><input type="checkbox" value="">I'd like to receive Tax Receipt.</label>
                      <textarea class="col-xs-12" rows="5" id="tax-address"></textarea>
                      <p>- Please enter your full address to obtain a Tax Receipt.</p>
                    </div>

                </div> -->
                <div class="button-pay col-xs-12">
                    <div class="button-pay-by">PAY BY CASH</div>

                    <div class="button-pay-by">PAY BY CREDIT CARD</div>

                </div>
            </div>
            <div id="button-etc" class="col-sm-4">
                <!-- <div id="discount-code-normal" class="button-etc-in col-sm-8 col-sm-offset-2">
                    Discount code
                </div>
                <div id="viz-btn" class="button-etc-in col-sm-8 col-sm-offset-2">
                    VIZ Card
                </div>
                <div class="button-etc-in col-sm-8 col-sm-offset-2">
                    Employee Discounts
                </div> -->

                <div id="discount-code-normal" class="button-item">
                    <img src="{{ URL::asset('images/discount-code-btn.png') }}" alt="Discount Code">

                    <div class="cover">
                        <span class="text">DISCOUNT<br>CODE</span>
                    </div>
                </div>

                <a href="intent:#Intent;action=com.smk.NK_NFC;end" id="viz-btn" class="button-item">
                    <img src="{{ URL::asset('images/discount-viz-btn.png') }}" alt="VIZ Card">

                    <div class="cover">
                        <span class="text">VIZ CARD</span>
                    </div>
                </a>

                <div id="tenant-btn" class="button-item">
                    <img src="{{ URL::asset('images/discount-employee-btn.png') }}" alt="Tenant">

                    <div class="cover">
                        <span class="text">TENANT<br>DISCOUNT</span>
                    </div>
                </div>
            </div>
        </div>

        <div id="cover-box-discount"></div>

        <div id="discount-code" class="discount-popup">
            <div class="loading">
                <img src="{{ URL::asset('images/loading-sm.gif') }}">
            </div>
            <div class="head-text">
                Discount code
            </div>

            <div class="content">
                <div class="input-discount">
                    Have a discount code?
                    <input type="text" name="discount" id="discount-input" class="input-value">
                </div>

                <div class="discount-alert">
                    You've got 20% Discount!
                </div>
            </div>

            <div class="button-discount">
                <div id="discount-apply-btn" class="btn-redeem button-discount-in">
                    Apply
                </div>
                <div id="discount-cancel-btn" class="btn-cancel button-discount-in">
                    Cancel
                </div>
            </div>
        </div>

        <div id="viz-card" class="discount-popup">
            <div class="head-text">
                VIZ MEMBER
            </div>

            <div class="content">
                <div class="input-discount">
                    Please Enter your VIZ ID
                    <input type="text" name="discount" class="input-value">
                </div>
                <div class="discount-alert">
                    <!-- You've got 20% Discount! -->
                </div>
            </div>

            <div class="button-discount">
                <div class="btn-redeem button-discount-in">
                    Ok
                </div>
                <div class="btn-cancel button-discount-in">
                    Cancel
                </div>
            </div>
        </div>

        <div id="yesno">
            <div id="headconfirm" class=" col-xs-12 ">
                <span class="en">Are you sure?</span>
                <span class="th">ต้องการลบ?</span>
            </div>
            <div id="yesnobutton" class="col-xs-12">
                <div id="yes" class="button-pay-by col-xs-offset-1 col-xs-4">
                    <span class="en">Yes</span>
                    <span class="th">ใช่</span>
                </div>
                <div id="no" class="button-pay-by col-xs-offset-2 col-xs-4">
                    <span class="en">No</span>
                    <span class="th">ไม่</span>
                </div>
            </div>

        </div>

        <div id="personalize">
          <div id="personalize-close" class="close-btn"><span class="text">X</span></div>
          <div id="personalize-login">
            <div id="title-1">Enter your mobile phone to</div>
            <div id="title-2">personalize your menu</div>
            <div id="title-3">Tax invoice will send to you moblie after order.</div>
            <div id="title-input"><input type="text"  placeholder="Mobile Phone" size="30"></div>
            <div id="title-enter"> <button class="enter-mobile btn">ENTER</button></div>
          </div>

          <div id="personalize-otp">
            <div id="title-1">Please Enter OTP</div>
            <div id="title-2">From Mobile Phone</div>
            <div id="title-3">Tax invoice will send to you moblie after order.</div>
            <div id="title-input"><input type="text"  placeholder="Please Enter OTP" size="30"></div>
            <div id="title-enter"> <button class="enter-otp btn">ENTER</button></div>
          </div>
        </div>
        <div id="cover"></div>

        {!! Html::script('assets/global/scripts/jquery.min.js') !!}
        {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}

        <script>
            var isLoading = false;
            var base_url = '{{ URL::to("/") }}';
            var api_url = '{{ URL::to("api") }}';
            if(add_to_cart==null){
                var add_to_cart = [];
                console.log("main_layout"+add_to_cart);
                $('.total-price').hide();
                $('.checkout-btn').hide();
                var plzorder = 'Please Order Product.';
                $('.text-des').text(plzorder);

            }



            $(function(){
                $('.add-favorite-btn').on('click', function(){
                    icon = $(this).find('i');

                    if (icon.hasClass('glyphicon-heart'))
                        icon.removeClass('glyphicon-heart').addClass('glyphicon-heart-empty');
                    else
                        icon.removeClass('glyphicon-heart-empty').addClass('glyphicon-heart');
                });
            });

            function showLoading(){
                $('#popup-div').css('background', 'rgba(255,255,255,0.6)').fadeIn('fast');
                $('#popup-div #loading').show();
            }

            function hideLoading(){
                $('#popup-div').css('background', 'none').fadeOut('fast');
                $('#popup-div #loading').hide();
            }
            $('#cover').on('click', function(){
                $('#cover').fadeOut();
                $('#personalize').fadeOut();
            });

        </script>
        @yield('script')
    </body>
</html>
