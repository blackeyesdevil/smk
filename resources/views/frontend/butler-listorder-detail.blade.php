@extends('frontend.layout.butler-layout')

@section('title', ' - Home')

@section('css')
{!! Html::style('css/frontend/butler-listorder-detail.css') !!}
@endsection

@section('content')

<div id="content">
   <div class="container">
       <div class="back">
           <img src="{{ URL::asset('images/butler/back.png') }}">
           <?php
              echo "<a href=\"javascript:history.go(-1)\">BACK</a>";
           ?>

           <!-- BACK -->
       </div>
       <div class="head-text">
           #{{ substr($orders['orders_no'],6)}}
       </div>
       <div class="table-no col-xs-12">
           TABLE NO .: {{$orders['table_no']}}
       </div>
       <div class="date col-xs-12">
            {{$orders['created_at']}}
           <!-- 14/09/2016 &nbsp;&nbsp;15:47 -->
       </div>
       <div class="list-no col-xs-12">
         <?php
              $statusCount = 0;

          ?>
          @foreach( $orders['orders_detail'] as $detail )
            @if($detail['status'] == 1)
                <?php

                    $statusCount = $statusCount + 1;
                 ?>
            @endif
          @endforeach

         {{$statusCount}}/{{count($orders['orders_detail'])}}

       </div>
       <div class="order-head col-xs-12">
           <div class="order-text col-xs-6">
               ORDER ID
           </div>
           <div class="order-status col-xs-6">
               STATUS
           </div>
       </div>
     </div>
     </div>

      @foreach( $orders['orders_detail'] as $detail )
    <div class="order-list">
      @if( $detail['status'] == 0)
      <div class="order-item processing-normal">
        @elseif( $detail['status'] == 1)
        <div class="order-item completed">
        @elseif( $detail['status'] == 2)
        <div class="order-item processing-cancel">
        @endif
        <div class="order-no">
          <div class="content">
            <div class="text">TABLE NO.</div>
            <div class="value">{{$orders['table_no']}}</div>
          </div>
          <div class="triangle"></div>
        </div>
        <?php
        $time_ago = MainFunction::get_time_ago(strtotime($detail->created_at));
        ?>
        <div class="time">{{$time_ago}}</div>

        <div class="product-info">
          <div class="product-name">{{$detail['product_name_en']}} </div>

          <div class="product-option">
            @if( $detail['status'] == 0)
            <div class="status-detail col-xs-2">Processing</div>
              @elseif( $detail['status'] == 1)
              <div class="status-detail col-xs-2">Served</div>
              @elseif( $detail['status'] == 2)
              <div class="status-detail col-xs-2">Cancel</div>
              @endif

            @if($detail['option_en'] != '')
            <!-- {{$detail['option_en']}} -->
                <?php
                // $optionSpit= explode(",", $detail['option_en']);
                        $optionDetail = $detail['option_en'];
                        $optionDetail = explode(",", $optionDetail);
                 ?>
                  @foreach($optionDetail as $key => $value)
                <div class="option-item row">
                  <div class="topic col-xs-5">Option{{$key}}:</div>
                  <div class="value col-xs-5">{{$value}}</div>
                </div>
                @endforeach

            @else
              <div class="option-item row" style="margin-bottom:50px;"></div>

            @endif

            <!-- <div class="option-item row">
              <div class="topic col-xs-5">Toppings:</div>
              <div class="value col-xs-7">Banana</div>
            </div>
            <div class="option-item row">
              <div class="topic col-xs-5">Toppings:</div>
              <div class="value col-xs-7">Banana</div>
            </div> -->
          </div>

          <div class="table-no">ORDER NO. #{{substr($orders['orders_no'],6)}}</div>
        </div>
      </div>
      </div>
    @endforeach



    <!-- <div id="content">
        <div class="container">
            <div class="back">
                <img src="{{ URL::asset('images/butler/back.png') }}">
                BACK
            </div>
            <div class="head-text">
                #AA00015
            </div>
            <div class="table-no col-xs-12">
                TABLE NO .: 50
            </div>
            <div class="date col-xs-12">
                14/09/2016 &nbsp;&nbsp;15:47
            </div>
            <div class="list-no col-xs-12">
                2/5
            </div>
            <div class="order-head col-xs-12">
                <div class="order-text col-xs-6">
                    ORDER ID
                </div>
                <div class="order-status col-xs-6">
                    STATUS
                </div>
            </div>
            <div id="list-order">
                <div class="order-process col-xs-12">
                    <div class="img-order col-xs-2">
                        <img src="{{ URL::asset('images/store-brix-logo-white.png') }}">
                    </div>
                    <div class="order-detail col-xs-7">
                        <div class="order-name">
                            <div class="name">
                                BRIX BOX
                            </div>
                            <div class="number">
                                x3
                            </div>
                        </div>
                        <div class="order-option">
                            <div class="option-item">
                                Ice cream Chocolate flavors
                            </div>
                            <div class="option-item">
                                Whip Cream
                            </div>
                        </div>
                    </div>
                    <div class="status col-xs-3">
                        Processing
                    </div>
                </div>
                <div class="order-served col-xs-12">
                    <div class="img-order col-xs-2">
                        <img src="{{ URL::asset('images/store-yuzu-logo-white.png') }}">
                    </div>
                    <div class="order-detail col-xs-7">
                        <div class="order-name">
                            <div class="name">
                                TAMAGO UNAGI
                            </div>
                            <div class="number">

                            </div>
                        </div>
                        <div class="order-option">
                            <div class="option-item">
                                add wasabi
                            </div>
                        </div>
                    </div>
                    <div class="status col-xs-3">
                        Served
                    </div>
                </div>
                <div class="order-served col-xs-12">
                    <div class="img-order col-xs-2">
                        <img src="{{ URL::asset('images/store-manfuyuan-logo-white.png') }}">
                    </div>
                    <div class="order-detail col-xs-7">
                        <div class="order-name">
                            <div class="name">
                                Rich noodle paste with bean
                            </div>
                            <div class="number">

                            </div>
                        </div>
                        <div class="order-option">
                            <div class="option-item">
                                add bean sprout
                            </div>
                        </div>
                    </div>
                    <div class="status col-xs-3">
                        Served
                    </div>
                </div>
            </div>
        </div>
    </div> -->


@endsection

@section('script')

@endsection
