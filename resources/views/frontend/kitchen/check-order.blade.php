@extends('frontend.layout.kitchen-layout')
@section('css')
    <style>
        /*#barcode{
            position: absolute;
            top: 50%;
            left: 50%;
            text-align: center;
            transform: translate(-50%, -50%);
        }*/
        *, *:after, *:before { -webkit-box-sizing: border-box; box-sizing: border-box; }
        body {
        	background: #f9f7f6;
        	color: #404d5b;
        	font-weight: 500;
        	font-size: 1.05em;
            margin: 0 auto;
        	text-align: center;
        }
        .content {
        	font-size: 150%;
        	padding: 3em 0;
        }
        .bgcolor-7 { background: #d0d6d6; }
        /* Component Start */
        .input {
        	position: relative;
        	z-index: 1;
        	display: inline-block;
        	margin: 1em;
        	max-width: 400px;
        	width: calc(100% - 2em);
        	vertical-align: top;
        }
        .input__field {
        	position: relative;
        	display: block;
        	float: right;
        	padding: 0.8em;
        	width: 60%;
        	border: none;
        	border-radius: 0;
        	background: #f0f0f0;
        	color: #aaa;
        	font-weight: bold;
        	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        	-webkit-appearance: none; /* for box shadows to show on iOS */
        }
        .input__field:focus {
        	outline: none;
        }

        .input__label {
        	display: inline-block;
        	float: right;
        	padding: 0 1em;
        	width: 40%;
        	color: #6a7989;
        	font-weight: bold;
        	font-size: 70.25%;
        	-webkit-touch-callout: none;
        	-webkit-user-select: none;
        	-khtml-user-select: none;
        	-moz-user-select: none;
        	-ms-user-select: none;
        	user-select: none;
        }

        .input__label-content {
        	position: relative;
        	display: block;
        	padding: 1.6em 0;
        	width: 100%;
        }

        /* Jiro */
        .input--jiro {
        	margin-top: 2em;
        }

        .input__field--jiro {
        	padding: 0.85em 0.5em;
        	width: 100%;
        	background: transparent;
        	color: #DDE2E2;
        	opacity: 0;
        	-webkit-transition: opacity 0.3s;
        	transition: opacity 0.3s;
        }

        .input__label--jiro {
        	position: absolute;
        	left: 0;
        	padding: 0 0.85em;
        	width: 100%;
        	height: 100%;
        	text-align: left;
        	pointer-events: none;
        }

        .input__label-content--jiro {
        	-webkit-transition: -webkit-transform 0.3s 0.3s;
        	transition: transform 0.3s 0.3s;
        }

        .input__label--jiro::before,
        .input__label--jiro::after {
        	content: '';
        	position: absolute;
        	top: 0;
        	left: 0;
        	width: 100%;
        	height: 100%;
        	-webkit-transition: -webkit-transform 0.3s;
        	transition: transform 0.3s;
        }

        .input__label--jiro::before {
        	border-top: 2px solid #6a7989;
        	-webkit-transform: translate3d(0, 100%, 0) translate3d(0, -2px, 0);
        	transform: translate3d(0, 100%, 0) translate3d(0, -2px, 0);
        	-webkit-transition-delay: 0.3s;
        	transition-delay: 0.3s;
        }

        .input__label--jiro::after {
        	z-index: -1;
        	background: #6a7989;
        	-webkit-transform: scale3d(1, 0, 1);
        	transform: scale3d(1, 0, 1);
        	-webkit-transform-origin: 50% 0%;
        	transform-origin: 50% 0%;
        }

        .input__field--jiro:focus,
        .input--filled .input__field--jiro {
        	opacity: 1;
        	-webkit-transition-delay: 0.3s;
        	transition-delay: 0.3s;
        }

        .input__field--jiro:focus + .input__label--jiro .input__label-content--jiro,
        .input--filled .input__label-content--jiro {
        	-webkit-transform: translate3d(0, -80%, 0);
        	transform: translate3d(0, -80%, 0);
        	-webkit-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        	transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        }

        .input__field--jiro:focus + .input__label--jiro::before,
        .input--filled .input__label--jiro::before {
        	-webkit-transition-delay: 0s;
        	transition-delay: 0s;
        }

        .input__field--jiro:focus + .input__label--jiro::before,
        .input--filled .input__label--jiro::before {
        	-webkit-transform: translate3d(0, 0, 0);
        	transform: translate3d(0, 0, 0);
        }

        .input__field--jiro:focus + .input__label--jiro::after,
        .input--filled .input__label--jiro::after {
        	-webkit-transform: scale3d(1, 1, 1);
        	transform: scale3d(1, 1, 1);
        	-webkit-transition-delay: 0.3s;
        	transition-delay: 0.3s;
        	-webkit-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        	transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        }

        /* Minoru */
        .input__field--minoru {
        	width: 100%;
        	background: #fff;
        	box-shadow: 0px 0px 0px 2px transparent;
        	color: #eca29b;
        	-webkit-transition: box-shadow 0.3s;
        	transition: box-shadow 0.3s;
        }

        .input__label--minoru {
        	padding: 0;
        	width: 100%;
        	text-align: left;
        }

        .input__label--minoru::after {
        	content: '';
          	position: absolute;
          	top: 0;
          	z-index: -1;
          	width: 100%;
          	height: 4em;
        	box-shadow: 0px 0px 0px 0px;
        	color: rgba(199,152,157, 0.6);
        }

        .input__field--minoru:focus {
        	box-shadow: 0px 0px 0px 2px #eca29b;
        }

        .input__field--minoru:focus + .input__label--minoru {
        	pointer-events: none;
        }

        .input__field--minoru:focus + .input__label--minoru::after {
        	-webkit-animation: anim-shadow 0.3s forwards;
        	animation: anim-shadow 0.3s forwards;
        }

        @-webkit-keyframes anim-shadow {
        	to {
        		box-shadow: 0px 0px 100px 50px;
            	opacity: 0;
        	}
        }

        @keyframes anim-shadow {
        	to {
        		box-shadow: 0px 0px 100px 50px;
            	opacity: 0;
        	}
        }

        .input__label-content--minoru {
        	padding: 0.75em 0.15em;
        }


    </style>

@endsection
@section('content')
<!-- <div id="barcode">
    <form id="form" action="{{ url()->to('kitchen/check-order/check') }}" method="post">
        <input type="text" id="barcode" name="barcode" placeholder="Barcode" autofocus>
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    </form>
</div> -->
<section class="content bgcolor-7">
  <h2>Kitchen</h2>
  <span class="input input--jiro">
      <form id="form" action="{{ url()->to('kitchen/check-order/check') }}" method="post">
        <input class="input__field input__field--jiro" type="text" id="barcode" name="barcode" autofocus />
        <label class="input__label input__label--jiro" for="barcode">
          <span class="input__label-content input__label-content--jiro">Barcode</span>
        </label>
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    </form>
  </span>
</section>
@endsection

@section('script')
    <script>
    $(function(){
        $('#barcode').on('keyup', function(e){
            e.preventDefault();
            console.log($("#barcode").val());
            $('#form').submit();
        });
    });
    </script>
@endsection
