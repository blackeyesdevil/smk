<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
{!! Html::style('https://fonts.googleapis.com/css?family=Open+Sans:400,300,700') !!}
{!! Html::style('fonts/foundation-icons/foundation-icons.css') !!}
{!! Html::style('css/frontend/customer-service.css') !!}
{!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
<script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
{!! Html::script('assets/global/scripts/jquery.min.js') !!}
{!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
</head>

<body>
        <div class="container-fluid">
            <div id="btn-type" class="col-sm-12">
                <div class="btn-type" >GRAB & GO</div>
                <div class="btn-type">CASHIER</div>
                <div class="btn-type" style="width: 250px;background:#C19F79;">CUSTOMER SERVICE</div>
                <div id="report" class="btn-type">PRINT REPORT</div>
            </div>
                <div id="table" class= "col-sm-6">
                    <div id="table-order" class="col-sm-12">
                        <table>
                            <thead>
                                <tr>
                          	        <th class="col-sm-4">Order No.</th>
                          	        <th class="col-sm-3">Amount</th>
                                    <th class="col-sm-2">Type</th>
                                    <th class="col-sm-2">Time</th>
                                    <th class="col-sm-1">Print</th>

                                </tr>
                            </thead>
                            <tbody id="table-order-tb">
                            </tbody>
                        </table>
                    </div>
                    <!-- <div id="table-payment" class="col-sm-12 inline">
                        <div class="table-payment-text col-sm-">
                            <div class="col-sm-2">
                                Order No. : <span></span>
                            </div>
                            <div class="col-sm-2">
                                Table No. : <span></span>
                            </div>
                            <div class="btn-payment">
                                <div class="btn-discount">Viz card</div>
                                <div class="btn-discount">Discount Code</div>
                            </div>
                        </div>


                        <table>
                            <thead>
                                <tr>
                                    <th class="col-sm-4">Payment By</th>
                                    <th class="col-sm-4">Amount<span></span></th>
                                    <th class="col-sm-4"></th>
                                </tr>
                            </thead>
                            <tbody id="table-payment-tb">

                            </tbody>
                        </table>
                        <div id="paid-left" class="col-sm-12">
                            <div class="paid-text">
                                <div id="paid-text-p" class="col-sm-6">Paid :<span id="paid-text-p-input">-</span></div>
                                <div id="paid-text-l" class="col-sm-6">Left :<span id="paid-text-l-input">-</span></div>
                            </div>
                        </div>
                    </div> -->

                </div>
                <div id="pay" class="col-sm-6">
                    <div id="head" class="col-sm-12">
                        <div class="head-pay"><span id="head-pay-text">2050.00</span> THB</div>
                        <div class="change-pay">Change :<span id="change-pay-text">00.00</span> THB</div>
                        <div id="button-etc">

                            <table class="col-sm-12" id="payment-method">
                                <tr>
                                    <td class="col-sm-4">Cash</td>
                                    <td class="col-sm-4">Credit Card</td>
                                    <td class="col-sm-4">Siam Gift Card</td>
                                </tr>
                                <tr>
                                    <td class="col-sm-4" style="background-color:#FFF;"></td>
                                    <td class="col-sm-4">Rabbit Card</td>
                                    <td class="col-sm-4">Other</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="bottom" class="col-sm-12">
                        <div class="input-pay col-sm-12">
                            <div id="pay-text">0</div>
                        </div>
                        <div id="bank" class="col-sm-4">
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b1000.jpg')}}" alt="" id="b1000"/></div>
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b500.jpg')}}" alt="" id="b500"/></div>
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b100.jpg')}}" alt="" id="b100"/></div>
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b50.jpg')}}" alt="" id="b50"/></div>
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b20.jpg')}}" alt="" id="b20"/></div>
                        </div>
                        <div id="btn-number" class="col-sm-8">
                            <table>
                                <tr>
                                    <td class="col-sm-3">7</td><td class="col-sm-3">8</td><td class="col-sm-3">9</td><td class="col-sm-3">C</td>
                                </tr>
                                <tr>
                                    <td class="col-sm-3">4</td><td class="col-sm-3">5</td><td class="col-sm-3">6</td><td class="col-sm-3">x</td>
                                </tr>
                                <tr>
                                    <td class="col-sm-3">1</td><td class="col-sm-3">2</td><td class="col-sm-3">3</td><td rowspan="2"class="col-sm-3 enter" style="font-size:20px;">ENTER</td>
                                </tr>
                                <tr>
                                    <td class="col-sm-3">00</td><td class="col-sm-3">0</td><td class="col-sm-3">.</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
        </div>
        <div id="cover"></div>
        <div id="cancel-box">
            <div class="receipt-id">
                Receipt ID : 6666666
                <div class="cancel-all">Cancel All</div>
                <div class="date-time">16/09/59 15:26</div>
                <div class="receipt-product-table">
                    <table>
                        <thead>
                            <th class="text-center col-sm-2">Code</th>
                            <th class="col-sm-3">Item</th>
                            <th class="text-center col-sm-2">Price</th>
                            <th class="text-center col-sm-1">Qty</th>
                            <th class="text-center col-sm-2">Returned</th>
                            <th class="text-center col-sm-2">Return</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>test</td><td>test</td><td>test</td><td>test</td><td>test</td>
                                <td>
                                    <div id="del">-</div>
                                    <div id="number">1</div>
                                    <div id="plush">+</div>
                                </td>
                            </tr>
                            <tr>
                                <td>test</td><td>test</td><td>test</td><td>test</td><td>test</td>
                                <td>
                                    <div id="del">-</div>
                                    <div id="number">1</div>
                                    <div id="plush">+</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="total-refund">Total for refund : <span>76.00 THB</span></div>
                <div id="reason">
                    Reason for refund<br>
                    <select class="form-control" name="reason">
                        <option value="">Choose your reason for refund</option>
                    </select>
                </div>
                <div id="remark">
                    Remark<br>
                    <textarea class="form-control" rows="5"></textarea>
                </div>
                <div id="btn-refund">
                    <div class="btn-refund">Return</div>
                    <div class="btn-refund" id="close-refund">Close</div>
                </div>
            </div>
        </div>
<!-- {!! Html::script('js/frontend/cashier-table2.js') !!} -->
{!! Html::script('js/frontend/cashier-table3.js') !!}
<script>
    var api_url = '{{ URL::to("api") }}';
    // $('#report').on('click', function(){
    //     $('#cover').fadeIn(200);
    //     $('#report-box').fadeIn(200);
    // });
    $('#close-refund').on('click', function(){
        $('#cancel-box').fadeOut(200);
        $('#cover').fadeOut(200);
    });
    $('#cover').on('click', function(){
        $('#cancel-box').fadeOut(200);
        $('#cover').fadeOut(200);
    });
</script>
</body>
</html>
