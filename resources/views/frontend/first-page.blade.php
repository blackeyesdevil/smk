<html>
<head>
    <!-- /*<style src="{!! Html::style('css/frontend/start.css') !!}">*/ -->
    <link rel="stylesheet" type="text/css" href="../css/frontend/first-page.css">

</head>
<body>
    <div id="start">
        <div class="image-head">
            <img src="{{ URL::asset('images/logo2.png') }}">
        </div>
        <div class="head-text">
            TABLE NO.
        </div>
        <div id="table-div"><div id="table-no"></div></div>
        <div class="number-div">
            <div class="number" id="no7"><div class="number-text">7</div></div>
            <div class="number" id="no8"><div class="number-text">8</div></div>
            <div class="number" id="no9"><div class="number-text">9</div></div>
            <div class="number" id="no4"><div class="number-text">4</div></div>
            <div class="number" id="no5"><div class="number-text">5</div></div>
            <div class="number" id="no6"><div class="number-text">6</div></div>
            <div class="number" id="no1"><div class="number-text">1</div></div>
            <div class="number" id="no2"><div class="number-text">2</div></div>
            <div class="number" id="no3"><div class="number-text">3</div></div>
            <div class="number" id="remove"><div class="number-text">&nbsp;</div></div>
            <div class="number" id="no0"><div class="number-text">0</div></div>
            <div class="number" id="remove"><div class="number-text">&nbsp;</div></div>
        </div>
        <div class="line"></div>
        <div class="head-text">STORE</div>
        <div class="store">
            <div class="store-circle">
                <div class="circle">
                    <div class="img-store"><img src="{{ URL::asset('images/store-nara-logo-white.png') }}" alt=""></div>
                </div>
            </div>
            <div class="store-circle">
                <div class="circle">
                    <div class="img-store"><img src="{{ URL::asset('images/store-cafe-chilli-logo-white.png') }}" alt=""></div>
                </div>
            </div>
            <div class="store-circle">
                <div class="circle">
                    <div class="img-store"><img src="{{ URL::asset('images/store-kuppadeli-logo-white.png') }}" width="65px" alt=""></div>
                </div>
            </div>
            <div class="store-circle">
                <div class="circle">
                    <div class="img-store"><img src="{{ URL::asset('images/store-brix-logo-white.png') }}" alt=""></div>
                </div>
            </div>
            <div class="store-circle">
                <div class="circle">
                    <div class="img-store"><img src="{{ URL::asset('images/yuzu-logo-white-2.png') }}" alt=""></div>
                </div>
            </div>
            <div class="store-circle">
                <div class="circle">
                    <div class="img-store"><img src="{{ URL::asset('images/manfuyuan-logo-white-2.png') }}" alt=""></div>
                </div>
            </div>
        </div>
    </div>
    {!! Html::script('assets/global/scripts/jquery.min.js') !!}
    {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
    <script>
        $('.number').on("click", function(){
            var no = $(this).text();
            var id = $(this).attr('id');
            // alert(id);
            var tableLength = $("#table-no").text().length;
            if(id!="remove"){
                if(tableLength < 2){
                    $('#table-no').append(no);
                }
            }else{
                $('#table-no').text(function (_,txt) {
                    return txt.slice(0, -1);
                });
            }
        });
    </script>
</body>
</html>
