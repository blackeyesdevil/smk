<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Print All</title>
        <link href="{{ url()->asset('css/frontend/print.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="content">
                <table class="order-heading" cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="2">
                            <div class="brand-name">
                                WELCOME TO MY KITCHEN<br>
                                SIAM PIWAT CO., LTD.<br>
                                TEL. +662 658 1000 สาขา SIAM DISCOVERY<br>
                                POS SERIAL NO:E0000000014<br>
                                POS TYPE: DININ TABLE<br>
                                ใบกำกับภาษีอย่างย่อ เลขที่ N160001<br>
                                เลขประจำตัวผู้เสียภาษี : 012345678910<br>
                                วันที่ 15/10/16, 12:00:00<br>
                                ORDER #40
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><div class="store-name">ORDER TYPE: DINE IN</div></td>
                        <td><div class="table-no">TABLE NO. 49</div></td>
                    </tr>
                </table>


                <table class="order-detail" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div class="product-name">COFFEE</div>
                        </td>
                        <td>
                            <div class="qty">1x100.00</div>
                        </td>
                        <td>
                            <div class="sub-price">100.00</div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="product-name">CHOCOLATE</div>
                        </td>
                        <td>
                            <div class="qty">1x100.00</div>
                        </td>
                        <td>
                            <div class="sub-price">100.00</div>
                        </td>
                    </tr>
                    <tr>
                        <td>เงินรวมภาษี</td>
                        <td colspan="2">
                            <div class="total-price">200.00</div>
                        </td>
                    </tr>
                    <tr>
                        <td>เงินรวมก่อนภาษี</td>
                        <td colspan="2">
                            <div class="total-price">186.92</div>
                        </td>
                    </tr>
                    <tr>
                        <td>ภาษีมุลค่าเพิ่ม 7%</td>
                        <td colspan="2">
                            <div class="total-price">13.08</div>
                        </td>
                    </tr>
                </table>
                {{--<div style="margin: 0 auto">{!! $barcode !!}</div>--}}
                <hr>
            </div>
        </div>
    </body>
</html>
