<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Print All</title>
    <link href="{{ url()->asset('css/frontend/print.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <div class="content">
        <table class="order-heading" cellspacing="0" cellpadding="0">
            <tr>
                <td><div class="brand-name">My Kitchen</div></td>
            </tr>
            <tr>
                <td><div class="store-name">NARA Thai Cuisine</div></td>
            </tr>
            <tr>
                <td><div class="table-no">Table No. 10</div></td>
            </tr>
        </table>


        <table class="order-detail" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <div class="product-name">HOTTO KEIKI</div>
                </td>
                <td>
                    <div class="qty">1x230.00</div>
                </td>
                <td>
                    <div class="sub-price">230.00</div>
                </td>
            </tr>
        </table>

        <div class="barcode">{!! $barcode !!}</div>
        <hr>
    </div>
</div>
</body>
</html>
