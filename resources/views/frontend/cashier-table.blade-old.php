<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
{!! Html::style('https://fonts.googleapis.com/css?family=Open+Sans:400,300,700') !!}
{!! Html::style('fonts/foundation-icons/foundation-icons.css') !!}
<script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
{!! Html::script('assets/global/scripts/jquery.min.js') !!}
{!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
</head>
    <style>
    #table-order{
        height: 350px;
        border: 1px solid black;
        overflow-y: scroll;
        white-space: nowrap;
    }
    #table-order::-webkit-scrollbar {
      /* This is the magic bit */
      display: none;
    }
    #table-order-tr tr.active td{
        background: red;
    }
    #table-payment{
        height: 200px;
        border: 1px solid black;
        overflow-y: scroll;
        white-space: nowrap;
        color:#FFFFFF;
    }
    #table-payment::-webkit-scrollbar {
      /* This is the magic bit */
      display: none;
    }
    #paid-left{
        text-align: right;
    }
    </style>
<body style="margin:0">
<div style="width:1024px; height:786px">
	<div style="width:494px; height: auto; float:left">
    	<div style="height:67px; padding:12px">
    	  <input type="button" name="button" id="button" value="GRAB & GO">
    	  <input type="button" name="button2" id="button2" value="CASHIER">
    	</div>
    	<div style="background-color:#c0c0c0; padding:10px">
            <div id="table-order">
                <table width="100%" border="0" cellspacing="1" cellpadding="0">
            	    <tbody id="table-order-tr">
                	      <tr id="th-order" style="background-color:#7a7a7a; font-family:bold 18px Helvetica; color:#FFFFFF">
                	        <td width="135" height="42" align="center" valign="middle" style="width:135px;">Order No.</td>
                	        <td align="center" valign="middle" style="width:69px">Table No.</td>
                	        <td align="center" valign="middle" style="width:135px">Amount</td>
                	        <td align="center" valign="middle">&nbsp;</td>
                          </tr>

            	      <!-- <tr>
            	        <td height="50" align="center" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
            	        <td align="center" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
            	        <td align="center" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
            	        <td align="center" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
                      </tr> -->
                    </tbody>
                </table>
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:33px; color:#FFFFFF">
                <tr>
                  <td colspan="1" width="50%" style="color:#000000" id="order-no-payment">Order No. :-</td>
                  <td colspan="1" width="50%" style="color:#000000" id="order-table-payment">Table No. :-</td>
                </tr>
            </table>
            <div id="table-payment">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:0px; color:#FFFFFF">
            	    <tbody id="table-payment-tr">
            	      <tr id="th-payment">
            	        <td width="149" height="42" align="center" valign="middle" bgcolor="#4c4c4c">Payment by</td>
            	        <td width="212" height="42" align="center" valign="middle" bgcolor="#4c4c4c">Amount</td>
            	        <td height="42" align="center" valign="middle" bgcolor="#4c4c4c">&nbsp;</td>
                      </tr>
                    </tbody>
                </table>
            </div>
            <div id="paid-left">
                <table  width="100%">
                    <tr>

                      <td height="50" width="50%" align="left" valign="middle" style="color:#000000">
                        Paid : <span id="paid">-</span>
                      </td>
                      <td height="50" width="50%" align="left" valign="middle" style="color:#000000">
                        Left : <span id="left">-</span>
                      </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
  <div style="width:530px; height:auto; float:left">
       	<div id="header-pay" style="width:375px; height:90px; float:left; text-align:right; padding:50px 55px 0 0">
            <div id="total-pay-order-head" style="font-size:30px; font-weight:bold">
                <span id="total-pay-order" >2050.00</span> THB
            </div>
            <div id="change-head" style="color:#FF0004">
                Change : <span id="change" >00.00</span> THB
            </div>
        </div>
       	<div id="payment-type" style="width:430px; height:117px; float:left; text-align:center">
            <input type="button" name="button2" id="button2" value="Cash" style=" width:132px; height:50px; background-color:#000; color:#FFFFFF; margin:0 0 5px 0">
            <input type="button" name="button2" id="button2" value="Credit Card" style=" width:132px; height:50px; background-color:#9a9a9a; color:#FFFFFF; margin:0 5px 5px 5px">
            <input type="button" name="button2" id="button2" value="Siam Gift Card" style=" width:132px; height:50px; background-color:#9a9a9a; color:#FFFFFF; margin:0 0 5px 0">
            <input type="button" name="button2" id="button2" value="Rabbit Card" style=" width:132px; height:50px; background-color:#9a9a9a; color:#FFFFFF; margin-left:138px">
            <input type="button" name="button2" id="button2" value="Other" style=" width:132px; height:50px; background-color:#9a9a9a; color:#FFFFFF; margin-left:5px">
        </div>
        <div id="calculator" style="background-color:#dbdbdb; float:left">
            <div style="width:520px; height:auto; float:left; text-align:center; padding:7px 0 7px 0">
                <input type="text" name="textfield" id="total" style="width:497px; height:45px; text-align:right; font-size:30px " readonly>
            </div>
            <div style="width:140px; height:auto; float:left; text-align:center">
                <input type="button" name="b1000" id="b1000" class="cash" value-data="1000" style=" width:128px; height:59px;background:url(images/cashier/b1000.jpg); border:none">
                <input type="button" name="b500" id="b500" class="cash" value-data="500" style=" width:128px; height:59px;background:url(images/cashier/b500.jpg); border:none">
                <input type="button" name="b100" id="b100" class="cash" value-data="100" style=" width:128px; height:59px;background:url(images/cashier/b100.jpg); border:none">
                <input type="button" name="b50" id="b50" class="cash" value-data="50" style=" width:128px; height:59px;background:url(images/cashier/b50.jpg); border:none">
                <input type="button" name="b20" id="b20" class="cash" value-data="20" style=" width:128px; height:59px;background:url(images/cashier/b20.jpg); border:none">
            </div>
            <div style="width:385px; height:auto; float:left; text-align:center">
                <div style="float:left;width:280px">
                    <input type="button" name="button2" id="button2" class="num" value="7" style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="8" style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="9" style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="4" style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="5" style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="6" style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="1"style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="2" style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="3" style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="00" style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="0" style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="." style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                </div>
                <div class="" style="float:left;width:0px">
                    <input type="button" name="button2" id="button2" class="num" value="C" style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="X" style=" width:89px; height:86px; background-color:#FFFFFF; font-size:25px;">
                    <input type="button" name="button2" id="button2" class="num" value="ENTER" style=" width:89px; height:172px; background-color:#000; color:#FFFFFF; font-size:20px;">

                </div>


            </div>
            <div style="clear:both"></div>
      </div>
    </div>
</div>
<script>
    var orders= [];
    var api_url = '{{ URL::to("api") }}';
    console.log(api_url+'/cashier');
    $.getJSON(api_url + '/cashier', null , function (data) {
        console.log(data);
        orders = data.orders;
        console.log(orders);
        $.each(orders,function( key,value){
            // console.log(value.table_no);
            // order =
            order = '<tr id="'+value.orders_no+'" data-amount="'+value.total_price+'" data-table="'+value.table_no+'">'+
              '<td height="50" align="center" valign="middle" bgcolor="#FFFFFF">'+value.orders_no+'</td>'+
              '<td align="center" valign="middle" bgcolor="#FFFFFF">'+value.table_no+'</td>'+
              '<td align="center" valign="middle" bgcolor="#FFFFFF">'+value.total_price+'</td>'+
              '<td align="center" valign="middle" bgcolor="#FFFFFF">DETAIL <i class="fi-x-circle"></i></td>'+
            '</tr>';
            $('#table-order-tr').append(order);
        });

    });
</script>
<script>
    $('input').on('click',function(){
        if($(this).attr('class')=='cash'){
            console.log($(this).val());
            console.log('cash');
            if($('#total').val()==''){
                x=0;
            }
            else{
                x=parseInt($('#total').val());
            }
            y=parseInt($(this).attr('value-data'));
            console.log(x+y);
            sum=x+y;
            $('#total').val(sum);
        }
        else if($(this).attr('class')=='num'){
            if($(this).val()=='C'){
                $('#total').val('');
            }
            else if($(this).val()=='X'){
                total = $('#total').val();
                total = total.substring(0,total.length - 1);
                $('#total').val(total);
            }
            else if($(this).val()=='ENTER'){
                var total_cal =parseInt($('#total').val());
                var pay_order =parseInt($('#total-pay-order').text());
                if($('#total').val()==''||total_cal==0){

                }
                else{
                    var table =
                    '<tr>'+
                      '<td  height="50" align="center" valign="middle" bgcolor="#858585">'+'Cash'+'</td>'+
                      '<td  height="50" align="center" valign="middle" bgcolor="#858585">'+total_cal+'</td>'+
                      '<td  height="50" align="center" valign="middle" bgcolor="#858585"><span id="remove-payment" data-amount="'+total_cal+'">remove</span></td>'+
                    '</tr>';
                    $('#table-payment-tr').append(table);
                    $('#total').val('');
                    console.log(total_cal);
                    updatePaidLeft(total_cal,pay_order);
                }
            }
            else{
            x = $(this).val();
            y =$('#total').val();
            $('#total').val(y+x);
            }
        }
    });
    $('#table-payment-tr').on('click','#remove-payment',function(){
        $(this).closest('tr').remove();
        var total_cal =$(this).attr('data-amount');
        var pay_order =parseInt($('#total-pay-order').text());
        updatePaidLeft(total_cal,pay_order,"1");
    });

    $('#table-order-tr').on('click','td',function(){
        $('#table-order-tr tr.active').removeClass('active');
        if($(this).parent().attr('id')=='th-order'){
        }
        else{
            $(this).parent().addClass('active');
        }
        // console.log($(this).parent());
        // console.log($(this).parent().attr('data-amount'));
        id= $(this).parent().attr('id');
        table =$(this).parent().attr('data-table');
        total_pay= $(this).parent().attr('data-amount');
        $('#total-pay-order').text(total_pay);
        $('#order-no-payment').text('Order No. :'+id);
        $('#order-table-payment').text('Table No. :'+table);
        $('#left').text(total_pay);
        $('#table-payment-tr').html('');
    });
    $('#table-order-tr').on('click','.fi-x-circle',function(){
      console.log("testt");
    });
    function updatePaidLeft(total_cal,pay_order,symbol="0"){
        console.log(total_cal);
        if(symbol=="0"){
            paid = $('#paid').text();
            console.log(paid);
            if(paid=='-'){
                paid = 0;
                console.log(paid);
            }
            else{
                paid = parseInt($('#paid').text());
            }
            paid = paid+total_cal;
            console.log(paid);
            $('#paid').text(paid);
        }
        else{
            paid = parseInt($('#paid').text());
            paid = paid - total_cal;
            $('#paid').text(paid);
            console.log(parseInt($('#left').text()));
            // if(parseInt($('#left').text())>0){
                $('#change').text('0.00');
            // }
        }
            left = parseInt($('#total-pay-order').text())-paid;
            if(left<0){
                change = left * (-1);
                console.log(change);
                $('#change').text(change);
                left = 0;
            }
        $('#left').text(left);
    }
</script>
</body>
</html>
