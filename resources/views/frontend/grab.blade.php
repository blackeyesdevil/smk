<html>
<head>
    <!-- /*<style src="{!! Html::style('css/frontend/start.css') !!}">*/ -->
    <link rel="stylesheet" type="text/css" href="../css/frontend/grab.css">
    {!! Html::style('https://fonts.googleapis.com/css?family=Open+Sans:400,300,700') !!}
    {!! Html::style('fonts/foundation-icons/foundation-icons.css') !!}

    <!-- CSS Files -->
    {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}

</head>
<body>
    <div id="grab">
        <div class="container">
            <div id="btn-type" class="col-xs-12">
                <div class="btn-type col-xs-3">Grab & GO</div>
                <div class="btn-type col-xs-3">Cashier</div>
            </div>
            <div id="table-barcode" class="col-xs-12" style="padding:0;">
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table id="eiei" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="text-center col-sm-2">Code</th>
                                <th class="text-center col-sm-4">Item</th>
                                <th class="text-center col-sm-1">Qty</th>
                                <th class="text-right col-sm-2">Price</th>
                                <th class="text-right col-sm-2">Total</th>
                                <th class="text-center col-sm-1">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="btn-barcode" class="col-xs-4">
                <div class="btn-barcode">
                    <input type="text" class="form-control" id="barcode" placeholder="Barcode input & Qty" autofocus>
                </div>
            </div>
            <div class="total-text col-xs-8">
                <div class="total">Total : 3 time(s)</div>
                <div class="amount">Amount : 172.00 THB</div>
            </div>
        </div>
    </div>
    {!! Html::script('assets/global/scripts/jquery.min.js') !!}
    {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
    <script>
        $('#barcode').on('keyup',function(){
            if($('#barcode').val().length==5)
            {
                // console.log($('#barcode').val());
                jQuery("#eiei tbody").append("<tr><td>"+$('#barcode').val()+"</td><td></td><td>1</td><td></td><td></td><td></td></tr>");
                $(this).val('');
            }
        });
        $('#barcode').on('keypress',function(e){
            if (e.which == 13) {
            //    $('form#login').submit();
                if($('#barcode').val().length>=2)
                {
                    // console.log($('#barcode').val());
                    jQuery("#eiei tbody").append("<tr><td>"+$('#barcode').val()+"</td><td></td><td>1</td><td></td><td></td><td></td></tr>");
                    $(this).val('');
                }
                return false;
            }

        });
    </script>
</body>
</html>
