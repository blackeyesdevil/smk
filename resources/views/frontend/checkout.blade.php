@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
    {!! Html::style('css/frontend/checkout.css') !!}
@endsection

@section('content')
    <div id="content-checkout">
        <div class="container-fluid" style="padding: 0;">
            <div id="checkout-box" class="col-xs-8">
                <div class="order-detail col-xs-12">
                    <div class="head-text col-xs-6">
                        MY ORDER
                    </div>
                    <div class="order-id col-xs-6">
                        Order ID : AA0001
                    </div>
                    <div class="product-orders">
                        <div class="product-name col-xs-9">
                            BRIX BOX
                        </div>
                        <div class="product-price col-xs-3">
                            290 THB
                        </div>
                    </div>
                    <div class="product-option col-xs-9">
                        <div class="product-option-item">
                            Ice cream Chocolate flavors
                        </div>
                        <div class="product-option-item">
                            Whimp Cream
                        </div>
                    </div>
                    <div class="product-orders">
                        <div class="product-name col-xs-9">
                            BRIX BOX
                        </div>
                        <div class="product-price col-xs-3">
                            290 THB
                        </div>
                    </div>
                    <div class="product-option col-xs-9">
                        <div class="product-option-item">
                            Ice cream Chocolate flavors
                        </div>
                        <div class="product-option-item">
                            Whimp Cream
                        </div>
                    </div>
                    <div class="total-price col-xs-12">
                        TOTAL&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;580 THB
                    </div>
                </div>
                <div id="tax-receipt" class="col-xs-12">
                    <div class="head-text col-xs-12">
                        Tax Receipt
                    </div>
                    <div class="checkbox col-xs-12">
                      <label><input type="checkbox" value="">I'd like to recive Tax Receipt.</label>
                      <textarea class="col-xs-12" rows="5" id="tax-address"></textarea>
                      <p>- Please enter your full address to obtain a Tax Receipt.</p>
                    </div>

                </div>
                <div class="button-pay col-xs-12">
                    <div class="button-pay-by col-xs-4 col-xs-offset-1" id="pay-cash">
                        PAY BY CASH
                    </div>

                    <div class="button-pay-by col-xs-5 col-xs-offset-1">
                        PAY BY CADIT CARD
                    </div>

                </div>
            </div>
            <div id="button-etc" class="col-xs-4 row">
                <div id="discount-code-normal" class="button-etc-in col-xs-4 col-xs-offset-3" >
                    Discount code
                </div>
                <div class="button-etc-in col-xs-4 col-xs-offset-3">
                    VIZ Card
                </div>
                <div class="button-etc-in col-xs-4 col-xs-offset-3">
                    Employee Discounts
                </div>
            </div>

            <div id="discount-code">
                <div class="close">
                    <img src="{{ URL::asset('images/close.png') }}">
                </div>
                <div class="head-text">
                    Discount code
                </div>

                <div class="input-discount col-xs-12">
                    Have a discount code?
                    <input type="text" id="discount-input" name="discout" class="col-xs-12">
                </div>
                <div class="discount-alert col-xs-12">
                    You've got 20% Discount!
                </div>
                <div class="button-discount col-xs-12">
                    <div id="discount-apply-btn" class="button-discount-in col-xs-3 col-xs-offset-1">
                        Apply
                    </div>
                    <div id="discount-cancel-btn" class="button-discount-in col-xs-3 col-xs-offset-1">
                        Cancel
                    </div>
                </div>
            </div>

            <div id="confirm-pay-cash">
                <div class="image-pay">
                    <img src="{{ URL::asset('images/confirm-pay.png') }}">
                </div>
                <div class="head-text">
                    Thank you!
                </div>
                <div class="confirm-order-detail">
                    <div class="order-text col-xs-6">
                        Your Order
                    </div>
                    <div class="order-id col-xs-6">
                        Order ID : AA0001
                    </div>

                    <div class="confirm-product-orders">
                        <br>
                        <div class="product-name col-xs-9">
                            BRIX BOX
                        </div>
                        <div class="product-price col-xs-3">
                            290 THB
                        </div>
                    </div>
                    <div class="confirm-product-option col-xs-9">
                        <div class="product-option-item">
                            Ice cream Chocolate flavors
                        </div>
                        <div class="product-option-item">
                            Whimp Cream
                        </div>
                    </div>
                    <div class="confirm-product-orders">
                        <br>
                        <div class="product-name col-xs-9">
                            BRIX BOX
                        </div>
                        <div class="product-price col-xs-3">
                            290 THB
                        </div>
                    </div>
                    <div class="confirm-product-option col-xs-9">
                        <div class="product-option-item">
                            Ice cream Chocolate flavors
                        </div>
                        <div class="product-option-item">
                            Whimp Cream
                        </div>
                    </div>
                    <div class="confirm-total-price">
                        TOTAL&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;580 THB
                    </div>
                    <div class="confirm-finish">
                        <div class="btn-finish">
                            Finish
                        </div>
                        <div class="confirm-time">
                            00:59
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div id="cover"></div>

@endsection

@section('script')
<script>
$('#cover, .close').on('click', function(){
    $('#cover').fadeOut(200);
    $('#discount-code').fadeOut(200);
});
$('#discount-code-normal').on('click', function(){
    $('#cover').fadeIn(200);
    $('#discount-code').fadeIn(200);
});
$('#pay-cash').on('click', function(){
    $('#cover').fadeIn(200);
    $('#confirm-pay-cash').fadeIn(200);
});
$('#cover, .btn-finish').on('click', function(){
    $('#cover').fadeOut(200);
    $('#confirm-pay-cash').fadeOut(200);
});
</script>
@endsection
