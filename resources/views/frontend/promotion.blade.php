
    <div id="content">
        <div class="container-fluid">
            <div id="promotion-list" class="col-xs-12">
                <div class="promotion-item col-xs-4">
                    <div class="promotion-info">
                        <div class="promotion-name">BUY 1 GET 1 FREE</div>
                    </div>
                </div>
                <div class="promotion-item col-xs-4">
                    <div class="promotion-info">
                            <div class="promotion-name">BUY 1 GET 1 FREE</div>
                    </div>
                </div>
                <div class="promotion-item col-xs-4">
                    <div class="promotion-info">
                            <div class="promotion-name">BUY 1 GET 1 FREE</div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <div id="cover"></div>
    <div id="promotion-detail">
        <div class="promotion-image">
            <div class="close-btn">
                <img src="{{URL::asset('images/close.png')}}" alt="">
            </div>
            <div class="promotion-name">BUY 1 GET 1 FREE</div>
        </div>
        <div class="step col-xs-12">Step 1</div>
        <div class="step-text col-xs-12">Choose 1  Dessert</div>
        <div class="product-bar col-xs-12">
            <div class="product-list">
                <div class="product-item-promotion">
                    <div class="produt-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                    <div class="product-name">FIRST LOVE</div>
                    <div class="product-price">250 THB</div>
                    <div class="product-price-old">285 THB</div>
                </div>
                <div class="product-item-promotion">
                    <div class="produt-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                    <div class="product-name">FIRST LOVE</div>
                    <div class="product-price">250 THB</div>
                    <div class="product-price-old">285 THB</div>
                </div>
                <div class="product-item-promotion">
                    <div class="produt-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                    <div class="product-name">FIRST LOVE</div>
                    <div class="product-price">250 THB</div>
                    <div class="product-price-old">285 THB</div>
                </div>
                <div class="product-item-promotion">
                    <div class="produt-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                    <div class="product-name">FIRST LOVE</div>
                    <div class="product-price">250 THB</div>
                    <div class="product-price-old">285 THB</div>
                </div>
            </div>
        </div>
        <div class="step col-xs-12">Step 2</div>
        <div class="step-text col-xs-12">Choose 1  Drink</div>
        <div class="product-bar col-xs-12">
            <div class="product-list">
                <div class="product-item-promotion">
                    <div class="produt-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                    <div class="product-name">FIRST LOVE</div>
                    <div class="product-price">250 THB</div>
                    <div class="product-price-old">285 THB</div>
                </div>
                <div class="product-item-promotion">
                    <div class="produt-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                    <div class="product-name">FIRST LOVE</div>
                    <div class="product-price">250 THB</div>
                    <div class="product-price-old">285 THB</div>
                </div>
                <div class="product-item-promotion">
                    <div class="produt-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                    <div class="product-name">FIRST LOVE</div>
                    <div class="product-price">250 THB</div>
                    <div class="product-price-old">285 THB</div>
                </div>
                <div class="product-item-promotion">
                    <div class="produt-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                    <div class="product-name">FIRST LOVE</div>
                    <div class="product-price">250 THB</div>
                    <div class="product-price-old">285 THB</div>
                </div>
            </div>
        </div>
        <div class="total-text col-xs-12">
            <div class="sub-total">
                Subtotal : &nbsp;&nbsp;&nbsp;580 THB
            </div>
            <div class="discount">
                Discount : &nbsp;&nbsp;&nbsp;-100 THB
            </div>
            <div class="total">
                Total : &nbsp;&nbsp;&nbsp;480 THB
            </div>
        </div>
        <div id="add-btn" class="col-xs-12">

            <div class="add-text">
                <img src="{{URL::asset('images/add-to-cart.png')}}" alt="">
                ADD TO CART
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('.promotion-item').on('click', function(){
            $('#cover').fadeIn();
            $('#promotion-detail').fadeIn();
        });
        $('.close-btn').on('click', function(){
            $('#cover').fadeOut();
            $('#promotion-detail').fadeOut();
        });
        $('#cover').on('click', function(){
            $('#cover').fadeOut();            
            $('#promotion-detail').fadeOut();
        });
    </script>
