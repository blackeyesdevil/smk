<div id="my-room">
  <div id="menu-list">
    <!-- <div class="menu-item col-xs-3">
      <div class="icon"><i class="glyphicon glyphicon-user"></i></div>
      <div class="text">EDIT PROFILE</div>
    </div> -->

    <div class="person-detail">
        <div class="person-name col-xs-8">
            <div class="person-circle col-xs-4">
                <div class="person-image"><img src="{{URL::asset('images/person.png')}}" alt=""></div>
            </div>
            <div class="person-text col-xs-8">
                <div class="text">
                    <div id="name"><span>Hi </span>Neunghathai</div>
                    <div id="tel">Tel : 098-831-8020</div>
                    <div id="email">E-mail : tiggertigger011@gmail.com</div>
                </div>

            </div>
        </div>
        <div class="point col-xs-4">
            <div class="point-in">
                <div class="point-text">Your Point</div>
                <div id="no-point">28</div>
                <div class="point-text">points</div>
            </div>
        </div>
    </div>

    <div class="menu-item col-xs-4">
      <div class="icon"><i class="glyphicon glyphicon-cutlery"></i></div>
      <div class="text">RECOMMENDED MENU</div>
    </div>

    <div class="menu-item col-xs-4">
      <div class="icon"><i class="glyphicon glyphicon-gift"></i></div>
      <div class="text">PROMOTIONS</div>
    </div>

    <div class="menu-item col-xs-4">
      <div class="icon"><i class="glyphicon glyphicon-heart-empty"></i></div>
      <div class="text">MY FAVORITE</div>
    </div>

    <div class="clearfix"></div>

    <!-- <div class="menu-item col-xs-3">
      <div class="icon"><i class="glyphicon glyphicon-barcode"></i></div>
      <div class="text">REDEEM</div>
    </div> -->

    <div class="menu-item col-xs-4">
      <div class="icon"><i class="glyphicon glyphicon-asterisk"></i></div>
      <div class="text">REWARDS</div>
    </div>

    <div class="menu-item col-xs-4" id="my-orders-history" data-member="1">
      <div class="icon"><i class="glyphicon glyphicon-time"></i></div>
      <div class="text">ORDER HISTORY</div>
    </div>

    <div class="menu-item col-xs-4">
      <div class="icon"><i class="glyphicon glyphicon-off"></i></div>
      <div class="text">LOG OUT</div>
    </div>

    <div class="clearfix"></div>

    <div class="head-text">What would you like to order today?</div>
    <div class="my-room-content">
      <div class="product">
          <div class="text col-xs-12 row">Today Special are</div>
          <div class="product-list col-xs-12">
              <div class="product-item">
                  <div class="product-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                  <div class="product-name"> TOM YUM KUNG</div>
                  <div class="product-price">220 THB</div>
                  <div class="product-price-old">250 THB</div>
              </div>
              <div class="product-item">
                  <div class="product-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                  <div class="product-name"> TOM YUM KUNG</div>
                  <div class="product-price">220 THB</div>
                  <div class="product-price-old">250 THB</div>
              </div>
              <div class="product-item">
                  <div class="product-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                  <div class="product-name"> TOM YUM KUNG</div>
                  <div class="product-price">220 THB</div>
                  <div class="product-price-old">250 THB</div>
              </div>
              <div class="product-item">
                  <div class="product-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                  <div class="product-name"> TOM YUM KUNG</div>
                  <div class="product-price">220 THB</div>
                  <div class="product-price-old">250 THB</div>
              </div>
          </div>
      </div>

      <div class="product">
          <div class="text col-xs-12 row">Check out your personalized menu</div>
          <div class="product-list col-xs-12">
              <div class="product-item">
                  <div class="product-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                  <div class="product-name"> TOM YUM KUNG</div>
                  <div class="product-price">220 THB</div>
                  <div class="product-price-old">250 THB</div>
              </div>
              <div class="product-item">
                  <div class="product-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                  <div class="product-name"> TOM YUM KUNG</div>
                  <div class="product-price">220 THB</div>
                  <div class="product-price-old">250 THB</div>
              </div>
              <div class="product-item">
                  <div class="product-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                  <div class="product-name"> TOM YUM KUNG</div>
                  <div class="product-price">220 THB</div>
                  <div class="product-price-old">250 THB</div>
              </div>
              <div class="product-item">
                  <div class="product-image"><img src="{{URL::asset('images/no-pic.png')}}" alt=""></div>
                  <div class="product-name"> TOM YUM KUNG</div>
                  <div class="product-price">220 THB</div>
                  <div class="product-price-old">250 THB</div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
