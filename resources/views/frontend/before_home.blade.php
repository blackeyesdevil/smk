<html>
<head>

</head>
<!-- {!! Html::style('css/frontend/mykitchen-choose-table.css') !!} -->
<link rel="stylesheet" type="text/css" href="css/frontend/mykitchen-choose-table.css">
<body>
<div id="content">
        <div class="container-fluid">
        <div class="my-kitchen"><img class="img-my-kitchen" src="{{ URL::asset('images/mykitchen-home.png')}}"></div>
        <div class="table-no">TABLE NO.</div>
        <!-- <div class="table-rectangle"><span>&nbsp</span></div> -->
        <!-- <div class="text-center"></div> -->
        <div class="number">
                <div>
                <!-- <div id="num" class="rectangle col-xs-4"><span>7</span></div> -->
                <div class="table-reg" style="background: #D8D8D8;font-size:30px;margin-bottom:10px;"><span>&nbsp</span></div>
                <!-- <div id="num" class="rectangle col-xs-4"><span>9</span></div> -->
              </div>
                <div>
                <div id="num" class="rectangle col-xs-4"><span>7</span></div>
                <div id="num" class="rectangle col-xs-4"><span>8</span></div>
                <div id="num" class="rectangle col-xs-4"><span>9</span></div>
              </div>
                <div>
                <div id="num" class="rectangle col-xs-4"><span>4</span></div>
                <div id="num" class="rectangle col-xs-4"><span>5</span></div>
                <div id="num" class="rectangle col-xs-4"><span>6</span></div>
              </div>
                <div>
                <div id="num" class="rectangle col-xs-4"><span>1</span></div>
                <div id="num" class="rectangle col-xs-4"><span>2</span></div>
                <div id="num" class="rectangle col-xs-4"><span>3</span></div>
              </div>
                <div>
                <div id="clear" class="rectangle col-xs-4"><span>c</span></div>
                <div id="num" class="rectangle col-xs-4"><span>0</span></div>
                <div id="enter" class="rectangle col-xs-4"><span>Enter</span></div>
              </div>
          </div>

        </div>
</div>
</body>
  {!! Html::script('assets/global/scripts/jquery.min.js') !!}
    <script>
    $(function(){
        $('.number').on('click', '#num', function(){
           var valueOld = $('.table-reg ').text();
            $('.table-reg ').text(valueOld+$(this).text());

        });
        $('.number').on('click', '#clear', function(){
           var valueOld = $('.table-reg ').text();
            $('.table-reg ').text('');

        });
        $('.number').on('click', '#enter', function(){
           var text_value = $('.table-reg ').text();
            if( text_value > 0 ){
              alert('OK!');
            }else{
              alert('Please Choose Table Number');
            }

        });
      });
    </script>
    </body>
</html>
