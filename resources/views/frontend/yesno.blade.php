@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
    {!! Html::style('css/frontend/yesno.css') !!}
@endsection

@section('content')
<div id="yesno">
    <div>Are you sure ?</div>
    <div id="yes">yes</div>
    <div id="no">no</div>
</div>
@endsection

@section('script')
<script>
$('#cover, .close').on('click', function(){
    $('#cover').fadeOut(200);
    $('#discount-code').fadeOut(200);
});
$('#discount-code-normal').on('click', function(){
    $('#cover').fadeIn(200);
    $('#discount-code').fadeIn(200);
});
$('#pay-cash').on('click', function(){
    $('#cover').fadeIn(200);
    $('#confirm-pay-cash').fadeIn(200);
});
$('#cover, .btn-finish').on('click', function(){
    $('#cover').fadeOut(200);
    $('#confirm-pay-cash').fadeOut(200);
});
</script>
@endsection
