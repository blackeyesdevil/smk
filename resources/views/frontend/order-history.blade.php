<div id="order-history" class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
  <div class="sub-headline">TABLE NO. 12</div>
  <div class="headline">Order History</div>

  <div id="order-list">
    @foreach ($orders as $order)
    <?php
    $time_ago = MainFunction::get_time_ago(strtotime($order->created_at));
    ?>
    <div class="order-item">
      <div class="order-info">
        <div class="order-datetime">{{ $time_ago }}</div>
        @if ($order->status == '0' || $order->status == '1')
        <div class="label label-default">IN PROCESS</div>
        @elseif ($order->status == '2')
        <div class="label label-danger">CANCELED</div>
        @elseif ($order->status == '99')
        <div class="label label-success">COMPLETED</div>
        @endif
        <div class="order-no">ORDER #{{ $order->orders_no}}</div>
        <div class="order-total-price">Total Price: &nbsp;&nbsp; {{ number_format($order->total_price, 2) }} THB</div>
      </div>

      <div class="order-detail">

        @foreach ($order->ordersDetail as $order_detail)
        <div class="order-detail-item">
            <div class="row">
                <div class="product-info col-xs-5">
                    <div class="product-name">{{ $order_detail->product_name_en }}</div>
                    <div class="product-option-list">
                        <div class="product-option-item">
                            {{ $order_detail->option_en }}
                        </div>
                    </div>
                </div>

                <div class="qty col-xs-4">{{ $order_detail->qty }} x {{ number_format($order_detail->price, 2) }} THB</div>

                <div class="subtotal-price col-xs-3">
                    {{ number_format($order_detail->qty * $order_detail->price, 2) }} THB
                </div>
            </div>
        </div>
        @endforeach

        <div class="grand-total">
          <div class="topic col-xs-8">Grand Total</div>
          <div class="value col-xs-4">{{ number_format($order->total_price, 2) }} THB</div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
