@extends('frontend.layout.butler-layout')

@section('title', ' - Home')

@section('css')
{!! Html::style('css/frontend/butler-home.css') !!}
@endsection

@section('content')
    <div id="content">
        <div class="container">
            <div id="logo-head">
                <img src="{{ URL::asset('images/butler/logo.png') }}">
            </div>
            <div id="table-map">
                <div class="image-in">
                    <img src="{{ URL::asset('images/butler/table.png') }}">
                </div>
                <div class="text">
                    ผังโต๊ะ
                </div>

            </div>
            <div id="order-status">
                <div class="image-in">
                    <img src="{{ URL::asset('images/butler/order.png') }}">
                </div>
                <div class="text">
                    Order Status
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection
