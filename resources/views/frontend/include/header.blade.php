<header>
    <div class="container">
        <div id="top-bar">
            <div id="lang-bar">
                <span class="lang-item">English</span>
                <span class="switch-slider">
                    <span class="switch-icon">&nbsp;</span>
                </span>
                <span class="lang-item">ไทย</span>
            </div>

            <img src="{{ URL::asset('images/logo.png') }}" alt="{{ Config::get('constants.APP_NAME') }}" id="logo">

            <div id="right-main-menu">
                <a href="#" id="home-btn" class="btn -circle">
                    <i class="fi-home"></i>
                </a>

                <a href="#" id="my-room-btn" class="btn">
                    <i class="glyphicon glyphicon-user"></i>
                    MY ROOM
                </a>

                <a href="#" id="order-list-btn" class="btn -circle">
                    <i class="fi-list-thumbnails"></i>

                    <div class="noti-div">
                        <span class="noti-number">0</span>
                    </div>
                </a>

                <div id="noti-add-cart"></div>
            </div>
        </div>

        <div id="store-list"></div>
    </div>
</header>
