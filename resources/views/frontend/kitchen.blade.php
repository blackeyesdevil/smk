<html>
  <head>
    <meta charset="utf-8">
    <title>My Kitchen</title>
    {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
    <link rel="stylesheet" href="{{ URL::asset('css/frontend/kitchen.css') }}">
  </head>

  <body>
    <div id="processing-lane" class="col-xs-5 swimlane">
      <div class="headline">IN PROCESS</div>

      <div class="order-list"></div>
    </div>

    <div id="completed-lane" class="col-xs-5 swimlane">
      <div class="headline">COMPLETED</div>

      <div class="order-list"></div>
      </div>
    </div>

    <div id="tags-lane" class="col-xs-2 swimlane">
      <div class="headline">TAGS</div>

      <div class="tag-list">
        @for ($i=0; $i<4; $i++)
        <div class="tag-item">
          <div class="block available">หมู</div>
        </div>

        <div class="tag-item">
          <div class="block unavailable">ไก่</div>
        </div>

        <div class="tag-item">
          <div class="block available">ปลาหมึก</div>
        </div>

        <div class="tag-item">
          <div class="block available">ปลากระพง</div>
        </div>

        <div class="clearfix"></div>
        @endfor
      </div>

      <div id="barcode-div">
        <input type="text" id="barcode" autofocus>
      </div>
    </div>

    {!! Html::script('assets/global/scripts/jquery.min.js') !!}

    <script>
    var base_url = '{{ URL::to("/") }}';
    var api_url = '{{ URL::to("api") }}';
    $(function(){
      $('.tag-item .block').on('click', function(){
        currentItem = $(this);

        if (currentItem.hasClass('available'))
          currentItem.removeClass('available').addClass('unavailable');
        else
          currentItem.removeClass('unavailable').addClass('available');
      });

      $('#barcode').focus();

      $('#barcode').focusout(function(){
        $('#barcode').focus();
      });

      $('#barcode').on('keyup', function(e) {
          clearTimeout($.data(this, 'timer'));

          $(this).data('timer', setTimeout(submitBarcode, 500));
      });
      function submitBarcode(force) {
          var existingString = $("#barcode").val();
          if (!force && existingString.length < 3) return; //wasn't enter, not > 2 char
          $.post('{{ url()->to("kitchen/check-order/check") }}',
                  { barcode: $('#barcode').val(), _token: '{{ csrf_token() }}' }, function(){
            $('#barcode').val('');
          });
      }
    });
    </script>
    {!! Html::script('js/frontend/kitchen.js') !!}
  </body>
</html>
