@extends('frontend.layout.main-layout')

@section('content')
    <form id="form">
        <input type="text" id="orderId" placeholder="ORDER ID">
        <input type="text" id="amount" placeholder="AMOUNT">
        <input type="hidden" id="token" value="abcd">
        <button type="submit">SUBMIT</button>
    </form>
@endsection

@section('script')
    <script>
    $(function(){
        $('#form').on('submit', function(e){
            e.preventDefault();
            var orderId = $('#orderId').val();
            var amount = $('#amount').val();
            var token = $('#token').val();

            window.location.href = 'intent:#Intent;action=com.smk.NK_CUSTOM_ACTION;S.amount=' + amount + ';S.orderid=' + orderId + ';S.token=' + token + ';end';
            // console.log('intent:#Intent;action=com.smk.NK_CUSTOM_ACTION;S.amount=' + amount + ';S.order=' + orderId + ';end');
        });
    });
    </script>
@endsection
