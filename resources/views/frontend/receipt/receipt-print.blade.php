<!DOCTYPE html>
<html>
    <head>
        {!! Html::style('css/frontend/receipt-print.css') !!}
    </head>
    <body>
    <div id="print">
        <div class="company-info">
            <table cellspacing="0" cellpadding="0">
                <tr valign="bottom">
                    <td class="left">
                        <div class="company-logo"><img src="{{ url()->asset('images/logo-b.png') }}"><span class="company-name">Siam Piwat Co., Ltd. Branch Siam Discovery</span></div>
                        <div class="company-address">
                        989 Rama 1 Road, Pathumwan,<br>
                        Bangkok 10330, Thailand. Tel 02 658 1000
                        </div>
                    </td>
                    <td class="right">
                        <div class="txt-original">TAX INVOICE</div>
                        <div class="tax-id">TAX ID. 0029092840300</div>
                        <div class="receipt-no">Receipt No. {{ $receipt->receipt_no }}</div>
                        <div class="date">Date 30 September 2559</div>
                    </td>
                </tr>
            </table>
        </div>

        <div class="customer-detail">
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <b>Customer Information</b><br>
                        {{ $receipt->company_name.' '.$receipt->branch }}<br>
                        {{ $receipt->address }}<br>
                        Tax ID. {{ $receipt->tax_id }}
                    </td>
                </tr>
            </table>
        </div>
        <div class="order-detail">
            <table cellspacing="0" cellpadding="5" >
                <thead>
                    <tr>
                        <th width="2%">No.</th>
                        <th>Item</th>
                        <th width="2%">Qty.</th>
                        <th width="15%">Price/Unit</th>
                        {{--<th width="15%">Discount</th>--}}
                        <th width="15%">Price</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $j = 0;
                        $orders_id = 0;
                    ?>
                    @foreach($order_details as $key => $value)
                        <?php
                            $product_name = $value->product_name_en;
                            $option_name = $value->option_en;
                            $qty = $value->qty;
                            $price = $value->price;

                            $sub_price = $price * $qty;

                        ?>
                        <tr valign="top">
                            <td class="text-center">{{ $key+1 }}</td>
                            <td>
                                {{ $product_name  }}
                                @if($option_name != '')
                                    {!! '<br> - '.preg_replace('/([\(+\d\)])/','', $option_name) !!}
                                @endif

                            </td>
                            <td class="text-center">{{ $qty  }}</td>
                            <td class="text-right">{{ number_format($price,2) }}</td>
                            {{--<td class="text-right">0.00</td>--}}
                            <td class="text-right">{{ number_format($sub_price,2) }}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="text-right">
                <?php
                $total_price = 0;
                $discount_price = 0;
                $total_vat = 0;
                $total_before_vat = 0;
                ?>
                @foreach($orders as $order)
                    <?php
                    $price = $order->total_price;
                    $discount = $order->discount_price;

                    $total_price += $price;
                    $discount_price += $discount;

                    $before_vat = round($price * 100 / 107,2);
                    $vat = $price - $before_vat;

                    $total_before_vat += $before_vat;
                    $total_vat += $vat;
                    ?>
                @endforeach
                    <tr>
                        <td colspan="4">Total</td>
                        <td>{{ number_format($total_price,2) }}</td>
                    </tr>
                    <tr>
                        <td colspan="4">Total VAT</td>
                        <td>{{ number_format($total_vat,2) }}</td>
                    </tr>
                    <tr>
                        <td colspan="4">Total Before VAT</td>
                        <td>{{ number_format($total_before_vat,2) }}</td>
                    </tr>
                    <tr>
                        <td colspan="4">Total Discount</td>
                        <td>{{ number_format($discount_price,2) }}</td>
                    </tr>

                    <?php
                    $grand_total = $total_price+$discount_price;
                    ?>
                    <tr>
                        <td colspan="4">Total Price</td>
                        <td>{{ number_format($grand_total,2) }}</td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>


    </body>
</html>