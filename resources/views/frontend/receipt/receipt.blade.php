@extends('frontend.layout.receipt-layout')
@section('css')
{!! Html::style('css/frontend/receipt.css') !!}
@endsection
@section('content')
    <div id="receipt">
        <div class="container">
            <h1 class="text-center">Print Receipt</h1>
            <form class="form-horizontal" action="{{ url()->to('receipt/send') }}" method="post" role="form">
                <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">

                <div class="head-text">Step 1: Scan Ticket</div>
                <div id="input-barcode">
                    <section class="content">
                      <span class="input input--jiro">
                            <input class="input__field input__field--jiro" type="text" id="barcode" name="barcode" autofocus />
                            <label class="input__label input__label--jiro" for="barcode">
                              <span class="input__label-content input__label-content--jiro">Barcode</span>
                            </label>

                      </span>
                    </section>
                </div>

                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="col-sm-1 text-center">Select</th>
                            <th class="col-sm-2 text-center"><div class="text-center">Date</div></th>
                            <th class="col-sm-2 text-left">Order Number</th>
                            <th class="col-sm-2 text-right">Total Price</th>
                            <th class="col-sm-1 text-center"></th>
                        </tr>
                    </thead>
                    <tbody class="orders-list">
                        <tr class="default-list">
                            <td colspan="5" class="text-center">Scan Ticket First</td>
                        </tr>
                    </tbody>
                </table>


                <div class="head-text">Step 2: Fill information</div>
                <div class="row input-info">
                    <div class="col-md-6">
                        <div class="input-send">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="error error-type">Please select type</div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="print_type" name="print_type" value="1"> SMS &nbsp;
                                        </label>
                                        <label>
                                            <input placeholder="Mobile Number" type="text" class="input-block mobile" name="mobile" maxlength="10" autocomplete="off" disabled>
                                        </label>
                                        <div class="error error-mobile">Please input mobile number</div>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="print_type" name="print_type" value="2"> Email
                                        </label>
                                        <label>
                                            <input placeholder="example@email.com" type="text" class="input-block email" name="email" autocomplete="off" disabled>
                                        </label>
                                        <div class="error error-email">Please input email address</div>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="print_type" name="print_type" value="3"> Paper
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        {{--<div class="text">Or New Address</div>--}}
                        <div class="input-address">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input placeholder="Company Name" type="text" class="input-block" id="" name="company_name" autocomplete="off" value="บริษัท บางกอกโซลูชั่น จำกัด">
                                    <div class="error">Please input company name</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input placeholder="Branch" type="text" class="input-block" id="" name="branch" value="สำนักงานใหญ่">
                                </div>
                            </div>
                            <div class="form-group" >
                                <div class="col-md-12">
                                    <textarea name="address" placeholder="Address" class="input-block" rows="5">{!! "92/46-47 สาธรธานี 2 ชั้น 17 \nถนนสาทรเหนือ แขวงสีลม เขตบางรัก \nกรุงเทพฯ 10500" !!}
                                    </textarea>
                                    <div class="error">Please input address</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input placeholder="Tax ID." type="text" class="input-block" id="" name="tax_id" autocomplete="off" value="16100300027">
                                    <div class="error">Please input tax id.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="submit text-right">
                    <a href="#" class="btn-submit">Print Receipt</a>
                    <a class="btn-cancel" href="{{ url()->to('receipt') }}">Cancel</a>
                </div>
            </form>
        </div>

    </div>
@endsection

@section('script')
    {!! HTML::script('js/frontend/receipt.js') !!}
@endsection
