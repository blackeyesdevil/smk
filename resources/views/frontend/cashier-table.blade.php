<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
{!! Html::style('https://fonts.googleapis.com/css?family=Open+Sans:400,300,700') !!}
{!! Html::style('fonts/foundation-icons/foundation-icons.css') !!}
{!! Html::style('css/frontend/cashier-table2.css') !!}
{!! Html::style('css/frontend/report-print.css') !!}
{!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
<script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
{!! Html::script('assets/global/scripts/jquery.min.js') !!}
{!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
</head>

<body>
        <div class="container-fluid">
            <div id="btn-type" class="col-sm-12">
                <a href="{{url()->to('cashier/grab-go')}}"><div class="btn-type" >GRAB & GO</div></a>
                <a href="{{url()->to('cashier/cashier-table')}}"><div class="btn-type" style="background:#C19F79;">CASHIER</div></a>
                <a href="{{url()->to('cashier/customer-service')}}"><div class="btn-type" style="width: 250px;">CUSTOMER-SERVICE</div></a>
                <div id="report" class="btn-type">PRINT REPORT</div>
            </div>
                <div id="table" class= "col-sm-6">
                    <div id="table-order" class="col-sm-12">
                        <table>
                            <thead>
                                <tr>
                          	        <th class="col-sm-4">Order No.</th>
                          	        <th class="col-sm-4">Table No.</th>
                          	        <th class="col-sm-4">Amount</th>

                                </tr>
                            </thead>
                            <tbody id="table-order-tb">
                            </tbody>
                        </table>
                    </div>
                    <div id="table-payment" class="col-sm-12 inline">
                        <div class="table-payment-text col-sm-12">
                            <div class="col-sm-2">
                                Order No. : <span></span>
                            </div>
                            <div class="col-sm-2">
                                Table No. : <span></span>
                            </div>
                            <div class="btn-payment">
                                <div class="btn-discount">Viz card</div>
                                <div class="btn-discount">Discount Code</div>
                            </div>
                        </div>


                        <table>
                            <thead>
                                <tr>
                                    <th class="col-sm-4">Payment By</th>
                                    <th class="col-sm-4">Amount<span></span></th>
                                    <th class="col-sm-4"></th>
                                </tr>
                            </thead>
                            <tbody id="table-payment-tb">

                            </tbody>
                        </table>
                        <div id="paid-left" class="col-sm-12">
                            <div class="paid-text">
                                <div id="paid-text-p" class="col-sm-6">Paid :<span id="paid-text-p-input">-</span></div>
                                <div id="paid-text-l" class="col-sm-6">Left :<span id="paid-text-l-input">-</span></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="pay" class="col-sm-6">
                    <div id="head" class="col-sm-12">
                        <div class="head-pay"><span id="head-pay-text">0.00</span> THB</div>
                        <div class="change-pay">Change :<span id="change-pay-text">00.00</span> THB</div>
                        <div id="button-etc">

                            <table class="col-sm-12" id="payment-method">
                                <tr>
                                    <td class="col-sm-4">Cash</td>
                                    <td class="col-sm-4">Credit Card</td>
                                    <td class="col-sm-4">Siam Gift Card</td>
                                </tr>
                                <tr>
                                    <td class="col-sm-4" style="background-color:#FFF;"></td>
                                    <td class="col-sm-4">Rabbit Card</td>
                                    <td class="col-sm-4">Other</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="bottom" class="col-sm-12">
                        <div class="input-pay col-sm-12">
                            <div id="pay-text">0</div>
                        </div>
                        <div id="bank" class="col-sm-4">
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b1000.jpg')}}" alt="" id="b1000"/></div>
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b500.jpg')}}" alt="" id="b500"/></div>
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b100.jpg')}}" alt="" id="b100"/></div>
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b50.jpg')}}" alt="" id="b50"/></div>
                            <div class="bank-image"> <img src="{{URL::asset('images/cashier/b20.jpg')}}" alt="" id="b20"/></div>
                        </div>
                        <div id="btn-number" class="col-sm-8">
                            <table>
                                <tr>
                                    <td class="col-sm-3">7</td><td class="col-sm-3">8</td><td class="col-sm-3">9</td><td class="col-sm-3">C</td>
                                </tr>
                                <tr>
                                    <td class="col-sm-3">4</td><td class="col-sm-3">5</td><td class="col-sm-3">6</td>
                                    <td class="col-sm-3" id="del">
                                        <image src="{{URL::asset('images/cashier/del.svg')}}" alt="" fill="red"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-sm-3">1</td><td class="col-sm-3">2</td><td class="col-sm-3">3</td><td rowspan="2"class="col-sm-3 enter" style="font-size:20px;">ENTER</td>
                                </tr>
                                <tr>
                                    <td class="col-sm-3">00</td><td class="col-sm-3">0</td><td class="col-sm-3">.</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
        </div>
        <div id="cover"></div>
        <div id="report-box">
            <div class="close"><img src="{{URL::asset('images/close.png')}}" alt=""></div>
            <div class="report-head">PRINT REPORT</div>
            <div class="btn-print-report">
                <div class="btn-report" id="eod_till">END OF DAY </div></a>
                <div class="btn-report" id="eod">END OF DAY (ALL TILL)</div></a>
                <div class="btn-report" id="z_out">Z-OUT</div></a>
                <div class="btn-report" id="transaction">TRANSACTION</div>
                <div class="btn-report" id="abb">ABB (ALL TILL)</div>
            </div>

        </div>
        <!-- <div id="print-report-z-out">
            <div class="print-head">PRINT PREVIEW</div>
            <div class="report-div">

            </div>
            <div class="report-paper"> -->
                {{-- @include('frontend.report.z-out-print') --}}
            <!-- </div>
            <div id="btn-print">
                <div class="btn-print">PRINT</div>
                <div id="back-print" class="btn-print">BACK</div>
            </div>
        </div> -->

<!-- {!! Html::script('js/frontend/cashier-table2.js') !!} -->
{!! Html::script('js/frontend/cashier-table3.js') !!}
{!! Html::script('js/frontend/report.js') !!}
<script>
    var api_url = '{{ URL::to("api") }}';
    $('#report').on('click', function(){
        $('#cover').fadeIn(200);
        $('#report-box').fadeIn(200);
    });
    $('.close').on('click', function(){
        $('#report-box').fadeOut(200);
        $('#cover').fadeOut(200);
    });
    $('#cover').on('click', function(){
        $('#report-box').fadeOut(200);
        $('#cover').fadeOut(200);
        $('#print-report-z-out').fadeOut(200);
    });
    // $('#z-out').on('click', function(){
    //     $('#cover').fadeIn(200);
    //     $('#print-report-z-out').fadeIn(200);
    // });
    // $('#back-print').on('click', function(){
    //     $('#print-report-z-out').fadeOut(200);
    //     $('#cover').fadeOut(200);
    // });
</script>
</body>
</html>
