@extends('frontend.layout.main-layout')

@section('content')
    <form id="form">
        <input type="text" name="nfc_code" value="{{ Input::get('code') }}" placeholder="NFC Code">
    </form>
@endsection

@section('script')
@endsection
