    <div id="content-special-set">
        <div class="container-fluid">
            <div class="step col-xs-12">
                Step 1
            </div>
            <div class="step-text col-xs-12">
                Choose 1  Appetizer or Salad
            </div>
            <div id="product-bar" class="col-xs-12">
                <div class="product-list col-xs-12">
                    <div class="product-item">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                    <div class="product-item col-xs-3">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="step col-xs-12">
                Step 2
            </div>
            <div class="step-text col-xs-12">
                Choose 1  Drink
            </div>
            <div id="product-bar" class="col-xs-12">
                <div class="product-list col-xs-12">
                    <div class="product-item">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                    <div class="product-item col-xs-3">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="step col-xs-12">
                Step 3
            </div>
            <div class="step-text col-xs-12">
                Choose 1  Dessert
            </div>
            <div id="product-bar" class="col-xs-12">
                <div class="product-list col-xs-12">
                    <div class="product-item">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                    <div class="product-item col-xs-3">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="product-img">
                            <img src="{{URL::asset('images/product-test.png')}}" alt="">
                        </div>
                        <div class="product-info-set">
                            <div class="product-text">
                                FIRST LOVE
                            </div>
                            <div class="product-text">
                                250 THB
                            </div>
                            <div class="product-price-old">
                                285 THB
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="total-text col-xs-12">
                <div class="sub-total">
                    Subtotal : &nbsp;&nbsp;&nbsp;580 THB
                </div>
                <div class="discount">
                    Discount : &nbsp;&nbsp;&nbsp;-100 THB
                </div>
                <div class="total">
                    Total : &nbsp;&nbsp;&nbsp;480 THB
                </div>
            </div>
            <div id="add-btn" class="col-xs-12">

                <div class="add-text">
                    <img src="{{URL::asset('images/add-to-cart.png')}}" alt="">
                    ADD TO CART
                </div>
            </div>
        </div>
    </div>
