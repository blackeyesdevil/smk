
<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')
 {{ Html::style('assets/global/plugins/select2/select2.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
    {{ Html::style('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Member Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="member_name" value="{{$member_name}}">
                            </div>
                        </div>
                    </div>
                   <div class="form-group">
                        <label class="control-label col-md-3">Birthday</label>
                        <div class="col-md-3">
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" class="form-control" name="birthday" @if($birthday!='0000-00-00') value="{{ $birthday }}" @endif>
                            <span class="input-group-btn">
                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Mobile</label>
                            <div class="col-md-4">
                            {{--<input type="text" class="form-control" name="mobile" value="{{ $mobile }}" onKeyUp="IsNumeric(this.value,this)">--}}
                                <input type="text" class="form-control" name="mobile" value="{{$mobile}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="email">
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Customer Preference</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="member_preference" value="{{$member_preference}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Customer Allergic</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="member_allergic" value="{{$member_allergic}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Company Name Tenant</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="member_name_tenant" value="{{$member_name_tenant}}">
                            </div>
                        </div>
                    </div>
                   <div class="form-group">
                        <label class="control-label col-md-3">Expired Date</label>
                        <div class="col-md-3">
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" class="form-control" name="expired_date" @if($expired_date!='0000-00-00') value="{{ $expired_date }}" @endif>
                            <span class="input-group-btn">
                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">VIZ Card Member ID</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="viz_cardmember_id" value="{{$viz_cardmember_id}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Tenant Employee ID</label>
                            <div class="col-md-4">
                                    <select name="tenant_person_id" class="form-control" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}">
                                        <option value="">Select...</option>
                                        @foreach($tenant_person as $field1)
                                            <option value="{{$field1->tenant_person_id}}" @if($tenant_person_id == $field1->tenant_person_id) selected @endif >{{$field1->firstname_th}}</option>
                                        @endforeach
                                    </select>
                               </div>
                        </div>
                    </div>
                    
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
{{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}
    {{ Html::script('assets/global/plugins/ckeditor/ckeditor.js') }}


    {{ Html::script('js/backend/theme_component/components-pickers.js') }}
    {{ Html::script('js/backend/theme_component/components-form-tools.js') }}
    {{ Html::script('js/backend/theme_component/components-form-tools2.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/role/validation.js') }}
    {{ Html::script('js/backend/member/validation.js') }}
    <script>
        FormValidation.init();
        ComponentsPickers.init();
    </script>
@endsection

