<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->

                @if(!empty(session()->get('alertMsg')))
                <div class="alert alert-success">
                    <strong>{{ session()->get('alertMsg') }}</strong>
                </div>
                @endif
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Store Name (EN)</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="store_name_en" value="{{ $store_name_en }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Store Name (TH)</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="store_name_th" value="{{ $store_name_th }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Company Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="company_name" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Branch Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="branch" value="{{ $branch }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Address</label>
                            <div class="col-md-4">
                                <textarea type="text" class="form-control" name="address" " style="height:150px;">{{ $address }}</textarea>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3">Phone</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="phone" value="{{ $phone}}">
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3">Fax</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="fax" value="{{ $fax }}">
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="email" value="{{ $email }}">
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3">Website</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="website" value="">
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3">Minimum Sales Target</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="rent" aria-describedby="basic-addon2" value="{{$rent}}">
                                    <span class="input-group-addon" id="basic-addon2">( บาท )</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">GP</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="gp_type" aria-describedby="basic-addon2" value="{{$gp_type}}">
                                    <span class="input-group-addon" id="basic-addon2">( % )</span>
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="control-label col-md-3">Company Registration Number</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="company_regis_number" value="{{ $company_regis_number }}">
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="control-label col-md-3">Company's TAX ID</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="company_taxid" value="{{ $company_taxid}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Passcode</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="passcode" minlength="4" value="{{ $passcode }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}

    {{ Html::script('assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/store/validation.js') }}
    <script>
        FormValidation.init();
        ComponentsFormTools.init();
    </script>
@endsection
