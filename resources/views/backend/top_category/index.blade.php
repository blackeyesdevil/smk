<?php
use Illuminate\Support\Facades\Input;

$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('backend.layout.main-layout')

@section('page-style')
    {{ Html::style('assets/global/plugins/select2/select2.css') }}
    {{ Html::style('assets/global/css/plugins.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
    {{ Html::style('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            @if(empty(Input::get('search')))
                <div class="form-search">
                    <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                        <input type="hidden" class="form-control" name="search" value="true">
                        <input type="hidden" class="form-control" name="date" value="{{ Input::get('date') }}">
                        <div class="form-group">
                            <label class="control-label col-md-1">Search</label>
                            <div class="col-md-5">
                                <div class="input-group input-daterange col-md-12" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control form_sql_datetime" name="from_date" value="{{ Input::get('from_date') }}">
                                    <span class="input-group-addon"> to </span>
                                    <input type="text" class="form-control form_sql_datetime" name="to_date" value="{{ Input::get('to_date') }}">
                                </div>
                                <span class="help-block">Select date range </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-1">Order by</label>
                            <div class="col-md-3">
                                <select class="form-control" name="search">
                                    <!-- <option value="">Select..</option> -->
                                    <option value="qty" @if(Input::get('search')=='qty') selected @endif >QTY</option>
                                    <option value="price" @if(Input::get('search')=='price') selected @endif >AMOUNT</option>
                                </select>
                                <span class="help-block">Select order by </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-1 col-md-3 ">
                                <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            @else
                <div class="portlet-title">
                    <div class="text-right">
                        <div>
                            <a href="{{URL::to($path.'?mode=print'.$str_param)}}" target="_blank"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-print"></i> Print</button></a>
                            <a href="{{URL::to($path.'?mode=export'.$str_param)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel</button></a>
                        </div>
                    </div>
                </div>
                <div class="portlet-title" align="center">
                        <!-- <span class="caption-subject font-green-sharp bold">{{ $page_title }}</span><br> -->
                        <span class="caption-subject font-green-sharp bold">From {{ $from }} Through {{ $through }}</span><br>
                        <span class="caption-subject font-green-sharp bold">Report was run on {{ date('F d, Y H:i') }}</span>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">No.</th>
                                <th class="col-md-3">Category</th>
                                <th class="text-center">QTY</th>
                                <th class="text-right col-md-2">Net Sales</th>
                                <th class="text-right col-md-2">Cost</th>
                                <th class="text-right col-md-2">Profit</th>
                                <th class="text-center col-md-2">Profit Ratio</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if($count_data > 0)
                                    <?php
                                        $count=1;
                                        $all_qty = 0;
                                        $all_net = 0;
                                        $all_cost = 0;
                                        $all_profit = 0;
                                    ?>
                                    @foreach($data as $key => $field)
                                        <?php
                                            $all_qty += $field->sum_qty;
                                            $all_net += $field->net_sales;
                                            $all_cost += $field->cost;
                                            $all_profit += $field->profit;
                                        ?>
                                        <tr>
                                            <td class="text-center">{{ $key+1 }}</td>
                                            <td>
                                            @foreach($category as $field1)
                                                @if($field1->category_id == $field->category_id)
                                                    {{$field1->category_name_en}}
                                                @endif
                                            @endforeach
                                            </td>
                                            <td class="text-center"> {{ number_format($field->sum_qty) }} </td> <!-- sum qty -->
                                            <td class="text-right">{{ number_format($field->net_sales,2) }}</td> <!-- Net Sales  -->
                                            <td class="text-right">{{ number_format($field->cost,2) }}</td> <!-- Cost  -->
                                            <td class="text-right">{{ number_format($field->profit,2) }}</td> <!-- Profit  -->
                                            <td class="text-center">{{ number_format($field->profit_ratio) }}%</td> <!-- Profit Ratio  -->
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <th colspan='2'>Total</th>
                                        <th class="text-center">{{ $all_qty }}</th>
                                        <th class="text-right">{{ $all_net }}</th>
                                        <th class="text-right">{{ $all_cost }}</th>
                                        <th class="text-right">{{ $all_profit }}</th>
                                    </tr>
                                @else
                                    <tr>
                                        <td class="text-center" colspan="7">No Result.</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            @endif
        </div>
    </div>
@endsection

@section('page-plugin')
    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}
    {{ Html::script('assets/global/plugins/ckeditor/ckeditor.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/theme_component/components-pickers.js') }}
    {{ Html::script('js/backend/theme_component/components-form-tools.js') }}
    {{ Html::script('js/backend/theme_component/components-form-tools2.js') }}
    <script>
        ComponentsPickers.init();
    </script>
    <script type="text/javascript">
        $(".form_sql_datetime").datetimepicker({
            isRTL: Metronic.isRTL(),
            format: "yyyy-mm-dd hh:ii:ss",
            autoclose: true,
            todayBtn: true,
            startDate: "2013-02-14 10:00:00",
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
            minuteStep: 10
        });
    </script>
@endsection
