@extends('backend.layout.main-layout')

@section('page-style')
@endsection
@section('more-style')
    <style media="screen">
        .thb {
            font-size: 20px;
        }
    </style>
@endsection

@section('page-title','Dashboard')

@section('page-content')
    <div class="col-md-12">

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- SUMMARY BLOCK -->

                <!-- All Stores -->
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="dashboard-stat1 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-green-sharp bold">
                                        <span data-counter="counterup" data-value="">All Stores</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><!-- Monthly -->
                        <div class="dashboard-stat2">
                            <div class="display">
                                <div class="number col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                    <h4 class="font-blue-sharp bold">Monthly Overview</h4>
                                </div>
                                <div class="icon">
                                    <i class="icon-basket"></i>
                                </div>
                                <div class="number col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <small class="bold">Total Orders</small>
                                    <h3 class="font-blue-sharp">
                                        <span data-counter="counterup" data-value="">{{ number_format($monthly_data['all_stores']['count']) }}</span>
                                    </h3>
                                    <small>THB {{ number_format($monthly_data['all_stores']['total_price'], 2) }}</small><br>
                                    <small>&nbsp;</small>
                                </div>
                                <div class="number col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <small class="bold">Average Order Size</small>
                                    <h3 class="font-blue-sharp">
                                        <span data-counter="counterup" data-value=""><span class="thb">THB </span>{{ number_format($monthly_data['all_stores']['aos'], 2) }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><!-- Today -->
                        <div class="dashboard-stat2">
                            <div class="display">
                                <div class="number col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                    <h4 class="font-purple-sharp bold">Today Overview</h4>
                                </div>
                                <div class="icon">
                                    <i class="icon-pie-chart"></i>
                                </div>
                                <div class="number col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <small class="bold">Total Orders</small>
                                    <h3 class="font-purple-sharp">
                                        <span data-counter="counterup" data-value="">{{ number_format($today_data['all_stores']['count']) }}</span>
                                    </h3>
                                    <small>THB {{ number_format($today_data['all_stores']['total_price'], 2) }}</small><br>
                                    <small>&nbsp;</small>
                                </div>
                                <div class="number col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <small class="bold">Average Order Size</small>
                                    <h3 class="font-purple-sharp">
                                        <span data-counter="counterup" data-value=""><span class="thb">THB </span>{{ number_format($today_data['all_stores']['aos'], 2) }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Each Stores -->
                @foreach($stores as $key => $field)
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="dashboard-stat1 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-sharp bold">
                                            <span data-counter="counterup" data-value="">{{ $field->store_name_en }}</span>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="dashboard-stat2" style="width: 100%; height: 253px;"><!-- Logo -->
                                <div class="display"  style="width: 100%; height: 100%; background: url('{{ URL::asset('images/'.$field->store_logo_normal) }}') no-repeat center;">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"><!-- Monthly -->
                            <div class="dashboard-stat2">
                                <div class="display" style="magin: 0px">
                                    <div class="number col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <h4 class="font-blue-sharp bold">Monthly Overview</h4>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-basket"></i>
                                    </div>
                                    <div class="number col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <small class="bold">Total Orders</small>
                                        <h3 class="font-blue-sharp">
                                            <span data-counter="counterup" data-value="">{{ number_format($monthly_data[$field->store_name_en]['count']) }}</span>
                                        </h3>
                                        <small>THB {{ number_format($monthly_data[$field->store_name_en]['total_price'], 2) }}</small><br>
                                        <small>&nbsp;</small>
                                    </div>
                                    <div class="number col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <small class="bold">Average Order Size</small>
                                        <h3 class="font-blue-sharp">
                                            <span data-counter="counterup" data-value=""><span class="thb">THB </span>{{ number_format($monthly_data[$field->store_name_en]['aos'], 2) }}</span>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"><!-- Today -->
                            <div class="dashboard-stat2">
                                <div class="display" style="magin: 0px">
                                    <div class="number col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <h4 class="font-purple-sharp bold">Today Overview</h4>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                    <div class="number col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <small class="bold">Total Orders</small>
                                        <h3 class="font-purple-sharp">
                                            <span data-counter="counterup" data-value="">{{ number_format($today_data[$field->store_name_en]['count']) }}</span>
                                        </h3>
                                        <small>THB {{ number_format($today_data[$field->store_name_en]['total_price'], 2) }}</small><br>
                                        <small>&nbsp;</small>
                                    </div>
                                    <div class="number col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <small class="bold">Average Order Size</small>
                                        <h3 class="font-purple-sharp">
                                            <span data-counter="counterup" data-value=""><span class="thb">THB </span>{{ number_format($today_data[$field->store_name_en]['aos'], 2) }}</span>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach




            </div>
        </div>
    </div>

@endsection

@section('page-plugin')
@endsection
@section('more-script')
@endsection
