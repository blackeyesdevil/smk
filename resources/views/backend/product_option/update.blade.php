<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
    <a href="{{ url()->to('_admin/product_option?product_id='.Input::get('product_id')) }}">{{ $txt_manage.' '.$page_title }}</a>  >> {{ $product_name->product_name_th }} @if(!empty($product_name->product_name_en)) ({{ $product_name->product_name_en }}) @endif
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <input type="hidden" name="product_id" value="{{ input::get('product_id') }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Product</label>
                            <div class="col-md-3">
                                <span class="btn btn-xs btn blue">{{ $product_name->product_name_th }}</span>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Type</label>
                            <div class="col-md-3">
                                <select name="type" class="form-control" >
                                    <option value="r" @if($type=='r') selected @endif>Radio</option>
                                    <option value="c" @if($type=='c') selected @endif>Checkbox</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Label Name</label>
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="label_th" value="{{ $label_th }}" required>
                            </div>
                            <div class="col-md-1">
                                ( TH )
                            </div>

                            <div class="col-md-3">
                                <input type="text" class="form-control" name="label_en" value="{{ $label_en }}">
                            </div>

                            <div class="col-md-1">
                                ( EN )
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Value</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="value_th" value="{{ $value_th }}" required>
                            </div>
                            <div class="col-md-1">
                                ( TH )
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Value</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="value_en" value="{{ $value_en }}" required>
                            </div>
                            <div class="col-md-1">
                                ( EN )
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-7">
                                <span style="color:#F00">* item(:20:),item2(:50d:)</span><br>
                                <span style="color:#F00">* d=default</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Tags</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="tags" value="{{ $tags }}" min="0">
                                <span style="color:#F00">* item1,item2</span>
                            </div>
                        </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">

                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/product_option/validation.js') }}
    <script>
        FormValidation.init();
    </script>
@endsection
