<?php
use Illuminate\Support\Facades\Input;
use App\Library\MainFunction;
use App\Models\Orders;

$mainFn = new MainFunction(); // New Object Main Function
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;

if (Input::get('mode') == 'export') {
    $filename = "excel_report_date.xls";
    header("Content-Type: application/vnd.ms-excel; charset=utf-8");
    header("Content-Disposition: attachment; filename=".$filename);
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);
}

?>
<html>
<head>
    <meta charset="UTF-8">
    <style media="screen">
        table {
            border-collapse: collapse;
            width: 100%;
            table-layout: fixed;
        }
        th,td {
            border: 1px solid black;
            padding: 1px 3px;
            /*table-layout: fixed;*/
            /*white-space: nowrap;*/
            /*overflow: hidden;*/
            /*text-overflow:ellipsis;*/
        }

        body {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        * {
            font-size: 12px;
            /*font-family: "THSarabunNew"; */
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }
        .page {
            width: 210mm;
            min-height: 297mm;
            padding: 5mm 5mm 5mm 5mm;
            margin: 10mm auto;
            border: 1px #D3D3D3 solid;
            border-radius: 5px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        }
        .subpage {
            padding: 0cm;
            height: 287mm;
        }
        .content {
            width: 100%;
            height: 100%;
        }
        .header_report {
            width: 100%;
        }
        @page {
            size: A4;
            margin: 0;
        }
        @media print {
            html, body {
                width: 210mm;
                height: 297mm;
            }
            .page {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        }
        @media print{
            .content{
                page-break-before: always;
            }

        }
    </style>
</head>
<!-- <body onload="window.print()"> -->
<body onload="">
    <div class = "book">
        @if($count_data > 0)
            <?php $count=1; ?>
            <?php
                $all_count_order = 0;
                $all_qty = 0;
                $all_total = 0;
                $all_discount = 0;
            ?>
            @foreach($data as $field)
                <?php
                    $sum_discount = Orders::where('status','=','1')
                                            ->where('created_at','like',$field->date.' %:%:%')
                                            ->sum('discount_price');

                    $all_count_order += $field->count_order;
                    $all_qty += $field->sum_qty;
                    $all_total += $field->sum_total;
                    $all_discount += $sum_discount;
                ?>
                @if ($count==1)
                    <div class="page">
                        <div class="subpage">
                            <div class="content">
                                @include('backend.include.report_header')
                                <table>
                                    <tr>
                                        <th align="left" width="10%">Date</th>
                                        <th align="center" width="16.5%">Number of Bill</th>
                                        <th align="center" width="16.5%">Quantity</th>
                                        <th align="right" width="19%">Gross Sales</th>
                                        <th align="right" width="19%">Discount</th>
                                        <th align="right" width="19%">Net Sales</th>
                                    </tr>
                @endif
                                    <tr>
                                        <td align="left">{{ $field->date }}</td>
                                        <td align="center">{{ number_format($field->count_order) }}</td>
                                        <td align="center">{{ number_format($field->sum_qty) }}</td>
                                        <td align="right">{{ number_format($field->sum_total,2) }}</td>
                                        <td align="right">{{ number_format($sum_discount,2) }}</td>
                                        <td align="right">{{ number_format($field->sum_total - $sum_discount,2) }}</td>
                                    </tr>
                @if ($count==30)
                                </table>
                            </div><!--end content-->
                        </div><!--end subpage-->
                    </div><!--end page-->
                    <?php $count=0; ?>
                @endif
                <?php $count++; ?>
            @endforeach
            @if ($count==1)
                <div class="page">
                    <div class="subpage">
                        <div class="content">
                            @include('backend.include.report_header')
                            <table>
                                <tr>
                                    <th align="left" width="10%">Date</th>
                                    <th align="center" width="16.5%">Number of Bill</th>
                                    <th align="center" width="16.5%">Quantity</th>
                                    <th align="right" width="19%">Gross Sales</th>
                                    <th align="right" width="19%">Discount</th>
                                    <th align="right" width="19%">Net Sales</th>
                                </tr>
            @endif
                                <tr>
                                    <th align="left">Total</th>
                                    <th align="center">{{ number_format($all_count_order) }}</th>
                                    <th align="center">{{ number_format($all_qty) }}</th>
                                    <th align="right">{{ number_format($all_total,2) }}</th>
                                    <th align="right">{{ number_format($all_discount,2) }}</th>
                                    <th align="right">{{ number_format($all_total - $all_discount,2) }}</th>
                                </tr>
            @if ($count==30)
                            </table>
                        </div><!--end content-->
                    </div><!--end subpage-->
                </div><!--end page-->
                <?php $count=0; ?>
            @endif
            <?php $count++; ?>
        @else
            <div class="page">
                <div class="subpage">
                    <div class="content">
                        @include('backend.include.report_header')
                        <table>
                            <tr>
                                <th align="left" width="10%">Date</th>
                                <th align="center" width="16.5%">Number of Bill</th>
                                <th align="center" width="16.5%">Quantity</th>
                                <th align="right" width="19%">Gross Sales</th>
                                <th align="right" width="19%">Discount</th>
                                <th align="right" width="19%">Net Sales</th>
                            </tr>
                            <tr>
                                <td align="center" colspan='6'>No Result.</td>
                            </tr>
                        </table>
                    </div><!--end content-->
                </div><!--end subpage-->
            </div><!--end page-->
        @endif
    </div><!--end book-->
</body>
</html>
