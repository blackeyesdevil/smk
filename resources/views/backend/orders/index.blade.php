<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');
$STATUS = Config::get('constants.STATUS');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);
$credit_color = '';
$cash_color = '';

?>
@extends('backend.layout.main-layout')

@section('page-style')
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="form-search">
                <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                    <div class="form-group">
                        <label class="control-label col-md-1">Search</label>
                        <div class="col-md-3">
                            <input class="form-control" type="text" name="search" value="{{ Input::get('search') }}">
                            <span class="help-block">Search by Table No.</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-3 ">
                            <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                </form>
                <hr>
            </div>

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold">Found {{ $count_data }} Record(s).</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('ID',$primaryKey,$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('Date, Time','created_at',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('Order No.','orders_no',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('Table No.','table_no',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('C.Name.','member_name',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('Amount','total_price',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('Payment Status','payment',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('Status','status',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="col-sm-1"></th>
                            <th class="col-sm-2"></th>
                            <th class="text-center col-sm-1 col-md-1"></th>
                        </tr>
                        </thead>
                        <tbody>
                            @if($count_data > 0)
                                @foreach($data as $key => $field)
                                    <tr>
                                        <td class="text-center">{{ $field->$primaryKey }}</td>
                                        <td class="text-center">{{ $obj_fn->format_date_en($field->created_at,4) }}</td>
                                        <td class="text-center">{{ $field->orders_no }}</td>
                                        <td class="text-center"><li class="btn btn-circle green btn-xs"> {{ $field->table_no }} </li></td>
                                        <td class="text-center">{{ $field->member_name }}</td>
                                        <td class="text-center">{{ number_format($field->total_price) }}</td>

                                        <td>
                                            <?php
                                            if($field->status == 1){
                                                $credit_color = "green";
                                                $cash_color = "green";
                                            }else{
                                                $credit_color = "red";
                                                $cash_color = "red";
                                            }
                                            ?>
                                            @if($field->payment==1)
                                                <i class="btn btn {{ $credit_color }} btn-xs"><i class="fa fa-credit-card"></i> Credit Card</i>
                                                <p>{{ $STATUS[$field->status] }}</p>
                                            @else
                                                <i class="btn btn {{ $cash_color }} btn-xs"><i class="fa fa-money"></i> Cash</i>
                                                <p>{{ $STATUS[$field->status] }}</p>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            {{ $field->StatusOrder($field->$primaryKey) }}
                                        </td>

                                        <td class="text-center"><a href="{{ url()->to($path."/".$primaryKey."=".$field->$primaryKey) }}" class="btn btn-circle blue btn-xs"><i class="fa fa-search"></i> Payment Log</a></td>

                                        <td class="text-center"><a href="{{ url()->to('_admin/orders_detail/create?'.$primaryKey."=".$field->$primaryKey) }}" class="btn btn-circle blue btn-xs"><i class="fa fa-search"></i> Order Detail</a></td>
                                        @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                            @if($permission['d'] == '1')
                                        <td class="text-center">
                                            <form action="{{ url()->to($path.'/'.$field->$primaryKey) }}" class="form-delete" parent-data-id="{{ $field->$primaryKey }}" method="POST" >
                                                <input type="hidden" name="_method" value="delete">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="button" class="btn btn-xs btn-circle red btn-delete" data-id="{{ $field->$primaryKey }}"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>
                                            @endif
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="9">No Result.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {!! $data->appends(Input::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-plugin')
@endsection
@section('more-script')
@endsection
