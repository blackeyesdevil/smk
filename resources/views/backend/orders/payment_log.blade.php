<?php
$table = $obj_model2->table;
$primaryKey = $obj_model2->primaryKey;
$fillable = $obj_model2->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);
?>
@extends('backend.layout.main-layout')

@section('page-style')
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">

            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('ID',$primaryKey,$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-4">{!! $obj_fn->sorting('Payment','payment',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-4">{!! $obj_fn->sorting('Price','price',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="text-center col-sm-4">{!! $obj_fn->sorting('Payment_other','payment_other',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if($count_data > 0)
                                @foreach($data as $key => $field)
                                    <tr>
                                        <td class="text-center">{{ $field->$primaryKey }}</td>
                                        <td class="text-center">{{ $field->payment }}</td>
                                        <td class="text-center">{{ $field->price }}</td>
                                        <td class="text-center">{{ $field->payment_other }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="9">No Result.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {!! $data->appends(Input::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-plugin')
@endsection
@section('more-script')
@endsection
