<?php
use Illuminate\Support\Facades\Input;
use App\Library\MainFunction;
use App\Models\Orders;

$mainFn = new MainFunction(); // New Object Main Function
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;

if (Input::get('mode') == 'export') {
    $filename = "excel_top_selling_category.xls";
    header("Content-Type: application/vnd.ms-excel; charset=utf-8");
    header("Content-Disposition: attachment; filename=".$filename);
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);
}

?>
<html>
<head>
    <meta charset="UTF-8">
    <style type="text/css">
        table {
            border-collapse: collapse;
            width: 100%;
            table-layout: fixed;
        }
        th,td {
            border: 1px solid black;
            padding: 1px 3px;
            table-layout: fixed;
            white-space: nowrap;
            overflow: hidden;
            text-overflow:ellipsis;
        }
        body {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        * {
            font-size: 12px;
            /*font-family: "THSarabunNew"; */
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }
        .page {
            width: 210mm;
            min-height: 297mm;
            padding:10mm 10mm 10mm 10mm;
            margin: 10mm auto;
            border: 1px #D3D3D3 solid;
            border-radius: 5px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        }
        .subpage {
            padding: 0cm;
            height: 277mm;
        }
        .content {
            width: 100%;
            height: 100%;
        }.header_report {
            width: 100%;
        }
        @page {
            size: A4;
            margin: 0;
        }
        @media print {
            html, body {
                width: 210mm;
                height: 297mm;
            }
            .page {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        }
        @media print{
            .content{
                page-break-before: always;
            }
        }
    </style>
</head>
<body onload="window.print()">
    <div class = "book">
        <div class="page">
            <div class="subpage">
                <div class="content">
                    @include('backend.include.report_header')
                    <br>
                    @if(count($a_data) > 0)
                        <?php
                            $all_qty = 0;
                            $all_net = 0;
                            $all_cost = 0;
                            $all_profit = 0;
                        ?>
                        @foreach($a_data as $category_id => $data)
                            <?php $category_name_en = DB::table('category')->where('category_id','=',$category_id)->first()->category_name_en; ?>
                            @if($a_count_data[$category_id] > 0)
                                <?php
                                    $all_qty += $field->sum_qty;
                                    $all_net += $field->net_sales;
                                    $all_cost += $field->cost;
                                    $all_profit += $field->profit;
                                ?>
                                <div class="text-head" align="center">อาหารหมวด {{ $category_name_en }} </div>
                                <div class="text-head" align="center">ราการอาหารที่มียอดขายสูงสุด 10 อันดับ <!-- คือ ... มี x Orders เป็นรายได้ xx บาท --></div>
                                <table>
                                    <tr>
                                        <th colspan='1'>No.</th>
                                        <th align='left' colspan='3'>Product</th>
                                        <th colspan='1'>Quantity</th>
                                        <th align='right' colspan='2'>Net Sales</th>
                                        <th align='right' colspan='2'>Cost</th>
                                        <th align='right' colspan='2'>Profit</th>
                                        <th colspan='2'>Profit Ratio</th>
                                    </tr>
                                    @foreach($data as $key => $field)
                                        <tr>
                                            <td align='center' colspan='1'>{{ $key+1 }}</td>
                                            <td align='left' colspan='3'>{{$field->product_name_en}}</td>
                                            <td align='center' colspan='1'>{{ number_format($field->sum_qty) }}</td>
                                            <td align='right' colspan='2'>{{ number_format($field->net_sales,2) }}</td> <!-- Net Sales  -->
                                            <td align='right' colspan='2'>{{ number_format($field->cost,2) }}</td> <!-- Cost  -->
                                            <td align='right' colspan='2'>{{ number_format($field->profit,2) }}</td> <!-- Profit  -->
                                            <td align='center' colspan='2'>{{ number_format($field->profit_ratio) }}%</td> <!-- Profit Ratio  -->
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <th align='left' colspan='4'>Total</th>
                                        <th align='center' colspan='1'>{{ $all_qty }}</th>
                                        <th align='right' colspan='2'>{{ $all_net }}</th>
                                        <th align='right' colspan='2'>{{ $all_cost }}</th>
                                        <th align='right' colspan='2'>{{ $all_profit }}</th>
                                    </tr>
                                </table> <br>
                            @endif
                        @endforeach
                </div><!--end content-->
            </div><!--end subpage-->
        </div><!--end page-->
        @else
            <div class="page">
                <div class="subpage">
                    <div class="content">
                        <div class="text-head" align="center"><b>My Kitchen</b></div>
                        <div class="text-head" align="center"><b>Best Seling Items</b></div>
                        <div class="text-head" align="center">As of {{ $obj_fn->format_date_en(Input::get('from_date'),1) }}</div>
                        <table>
                            <tr>
                                <th colspan='1'>No.</th>
                                <th align='left' colspan='3'>Product</th>
                                <th colspan='1'>Quantity</th>
                                <th align='right' colspan='2'>Net Sales</th>
                                <th align='right' colspan='2'>Cost</th>
                                <th align='right' colspan='2'>Profit</th>
                                <th colspan='2'>Profit Ratio</th>
                            </tr>
                            <tr>
                                <td colspan='7'>No Result.</td>
                            </tr>
                        </table>
                    </div><!--end content-->
                </div><!--end subpage-->
            </div><!--end page-->
        @endif
    </div><!--end book-->
</body>
</html>
