<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
{{--@section('more-style')--}}
{{--<script language="javascript">  --}}
{{--function IsNumeric(sText,obj)  --}}
{{--{  --}}
    {{--var ValidChars = "0123456789.";  --}}
   {{--var IsNumber=true;  --}}
   {{--var Char;  --}}
   {{--for (i = 0; i < sText.length && IsNumber == true; i++)   --}}
      {{--{   --}}
      {{--Char = sText.charAt(i);   --}}
      {{--if (ValidChars.indexOf(Char) == -1)   --}}
         {{--{  --}}
         {{--IsNumber = false;  --}}
         {{--}  --}}
      {{--}  --}}
        {{--if(IsNumber==false){  --}}
            {{--alert("Only numberic value");  --}}
            {{--obj.value=sText.substr(0,sText.length-1);  --}}
        {{--}  --}}
   {{--}  --}}
{{--</script> --}}
{{--@endsection--}}

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">

                        <div class="form-group">
                            <label class="control-label col-md-3">Zone</label>
                            <div class="col-md-4">
                                <select class="form-control" name="zone_id">
                                    @foreach($zone as $value)

                                        <option value="{{ $value->zone_id }}" @if($zone_id == $value->zone_id) selected @endif> {{ $value->zone_name }}</option>

                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">Firstname</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="firstname" value="{{ $firstname }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Lastname</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="lastname" value="{{ $lastname }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="email" value="{{ $email }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Mobile</label>
                            <div class="col-md-4">
                                {{--<input type="text" class="form-control" name="mobile" value="{{ $mobile }}" onKeyUp="IsNumeric(this.value,this)">--}}
                                <input type="text" class="form-control" name="mobile" value="{{ $mobile }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Address</label>
                            <div class="col-md-4">
                                <textarea name="address" class="form-control">{{ $address }}</textarea>
                            </div>
                        </div>

                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}

    {{ Html::script('assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/serviceman/validation.js') }}
    <script>
        FormValidation.init();
        ComponentsFormTools.init();
    </script>
@endsection
