<?php


$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;
$STATUS = Config::get('constants.STATUS');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div id="summary" class="table-responsive">               
                    <table class="table table-bordered" >
                        <tbody style="font-size:11pt">
                            {{--<tr>--}}
                                {{--<td colspan="5"> --}}
                                    {{--<p><b>Date : </b>{{$orders->created_at}}</p>--}}
                                    {{--<p><b>Customer Name : </b>{{$order->customer_name}}</p>--}}
                                {{--</td>--}}
                                {{--<td colspan="3"> --}}
                                    {{--<p><b>Order No. </b>{{$orders->orders_no}}</p>--}}
                                    {{--<p><b>Table No. </b>{{$orders->table_no}}</p>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                            <tr>
                                <th class="text-center">#</th>
                                <th colspan="3">Item, Option</th>
                                <th class="text-center col-md-1">Qty</th>
                                <th class="text-right col-md-2">Price</th>
                                <th class="text-right col-md-2">Total ( ฿ )</th>
                                <th class="text-center col-md-2">Delivery Status</th>
                            </tr>
                            <?php $sudtotal = 0; ?>
                            @foreach($order as $key => $item)
                                <?php  
                                    $total_price = $item->qty * $item->price;
                                    $sudtotal += $total_price;
                                ?>
                                <tr>
                                    <td class="text-center">{{ $key+1 }}</td>
                                    <td colspan="3">{{ $item->product_name_en }}</td>
                                    <td class="text-center">{{ $item->qty }}</td>
                                    <td class="text-right">{{ number_format($item->price,2) }}</td>
                                    <td class="text-right">{{ number_format($total_price,2) }}</td>
                                    <td class="text-center">
                                        @if($item->status == 0)
                                            <p> Preparing </p>
                                        @elseif($item->status == 1)
                                            <p> Delivered </p>
                                        @elseif($item->status == 2)
                                            <p> Cancel </p>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            <?php 
                                $total = $sudtotal-$item->discount_price;
                                $tax = ($item->discount_price + $sudtotal)*0.07;
                            ?>
                            <tr>
                                <td colspan="7" class="text-right"><b>Subtotal</b></td>
                                <td class="text-right">{{ number_format($sudtotal,2) }}</td>
                            </tr>
                            <tr>
                                <td colspan="7" class="text-right"><b>Discount</b></td>
                                <td class="text-right">{{ number_format($item->discount_price,2) }}</td>
                            </tr>
                            <tr>
                                <td colspan="7" class="text-right"><b>Total</b></td>
                                <td class="text-right">{{ number_format($total,2) }}</td>
                            </tr>
                            <tr>
                                <td colspan="7" class="text-right"><b>Tax ( 7% )</b></td>
                                <td class="text-right">{{ number_format($tax,2) }}</td>
                            </tr>
                            <tr>
                                <td colspan="7" class="text-right"><b>Grand Total</b></td>
                                <td class="text-right">{{ number_format($total+$tax,2) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}

    {{ Html::script('assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}
@endsection
@section('more-script')
    <script>
        FormValidation.init();
        ComponentsFormTools.init();
    </script>
@endsection
