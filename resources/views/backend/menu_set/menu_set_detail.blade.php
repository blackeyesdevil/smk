<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$i = 1;
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
     <a href="{{ url()->to('_admin/menu_set') }}">Set Name</a> >> {{ $menu_set_name }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="form-body">
                    <div class="form-group">
                        @foreach($data as $key => $value)

                            <?php $total = ''; ?>

                            <h3><strong>Set {{ $i }} :</strong> {{ $value->set_name_detail_th }} ({{ $value->set_name_detail_en }})</h3>
                            <h4><strong>Qty</strong> : {{ $value->qty }}</h4>

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center col-sm-1">ID</th>
                                        <th class="text-center col-sm-1">Image</th>
                                        <th class="col-sm-4">Prdocut Name</th>
                                        <th class="text-center col-sm-1">Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data_product[$value->menu_set_detail_id] as $key => $value2)

                                        <tr>
                                            <td class="text-center">{{ $value2->product_id }}</td>
                                            <td class="text-center">
                                                <img src="{{ URL::asset('uploads/product/100/' . $value2->img_name) }}">
                                            </td>
                                            <td>{{ $value2->product_name_th }} ({{ $value2->product_name_en }})</td>
                                            <td class="text-center">
                                                <?php
                                                    $price = $sub_product[$value->menu_set_detail_id][$key][1];
                                                    $total += $price;
                                                ?>
                                                {{ $price }}
                                            </td>
                                        </tr>

                                    @endforeach
                                    <tr>
                                        <td colspan="3" class="text-right"><b>Total</b></td>
                                        <td class="text-center">{{ $total }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php $i++;?>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
