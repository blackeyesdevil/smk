<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
if($method=="PUT"){
    $data_detail = DB::table('menu_set_detail')->where('menu_set_id',$data->$primaryKey)->get();
    $product_name_set = '';
    foreach ($data_detail as $key => $value){
        $key = $key+1;
        $set_name_detail_th[$key] = $value->set_name_detail_th;
        $set_name_detail_en[$key] = $value->set_name_detail_en;
        $qty[$key] = $value->qty;
        $menu_set_detail_id[$key] = $value->menu_set_detail_id;
        $product_name_set[$key] = $value->product_name_set;

//    $product = explode(',',$value->product_name_set);
//    foreach ($product as $key2 => $value2){
//        $product_ex = array_slice(explode(':',$value2),-2,1);
//        foreach ($product_ex as $key3 => $value3){
//
//            $data_product = DB::table('product')->select('product_id','price','product_name_en')->where('product_id',$value3)->first();
//            $product_name_set[] = $key;
//            $product_name_set[$key2] = $data_product->product_id.":".$data_product->price.":".$data_product->product_name_en;
////            echo $product_name_set."<br>";
//
//        }
//    }

    }
}

?>
@extends('backend.layout.main-layout')
@section('page-style')

    {{ Html::style('assets/global/plugins/select2/select2.css') }}

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Head Name</label>
                            <div class="col-md-3">
                                <input type="text" name="menu_set_name_th" class="form-control" value="{{ $menu_set_name_th }}">
                            </div>
                            <div class="col-md-1">
                                ( TH )
                            </div>
                            <div class="col-md-3">
                                <input type="text" name="menu_set_name_en" class="form-control" value="{{ $menu_set_name_en }}">
                            </div>
                            <div class="col-md-1">
                                ( EN )
                            </div>
                        </div>

                        <hr>

                        @for($i=1;$i<=5;$i++)

                            <h3><strong>Set {{ $i }}</strong></h3>

                            <input type="hidden" name="menu_set_detail_id[{{ $i }}]" class="form-control" @if(!empty($menu_set_detail_id[$i])) value="{{ $menu_set_detail_id[$i] }}" @endif>

                            <div class="form-group">
                                <label class="control-label col-md-3">Set Name</label>
                                <div class="col-md-3">
                                    <input type="text" name="set_name_detail_th[{{ $i }}]" class="form-control" @if(!empty($set_name_detail_th[$i])) value="{{ $set_name_detail_th[$i] }}" @endif>
                                </div>
                                <div class="col-md-1">
                                    ( TH )
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="set_name_detail_en[{{ $i }}]" class="form-control" @if(!empty($set_name_detail_en[$i])) value="{{ $set_name_detail_en[$i] }}" @endif>
                                </div>
                                <div class="col-md-1">
                                    ( EN )
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Qty</label>
                                <div class="col-md-1">
                                    <input type="text" name="qty[{{ $i }}]" class="form-control" @if(!empty($qty[$i])) value="{{ $qty[$i] }}" @endif>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Filter</label>

                                <div class="col-md-4">
                                    <select name="store_id" id="store_id_{{ $i }}" class="change-category form-control select2me" data-no="{{$i}}">
                                        <option value="">Select Store</option>
                                        @foreach($store as $value)
                                            <option value="{{ $value->store_id }}">{{ $value->store_name_en }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <select name="category_id" id="category_id_{{ $i }}" class="change-product  form-control" data-no="{{$i}}">

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Product</label>
                                <div class="col-md-4">
                                    <select name="product_id" id="product_id_{{ $i }}" class="add-product form-control select2-container select2me" multiple="multiple" data-no="{{$i}}">
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-9">
                                    <textarea name="product_name_set[{{ $i }}]" id="product_name_set_{{ $i }}" class="form-control">@if(!empty($product_name_set[$i])) {{ $product_name_set[$i] }} @endif</textarea>
                                </div>
                            </div>

                        @endfor

                    </div>


                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}

    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}

    {{ Html::script('assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/category/validation.js') }}
    <script>
        FormValidation.init();
        ComponentsFormTools.init();
    </script>
@endsection
