@extends('backend.layout.main-layout')

@section('page-style')
@endsection
@section('more-style')
@endsection

@section('page-title','Report')

@section('page-content')
    <div class="col-md-12">


        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- SUMMARY BLOCK -->
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">

                                <div class="number">
                                    <div class="alert">
                                        <div class="icon">
                                            <i class="icon-basket"></i>
                                        </div>
                                        <h3 class="font-green-sharp">
                                            <span data-counter="counterup" data-value="">Sales Summary</span>
                                        </h3>
                                        </div>

                                    <a href="{{ url()->to('_admin/report_store') }}"><small>End of Day Report</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Sales Summary</small></a><br>
                                    <a href="{{ url()->to('_admin/report_product') }}"><small>Sales Product</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Sales Detail</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Sales Summary By User</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Sales Detail By User</small></a><br>
                                    <a href="{{ url()->to('_admin/report_category') }}"><small>Sales by Category</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Sales by Shop</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Sales by Item</small></a><br>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">

                                <div class="number">
                                    <div class="alert">
                                        <div class="icon">
                                            <i class="icon-basket"></i>
                                        </div>
                                        <h3 class="font-red-haze">
                                            <span data-counter="counterup" data-value="">Tax & Audit Summary</span>
                                        </h3>
                                    </div>

                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Sales by Tax</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Email Sent</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>User login Summary</small></a><br>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">

                                <div class="number">
                                    <div class="alert">
                                        <div class="icon">
                                            <i class="icon-basket"></i>
                                        </div>
                                        <h3 class="font-blue-sharp">
                                            <span data-counter="counterup" data-value="">Sales Analysis</span>
                                        </h3>
                                    </div>

                                    <a href="{{ url()->to('_admin/top_category') }}"><small>Top Selling Category</small></a><br>
                                    <a href="{{ url()->to('_admin/top_product') }}"><small>Top Selling Product</small></a><br>
                                    <a href="{{ url()->to('_admin/sales_by_hour') }}"><small>Sales by Hour</small></a><br>
                                    <a href="{{ url()->to('_admin/sales_by_date') }}"><small>Sales by Date</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Non Selling Item</small></a><br>
                                    <a href="{{ url()->to('_admin/report_member') }}"><small>Sales by Member</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Type</small></a><br>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">

                                <div class="number">
                                    <div class="alert">
                                        <div class="icon">
                                            <i class="icon-basket"></i>
                                        </div>
                                        <h3 class="font-blue-sharp">
                                            <span data-counter="counterup" data-value="">Transaction Summary</span>
                                        </h3>
                                    </div>

                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Sales by Section</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Order summary by receipt</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Refund summary by receipt</small></a><br>
                                    <a href="{{ url()->to('_admin/report_store') }}"><small>Refund Detail by receipt</small></a><br>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>

    </div>
@endsection

@section('page-plugin')
@endsection
@section('more-script')
@endsection
