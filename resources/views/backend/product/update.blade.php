<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>

@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection
@section('page-content')

    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Store Name</label>
                            <div class="col-md-4">
                                @if (!empty(session()->get('s_admin_store_id')) != 0)
                                    @foreach($store as $field1)
                                        <button type="button" class="btn btn-info">{{$field1->store_name_en}}</button>
                                    @endforeach
                                @else
                                    <select name="store_id" id="store_id" class="change-shop-category form-control select2">
                                        <option value="">Select...</option>
                                        @foreach($store as $field1)
                                            <option value="{{$field1->store_id}}" @if($store_id == $field1->store_id) selected @endif >{{$field1->store_name_en}}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Shop Category</label>
                            <div class="col-md-4">
                                <select name="category_id" id="category_id" class="form-control">
                                    {{--<option value="">Select...</option>--}}
                                    {{--@foreach($category as $field2)--}}
                                    {{--<option value="{{$field2->category_id}}" @if($category_id == $field2->category_id) selected @endif >{{$field2->category_name_en}}</option>--}}
                                    {{--@endforeach--}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">My Kitchen Category</label>
                            <div class="col-md-4">
                                <select name="collection_id" class="form-control">
                                    <option value="">Select...</option>
                                    @foreach($collection as $field3)
                                        <option value="{{$field3->collection_id}}" @if($collection_id == $field3->collection_id) selected @endif >{{$field3->collection_name_en}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Product Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="product_name_th" value="{{ $product_name_th }}" required>
                            </div>
                            <div class="col-md-1">
                                ( TH )
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Product Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="product_name_en" value="{{ $product_name_en }}">
                            </div>
                            <div class="col-md-1">
                                ( EN )
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Product Code</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="product_code" value="{{ $product_code }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Description ( TH )</label>
                            <div class="col-md-4">
                                <textarea name="description_th" id="description_th" class="form-control ckeditor" data-error-container="#detail_error" style="height:150px;">{{ $description_th}}</textarea>
                                <div id="detail_error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Description ( EN )</label>
                            <div class="col-md-4">
                                <textarea name="description_en" id="description_en" class="form-control ckeditor" data-error-container="#detail_error" style="height:150px;">{{ $description_en}}</textarea>
                                <div id="detail_error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Price</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="price" value="{{ $price }}" min="0" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Special Price</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="special_price" value="{{ $special_price }}" min="0">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Cost</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="cost" value="{{ $cost }}" min="0">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Barcode</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="barcode" value="{{ $barcode }}" min="0">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Gen Barcode</label>
                            <div class="col-md-4">
                                <?php
                                if($method=='POST' or $gen_barcode==''){
                                    $gen_barcode = $obj_fn->gen_random(15,1);
                                }
                                ?>
                                <input type="text" class="form-control" name="gen_barcode" value="{{ $gen_barcode }}" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Recommended</label>
                            <div class="col-md-4">
                                <select class="form-control" name="is_recommended">
                                    <option value="0" @if($is_recommended==0) selected @endif> Not Recommended </option>
                                    <option value="1" @if($is_recommended==1) selected @endif> Recommended </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Available</label>
                            <div class="col-md-4">
                                <select class="form-control" name="is_available">
                                    <option value="0" @if($is_available==0) selected @endif> Unavailable </option>
                                    <option value="1" @if($is_available==1) selected @endif> Available </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Vat</label>
                            <div class="col-md-4">
                                <input type="checkbox" class="form-control" name="vat" @if($vat==1) checked @endif value="1">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Tags</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="tags" value="{{ $tags }}" min="0">
                            </div>
                        </div>

                        <!-- START PRODUCT IMAGE -->
                        <?php
                        $field = 'img_name';
                        $label = 'Product';
                        $upload_path = 'product';
                        ?>
                        <div id="bg-img">
                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="img_thumbnail">
                                        @if($$field != '')
                                            <img src="<?php echo URL::asset('uploads/' . $upload_path . '/' . $$field) ?>" class="img_path" style="max-width:200px;">
                                        @else
                                            <img src="" class="img_path" style="max-width:400px;">
                                        @endif

                                        <input type="hidden" name="{{ $field }}" class="img_hidden" value="<?php echo $$field; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">{{ $label }}</label>
                                <div class="col-md-4">
                                    @if($$field != '')
                                        <div class="btn btn-danger remove_img" >Remove</div>
                                        <div class="btn blue upload" data-file="{{ $field }}" style="display:none;"><i class="icon-picture"></i> Select File</div>
                                    @elseif ($$field == '')
                                        <div class="btn blue upload" data-file="{{ $field }}"><i class="icon-picture"></i> Select File</div>
                                        <div class="btn btn-danger remove_img" style="display:none;" >Remove</div>
                                    @endif

                                    <input type="hidden" name="img_del" class="img_del" value="n">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"> </label>
                                <div class="col-md-4">
                                    <span style="color:#F00">* หมายเหตุ</span><br>
                                                <span style="">
        							      		    - รูปควรมีขนาด 1600 x 460 pixel. <br>
                                                </span>
                                </div>
                            </div>
                        </div>
                        <!-- END PRODUCT IMAGE -->
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <input type="hidden" class="form-control" name="store" value="{{ $store_id }}">
                                <input type="hidden" class="form-control" name="category" value="{{ $category_id }}">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/product/validation.js') }}
    <script>
        FormValidation.init();

        $(document).ready(function() {
            var value = $('input[name=store]').val();
            var value2 = $('input[name=category]').val();
            if(value != '' && value2 != ''){
//                alert(value);
                ChangeShopCategory2(value,value2);
            }

        });
    </script>
@endsection