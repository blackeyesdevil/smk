<?php
use Illuminate\Support\Facades\Input;
use App\Models\Orders;

$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('backend.layout.main-layout')

@section('page-style')
    {{ Html::style('assets/global/plugins/select2/select2.css') }}
    {{ Html::style('assets/global/css/plugins.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
    {{ Html::style('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            @if(empty(Input::get('search')))
                <div class="form-search">
                    <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                        <input type="hidden" class="form-control" name="search" value="true">
                        <div class="form-group">
                            <label class="control-label col-md-1">Date</label>
                            <div class="col-md-3">
                                <input type="text" class="form-control date-picker text-center" name="date" value="{{ Input::get('date') }}" data-date-format="yyyy-mm-dd">
                                <span class="help-block">Select date</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-1 col-md-3 ">
                                <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            @else
            <div class="portlet-title">
                <div class="text-right">
                    <div>
                        <a href="{{URL::to($path.'?mode=print'.$str_param)}}" target="_blank"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-print"></i> Print</button></a>
                        <a href="{{URL::to($path.'?mode=export'.$str_param)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel</button></a>
                    </div>
                </div>
            </div>
            <div class="portlet-title" align="center">
                <span class="caption-subject font-green-sharp bold">As of {{ $as_of }}</span><br>
                <span class="caption-subject font-green-sharp bold">Report was run on {{ date('F d, Y H:i') }}</span>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-left">Hour</th>
                            <th class="text-center">Number of Bill</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-right">Gross Sales</th>
                            <th class="text-right">Discount</th>
                            <th class="text-right">Net Sales</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if($count_data > 0)
                                <?php
                                    $all_count_order = 0;
                                    $all_qty = 0;
                                    $all_total = 0;
                                    $all_discount = 0;
                                ?>
                                @foreach($data as $key => $field)
                                    <?php
                                        $date = Input::get('date');
                                        if(empty($date)) $date = '%';

                                        $sum_discount = Orders::where('status','=','1')
                                                                ->where('created_at','like',$date.' '.$key.':%:%')
                                                                ->sum('discount_price');

                                        $all_count_order += $field->count_order;
                                        $all_qty += $field->sum_qty;
                                        $all_total += $field->sum_total;
                                        $all_discount += $sum_discount;
                                    ?>
                                    <tr>
                                        <td class="text-left">{{ date('g a', strtotime($key.':00:00')) }}</a></td>
                                        <td class="text-center">{{ number_format($field->count_order) }}</td>
                                        <td class="text-center">{{ number_format($field->sum_qty) }}</td>
                                        <td class="text-right">{{ number_format($field->sum_total,2) }}</td>
                                        <td class="text-right">{{ number_format($sum_discount,2) }}</td>
                                        <td class="text-right">{{ number_format($field->sum_total - $sum_discount,2) }}</td>
                                    </tr>

                                @endforeach
                                <tr class="bold">
                                    <td class="text-left">Total</td>
                                    <td class="text-center">{{ number_format($all_count_order) }}</td>
                                    <td class="text-center">{{ number_format($all_qty) }}</td>
                                    <td class="text-right">{{ number_format($all_total,2) }}</td>
                                    <td class="text-right">{{ number_format($all_discount,2) }}</td>
                                    <td class="text-right">{{ number_format($all_total - $all_discount,2) }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection

@section('page-plugin')
    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}
    {{ Html::script('assets/global/plugins/ckeditor/ckeditor.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/theme_component/components-pickers.js') }}
    {{ Html::script('js/backend/theme_component/components-form-tools.js') }}
    {{ Html::script('js/backend/theme_component/components-form-tools2.js') }}
    <script>
        ComponentsPickers.init();
    </script>
@endsection
