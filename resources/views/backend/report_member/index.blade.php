<?php
use Illuminate\Support\Facades\Input;
use App\Models\Orders;

$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('backend.layout.main-layout')

@section('page-style')
    {{ Html::style('assets/global/plugins/select2/select2.css') }}
    {{ Html::style('assets/global/css/plugins.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
    {{ Html::style('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            @if(empty(Input::get('search')))
                <div class="form-search">
                    <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                        <input type="hidden" class="form-control" name="search" value="true">
                        <input type="hidden" class="form-control" name="date" value="{{ Input::get('date') }}">
                        <div class="form-group">
                            <label class="control-label col-md-1">Search</label>
                            <div class="col-md-5">
                                <div class="input-group input-daterange col-md-12" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control form_sql_datetime" name="from_date" value="{{ Input::get('from_date') }}">
                                    <span class="input-group-addon"> to </span>
                                    <input type="text" class="form-control form_sql_datetime" name="to_date" value="{{ Input::get('to_date') }}">
                                </div>
                                <span class="help-block">Select date range </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-1"></label>
                            <div class="col-md-3">
                                <input class="form-control" type="text" name="search1" value="{{ Input::get('search1') }}">
                                <span class="help-block">Search by Name</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-1 col-md-3 ">
                                <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                            </div>
                        </div>
                    </form>
                    <hr>
                </div>
            @else
                <div class="portlet-title">
                    <div class="text-right">
                            <a href="{{URL::to($path.'?mode=print'.$str_param)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-print"></i> Print</button></a>
                            <a href="{{URL::to($path.'?mode=export'.$str_param)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel</button></a>
                    </div>
                </div>
                <div class="portlet-title">
                    <div class="caption">
                        <!-- <span class="caption-subject font-green-sharp bold">{{ $page_title }}</span><br> -->
                        <span class="caption-subject font-green-sharp bold">From {{ $from }}</span><br>
                        <span class="caption-subject font-green-sharp bold">Through {{ $through }}</span><br>
                        <span class="caption-subject font-green-sharp bold">Report was run on {{ date('F d, Y H:i') }}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center col-md-3">Member Name</th>
                                    <th class="text-center">Number of Bill</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-center">Gross Sales</th>
                                    <th class="text-center col-md-2">Discount</th>
                                    <th class="text-center col-md-2">Net Sales</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($count_data > 0)
                                    <?php $total_gross=0; $total_bill=0; $total_qty=0; $total_dis=0; $total_net=0; ?>
                                    @foreach($data as $key => $field)
                                        <?php 
                                            $sum_discount = Orders::where('status','=','1')
                                                                    ->where('created_at','like',$field->date.' %:%:%')
                                                                    ->sum('discount_price');
                                            $total_gross +=  $field->sum_price; 
                                            $total_bill += $field->count_order; 
                                            $total_qty += $field->sum_qty; 
                                            $total_dis += $sum_discount; 
                                            $total_net += $field->sum_price-$sum_discount;
                                        ?>
                                        <tr>               
                                            <td class="col-md-4">{{$field->member_name}} </td>
                                            <td class="text-center"> {{ $field->count_order }} </td> <!-- count orders_id -->
                                            <td class="text-center"> {{ $field->sum_qty }} </td> <!-- sum qty -->
                                            <td class="text-right">
                                                <!-- <div class="progress">
                                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{-- $total_price--}}%"></div>
                                                </div> -->
                                                {{ number_format($field->sum_price,2) }} <!-- ( {{-- number_format($total_price,2) --}}% ) --></td> <!-- sum price  -->
                                            <td class="text-right"> {{ number_format($sum_discount,2) }} </td> <!--Discount -->
                                            <td class="text-right"> {{ number_format($field->sum_price-$sum_discount,2) }} </td> <!-- Net sales -->
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td><b> รวม </b></td>
                                        <td class="text-center"> <b> {{ number_format($total_bill) }} </b> </td> <!-- count orders_id -->
                                        <td class="text-center"> <b>{{ number_format($total_qty) }}</b> </td> <!-- sum qty -->
                                        <td class="text-right"> <b>{{ number_format($total_gross,2) }}</b> </td> <!-- Gross Sales  -->
                                        <td class="text-right"> <b>{{ number_format($total_dis,2)  }}</b> </td> <!--Discount -->
                                        <td class="text-right"> <b>{{ number_format($total_net,2) }}</b> </td> <!-- Net sales -->
                                    </tr>
                                @else
                                    <tr>
                                        <td class="text-center" colspan="6">No Result.</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    {!! $data->appends(Input::except('page'))->render() !!}
                </div>
            @endif
        </div>
    </div>
@endsection

@section('page-plugin')
    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}
    {{ Html::script('assets/global/plugins/ckeditor/ckeditor.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/theme_component/components-pickers.js') }}
    {{ Html::script('js/backend/theme_component/components-form-tools.js') }}
    {{ Html::script('js/backend/theme_component/components-form-tools2.js') }}
    <script>
        ComponentsPickers.init();
    </script>
    <script type="text/javascript">
        $(".form_sql_datetime").datetimepicker({
            isRTL: Metronic.isRTL(),
            format: "yyyy-mm-dd hh:ii:ss",
            autoclose: true,
            todayBtn: true,
            startDate: "2013-02-14 10:00:00",
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
            minuteStep: 10
        });
    </script>
@endsection