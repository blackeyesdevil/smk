<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');
$member_id = Input::get('member_id');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('backend.layout.main-layout')

@section('page-style')
@endsection
@section('more-style')
@endsection

@section('page-title')
    <a href="{{ url()->to('_admin/store') }}">Orders</a> >> {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <td>
                                <h4><b>Customer Name : </b> {{ $member->customer_name}}</h4>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center col-md-2">ลำดับ</th>
                                            <th class="text-center">อาหาร</th>
                                            <th class="text-center col-md-2">จำนวน</th>
                                            <!-- <th class="text-center col-md-3">วันที่</th> -->
                                            <th class="text-center col-md-2">ราคารวม</th>
                                        </tr>
                                    </thead>
                                    @if($count_data > 0 ) 
                                        @foreach($data as $key => $field)
                                        <?php $total= $field->price * $field->qty;?>
                                       
                                            <tr>
                                                <td class="text-center">{{ $key+1 }}</td>
                                                <td class="text-left">{{ $field->product_name_en }} ( {{ $field->product_name_th }} )</td>
                                                <td class="text-center">{{ $field->qty }}</td>
                                                <!-- <td class="text-center">{{ $field->created_at }}</td> -->
                                                <td class="text-right">{{ number_format($total,2)}}</td>
                                            </tr>
                                      
                                        @endforeach

                                    @else
                                        <tr>
                                            <td class="text-center" colspan="13">No Result.</td>
                                        </tr>
                                    @endif
                                </table>
                            </td>
                        </tr>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-plugin')
@endsection
@section('more-script')
@endsection
