<?php
use Illuminate\Support\Facades\Input;
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;
$tb_name = $obj_model->tbname;
$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);
$frequency = Input::all();
$str_frequency = $obj_fn->parameter($frequency);
$time = Input::all();
$str_time = $obj_fn->parameter($time);

?>
@extends('backend.layout.main-layout')

@section('page-style')
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="form-search">
                <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                    <div class="form-group">
                        <label class="control-label col-md-1">Search</label>
                        <div class="col-md-3">
                            <input class="form-control" type="text" name="search" value="{{ Input::get('search') }}">
                            <span class="help-block">Search by Code Name</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-3 ">
                            <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                </form>
                <hr>
            </div>

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold">Found {{ $count_data }} Record(s).</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>

                            <th class="col-md-1 text-center">{!! $obj_fn->sorting('ID',$primaryKey,$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="col-md-1">{!! $obj_fn->sorting('Code','code',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="col-md-2">{!! $obj_fn->sorting('Code Name','code',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="col-md-2 text-center">
                                <span class="font-green">Allot</span> / <span class="font-red">Reserved</span> / <span class="font-yellow">Used</span> / <span class="font-blue">Pre use</span></th>
                            <th class="col-md-1 text-center">{!! $obj_fn->sorting('Category code','show_date',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="col-md-1 text-center">{!! $obj_fn->sorting('Product code','show_date',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                                <th class="col-md-4 text-center">
                                <span class="font-blue">Year</span> | <span class="font-blue">Day of Week</span> | <span class="font-blue">Month</span> | <span class="font-blue">Day</span> | <span class="font-blue">Hour</span> | <span class="font-blue">Minute</span></th>

                            <!-- <th class="col-md-1 text-center">{!! $obj_fn->sorting('Show Date','show_date',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th class="col-md-2 text-center">{!! $obj_fn->sorting('Date','start_date',$order_by,$sort_by,$str_param_sort,'') !!}</th> -->
                            <th class="col-md-1">{!!('Status') !!}</th>
                            <th class="col-md-1 text-center">{!! $obj_fn->sorting('Available','is_available',$order_by,$sort_by,$str_param_sort,'') !!}</th>

                            @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                <th class="text-center col-sm-2 col-md-2">
                                    @if($permission['c'] == '1')
                                        <a href="{{ url()->to($path.'/create') }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                                    @endif
                                </th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>

                        @if($count_data > 0)
                            @foreach($data as $key => $field)
                         <!--    <?php
                                // $txt = $field->minute.' '.$field->hour.' '.$field->day.' '.$field->month.' '.$field->year;
                                // if($obj_fn->parse_crontab($txt)){

                                    // echo "test";
                                // }else {
                                    // echo "dfsdfdsf";
                                // }
                            ?>  -->
                            <?php
                                     $txt = $field->minute.' '.$field->hour.' '.$field->day.' '.$field->month.' '.$field->dayofweek.' '.$field->year;
                            ?>
                                <tr>
                                    <td class="text-center">{{ $field->$primaryKey }}</td>
                                    <td>{{ $field->code }}</td>
                                    <td>{{ $field->code_name_th }} @if(!empty($field->code_name_en)) {{ $field->code_name_en }} @endif</td>
                                    <td class="text-center">
                                        <span class="font-green">{{ $field->allot}}</span> / <span class="font-red">{{ $field->reserved }}</span> / <span class="font-yellow">{{ $field->used }}</span> / <span class="font-blue">{{ $field->per_use }}</span>
                                         </td>
                                    <td class="text-center">{{ $field->category_code}}</td>
                                    <td class="text-center">{{ $field->product_code}}</td>

                                    
                                    
                                        <td class="text-center">
                                            <span class="font-green">{{($field->year)}}</span> | <span class="font-green">{{($field->dayofweek)}}</span> | <span class="font-green">{{ ($field->month) }}</span> | <span class="font-green">{{ ($field->day) }}</span> | <span class="font-green">{{ ($field->hour) }}</span> | <span class="font-green">{{ ($field->minute) }}</span>
                                        </td>
                                  

                                    <!-- <td class="text-center">{{ $obj_fn->format_date_th($field->show_date,2) }}</td>
                                    <td class="text-center">{{ $obj_fn->format_date_th($field->start_date,2) }} - {{ $obj_fn->format_date_th($field->end_date,2) }}</td> -->

                                    <td class="text-center"> 
                                    @if($obj_fn->parse_crontab($txt))
                                     <i class="fa fa-check" style="color:green"></i>
                                    @else
                                    <i class="fa fa-close" style="color:red"></i>
                                    @endif
                                    </td>

                                    @if($field->is_available == 0 )
                                        <td><div class="text-center"><a id="is_available-{{$field->$primaryKey}}" class=" change-active fa fa-close text-red" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="is_available"  data-value="{{$field->is_available}}"></a></div></td>
                                    @else
                                        <td><div class="text-center"><a id="is_available-{{$field->$primaryKey}}" class=" change-active fa fa-check text-green" data-tb_name="{{$table}}" data-pk_field="{{$primaryKey}}" data-v_pk_field="{{$field->$primaryKey}}" data-change_field="is_available"  data-value="{{$field->is_available}}"></a></div></td>
                                    @endif

                                    @if($permission['c'] == '1' || $permission['u'] == '1' || $permission['d'] == '1')
                                        <td class="text-center">
                                            @if($permission['u'] == '1')
                                                <a href="{{ url()->to($path.'/'.$field->$primaryKey.'/edit?1'.$str_param) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                            @endif
                                            @if($permission['d'] == '1')
                                                <form action="{{ url()->to($path.'/'.$field->$primaryKey) }}" class="form-delete" parent-data-id="{{ $field->$primaryKey }}" method="POST" >
                                                    <input type="hidden" name="_method" value="delete">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button type="button" class="btn btn-xs btn-circle red btn-delete" data-id="{{ $field->$primaryKey }}"><i class="fa fa-trash-o"></i></button>
                                                </form>
                                            @endif
                                        </td>
                                        @endif
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="12">No Result.</td>
                            </tr>
                        @endif

                        </tbody>
                    </table>
                </div>
                {!! $data->appends(Input::except('page'))->render() !!}
            </div>
        </div>
    </div>
@section('js')
    <script src="{{URL::asset('js/backend/main.js')}}"></script>
@endsection
@endsection

@section('page-plugin')
@endsection
@section('more-script')
@endsection
                                
                            