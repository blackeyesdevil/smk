<?php

use Illuminate\Support\Facades\Input;


$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}

?>

@extends('backend.layout.main-layout')
@section('page-style')

    {{ Html::style('assets/global/plugins/select2/select2.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}
    {{ Html::style('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}
    {{ Html::style('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')

    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">
                        
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Available</label>
                                            <div class="col-md-4">

                                                <select name="is_available" class="form-control">

                                                    <option value="0" @if($is_available == 0) selected @endif >Close</option>
                                                    <option value="1" @if($is_available == 1) selected @endif >Open</option>

                                                </select>
                                            </div>
                                        </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Category Code</label>
                                                <div class="col-md-4">
                                                    <select name="category_code[]" class="form-control select2-container form-control select2me" multiple="multiple" id="category_code[]">
                                                    @foreach($category as $cat)
                                                    <option value="{{$cat->category_id}}">{{$cat->category_name_th}}</option>

                                                    @endforeach
                                                    </select>
                                                </div>
                                            </div>
 

                                           <!--  @if($countCategory !="")
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">List Category</label>
                                                    <div class="col-md-4">
                                                        @if($countCategory > 0)
                                                            @foreach($category as $cat)
                                                            
                                                                <span style="display: inline;">
                                                                    <a id="delete_category->{{$cat->category_id}}" data-change_field="delete_category" class="delete-category btn btn-xs btn-default"  data-tb_name="{{$tb_name}}" data-value="{{$cat->category_id}}"><i class="fa fa-times"></i>
                                                                    </a>
                                                                </span>
                                                            @endforeach
                                                        @else
                                                            No Result.
                                                        @endif
                                                        <input type="hidden" name="category_id" value="{{ $category_id }}">
                                                    </div>
                                                </div>
                                            @endif 
 -->


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Product Code</label>
                                                <div class="col-md-4">
                                                    <select name="product_code[]" class="form-control select2-container form-control select2me" multiple="multiple" id="product_code[]">
                                                     @foreach($product as $pro)
                                                <option value="{{$pro->product_id}}">{{$pro->product_name_th}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                            </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">Discount Type</label>
                                            <div class="col-md-4">
                                                <select class="form-control" name="discount_type">
                                                    <option value="fix" @if($discount_type == "fix") selected @endif >Fix</option>
                                                    <option value="%"@if($discount_type == "%") selected @endif >%</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Code </label>
                                            <div class="col-md-4">
                                                <input type='text'  name="code" id="code" class="form-control" value="{{ $code }}">
                                            </div>
                                        </div>
                                         

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Code Name</label>
                                            <div class="col-md-4">
                                                <input type='text'  name="code_name_th" id="code_name_th" class="form-control" value="{{ $code_name_th }}">
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Code Name</label>
                                            <div class="col-md-4">
                                                <input type='text'  name="code_name_en" id="code_name_en" class="form-control" value="{{ $code_name_en }}">
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">Allot</label>
                                            <div class="col-md-2">
                                                <input type='number' min="0" name="allot" id="allot" class="form-control" value="{{ $allot }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Minimum Price</label>
                                            <div class="col-md-2">
                                                <input type='number' min="0" name="min_price" id="min_price" class="form-control" value="{{ $min_price }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Maximum Price</label>
                                            <div class="col-md-2">
                                                <input type='number' min="0" name="max_price" id="max_price" class="form-control" value="{{ $max_price }}">
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label class="control-label col-md-3">Discount</label>
                                            <div class="col-md-2">
                                                <input type='number' min="0" name="discount" id="discount" class="form-control" value="{{ $discount }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Discount Max</label>
                                            <div class="col-md-2">
                                                <input type='number' min="0" name="discount_max" id="discount_max" class="form-control" value="{{ $discount_max }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-1">Year</label>
                                            <div class="col-md-1">
                                                <input type='text'  name="year" id="year" class="form-control" value="{{$year}}">
                                            </div>
                                            <label class="control-label col-md-1">Day of Week</label>
                                            <div class="col-md-1">
                                                <input type='text'  name="dayofweek" id="dayofweek" class="form-control" value="{{$dayofweek}}">
                                            </div>
                                       
                                            <label class="control-label col-md-1">Month</label>
                                            <div class="col-md-1">
                                                <input type='text'  name="month" id="month" class="form-control" value="{{$month}}">
                                            </div>
                                       
                                            <label class="control-label col-md-1">Day</label>
                                            <div class="col-md-1">
                                                <input type='text'  name="day" id="day" class="form-control" value="{{$day}}">
                                            </div>
                                       
                                            <label class="control-label col-md-1">Hour</label>
                                            <div class="col-md-1">
                                                <input type='text'  name="hour" id="hour" class="form-control" value="{{$hour}}">
                                            </div>
                                        
                                            <label class="control-label col-md-1">Minute</label>
                                            <div class="col-md-1">
                                                <input type='text'  name="minute" id="minute" class="form-control" value="{{$minute}}">
                                            </div>
                                        </div>




                                        <!-- <div class="form-group">
                                            <label class="control-label col-md-3">Start Show Date</label>
                                            <div class="col-md-2">
                                                <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                    <input type="text" class="form-control" name="show_date" @if($show_date!='0000-00-00') value="{{ $show_date }}" @endif>
												<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Start Use Date</label>
                                            <div class="col-md-2">
                                                <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                    <input type="text" class="form-control" name="start_date" @if($start_date!='0000-00-00') value="{{ $start_date }}" @endif>
												<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">End Date</label>
                                            <div class="col-md-2">
                                                <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                    <input type="text" class="form-control" name="end_date" @if($end_date!='0000-00-00') value="{{ $end_date }}" @endif>
												<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
 -->

                                    </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <input type='hidden' min="0" name="per_use" id="per_use" class="form-control" value="{{ $per_use }}">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>

@endsection
@section('page-plugin')



    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}

    {{ Html::script('assets/global/plugins/select2/select2.min.js') }}

    {{ Html::script('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}

    {{ Html::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}
    {{ Html::script('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}
    {{ Html::script('assets/global/plugins/ckeditor/ckeditor.js') }}
    
@endsection
@section('more-script')
    {{ Html::script('js/backend/theme_component/validation.js') }}
    {{ Html::script('js/backend/discount_code/validation.js') }}


    <script>
        FormValidation.init();
        ComponentsPickers.init();
    </script>

    <!-- BEGIN GOOGLE RECAPTCHA -->
@endsection
@section('js')
  <!--   <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/product.js')}}"></script>
    <script src="{{URL::asset('js/del-category.js')}}"></script> -->

@endsection