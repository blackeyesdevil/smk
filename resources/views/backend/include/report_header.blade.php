<div class="header_report">
    <div align="center"><img src="{{ url()->asset('/images/logo.png') }}" alt="logo"></div><br>
    @if(isset($main_title)) <div align="center"><b>{{ $main_title }}</b></div> @endif
    <div align="center"><b>{{ $report_title }}</b></div>
    @if(isset($description)) <div align="center"><b>{{ $description }}</b></div> @endif
    @if(isset($from) && isset($through)) <div class="text-head" align="center"><b>From {{ $from }} Through {{ $through }}</b></div> @endif
    @if(isset($as_of)) <div class="text-head" align="center"><b>As of {{ $as_of }}</b></div> @endif
    <div class="text-head" align="center"><b>Report was run on {{ date('F d, Y H:i') }}</b></div><br>
</div>
