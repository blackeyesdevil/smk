
<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{ url()->to('_admin') }}"><img src="{{ url()->asset('/images/logo.png') }}" alt="logo"></a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu hidden-xs">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-extended">
                        <a href="#">{{ session()->get('s_admin_name') }}</a>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                    <li class="droddown dropdown-separator"><span class="separator"></span></li>
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-extended">
                        <a href="{{ url()->to('_admin/logout') }}"><i class="icon-logout"></i> Logout</a>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->

    <!-- BEGIN HEADER MENU -->

    <div class="page-header-menu">
        <div class="container">
            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu ">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ url()->to('_admin/store') }}">Store</a>
                    </li>
                      @if (session()->get('s_admin_store_id') <> 0)
                    <li>
                        <a href="{{ url()->to('_admin/category').'?store_id='.session()->get('s_admin_store_id') }}">Category</a>
                    </li>
            @endif

                    <?php
//                        $permission = $obj_fn->permission('product','c');
                    ?>
                    {{--@if($permission[1]==1)--}}

                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            Product<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li>
                                <a href="{{ url()->to('_admin/product') }}">Product</a>
                            </li>
                            <li>
                                <a href="{{ url()->to('_admin/collection') }}">My Kitchen Category</a>
                            </li>
                            <li>
                                <a href="{{ url()->to('_admin/discount_code') }}">Discount Code</a>
                            </li>
                        </ul>
                    </li>

                    {{--@endif--}}

                    <li>
                        <a href="{{ url()->to('_admin/orders') }}">Order</a>
                    </li>

                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            Member<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li>
                                <a href="{{ url()->to('_admin/member') }}">Member</a>
                            </li>
                            <li>
                                <a href="{{ url()->to('_admin/tenant') }}">Tenant</a>
                            </li>
                        </ul>
                    </li>

                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            Menu Set<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li>
                                <a href="{{ url()->to('_admin/menu_set') }}">Menu Set</a>
                            </li>
                            <li>
                                <a href="{{ url()->to('_admin/combo_set') }}">Combo Set</a>
                            </li>
                            <li>
                                <a href="{{ url()->to('_admin/promotion') }}">Promotion</a>
                            </li>
                        </ul>
                    </li>

                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            Serviceman <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li>
                                <a href="{{ url()->to('_admin/serviceman') }}">Seviceman</a>
                            </li>
                            <li>
                                <a href="{{ url()->to('_admin/zone') }}">Zone</a>
                            </li>
                            <li>
                                <a href="{{ url()->to('_admin/table') }}">Table</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="{{ url()->to('_admin/receipt_footer/1/edit') }}">Receipt Footer</a>
                    </li>

                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            User Management <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li>
                                <a href="{{ url()->to('_admin/user-management') }}">User</a>
                            </li>
                            <li>
                                <a href="{{ url()->to('_admin/role') }}">Role</a>
                            </li>
                            <li>
                                <a href="{{ url()->to('_admin/page') }}">Page</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="{{ url()->to('_admin/report') }}">Report</a>
                    </li>

                    <li class="visible-xs">
                        <a href="{{ url()->to('_admin/logout') }}">Logout</a>
                    </li>
                </ul>
            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>
