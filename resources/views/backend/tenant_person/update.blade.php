<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $tenant->tenant_name_th.' >> '.$txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <input type="hidden" name="tenant_id" value="{{ Input::get('tenant_id') }}">
                    <input type="hidden" name="gen_barcode" id="gen_barcode" value="{{ $gen_barcode }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Firstname (EN)</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="firstname_en" value="{{ $firstname_en }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Lastname (EN)</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="lastname_en" value="{{ $lastname_en }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Firstname (TH)</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="firstname_th" value="{{ $firstname_th }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Lastname (TH)</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="lastname_th" value="{{ $lastname_th }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Barcode</label>
                            <div class="col-md-4">
                                <div class="input-group" style="text-align:left">
                                    <input type="text" class="form-control" name="barcode" id="barcode" value="{{ $barcode }}" readonly>
                                    @if(empty($barcode))
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn green" id="random"><i class="fa fa-random"></i></a>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default" id="reset">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}

    {{ Html::script('assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/admin/more.js') }}
    {{ Html::script('js/backend/tenant_person/validation.js') }}
    <script>
        FormValidation.init();
        ComponentsFormTools.init();
    </script>
    <script>
        $(function () {
            $('#random').click(function() {
                $('#barcode').val( $('#gen_barcode').val() );
                $('#random').addClass('disabled');
            });

            $('#reset').click(function() {
                $('#random').removeClass('disabled');
            });

        });
    </script>
@endsection
