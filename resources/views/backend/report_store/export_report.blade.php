<?php
//use App\Http\Controllers\Backend\Controller;
use App\Library\MainFunction;
$mainFn = new MainFunction(); // New Object Main Function
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey

//$filename  ="excel_report.xls";
//header("Content-Type: application/vnd.ms-excel; charset=utf-8");
//header("Content-Disposition: attachment; filename=report_customer.xls");
//header("Expires: 0");
//header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//header("Cache-Control: private",false);

?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
<TABLE BORDER="1" width="100%">
    <tr>
        <th><div align="left">ID</div></th>
        <th><div align="left">Use No.</div></th>
    </tr>
    @if($count_data > 0)
        @foreach($data as $field)
            <tr>
                <td class="text-center" rowspan="2">{{ $field->$primaryKey }}</td>
                <td>{{ $field->store_name_th }} @if(!empty($field->store_name_en)) ( {{ $field->store_name_en }} ) @endif</td>
            </tr>
            <tr>

                <?php

                $data_order = DB::table('orders')
                        ->leftjoin('orders_detail','orders_detail.orders_id','=','orders.orders_id')
                        ->leftjoin('product','product.product_id','=','orders_detail.product_id')
//                                            ->select('orders.table_no',DB::raw('SUM(orders_detail.price) as price'))
                        ->select('orders.table_no')
                        ->where('product.store_id',$field->$primaryKey);
                //                                            ->groupBy('orders.table_no');
                $count = $data_order->count();
                $data_order = $data_order->get();

                ?>

                <td>
                    <table BORDER="1" width="100%">

                        @if($count > 0)
                            @foreach($data_order as $value)

                                <tr>
                                    @if($table != $value->table_no)
                                        <td colspan="3"><li class="btn btn-circle green btn-xs"><i class="fa fa-tablet"></i>โต๊ะ {{ $value->table_no }}</li></td>
                                </tr>
                                <?php
                                $total= 0 ;
                                $table = $value->table_no;
                                $order = DB::table('orders')
                                        ->rightjoin('orders_detail','orders_detail.orders_id','=','orders.orders_id')
                                        ->select('orders_detail.price','orders_detail.product_name_th','orders_detail.product_name_en','orders_detail.qty','orders_detail.option_th','orders_detail.option_en')
                                        ->where('table_no',$value->table_no)
                                        ->get();
                                ?>
                                <tr>
                                    <td><strong>Product Name</strong></td>
                                    <td class="text-center"><strong>Qty</strong></td>
                                    <td><strong>Price</strong></td>
                                </tr>
                                @foreach($order as $value2)
                                    <tr>
                                        <?php   $total += $value2->price;   ?>

                                        <td>{{ $value2->product_name_th }}
                                            @if(!empty($value2->product_name_en ))
                                                ( {{ $value2->product_name_en }} )
                                            @endif
                                            {{--<ul>--}}
                                            {{--<li>{{ $value2->option_th }}--}}
                                            {{--@if(!empty($value2->option_en ))--}}
                                            {{--( {{ $value2->option_en }} )--}}
                                            {{--@endif--}}
                                            {{--</li>--}}
                                            {{--</ul>--}}
                                        </td>

                                        <td class="text-center">{{ $value2->qty }}</td>
                                        <td>{{ number_format($value2->price) }} บาท</td>
                                    </tr>

                                @endforeach
                                <tr>
                                    <td class="text-right" colspan="2"><strong>รวมราคา</strong></td>
                                    <td>{{ number_format($total) }} บาท</td>
                                </tr>
                                @endif



                            @endforeach
                        @else

                            <tr>
                                <td colspan="2" class="text-center">ไม่มีข้อมูล</td>
                            </tr>

                        @endif

                    </table>

                </td>
            </tr>
        @endforeach
    @else
        <tr><td colspan='6' class='text-center'>No Result.</td></tr>
    @endif
</TABLE>
</body>
</html>