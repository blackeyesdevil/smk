<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$order_by = Input::get('order_by');
$sort_by = Input::get('sort_by');

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);
$a_param_sort = Input::except(['order_by','sort_by']);
$str_param_sort = $obj_fn->parameter($a_param_sort);

?>
@extends('backend.layout.main-layout')

@section('page-style')
@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $page_title }}
@endsection
@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="form-search">
                <form action="{{ url()->to($path) }}" class="form-horizontal" method="GET">
                    <div class="form-group">
                        <label class="control-label col-md-1">Search</label>
                        <div class="col-md-3">
                            <input class="form-control" type="text" name="search" value="{{ Input::get('search') }}">
                            <span class="help-block">Search by Name</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-3 ">
                            <button class="btn blue btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                </form>
                <hr>
            </div>

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold">Found {{ $count_data }} Record(s).</span>
                </div>
                <div class="text-right">
                    <div><a href="{{URL::to('_admin/report_store?&export=true'.$str_param)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel</button></a></div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center col-sm-1">{!! $obj_fn->sorting('ID',$primaryKey,$order_by,$sort_by,$str_param_sort,'') !!}</th>
                            <th>{!! $obj_fn->sorting('Store Name','store_name_th',$order_by,$sort_by,$str_param_sort,'') !!}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($count_data > 0)
                            @foreach($data as $key => $field)
                                <tr>
                                    <td class="text-center" rowspan="2">{{ $field->$primaryKey }}</td>
                                    <td>{{ $field->store_name_th }} @if(!empty($field->store_name_en)) ( {{ $field->store_name_en }} ) @endif</td>
                                </tr>
                                <tr>

                                    <?php

                                    $data_order = DB::table('orders')
                                            ->leftjoin('orders_detail','orders_detail.orders_id','=','orders.orders_id')
                                            ->leftjoin('product','product.product_id','=','orders_detail.product_id')
//                                            ->select('orders.table_no',DB::raw('SUM(orders_detail.price) as price'))
                                            ->select('orders.table_no')
                                            ->where('product.store_id',$field->$primaryKey);
                                    //                                            ->groupBy('orders.table_no');
                                    $count = $data_order->count();
                                    $data_order = $data_order->get();

                                    ?>

                                    <td>
                                        <table class="table table-striped table-bordered table-hover">

                                            @if($count > 0)
                                                @foreach($data_order as $value)

                                                    <tr>
                                                        @if($table != $value->table_no)
                                                            <td colspan="3"><li class="btn btn-circle green btn-xs"><i class="fa fa-tablet"></i>โต๊ะ {{ $value->table_no }}</li></td>
                                                    </tr>
                                                    <?php
                                                    $total= 0 ;
                                                    $table = $value->table_no;
                                                    $order = DB::table('orders')
                                                            ->rightjoin('orders_detail','orders_detail.orders_id','=','orders.orders_id')
                                                            ->select('orders_detail.price','orders_detail.product_name_th','orders_detail.product_name_en','orders_detail.qty','orders_detail.option_th','orders_detail.option_en')
                                                            ->where('table_no',$value->table_no)
                                                            ->get();
                                                    ?>
                                                    <tr>
                                                        <td><strong>Product Name</strong></td>
                                                        <td class="text-center"><strong>Qty</strong></td>
                                                        <td><strong>Price</strong></td>
                                                    </tr>
                                                    @foreach($order as $value2)
                                                        <tr>
                                                            <?php   $total += $value2->price;   ?>

                                                            <td>{{ $value2->product_name_th }}
                                                                @if(!empty($value2->product_name_en ))
                                                                    ( {{ $value2->product_name_en }} )
                                                                @endif
                                                                {{--<ul>--}}
                                                                    {{--<li>{{ $value2->option_th }}--}}
                                                                        {{--@if(!empty($value2->option_en ))--}}
                                                                            {{--( {{ $value2->option_en }} )--}}
                                                                        {{--@endif--}}
                                                                    {{--</li>--}}
                                                                {{--</ul>--}}
                                                            </td>

                                                            <td class="text-center">{{ $value2->qty }}</td>
                                                            <td>{{ number_format($value2->price) }} บาท</td>
                                                        </tr>

                                                    @endforeach
                                                    <tr>
                                                        <td class="text-right" colspan="2"><strong>รวมราคา</strong></td>
                                                        <td>{{ number_format($total) }} บาท</td>
                                                    </tr>
                                                    @endif



                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="2" class="text-center">ไม่มีข้อมูล</td>
                                                </tr>

                                            @endif

                                        </table>

                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="6">No Result.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                {!! $data->appends(Input::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-plugin')
@endsection
@section('more-script')
@endsection
