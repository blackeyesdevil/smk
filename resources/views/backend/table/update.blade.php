<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')
@endsection
@section('more-style')
    <style media="screen">
        .table_map {
            width: 100%;
            background-image: url(../../../images/table_plan.png);
            background-size: 100% 100%;
            background-repeat: no-repeat;
            border: 1px solid black;
            position: relative;
        }
        .coords {
            width: 100px;
            height: 20px;
            background-color: rgba(0, 0, 0, 0.5);
            color: white;
            text-align: center;
            position: absolute;
        }
        #pin {
            width: 20px;
            height: 30px;
            position: absolute;
        }
    </style>
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">

                <!-- Table Map-->
                <div class="col-md-12" style="margin-bottom: 40px;">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="table_map" id="table_map">
                            <img id="pin" src="{{ URL::asset('images/map_pin.png') }}">
                            <div class="coords" id="coords">0, 0</div>
                        </div>
                    </div>
                </div>

                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Zone</label>
                            <div class="col-md-4">
                                <select class="form-control" name="zone_id" id="er">
                                    @foreach($zone as $value)
                                        <option value="{{ $value->zone_id }}" @if($zone_id==$value->zone_id) selected @endif>{{ $value->zone_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Available</label>
                            <div class="col-md-4">
                                <select class="form-control" name="is_available">
                                    <option value="0" @if($is_available==0) selected @endif> Unavailable </option>
                                    <option value="1" @if($is_available==1) selected @endif> Available </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Table No.</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="table_no" value="{{ $table_no }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Coordinate X</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="coordinate_x" id="coordinate_x" value="{{ $coordinate_x }}" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Coordinate Y</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="coordinate_y" id="coordinate_y" value="{{ $coordinate_y }}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" id="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}

    {{ Html::script('assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/table/validation.js') }}
    {{ Html::script('js/backend/table/more.js') }}
    <script>
        FormValidation.init();
        ComponentsFormTools.init();
    </script>
@endsection
