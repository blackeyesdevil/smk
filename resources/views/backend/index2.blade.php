<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new DashboardController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;

?>


@extends('admin')

@section('css')
    <!-- <link rel="stylesheet" href="{{ URL::asset('css/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.fileupload-ui.css') }}"> -->
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="{{ URL::asset('css/jquery.fileupload-noscript.css') }}"></noscript>
    <noscript><link rel="stylesheet" href="{{ URL::asset('css/jquery.fileupload-ui-noscript.css') }}"></noscript>
@endsection

<style>
    .block-1{
        width:250px;
        height:110px;
        margin: 0 auto 10px;
        border-radius: 5px;
    }
    .padding-5{
        padding:5px;
    }
    .padding-15{
        padding:15px;
    }
    .text-26{
        font-size: 26px;
    }
    .text-16{
        font-size: 16px;
    }
    .white{
        color:white;
    }
    .dashboard-stat2{
        padding: 15px !important;
    }
    .dashboard-stat2 .display{
        margin: 0 !important;
    }

</style>

@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>ยินดีต้อนรับค่ะ</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- SUMMARY BLOCK -->
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-green-sharp">
                                        <span data-counter="counterup" data-value="{{ $count_product }}">{{ number_format($count_product, 0) }}</span>
                                    </h3>
                                    <small>TOTAL PRODUCTS</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-pie-chart"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-red-haze">
                                        <span data-counter="counterup" data-value="{{ $total_order }}">{{ number_format($total_order, 0) }}</span>
                                    </h3>
                                    <small>NEW ORDERS</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-like"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-blue-sharp">
                                        <span data-counter="counterup" data-value="{{ $count_paid }}">{{ number_format($count_paid, 0) }}</span>
                                    </h3>
                                    <small>PAID ORDERS</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-basket"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-purple-soft">
                                        @if ($total_price != '')
                                            <span data-counter="counterup" data-value="{{ $total_price }}">{{ number_format($total_price) }}</span>
                                        @else
                                            <span data-counter="counterup" data-value="0">0</span>
                                        @endif
                                    </h3>
                                    <small>AVERAGE VALUE PER ORDER</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-user"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
    					<!-- BEGIN PORTLET-->
    					<div class="portlet light ">
    						<div class="portlet-title">
    							<div class="caption caption-md">
    								<span class="caption-subject theme-font bold uppercase">Product Top View</span>
    							</div>
    						</div>
    						<div class="portlet-body" style="padding-top: 0;">
    							<div class="table-scrollable table-scrollable-borderless">
    								<table class="table table-hover table-light">
    								    <thead>
                                            <tr class="uppercase">
                                                <th class="text-center">No.</th>
                                                <th>Name</th>
                                                <th class="text-center">Views</th>
          								    </tr>
        								</thead>

                                        <tbody>
                                        @foreach($get_product_views as $key => $product_view)
    								        <tr>
                                                <td class="text-center">
                                                  {{ $key+1 }}
                                                </td>
              									<td>
              									  {{ $product_view->product_name_th }}
              									</td>
              									<td class="text-center">
                                                  {{ $product_view->view }}
  									            </td>
    								        </tr>
                                        @endforeach
                                        </tbody>
    								</table>
    							</div>
    						</div>
    					</div>
    					<!-- END PORTLET-->
    				</div>

                    <div class="col-md-6">
    					<!-- BEGIN PORTLET-->
    					<div class="portlet light">
    						<div class="portlet-title">
    							<div class="caption caption-md">
    								<span class="caption-subject theme-font bold uppercase">Product Top Seller</span>
    							</div>
    						</div>
    						<div class="portlet-body" style="padding-top: 0;">
    							<div class="table-scrollable table-scrollable-borderless">
    								<table class="table table-hover table-light">
        								<thead>
                                            <tr class="uppercase">
              									<th class="text-center">No.</th>
              									<th>name</th>
                                                <th>size</th>
                                                <th>Sold</th>
          								    </tr>
        								</thead>
                                        <tbody>
                                            <?php
                                                   $get_product_sells = json_decode($get_product_sells);
                                            ?>
                                            @foreach($get_product_sells as $key => $product_sell)
            								<tr>
                                                <td class="text-center">
                                                  {{ $key+1 }}
                                                </td>
              									<td>
              									  {{ $product_sell->product_name_th }}
              									</td>
                                                <td>
          									      {{ $product_sell->option_name_th}}
          									    </td>
              									<td>
                                                  {{ $product_sell->view }}
              									</td>
            								</tr>
                                            @endforeach
                                        </tbody>
    								</table>
    							</div>
    						</div>
    					</div>
    					<!-- END PORTLET-->
    				</div>
                </div>

                <div class="clearfix"></div>

				<!-- BEGIN PORTLET-->
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption caption-md">
							<!-- <i class="icon-bar-chart theme-font hide"></i> -->
							<span class="caption-subject theme-font bold uppercase">Member Top Buying</span>
							<!-- <span class="caption-helper hide">weekly stats...</span> -->
						</div>
					</div>
					<div class="portlet-body" style="padding-top: 0;">
						<div class="table-scrollable table-scrollable-borderless">
							<table class="table table-hover table-light">
							    <thead>
							        <tr class="uppercase">
        								<th class="text-center">No.</th>
        								<th colspan="3" class="text-center">MEMBER</th>
        								<th class="text-center">TOTAL</th>
        							</tr>
							    </thead>
                                <tbody>
                                        @if(!is_array($success_buy) && !is_object($success_buy))
                                            <?php $success_buy = json_decode($success_buy); ?>
                                        @endif

                                        <?php $number = 1 ?>
                                        @foreach($success_buy as $key => $value)
                                            @if($number <= 10)
            								<!-- <td class="fit">
            									<img class="user-pic" src="../../assets/admin/layout3/img/avatar4.jpg">
            								</td> -->
                                                <td class="text-center">{{ $number }}</td>
    								            <td>{{ $value->membername }}</td>
    							                <td colspan="5" class="text-center">{{ number_format($value->price, 2) }} THB</td>
    					                </tr>
                                        @endif
                                        <?php $number = $number + 1;?>
                                    @endforeach
                                </tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END PORTLET-->

                <!-- BEGIN PORTLET-->
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<span class="caption-subject theme-font bold uppercase">SALES SUMMARY REPORT</span>
						</div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_actions_daily" data-toggle="tab"> Daily</a>
                            </li>
                            <li>
                                <a href="#tab_actions_monthly" data-toggle="tab"> Monthly</a>
                            </li>
                            <li>
                                <a href="#tab_actions_yearly" data-toggle="tab"> Yearly</a>
                            </li>
                        </ul>
					</div>
					<div class="portlet-body" style="padding-top: 0;">
						<div class="tab-content">
						    <div class="tab-pane active" id="tab_actions_daily">
                                <div id="dashboard_amchart_1" class="CSSAnimationChart"></div>
                            </div>

						    <div class="tab-pane" id="tab_actions_monthly">
                                <div id="dashboard_amchart_2" class="CSSAnimationChart"></div>
                            </div>

                            <div class="tab-pane" id="tab_actions_yearly">
                                <div id="dashboard_amchart_3" class="CSSAnimationChart"></div>
                            </div>
						</div>
					</div>
				</div>
				<!-- END PORTLET-->
			</div>
            <!-- END PAGE CONTENT INNER -->
        </div>
        <!-- END PAGE CONTENT -->
@endsection

@section('js')
<script src="{{ URL::asset('assets/global/plugins/amcharts/amcharts.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/global/plugins/amcharts/serial.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/global/plugins/amcharts/themes/light.js') }}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            var Dashboard = function(){
                return {
                    initAmChart1: function() {
                        if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_1').size() === 0) {
                            return;
                        }

                        var chart = AmCharts.makeChart("dashboard_amchart_1", {
                            "type": "serial",
                            "addClassNames": true,
                            "theme": "light",
                            "path": "assets/global/plugins/amcharts/ammap/images/",
                            "autoMargins": false,
                            "marginLeft": 100,
                            "marginRight": 8,
                            "marginTop": 10,
                            "marginBottom": 26,
                            "balloon": {
                                "adjustBorderColor": false,
                                "horizontalPadding": 10,
                                "verticalPadding": 8,
                                "color": "#ffffff"
                            },

                            "dataProvider": [
                                <?php $i=0; ?>
                                @foreach ($daily_sales as $key => $sale)
                                {
                                    "date": "{{ $sale['date'] }}",
                                    "revenue": {{ $sale['sum'] }}
                                }
                                @if ($i!=count($daily_sales)-1),@endif
                                <?php $i++; ?>
                                @endforeach
                            ],
                            "valueAxes": [{
                                "axisAlpha": 0,
                                "position": "left"
                            }],
                            "startDuration": 1,
                            "graphs": [{
                                "alphaField": "alpha",
                                "balloonText": "<span style='font-size:12px;'>[[title]] on [[category]]:<br><span style='font-size:20px;'>[[value]]</span> THB</span>",
                                "fillAlphas": 1,
                                "title": "REVENUE",
                                "type": "column",
                                "valueField": "revenue",
                                "dashLengthField": "dashLengthColumn"
                            }],
                            "categoryField": "date",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "axisAlpha": 0,
                                "tickLength": 0
                            },
                            "export": {
                                "enabled": true
                            }
                        });
                    },
                    initAmChart2: function() {
                        if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_2').size() === 0) {
                            return;
                        }

                        var chart = AmCharts.makeChart("dashboard_amchart_2", {
                            "type": "serial",
                            "addClassNames": true,
                            "theme": "light",
                            "path": "assets/global/plugins/amcharts/ammap/images/",
                            "autoMargins": false,
                            "marginLeft": 100,
                            "marginRight": 8,
                            "marginTop": 10,
                            "marginBottom": 26,
                            "balloon": {
                                "adjustBorderColor": false,
                                "horizontalPadding": 10,
                                "verticalPadding": 8,
                                "color": "#ffffff"
                            },

                            "dataProvider": [
                                <?php $i=0; ?>
                                @foreach ($monthly_sales as $key => $sale)
                                {
                                    "month": "{{ $sale['month'] }}",
                                    "revenue": {{ $sale['sum'] }}
                                }
                                @if ($i!=count($monthly_sales)-1),@endif
                                <?php $i++; ?>
                                @endforeach
                            ],
                            "valueAxes": [{
                                "axisAlpha": 0,
                                "position": "left"
                            }],
                            "startDuration": 1,
                            "graphs": [{
                                "alphaField": "alpha",
                                "balloonText": "<span style='font-size:12px;'>[[title]] on [[category]]:<br><span style='font-size:20px;'>[[value]]</span> THB</span>",
                                "fillAlphas": 1,
                                "title": "REVENUE",
                                "type": "column",
                                "valueField": "revenue",
                                "dashLengthField": "dashLengthColumn"
                            }],
                            "categoryField": "month",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "axisAlpha": 0,
                                "tickLength": 0
                            },
                            "export": {
                                "enabled": true
                            }
                        });
                    },
                    initAmChart3: function() {
                        if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_3').size() === 0) {
                            return;
                        }

                        var chart = AmCharts.makeChart("dashboard_amchart_3", {
                            "type": "serial",
                            "addClassNames": true,
                            "theme": "light",
                            "path": "assets/global/plugins/amcharts/ammap/images/",
                            "autoMargins": false,
                            "marginLeft": 100,
                            "marginRight": 8,
                            "marginTop": 10,
                            "marginBottom": 26,
                            "balloon": {
                                "adjustBorderColor": false,
                                "horizontalPadding": 10,
                                "verticalPadding": 8,
                                "color": "#ffffff"
                            },

                            "dataProvider": [
                                <?php $i=0; ?>
                                @foreach ($yearly_sales as $key => $sale)
                                {
                                    "year": "{{ $sale['year'] }}",
                                    "revenue": {{ $sale['sum'] }}
                                }
                                @if ($i!=count($yearly_sales)-1),@endif
                                <?php $i++; ?>
                                @endforeach
                            ],
                            "valueAxes": [{
                                "axisAlpha": 0,
                                "position": "left"
                            }],
                            "startDuration": 1,
                            "graphs": [{
                                "alphaField": "alpha",
                                "balloonText": "<span style='font-size:12px;'>[[title]] on [[category]]:<br><span style='font-size:20px;'>[[value]]</span> THB</span>",
                                "fillAlphas": 1,
                                "title": "REVENUE",
                                "type": "column",
                                "valueField": "revenue",
                                "dashLengthField": "dashLengthColumn"
                            }],
                            "categoryField": "year",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "axisAlpha": 0,
                                "tickLength": 0
                            },
                            "export": {
                                "enabled": true
                            }
                        });
                    },
                    init: function() {
                        this.initAmChart1();
                        this.initAmChart2();
                        this.initAmChart3();
                    }
                }
            }();

            Dashboard.init();
        });
    </script>
@endsection
