<?php
use Illuminate\Support\Facades\Input;
use App\Library\MainFunction;
use App\Models\Orders;

$mainFn = new MainFunction(); // New Object Main Function
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;

if (Input::get('mode') == 'export') {
    $filename = "excel_report_category.xls";
    header("Content-Type: application/vnd.ms-excel; charset=utf-8");
    header("Content-Disposition: attachment; filename=".$filename);
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);
}

?>
<html>
<head>
    <meta charset="UTF-8">
    <style type="text/css">
        table {
            border-collapse: collapse;
            width: 100%;
            table-layout: fixed;
        }
        th,td {
            border: 1px solid black;
            padding: 1px 3px;
            table-layout: fixed;
            white-space: nowrap;
            overflow: hidden;
            text-overflow:ellipsis;
        }
        body {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        * {
            font-size: 12px;
            /*font-family: "THSarabunNew"; */
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }
        .page {
            width: 210mm;
            min-height: 297mm;
            padding:10mm 10mm 10mm 10mm;
            margin: 10mm auto;
            border: 1px #D3D3D3 solid;
            border-radius: 5px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        }
        .subpage {
            padding: 0cm;
            height: 277mm;
        }
        .content {
            width: 100%;
            height: 100%;
        }.header_report {
            width: 100%;
        }
        @page {
            size: A4;
            margin: 0;
        }
        @media print {
            html, body {
                width: 210mm;
                height: 297mm;
            }
            .page {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        }
        @media print{
            .content{
                page-break-before: always;
            }
        }
    </style>
</head>
<body onload="window.print()">
    <div class = "book">
        @if($count_data > 0)
            <?php 
                $count=1; 
                $all_count_order = 0;
                $all_qty = 0;
                $all_total = 0;
                $all_discount = 0;
                $all_nat = 0;
            ?>
            @foreach($data as $field)
                <?php 
                    $sum_discount = Orders::where('status','=','1')
                                            ->where('created_at','like',$field->date.' %:%:%')
                                            ->sum('discount_price');
                    $all_count_order += $field->count_order;
                    $all_qty += $field->sum_qty;
                    $all_total += $field->sum_price;
                    $all_discount += $sum_discount;
                    $all_nat += $field->sum_price-$sum_discount;
                ?>
                @if ($count==1)
                    <div class="page">
                        <div class="subpage">
                            <div class="content">
                                @include('backend.include.report_header')
                                <br>
                                <table>
                                    <tr>
                                        <th align='left' colspan='4'>Category</th>
                                        <th align='center' colspan='1'>Number of Bill</th>
                                        <th align='center' colspan='1'>Quantity</th>
                                        <th align='right' colspan='2'>Gross Sales</th>
                                        <th align='right' colspan='2'>Discount</th>
                                        <th align='right' colspan='2'>Net Sales</th>
                                    </tr>
                @endif
                                    <tr>
                                        <td align='left' colspan='4'> 
                                            @foreach($category as $cate)
                                                @if($cate->category_id == $field->category_id)
                                                    {{$cate->category_name_th}} ( {{$cate->category_name_en}} )
                                                @endif
                                            @endforeach
                                        </td>
                                        <td align='center' colspan='1'>{{ $field->count_order }}</td>
                                        <td align='center' colspan='1'>{{ $field->sum_qty }}</td>
                                        <td align='right' colspan='2'>{{ number_format($field->sum_price,2) }}</td>
                                        <td align='right' colspan='2'> {{ number_format($sum_discount,2) }} </td> <!--Discount -->
                                        <td align='right' colspan='2'> {{ number_format($field->sum_price-$sum_discount,2) }} </td> <!-- Net sales -->
                                    </tr>
                @if ($count==30)
                                </table>
                            </div><!--end content-->
                        </div><!--end subpage-->
                    </div><!--end page-->
                    <?php $count=0; ?>
                @endif
                <?php $count++; ?>
            @endforeach
            <tr>
                <th align='left' colspan='4'> รวม </th>
                <th align='center' colspan='1'>{{ $all_count_order }}</th>
                <th align='center' colspan='1'>{{ $all_qty }}</th>
                <th align='right' colspan='2'>{{ $all_total }}</th>
                <th align='right' colspan='2'>{{ $all_discount }}</th>
                <th align='right' colspan='2'>{{ $all_nat }}</th>
            </tr>
        @else
            <div class="page">
                <div class="subpage">
                    <div class="content">
                        <!-- <div class="text-head" align="center"><b>Items Sold Report</b></div>
                        <div class="text-head" align="center"><b>My Kitchen</b></div>
                        <div class="text-head" align="center"><b>From {{ $obj_fn->format_date_en(Input::get('from_date'),8) }} Through {{ $obj_fn->format_date_en(Input::get('to_date'),8) }} </b></div>
                        <div class="text-head" align="center">Report was run on {{ date("F d, Y H:i") }}</div> -->
                        @include('backend.include.report_header')
                        <table>
                            <tr>
                                <th align='left' colspan='4'>Category</th>
                                <th align='center' colspan='1'>Number of Bill</th>
                                <th align='center' colspan='1'>Quantity</th>
                                <th align='right' colspan='2'>Gross Sales</th>
                                <th align='right' colspan='2'>Discount</th>
                                <th align='right' colspan='2'>Net Sales</th>
                            </tr>
                            <tr>
                                <td colspan='12'>No Result.</td>
                            </tr>
                        </table>
                    </div><!--end content-->
                </div><!--end subpage-->
            </div><!--end page-->
        @endif
    </div><!--end book-->
</body>
</html>
