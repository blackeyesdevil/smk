<?php
// file : app/config/constants.php

return [
    'APP_NAME' => 'My Kitchen',
    'PER_PAGE' => '25',
    // 'NO_PIC' => 'http://dummyimage.com/600x400/e8e8e8/ffffff.jpg?text=My%20Kitchen',
    'NO_PIC' => 'http://smk.bkksol.com/images/no-pic.png',
    'PATH_IMG_PRODUCT' => 'uploads/product/',
    'STATUS' => ['0'=>'รอการชำระเงิน','1'=>'ชำระเงินแล้ว','2'=>'ยกเลิก','99'=>'เสร็จสมบูรณ์']
];
