<?php
namespace App\Library;
use DB;
use Request;
use URL;
use Intervention\Image\ImageManagerStatic as Image;
use Config;
use Storage;
use App\Models\Permission;

class MainFunction {

    public function permission($page_id,$act){
        $role_id = session()->get('s_admin_role_id');

        $data = Permission::where('page_id',$page_id)
            ->where('admin_role_id',$role_id)
            ->first();
        if(empty($data)){
            return abort(404); // 404 page not found.
        }

        $create = $data->c; $read = $data->r; $update = $data->u; $delete = $data->d;
        if( ($act == 'c' && $create == '0') || (($act == 'r' && $read == '0') && ($act == 'r' && $update == '0')) || ($act == 'u' && $update == '0') || ($act == 'd' && $delete == '0') ){
            return abort(403); // 403 permission denied.
        }

        $result = ['c' => $create, 'r' => $read, 'u' => $update, 'd' => $delete];
        return $result;
    }

    public function db_add($objData,$pkField,$input,$filedList)
    {
        foreach($filedList as $field) {
            $objData->$field = $input[$field];
        }
        $objData->save();
        return $objData->$pkField;
    }
    public function db_update($objData,$pkField,$input,$filedList)
    {
        foreach($filedList as $field) {
            $objData->$field = $input[$field];
        }
        $objData->save();
        return $objData->$pkField;
    }

    public function db_delete($objData)
    {
        $objData->deleted_at = date('Y-m-d H:i:s');
        $objData->save();
    }
    public function sorting($txtField,$field,$orderBy,$sortBy,$strParam,$alt)
    {
        if($field == $orderBy && $sortBy == 'desc'){
            $sortBy = "asc"; $icon = "<i class='fa fa-sort-desc'></i>";
        } else if($field == $orderBy && $sortBy == 'asc') {
            $sortBy = "desc"; $icon = "<i class='fa fa-sort-asc'></i>";
        } else {
            $sortBy = "desc"; $icon = "";
        }

        return "<a href='".URL::to(Request::path())."?order_by=".$field."&sort_by=".$sortBy.$strParam."' title='".$alt."'>$txtField $icon</a>";
    }
    public function parameter($a_input)
    {
        $strParam = '';
        foreach($a_input as $key => $values){
            if(!empty($values)) $strParam .= '&'.$key.'='.$values;
        }
        return $strParam;
    }

    public function loop_category($query,$lang) {
        $check = 0;
        $selCategory = $query;
        foreach($selCategory as $fieldCate)
        {
            $category_name = $fieldCate->category_name;
            $path = $category_name;
            $parent = $fieldCate->parent_category_id;
            while($parent != 0)
            {
                $query2 = DB::table('category')
                    ->join('category_tr','category.category_id','=','category_tr.category_id')
                    ->select('category.category_id','category.parent_category_id','category_tr.category_name')
                    ->whereNull('category.deleted_at')
                    ->whereNull('category_tr.deleted_at')
                    ->where('category_tr.lang',$lang)
                    ->where('category.category_id',$parent)
                    ->orderBy('category_tr.category_name','asc')->get();

                foreach($query2 as $fieldSubCate)
                {
                    $category_name2 = $fieldSubCate->category_name;
                    // New path and parent
                    $path = $category_name2 ." > " . $path ;
                    $parent = $fieldSubCate->parent_category_id;
                }
            }
            $list_cat[$fieldCate->category_id] = $path;
        }
        // เรียงค่าใน array จากน้อยไปมาก
        asort ($list_cat);
        //return $list_cat;
        return $list_cat;
    }

    public function list_category_photo($category_id,$lang){
        $list_category_value = "";
        $query = DB::table('category')
            ->join('category_tr','category.category_id','=','category_tr.category_id')
            ->select('category.category_id','category.parent_category_id','category_tr.category_name')
            ->whereNull('category.deleted_at')->whereNull('category_tr.deleted_at')->where('category_tr.lang',$lang)->where('category.category_id',$category_id)->get();
        $list_cat = $this->loop_category($query,$lang);
        foreach($list_cat as $key => $value){
            $list_category_value .= $value;
        }
        return $list_category_value;
    }
    public function list_category($category_id,$lang){
        $list_category_value = "";
        $query = DB::table('category')
            ->join('category_tr','category.category_id','=','category_tr.category_id')
            ->select('category.category_id','category.parent_category_id','category_tr.category_name')
            ->whereNull('category.deleted_at')->whereNull('category_tr.deleted_at')->where('category_tr.lang',$lang)->where('category.category_id',$category_id)->get();
        $list_cat = $this->loop_category($query,$lang);
        foreach($list_cat as $key => $value){
            $list_category_value .= $value;
        }
        return $list_category_value;
    }

    public function search_select_category($search_category_id,$category_id,$root,$lang){
        // $search_category_id from search select box
        // $root 0 is parent_category_id=0
        $option = '';
        $query = DB::table('category')
            ->join('category_tr','category.category_id','=','category_tr.category_id')
            ->select('category.category_id','category.parent_category_id','category_tr.category_name')
            ->whereNull('category.deleted_at')->whereNull('category_tr.deleted_at')->where('category_tr.lang',$lang);
        if(!empty($category_id))
        {
            $query = $query->where('category.category_id','!=',$category_id);
        }
        if(!empty($root) || $root=='0')
        {
            $query = $query->where('category.parent_category_id','0');
        }
        $query = $query->orderBy('category_tr.category_name','asc')->get();

        $list_cat = $this->loop_category($query,$lang);
        foreach($list_cat as $key => $value){
            $select = "";
            if($search_category_id == $key ) $select = "selected";
            $option .= "<option value=\"".$key."\" ".$select.">".$value."</option>";
        }
        return $option;
//        return $list_cat;
    }

    public function month_list($selected,$type = null,$lang){
        // $type == full : Full month
        $list_menu = "";
        if($lang == 'TH')
        {
            $list_menu .= "<option value=\"\">เดือน</option>";
            if($type == 'full')
            {
                $month = array(1 => "มกราคม",2 => "กุมภาพันธ์",3 => "มีนาคม",4 => "เมษายน",5 => "พฤษภาคม",6 => "มิถุนายน",7 => "กรกฎาคม",8 => "สิงหาคม",9 => "กันยายน",10 => "ตุลาคม",11 => "พฤศจิกายน",12 => "ธันวาคม");
            }
            else
            {
                $month = array(1 => "ม.ค.",2 => "ก.พ.",3 => "มี.ค.",4 => "เม.ย.",5 => "พ.ค.",6 => "มิ.ย.",7 => "ก.ค.",8 => "ส.ค.",9 => "ก.ย.",10 => "ต.ค.",11 => "พ.ย.",12 => "ธ.ค.");
            }
        }
        else
        {
            $list_menu .= "<option value=\"\">Month</option>";
            if($type == 'full')
            {
                $month = array(1 => "January",2 => "February",3 => "March",4 => "April",5 => "May",6 => "June",7 => "July",8 => "August",9 => "September",10 => "October",11 => "November",12 => "December");
            }
            else
            {
                $month = array(1 => "Jan",2 => "Feb",3 => "Mar",4 => "Apr",5 => "May",6 => "Jun",7 => "Jul",8 => "Aug",9 => "Sept",10 => "Oct",11 => "Nov",12 => "Dec");
            }
        }

        foreach($month as $key => $value){
            if($selected == $key) $val = "selected='selected'"; else $val = "";
            $list_menu .= "<option value=\"$key\" $val >$value</option>";
        }
        return 	$list_menu;
    }

    public function day_list($selected,$lang){
        $list_menu = "";
        if($lang == 'TH') $list_menu .= "<option value=\"\">วัน</option>";
        else $list_menu .= "<option value=\"\">Day</option>";

        for($day=1;$day<=31;$day++){
            if ( $selected == $day ) $val = "selected='selected'"; else $val = "";
            $list_menu .= "<option value=\"$day\" $val >$day</option>";
        }
        return 	$list_menu;
    }

    public function gen_random ($length,$type){
        if($type == '1') $keychars = 'abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        else if($type == '2') $keychars = '123456789';
        else if($type == '3') $keychars = 'abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
        else if($type == '4') $keychars = 'abcdefghijkmnpqrstuvwxyz';
        else if($type == '5') $keychars = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
        else if($type == '6') $keychars = 'abcdefghijkmnpqrstuvwxyz23456789';
        else if($type == '7') $keychars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';

        // RANDOM KEY GENERATOR
        $randkey = "";
        $max=strlen($keychars)-1;
        for ($i=0;$i<=$length;$i++) {
            $randkey .= substr($keychars, rand(0, $max), 1);
        }
        return $randkey;
    }

    public function gen_order_no($table_no){
        $queryLast = DB::table('orders')->select('orders_no','round')->orderBy('orders_id','desc')->take('1')->first();

        $currentYear = substr(date('Y'),2,2);
        $currentMonth = date('m');
        $currentDay = date('d');

        if (empty($queryLast)){
            $newRandomNo = '00001';
            $newRound = 1;
        }else{
            $lastNo = $queryLast->orders_no;
            $lastRound =  $queryLast->round;

            $lastYear = substr($lastNo,0,2);
            $lastMonth = substr($lastNo,2,2);
            $lastDay = substr($lastNo,4,2);
            $lastRandomNo = substr($lastNo,6,5);

            if($lastMonth < $currentMonth || $lastYear < $currentYear){
                $newRandomNo = "00001";
            }else{
                $newRandomNo = $lastRandomNo+1;
                $newRandomNo = str_pad($newRandomNo,5,"0",STR_PAD_LEFT);
            }

            if($currentDay != $lastDay){
                $newRound = 1;
            }else{
                $queryRound = DB::table('orders')->select('round','table_no')->where('table_no',$table_no)->orderBy('orders_id','desc')->take('1')->first();
                if(empty($queryRound)){
                    $newRound = 1;
                }else{
                    $newRound = $queryRound->round + 1;
                }
            }
        }
        $newNo = substr(date('Y'),2,2).date("md").$newRandomNo;

        $result = ['orders_no' => $newNo, 'round' => $newRound];
        return $result;
    }
    public function gen_receipt_no(){
        $queryLast = DB::table('receipt')->select('receipt_no')->orderBy('receipt_id','desc')->take('1')->first();

        $currentYear = substr(date('Y'),2,2);
        $currentMonth = date('m');
        $currentDay = date('d');

        if (empty($queryLast)){
            $newRandomNo = '00001';
        }else{
            $lastNo = $queryLast->receipt_no;
            $lastYear = substr($lastNo,0,2);
            $lastMonth = substr($lastNo,2,2);
            $lastDay = substr($lastNo,4,2);
            $lastRandomNo = substr($lastNo,6,5);

            if($lastMonth < $currentMonth || $lastYear < $currentYear){
                $newRandomNo = "00001";
            }else{
                $newRandomNo = $lastRandomNo+1;
                $newRandomNo = str_pad($newRandomNo,5,"0",STR_PAD_LEFT);
            }
        }
        $newNo = substr(date('Y'),2,2).date("md").$newRandomNo;

        return $newNo;
    }
    public function format_date_en ($value,$type) {
        list ($s_date,$s_time)  = explode (" ", $value);
        list ($s_year, $s_month, $s_day) = explode ("-", $s_date);
        if(!empty($s_time)){
            list ($s_hour, $s_minute, $s_second) = explode (":", $s_time);
        }else{
            $s_time = "00:00:00";
            list ($s_hour, $s_minute, $s_second) = explode (":", $s_time);
        }
        $s_month +=0;
        $s_day += 0;
        if ($s_day == "0") return "";
        $mktime = mktime ($s_hour, $s_minute, $s_second, $s_month, $s_day, $s_year);
        switch ($type) {
            case "1" :  // Friday 11 November 2005
                $msg = date ("l d F Y", $mktime);
                break;
            case "2" :  // 11 Nov 05
                $msg = date ("d M y", $mktime);
                break;
            case "3" :  // Friday 11 November 2005 00:11
                $msg = date ("l d F Y H:i", $mktime);
                break;
            case "4" :  // 11 Nov 05 00:11
                $msg = date ("d M y  H:i", $mktime);
                break;
            case "5" :  // 11 Nov 2005
                $msg = date ("d M Y", $mktime);
                break;
            case "6" :  // Nov 11, 2005
                $msg = date ("M d, Y", $mktime);
                break;
            case "7" :  // 11 November 2005
                $msg = date ("d F Y", $mktime);
                break;
            case "8" :  // November 11, 2005 00:11
                $msg = date ("F d, Y H:i", $mktime);
                break;
        }
        return ($msg);
    }
    public function format_date_th ($value,$type) {
        if($value=='') return $value;
        if (strlen ($value) > 10) {
            list ($s_date,$s_time)  = explode (" ", $value);
            list ($s_year, $s_month, $s_day) = explode ("-", $s_date);
            list ($s_hour, $s_minute, $s_second) = explode (":", $s_time);
        }
        else
        {
            list ($s_year, $s_month, $s_day) = explode ("-", $value);
        }
        $s_month +=0;
        $s_day += 0;
        if ($s_day == "0") return "";
        $s_year += 543;
        $month_full_th = array ('','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
        $month_brief_th = array ('','ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');
        $day_of_week = array ('อาทิตย์','จันทร์','อังคาร','พุธ','พฤหัสบดี','ศุกร์','เสาร์');
        switch ($type) {
            case "1" : // วันที่ 4 พฤศจิกายน 2548
                $msg =  $s_day . " " .  $month_full_th[$s_month]  . " " .  $s_year ;
                break;
            case "2" :  // 4 พ.ย. 2548
                $msg =  $s_day . " " .  $month_brief_th[$s_month]  . " " .  $s_year ;
                break;
            case "3" :  // วันที่ 4 พฤศจิกายน 2548 เวลา 14.11 น.
                $msg =  $s_day . " " .  $month_full_th[$s_month]  . " " .  $s_year . " เวลา " . $s_hour . "." . $s_minute . " น." ;
                break;
            case "4" :  // 4 พ.ย. 2548 14.11 น.
                $msg =  $s_day . " " .  $month_brief_th[$s_month]  . " " .  $s_year . "  " . $s_hour . "." . $s_minute . " น." ;
                break;
        }
        return ($msg);

    }

    public function image_resize($photo , $destinationPath , $size,$filename)
    {
        $img = new Image();
        $set_width = $size;
        $set_height = $size;
        $normal_width = $img->make($photo)->width();
        $normal_height = $img->make($photo)->height();
        if($normal_width > $normal_height){
            // ความกว้างของรูปปกติ มากกว่า ความกว้างที่จะรีไซต์
            if($normal_width > $set_width){
                $new_width = $set_width;
                $new_height = round($normal_height * $set_width / $normal_width);
            }else{
                $new_width = $normal_width;
                $new_height = $normal_height;
            }
        }else{
            // ความสูงของรูปปกติ มากกว่า ความสูงที่จะรีไซต์
            if($normal_height > $set_height){
                $new_width = round($normal_width * $set_height / $normal_height);
                $new_height = $set_height;
            }else{
                $new_width = $normal_width;
                $new_height = $normal_height;
            }
        }
//        echo "--------------"."<br>";
//        echo $new_width." ".$new_height."<br>";
//        echo $destinationPath.'/'.$size.'/'.$filename;
        $img = new Image();
        $img->make($photo)->resize($new_width,$new_height)->save($destinationPath.'/'.$size.'/'.$filename);

    }

    public function img_full_resize($photo, $destinationPath, $filename)
    {
        $img = new Image();                                     // get Class Image
        $img->make($photo)->save($destinationPath . '/' . $filename);   // move image into storage

//        $a_resize = Config::get('mainConfig.a_resize');
//        foreach ($a_resize as $size) {
//            $this->image_resize($photo, $destinationPath,$size, $filename);
//        }
    }

    public function del_storage($old_path,$old_name)
    {
//        $a_resize = Config::get('mainConfig.a_resize');
        if($old_name != '' && file_exists($old_path.'/'.$old_name)){
            // Storage::files($old_path.'/'.$old_name);
            unlink($old_path.'/'.$old_name);
//            foreach($a_resize as $size) {
//                Storage::files($old_path.$size.'/'.$old_name);
//            }
        }
    }

    public static function get_time_ago($time_stamp)
    {
        $time_difference = strtotime('now') - $time_stamp;

        if ($time_difference >= 60 * 60 * 24 * 365.242199)
        {
            /*
             * 60 seconds/minute * 60 minutes/hour * 24 hours/day * 365.242199 days/year
             * This means that the time difference is 1 year or more
             */
            return MainFunction::get_time_ago_string($time_stamp, 60 * 60 * 24 * 365.242199, 'year');
        }
        elseif ($time_difference >= 60 * 60 * 24 * 30.4368499)
        {
            /*
             * 60 seconds/minute * 60 minutes/hour * 24 hours/day * 30.4368499 days/month
             * This means that the time difference is 1 month or more
             */
            return MainFunction::get_time_ago_string($time_stamp, 60 * 60 * 24 * 30.4368499, 'month');
        }
        elseif ($time_difference >= 60 * 60 * 24 * 7)
        {
            /*
             * 60 seconds/minute * 60 minutes/hour * 24 hours/day * 7 days/week
             * This means that the time difference is 1 week or more
             */
            return MainFunction::get_time_ago_string($time_stamp, 60 * 60 * 24 * 7, 'week');
        }
        elseif ($time_difference >= 60 * 60 * 24)
        {
            /*
             * 60 seconds/minute * 60 minutes/hour * 24 hours/day
             * This means that the time difference is 1 day or more
             */
            return MainFunction::get_time_ago_string($time_stamp, 60 * 60 * 24, 'day');
        }
        elseif ($time_difference >= 60 * 60)
        {
            /*
             * 60 seconds/minute * 60 minutes/hour
             * This means that the time difference is 1 hour or more
             */
            return MainFunction::get_time_ago_string($time_stamp, 60 * 60, 'hour');
        }
        else
        {
            /*
             * 60 seconds/minute
             * This means that the time difference is a matter of minutes
             */
            return MainFunction::get_time_ago_string($time_stamp, 60, 'minute');
        }
    }

    public static function get_time_ago_string($time_stamp, $divisor, $time_unit)
    {
        $time_difference = strtotime("now") - $time_stamp;
        $time_units      = floor($time_difference / $divisor);

        settype($time_units, 'string');

        if ($time_units === '0')
        {
            return 'less than 1 ' . $time_unit . ' ago';
        }
        elseif ($time_units === '1')
        {
            return '1 ' . $time_unit . ' ago';
        }
        else
        {
            /*
             * More than "1" $time_unit. This is the "plural" message.
             */
            // TODO: This pluralizes the time unit, which is done by adding "s" at the end; this will not work for i18n!
            return $time_units . ' ' . $time_unit . 's ago';
        }
    }

    public function parse_crontab($frequency='* * * * * *', $time=false) {
        $time = is_string($time) ? strtotime($time) : time();
        $time = explode(' ', date('i G j n w Y', $time));

        $crontab = explode(' ', $frequency);
        foreach ($crontab as $k => &$v) {
            $v = explode(',', $v);
            $regexps = array(
                '/^\*$/', # every
                '/^(\d+)$/', # digit
                '/^(\d+)\-(\d+)$/', # range
                '/^\*\/(\d+)$/' # every digit
            );
            $content = array(
                "true", # every
                "{$time[$k]} == $1", # digit
                "($1 <= {$time[$k]} && {$time[$k]} <= $2)", # range
                "{$time[$k]} % $1 == 0" # every digit
            );
            foreach ($v as &$v1)
                $v1 = preg_replace($regexps, $content, $v1);
            $v = '('.implode(' || ', $v).')';

        }

        $crontab = implode ('&&', $crontab);
        return eval ("return ('$crontab');");
    }

}
?>
