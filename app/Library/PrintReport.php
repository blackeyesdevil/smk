<?php
namespace App\Library;

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;

use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\Product;
use App\Models\ProductOption;
use App\Models\Store;
use App\Models\Receipt;
use App\Models\Collection;

class PrintReport {

    public function printEOD($ip) {
        $logo = EscposImage::load(public_path().'/images/logo-b.png');

        /* Justification */
        $justification = array(
            Printer::JUSTIFY_LEFT,
            Printer::JUSTIFY_CENTER,
            Printer::JUSTIFY_RIGHT);

        //Column Size
        $tb1 = ['c1'=>12 , 'c2'=>11, 'c3'=>9, 'c4'=>10];
        $tb2 = ['c1'=>29, 'c2'=>17];

        try {
            // Connect Printer
            $connector = new NetworkPrintConnector($ip, 9100);
            $printer = new Printer($connector);

            /* Initialize */
            $printer -> initialize();
            // $printer -> setJustification($justification[0]);
            // $printer -> text("123456789_123456789_123456789_123456789_123456789\n");

            /* Header */
            $printer -> setJustification($justification[1]);
            $printer -> graphics($logo);
            $printer -> feed(1);
            $printer -> setEmphasis(true);
            $printer -> text("End of Day Report\n");
            $printer -> setEmphasis(false);
            $printer -> text("As of ".date("d F Y")."\n");
            $printer -> text("Report was run on ".date("d F Y, H:i")."\n");
            $printer -> feed(1);
            $printer -> text("================================================\n");


            /* Product Sales by Category */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text("Product Sales by Category\n");
            $printer -> text(str_pad("Category",$tb1['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Gross Sales",$tb1['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Discount",$tb1['c3'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Net Sales",$tb1['c4'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $collections = Collection::all();
            $all_gross = 0; $all_discount = 0; $all_net = 0;
            foreach ($collections as $collection) {
                $printer -> text(str_pad(substr($collection->collection_name_en,0,$tb1['c1']),$tb1['c1'],' ',STR_PAD_RIGHT)
                            ."  ".str_pad(number_format("19000",2),$tb1['c2'],' ',STR_PAD_LEFT)
                            ."  ".str_pad(number_format("1550",2),$tb1['c3'],' ',STR_PAD_LEFT)
                            ."  ".str_pad(number_format(19000 - 1550,2),$tb1['c4'],' ',STR_PAD_LEFT)."\n");
                $all_gross += 19000; $all_discount += 1550; $all_net += 19000 - 1550;
            }
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("   Total Gross Sales",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_gross,2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Discount",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_discount,2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Net Sales",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_net,2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");

            /* Product Sales by Shop */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text("Product Sales by Shop\n");
            $printer -> text(str_pad("Shop",$tb1['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Gross Sales",$tb1['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Discount",$tb1['c3'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Net Sales",$tb1['c4'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $stores = Store::all();
            $all_gross = 0; $all_discount = 0; $all_net = 0;
            foreach ($stores as $store) {
                $printer -> text(str_pad(substr($store->store_name_en,0,$tb1['c1']),$tb1['c1'],' ',STR_PAD_RIGHT)
                            ."  ".str_pad(number_format("38000",2),$tb1['c2'],' ',STR_PAD_LEFT)
                            ."  ".str_pad(number_format("3100",2),$tb1['c3'],' ',STR_PAD_LEFT)
                            ."  ".str_pad(number_format(38000 - 3100,2),$tb1['c4'],' ',STR_PAD_LEFT)."\n");
                $all_gross += 38000; $all_discount += 3100; $all_net += 38000 - 3100;
            }
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("   Total Gross Sales",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_gross,2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Discount",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_discount,2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Net Sales",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_net,2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");


            /* Order Summary */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text("Order Summary\n");
            $printer -> text(str_pad("   Total Net Sales before TAX",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("130467.29",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Service Charges",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("11860.66",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Net Product Sales",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("118606.63",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Sales Tax",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("9132.71",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Ticket Sales",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("139600",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Open (Unpaid) Tickets",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("-",$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Closed (Tendered) Tickets",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("139600",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");

            /* Payment Group by Tender */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text("Payment Group by Tender\n");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("Cash",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("6500",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("Siam Gift Card",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("6300",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("Credit Card",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("126800",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("   Total Payment Received",$tb2['c1'],' ',STR_PAD_RIGHT )
                        ."  ".str_pad(number_format("139600",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");

            $printer -> feed();
            $printer -> cut();
            $printer -> close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }

    public function printEODTill($ip) {
        $logo = EscposImage::load(public_path().'/images/logo-b.png');

        /* Justification */
        $justification = array(
            Printer::JUSTIFY_LEFT,
            Printer::JUSTIFY_CENTER,
            Printer::JUSTIFY_RIGHT);

        //Column Size
        $tb1 = ['c1'=>12 , 'c2'=>11, 'c3'=>9, 'c4'=>10];
        $tb2 = ['c1'=>29, 'c2'=>17];

        try {
            // Connect Printer
            $connector = new NetworkPrintConnector($ip, 9100);
            $printer = new Printer($connector);

            /* Initialize */
            // Header
            $printer -> initialize();
            $printer -> setJustification($justification[1]);
            $printer -> graphics($logo);
            $printer -> feed(1);
            $printer -> setEmphasis(true);
            $printer -> text("End of Day Report\n");
            $printer -> text("For Till "."100"."\n");
            $printer -> setEmphasis(false);
            $printer -> text("As of ".date("d F Y")."\n");
            $printer -> text("Report was run on ".date("d F Y, H:i")."\n");
            $printer -> feed(1);
            $printer -> text("================================================\n");

            // Product Sales by Category
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text("Product Sales by Category\n");
            $printer -> text(str_pad("Category",$tb1['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Gross Sales",$tb1['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Discount",$tb1['c3'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Net Sales",$tb1['c4'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $collections = Collection::all();
            $all_gross = 0; $all_discount = 0; $all_net = 0;
            foreach ($collections as $collection) {
                $printer -> text(str_pad(substr($collection->collection_name_en,0,$tb1['c1']),$tb1['c1'],' ',STR_PAD_RIGHT)
                            ."  ".str_pad(number_format("19000",2),$tb1['c2'],' ',STR_PAD_LEFT)
                            ."  ".str_pad(number_format("1550",2),$tb1['c3'],' ',STR_PAD_LEFT)
                            ."  ".str_pad(number_format(19000 - 1550,2),$tb1['c4'],' ',STR_PAD_LEFT)."\n");
                $all_gross += 19000; $all_discount += 1550; $all_net += 19000 - 1550;
            }
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("   Total Gross Sales",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_gross,2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Discount",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_discount,2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Net Sales",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_net,2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");

            // Product Sales by Shop
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text("Product Sales by Shop\n");
            $printer -> text(str_pad("Shop",$tb1['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Gross Sales",$tb1['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Discount",$tb1['c3'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Net Sales",$tb1['c4'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $stores = Store::all();
            $all_gross = 0; $all_discount = 0; $all_net = 0;
            foreach ($stores as $store) {
                $printer -> text(str_pad(substr($store->store_name_en,0,$tb1['c1']),$tb1['c1'],' ',STR_PAD_RIGHT)
                            ."  ".str_pad(number_format("38000",2),$tb1['c2'],' ',STR_PAD_LEFT)
                            ."  ".str_pad(number_format("3100",2),$tb1['c3'],' ',STR_PAD_LEFT)
                            ."  ".str_pad(number_format(38000 - 3100,2),$tb1['c4'],' ',STR_PAD_LEFT)."\n");
                $all_gross += 38000; $all_discount += 3100; $all_net += 38000 - 3100;
            }
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("   Total Gross Sales",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_gross,2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Discount",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_discount,2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Net Sales",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_net,2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");


            // Order Summary
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text("Order Summary\n");
            $printer -> text(str_pad("   Total Net Sales before TAX",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("130467.29",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Service Charges",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("11860.66",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Net Product Sales",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("118606.63",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Sales Tax",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("9132.71",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Ticket Sales",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("139600",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Open (Unpaid) Tickets",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("-",$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Closed (Tendered) Tickets",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("139600",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");

            // Payment Group by Tender
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text("Payment Group by Tender\n");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("Cash",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ." ".str_pad(number_format("6500",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("Siam Gift Card",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ." ".str_pad(number_format("6300",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("Credit Card",$tb2['c1'],' ',STR_PAD_RIGHT)
                        ." ".str_pad(number_format("126800",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("   Total Payment Received",$tb2['c1'],' ',STR_PAD_RIGHT )
                        ." ".str_pad(number_format("139600",2),$tb2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");

            $printer -> feed();
            $printer -> cut();
            $printer -> close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }

    public function printZOut($ip, $casher_name_en) {
        $logo = EscposImage::load(public_path().'/images/logo-b.png');

        /* Justification */
        $justification = array(
            Printer::JUSTIFY_LEFT,
            Printer::JUSTIFY_CENTER,
            Printer::JUSTIFY_RIGHT);

        /* Column Size */
        $tb_2 = ['c1'=>33, 'c2'=>13];
        $tb_3 = ['c1'=>16, 'c2'=>11, 'c3'=>17];

        try {
            // Connect Printer
            $connector = new NetworkPrintConnector($ip, 9100);
            $printer = new Printer($connector);

            /* Initialize */
            /* Header */
            $printer -> initialize();
            $printer -> setJustification($justification[1]);
            $printer -> graphics($logo);
            $printer -> feed();
            $printer -> setEmphasis(true);
            $printer -> text("Z-out Report\n");
            $printer -> text("For Till "."100"." Sequence "."200"."\n");
            $printer -> setEmphasis(false);
            $printer -> text("Report was run on ".date("d F Y, H:i")."\n");
            $printer -> feed(1);
            $printer -> text("================================================\n");

            /* Tender Summary */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Tender Summary",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ".str_pad("Amount",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("Debit",$tb_2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("20000",2),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("Siam Gift Card",$tb_2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("20000",2),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("Credit Card",$tb_2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("20000",2),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("Cash",$tb_2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("20000",2),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("   Tendering Total",$tb_2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("0",2),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Credit Card Tips",$tb_2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("0",2),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Total Tops & Auto Gratuities",$tb_2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("0",2),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Net Cash",$tb_2['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format("0",2),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");

            /* Sales Taxes */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Sales Taxes",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ".str_pad("Amount",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("7% VAT",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ".str_pad(number_format("9132.71",2),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text("================================================\n");

            /* Category */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Category",$tb_3['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Quantity",$tb_3['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Net Sales",$tb_3['c3'],' ',STR_PAD_LEFT)."\n");
                        $printer -> setEmphasis(false);
            $collections = Collection::all();
            $all_qty = 0; $all_net = 0;
            foreach ($collections as $collection) {
                $printer -> text(str_pad(substr($collection->collection_name_en,0,$tb_3['c1']),$tb_3['c1'],' ',STR_PAD_RIGHT)
                            ."  ".str_pad(number_format("200"),$tb_3['c2'],' ',STR_PAD_LEFT)
                            ."  ".str_pad(number_format("17450",2),$tb_3['c3'],' ',STR_PAD_LEFT)."\n");
                $all_qty += 200; $all_net += 17450;
            }
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("   Total",$tb_3['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_qty),$tb_3['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format($all_net,2),$tb_3['c3'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");


            /* Void/Return */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Void / Return",$tb_3['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Quantity",$tb_3['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Amount",$tb_3['c3'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            // $collections = Collection::all();
            $all_qty = 0; $all_net = 0;
            // foreach ($collections as $collection) {
                $printer -> text(str_pad(substr("Appetizers",0,$tb_3['c1']),$tb_3['c1'],' ',STR_PAD_RIGHT)
                            ."  ".str_pad(number_format("2"),$tb_3['c2'],' ',STR_PAD_LEFT)
                            ."  ".str_pad(number_format("400",2),$tb_3['c3'],' ',STR_PAD_LEFT)."\n");
                $all_qty += 2; $all_net += 400;
                $printer -> text(str_pad(substr("Soup",0,$tb_3['c1']),$tb_3['c1'],' ',STR_PAD_RIGHT)
                            ."  ".str_pad(number_format("1"),$tb_3['c2'],' ',STR_PAD_LEFT)
                            ."  ".str_pad(number_format("150",2),$tb_3['c3'],' ',STR_PAD_LEFT)."\n");
                $all_qty += 1; $all_net += 150;
            // }
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("   Total",$tb_3['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(number_format($all_qty),$tb_3['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format($all_net,2),$tb_3['c3'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");

            /* Count */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("   Transaction Count",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ".str_pad(number_format("39"),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Credit Card Count",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ".str_pad(number_format("40"),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Debit Card Count",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ".str_pad(number_format("20"),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Guest Count",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ".str_pad(number_format("400"),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");

            /* Total */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("   Taxable Total",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ".str_pad(number_format("139600",2),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Non-Taxable Total",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ".str_pad(number_format("0",2),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("   Tax Total",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ".str_pad(number_format("9132.71",2),$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text("================================================\n");

            /* Cashier Name */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text("   Server: ".$casher_name_en."\n");
            $printer -> setEmphasis(false);

            $printer -> feed();
            $printer -> cut();
            $printer -> close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }

    public function printTransaction($ip) {
        $logo = EscposImage::load(public_path().'/images/logo-b.png');

        /* Justification */
        $justification = array(
            Printer::JUSTIFY_LEFT,
            Printer::JUSTIFY_CENTER,
            Printer::JUSTIFY_RIGHT);

        /* Column Size */
        $tb_3 = ['c1'=>11, 'c2'=>14, 'c3'=>8];
        $tb_6 = ['c1'=>15, 'c2'=>3, 'c3'=>6, 'c4'=>9, 'c5'=>3, 'c6'=>7];
        $tb_2 = ['c1'=>13, 'c2'=>35];


        try {
            // Connect Printer
            $connector = new NetworkPrintConnector($ip, 9100);
            $printer = new Printer($connector);

            /* Initialize */
            /* Header */
            $printer -> initialize();
            $printer -> setJustification($justification[1]);
            $printer -> graphics($logo);
            $printer -> feed();
            $printer -> setEmphasis(true);
            $printer -> text("Transaction Report\n");
            $printer -> text("Post ID: "."100"."\n");
            $printer -> text("As of ".date("d F Y")."\n");
            $printer -> setEmphasis(false);
            $printer -> text("Report was run on ".date("d F Y, H:i")."\n");
            $printer -> feed(1);
            $printer -> text("================================================\n");

            /* Sec 1 */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Transaction",$tb_3['c1'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("Invoice",$tb_3['c2'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("Till",$tb_3['c3'],' ',STR_PAD_RIGHT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("17576",$tb_3['c1'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("17583",$tb_3['c2'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("100",$tb_3['c3'],' ',STR_PAD_RIGHT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Sequence",$tb_3['c1'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("Date",$tb_3['c2'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("Customer",$tb_3['c3'],' ',STR_PAD_RIGHT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("646",$tb_3['c1'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("02-01-14 10:02",$tb_3['c2'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("",$tb_3['c3'],' ',STR_PAD_RIGHT)."\n");

            /* Sec 2 */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Description",$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ." ".str_pad("Qty",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ." ".str_pad("Price",$tb_6['c3'],' ',STR_PAD_LEFT)
                        ." ".str_pad("Sub-Total",$tb_6['c4'],' ',STR_PAD_LEFT)
                        ." ".str_pad("Tax",$tb_6['c5'],' ',STR_PAD_RIGHT)
                        ." ".str_pad("User",$tb_6['c6'],' ',STR_PAD_RIGHT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad(substr("Libertine Absinthe",0,$tb_6['c1']),$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ." ".str_pad("1",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ." ".str_pad("12.99",$tb_6['c3'],' ',STR_PAD_LEFT)
                        ." ".str_pad("12.99",$tb_6['c4'],' ',STR_PAD_LEFT)
                        ." ".str_pad("Yes",$tb_6['c5'],' ',STR_PAD_RIGHT)
                        ." ".str_pad("Melissa",$tb_6['c6'],' ',STR_PAD_RIGHT)."\n");
            $printer -> text(str_pad(substr("Absinthe Sugar",0,$tb_6['c1']),$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ." ".str_pad("1",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ." ".str_pad("0.25",$tb_6['c3'],' ',STR_PAD_LEFT)
                        ." ".str_pad("0.25",$tb_6['c4'],' ',STR_PAD_LEFT)
                        ." ".str_pad("Yes",$tb_6['c5'],' ',STR_PAD_RIGHT)
                        ." ".str_pad("Melissa",$tb_6['c6'],' ',STR_PAD_RIGHT)."\n");
            $printer -> text(str_pad(substr("Absinthe Sugar",0,$tb_6['c1']),$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ." ".str_pad("1",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ." ".str_pad("0.25",$tb_6['c3'],' ',STR_PAD_LEFT)
                        ." ".str_pad("0.25",$tb_6['c4'],' ',STR_PAD_LEFT)
                        ." ".str_pad("Yes",$tb_6['c5'],' ',STR_PAD_RIGHT)
                        ." ".str_pad("Melissa",$tb_6['c6'],' ',STR_PAD_RIGHT)."\n");

            /* Sec 3 */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Tender Type",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("Master",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Tender Amount",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("14.30",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Approval",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("036167",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Ref",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("116861556",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Cashier",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("Melissa",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Taxable Total",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("13.49",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Non-Tax Total",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("0.00",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Tax Total",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("0.81",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Trans Total",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("14.30",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text("================================================\n");

            /* Sec 1 */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Transaction",$tb_3['c1'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("Invoice",$tb_3['c2'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("Till",$tb_3['c3'],' ',STR_PAD_RIGHT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("17577",$tb_3['c1'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("17584",$tb_3['c2'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("100",$tb_3['c3'],' ',STR_PAD_RIGHT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Sequence",$tb_3['c1'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("Date",$tb_3['c2'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("Customer",$tb_3['c3'],' ',STR_PAD_RIGHT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("646",$tb_3['c1'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("02-01-14 10:05",$tb_3['c2'],' ',STR_PAD_RIGHT)
                        ."   ".str_pad("031513",$tb_3['c3'],' ',STR_PAD_RIGHT)."\n");

            /* Sec 2 */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Description",$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ." ".str_pad("Qty",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ." ".str_pad("Price",$tb_6['c3'],' ',STR_PAD_LEFT)
                        ." ".str_pad("Sub-Total",$tb_6['c4'],' ',STR_PAD_LEFT)
                        ." ".str_pad("Tax",$tb_6['c5'],' ',STR_PAD_RIGHT)
                        ." ".str_pad("User",$tb_6['c6'],' ',STR_PAD_RIGHT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad(substr("Capitian White",0,$tb_6['c1']),$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ." ".str_pad("1",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ." ".str_pad("23.99",$tb_6['c3'],' ',STR_PAD_LEFT)
                        ." ".str_pad("23.99",$tb_6['c4'],' ',STR_PAD_LEFT)
                        ." ".str_pad("Yes",$tb_6['c5'],' ',STR_PAD_RIGHT)
                        ." ".str_pad("Melissa",$tb_6['c6'],' ',STR_PAD_RIGHT)."\n");

            /* Sec 3 */
            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Tender Type",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("Visa",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Tender Amount",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("25.43",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Approval",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("003581",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Ref",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("11686137",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Cashier",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("Melissa",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Taxable Total",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("23.99",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Non-Tax Total",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("0.00",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Tax Total",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("1.44",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");

            $printer -> setEmphasis(true);
            $printer -> text(str_pad("Trans Total",$tb_2['c1'],' ',STR_PAD_RIGHT)."  ");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("25.43",$tb_2['c2'],' ',STR_PAD_LEFT)."\n");
            $printer -> text("================================================\n");

            $printer -> feed();
            $printer -> cut();
            $printer -> close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }

    public function printABB($ip) {
        $logo = EscposImage::load(public_path().'/images/logo-b.png');

        /* Justification */
        $justification = array(
            Printer::JUSTIFY_LEFT,
            Printer::JUSTIFY_CENTER,
            Printer::JUSTIFY_RIGHT);

        /* Column Size */
        $tb_6 = ['c1'=>11, 'c2'=>7, 'c3'=>4, 'c4'=>8, 'c5'=>14, 'c6'=>8];

        try {
            // Connect Printer
            $connector = new NetworkPrintConnector($ip, 9100);
            $printer = new Printer($connector);

            /* Initialize */
            /* Header */
            $printer -> initialize();
            $printer -> setJustification($justification[1]);
            $printer -> graphics($logo);
            $printer -> feed();
            $printer -> setEmphasis(true);
            $printer -> text("Tax Invoice (ABB) Report\n");
            $printer -> text("As of ".date("d F Y")."\n");
            $printer -> setEmphasis(false);
            $printer -> text("Report was run on ".date("d F Y, H:i")."\n");
            $printer -> feed(1);
            $printer -> text("================================================\n");

            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("No.",$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Tax Invoice (ABB) No.",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Date",$tb_6['c3'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Amount excl.VAT",$tb_6['c4'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("VAT",$tb_6['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Total",$tb_6['c6'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("1",$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Sk1610010001",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(date("d/m/Y"),$tb_6['c3'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("10000",2),$tb_6['c4'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("700",2),$tb_6['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("1070",2),$tb_6['c6'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("2",$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Sk1610010002",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(date("d/m/Y"),$tb_6['c3'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("10000",2),$tb_6['c4'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("700",2),$tb_6['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("1070",2),$tb_6['c6'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("3",$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Sk1610010003",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(date("d/m/Y"),$tb_6['c3'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("10000",2),$tb_6['c4'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("700",2),$tb_6['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("1070",2),$tb_6['c6'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("4",$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Sk1610010004",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(date("d/m/Y"),$tb_6['c3'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("10000",2),$tb_6['c4'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("700",2),$tb_6['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("1070",2),$tb_6['c6'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("5",$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Sk1610010005",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(date("d/m/Y"),$tb_6['c3'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("10000",2),$tb_6['c4'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("700",2),$tb_6['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("1070",2),$tb_6['c6'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("6",$tb_6['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Sk1610010006",$tb_6['c2'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(date("d/m/Y"),$tb_6['c3'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("10000",2),$tb_6['c4'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("700",2),$tb_6['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("1070",2),$tb_6['c6'],' ',STR_PAD_LEFT)."\n");
            $printer -> text("================================================\n");

            $printer -> feed();
            $printer -> cut();
            $printer -> close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }

    public function printTaxInvoice($ip) {
        $logo = EscposImage::load(public_path().'/images/logo-b.png');

        /* Justification */
        $justification = array(
            Printer::JUSTIFY_LEFT,
            Printer::JUSTIFY_CENTER,
            Printer::JUSTIFY_RIGHT);

        /* Column Size */
        $tb_7 = ['c1'=>11, 'c2'=>7, 'c3'=>4, 'c4'=>13, 'c5'=>8, 'c6'=>14, 'c7'=>8];

        try {
            // Connect Printer
            $connector = new NetworkPrintConnector($ip, 9100);
            $printer = new Printer($connector);

            /* Initialize */
            /* Header */
            $printer -> initialize();
            $printer -> setJustification($justification[1]);
            $printer -> graphics($logo);
            $printer -> feed();
            $printer -> setEmphasis(true);
            $printer -> text("Tax Invoice Report\n");
            $printer -> text("As of ".date("d F Y")."\n");
            $printer -> setEmphasis(false);
            $printer -> text("Report was run on ".date("d F Y, H:i")."\n");
            $printer -> feed(1);
            $printer -> text("================================================\n");

            $printer -> setJustification($justification[0]);
            $printer -> setEmphasis(true);
            $printer -> text(str_pad("No.",$tb_7['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Tax Invoice No.",$tb_7['c2'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Date",$tb_7['c3'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Cutomer Name",$tb_7['c4'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Amount excl.VAT",$tb_7['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("VAT",$tb_7['c6'],' ',STR_PAD_LEFT)
                        ."  ".str_pad("Total",$tb_7['c7'],' ',STR_PAD_LEFT)."\n");
            $printer -> setEmphasis(false);
            $printer -> text(str_pad("1",$tb_7['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Sk1610010001",$tb_7['c2'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(date("d/m/Y"),$tb_7['c3'],' ',STR_PADRIGHT)
                        ."  ".str_pad(substr("BKS Co., Ltd.",0,$tb_7['c4']),$tb_7['c4'],' ',STR_PADRIGHT)
                        ."  ".str_pad(number_format("10000",2),$tb_7['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("700",2),$tb_7['c6'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("1070",2),$tb_7['c7'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("1",$tb_7['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Sk1610010002",$tb_7['c2'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(date("d/m/Y"),$tb_7['c3'],' ',STR_PADRIGHT)
                        ."  ".str_pad(substr("BKS Co., Ltd.",0,$tb_7['c4']),$tb_7['c4'],' ',STR_PADRIGHT)
                        ."  ".str_pad(number_format("10000",2),$tb_7['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("700",2),$tb_7['c6'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("1070",2),$tb_7['c7'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("1",$tb_7['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Sk1610010003",$tb_7['c2'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(date("d/m/Y"),$tb_7['c3'],' ',STR_PADRIGHT)
                        ."  ".str_pad(substr("BKS Co., Ltd.",0,$tb_7['c4']),$tb_7['c4'],' ',STR_PADRIGHT)
                        ."  ".str_pad(number_format("10000",2),$tb_7['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("700",2),$tb_7['c6'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("1070",2),$tb_7['c7'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("1",$tb_7['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Sk1610010004",$tb_7['c2'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(date("d/m/Y"),$tb_7['c3'],' ',STR_PADRIGHT)
                        ."  ".str_pad(substr("BKS Co., Ltd.",0,$tb_7['c4']),$tb_7['c4'],' ',STR_PADRIGHT)
                        ."  ".str_pad(number_format("10000",2),$tb_7['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("700",2),$tb_7['c6'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("1070",2),$tb_7['c7'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("1",$tb_7['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Sk1610010005",$tb_7['c2'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(date("d/m/Y"),$tb_7['c3'],' ',STR_PADRIGHT)
                        ."  ".str_pad(substr("BKS Co., Ltd.",0,$tb_7['c4']),$tb_7['c4'],' ',STR_PADRIGHT)
                        ."  ".str_pad(number_format("10000",2),$tb_7['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("700",2),$tb_7['c6'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("1070",2),$tb_7['c7'],' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("1",$tb_7['c1'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad("Sk1610010006",$tb_7['c2'],' ',STR_PAD_RIGHT)
                        ."  ".str_pad(date("d/m/Y"),$tb_7['c3'],' ',STR_PADRIGHT)
                        ."  ".str_pad(substr("BKS Co., Ltd.",0,$tb_7['c4']),$tb_7['c4'],' ',STR_PADRIGHT)
                        ."  ".str_pad(number_format("10000",2),$tb_7['c5'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("700",2),$tb_7['c6'],' ',STR_PAD_LEFT)
                        ."  ".str_pad(number_format("1070",2),$tb_7['c7'],' ',STR_PAD_LEFT)."\n");
            $printer -> text("================================================\n");

            $printer -> feed();
            $printer -> cut();
            $printer -> close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }
}
?>
