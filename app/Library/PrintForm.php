<?php
namespace App\Library;

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;

use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\Product;
use App\Models\ProductOption;
use App\Models\Store;
use App\Models\Receipt;
use App\Models\Collection;

class PrintForm {
    public $pattern = '/([\(+\d\)])/';
    public function drawer($ip = '192.168.1.102'){
        // Connect Printer
        $connector = new NetworkPrintConnector($ip, 9100);
        $printer = new Printer($connector);
        /* Initialize */
        $printer -> initialize();
        $printer -> pulse();
    }
    public function printOrder($orders_id){
        /* Justification */
        $justification = array(
            Printer::JUSTIFY_LEFT,
            Printer::JUSTIFY_CENTER,
            Printer::JUSTIFY_RIGHT);
        $data_order = Orders::find($orders_id);
        $data_detail = OrdersDetail::where('orders_id',$orders_id)->get();
        foreach($data_detail as $detail){
            // get store print ip
            $store_print = Product::select('printer_ip')->join('store','store.store_id','=','product.store_id')->where('product.product_id',$detail->product_id)->first();
            $ip = $store_print->printer_ip;

            $arr_orders_dt = explode(' ',$data_order->created_at);
            $arr_date = explode('-',$arr_orders_dt[0]);
            $date = $arr_date[2].'/'.$arr_date[1].'/'.$arr_date[0];
            $time = $arr_orders_dt[1];

            $qty = $detail->qty; // Get qty:product
            $product_name = $detail->product_name_en;
            $option_name = preg_replace($this->pattern,'', $detail->option_en);
            for($i=1; $i<=$qty; $i++){
                for($j=1; $j<=2; $j++){
                    try {
                        // Connect Printer
                        $connector = new NetworkPrintConnector($ip, 9100);
                        $printer = new Printer($connector);
                        /* Initialize */
                        $printer -> initialize();
                        $printer -> setJustification($justification[1]);
                        if($j==2){
                            $printer -> text("(Copy)\n");
                            $product_name = $detail->product_name_th;
                            $option_name = preg_replace($this->pattern,'', $detail->option_th);
                        }
                        $printer -> text("Order No. ".$data_order->orders_no."\n");
                        $printer -> text("Date ".$date.', '.$time."\n");

                        $printer -> setTextSize(2, 2);
                        $printer -> text("Table ".$data_order->table_no.' ('.$data_order->round.')'."\n");

                        $printer -> setTextSize(1, 1);
                        $printer -> text('('.$i.'/'.$qty.') '.$product_name."\n");
                        if($option_name != '')
                            $printer -> text($option_name."\n");

                        $printer -> setBarcodeHeight(50);
                        $printer -> setBarcodeWidth(2);
                        $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_NONE);
                        $printer -> barcode($orders_id.'-'.$detail->orders_detail_id);
                        $printer -> setJustification();

                        $printer -> feed();
                        $printer -> cut();
                        $printer -> close();
                    } catch (Exception $e) {
                        echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
                    }
                }
            }
        }

    }

    public function printOrder02($ip = '192.168.1.102', $orders_id){
        /* Justification */
        $logo = EscposImage::load(public_path().'/images/logo-b.png');

        $data_order = Orders::find($orders_id);
        $data_detail = OrdersDetail::where('orders_id',$orders_id)->get();
        $total_price = 0;

        $orders_no = $data_order->orders_no;
        $table_no = $data_order->table_no;
        $round = $data_order->round;
        $arr_orders_dt = explode(' ',$data_order->created_at);
        $arr_date = explode('-',$arr_orders_dt[0]);
        $date = $arr_date[2].'/'.$arr_date[1].'/'.$arr_date[0];
        $time = $arr_orders_dt[1];
        try {
            // Connect Printer
            $connector = new NetworkPrintConnector($ip, 9100);
            $printer = new Printer($connector);
            /* Initialize */
            $printer -> initialize();
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer -> graphics($logo);
            $printer -> feed(1);
            $printer -> text("ORDER NO. ".$orders_no."\n");
            $printer -> text("DATE ".$date.", ".$time."\n");
            $printer -> setTextSize(2, 2);
            $printer -> text("Table ".$table_no."(".$round.")\n");
            $printer->feed(1);
            $printer -> setJustification();

            $printer -> setTextSize(1, 1);
            $size_qty = 3; $size_item = 35; $size_total = 8;

            // Foreach Detail
            foreach($data_detail as $detail){
                $product_name = substr($detail->product_name_en,0,$size_item);
                $option_name = preg_replace($this->pattern,'', $detail->option_en);
                $qty = $detail->qty;
                $price = $detail->price;
                $sub_price = $qty * $price;
                $printer -> text(str_pad($qty,$size_qty,' ',STR_PAD_RIGHT).' '.str_pad($product_name,$size_item,' ',STR_PAD_RIGHT).' '.str_pad(number_format($sub_price,2),$size_total, ' ',STR_PAD_LEFT)."\n");
                if($option_name != ''){
                    $printer -> text(str_pad('',$size_qty,' ',STR_PAD_RIGHT).' '.str_pad(' '.$option_name,$size_item,' ',STR_PAD_RIGHT).' '.str_pad('',$size_total, ' ',STR_PAD_LEFT)."\n");
                }


                $total_price += $sub_price;
            }
            $printer -> feed(1);
            $printer -> text(str_pad("",$size_qty,' ',STR_PAD_RIGHT).' '.str_pad(substr("Total",0,$size_item),$size_item,' ',STR_PAD_RIGHT).' '.str_pad(number_format($total_price,2),$size_total, ' ',STR_PAD_LEFT)."\n");

            $printer->feed(1);
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer -> setBarcodeHeight(50);
            $printer -> setBarcodeWidth(2);

            $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_BELOW);
            $printer -> barcode($orders_no);
            $printer -> setJustification();

            $printer->feed();
//            $printer -> pulse();

            /* Printer shutdown */
            $printer -> cut();
            $printer -> close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }


    public function printReceipt01($ip = '192.168.1.102', $orders_id, $type){
        // $type 01 = อย่างย่อ, 02 = อย่างย๊ออออย่อ
        /* Justification */
        $logo = EscposImage::load(public_path().'/images/logo-b.png');

        $data_order = Orders::find($orders_id);
        $data_detail = OrdersDetail::where('orders_id',$orders_id)->get();
        $total_price = 0;
        $total_qty = 0;

        $orders_no = $data_order->orders_no;
        $table_no = $data_order->table_no;
        $round = $data_order->round;
        $arr_orders_dt = explode(' ',$data_order->created_at);
        $arr_date = explode('-',$arr_orders_dt[0]);
        $date = $arr_date[2].'/'.$arr_date[1].'/'.$arr_date[0];
        $time = $arr_orders_dt[1];
        try {
            // Connect Printer
            $connector = new NetworkPrintConnector($ip, 9100);
            $printer = new Printer($connector);
            /* Initialize */
            $printer -> initialize();
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer -> graphics($logo);
            $printer -> feed(1);
            $printer -> setEmphasis(true);
            $printer -> text("TAX INVOICE (ABB)\n");
            $printer -> setEmphasis(false);
            $printer -> text("WELCOME TO MY KITCHEN\n");
            $printer -> text("SIAM PIWAT CO., LTD.\n");
            $printer -> text("TEL.+6626581000 Branch SIAM DISCOVERY\n");
            $printer -> text("TAX ID.0029092840300\n");
            $printer -> text("POS SERIAL NO.E3451286090014\n");
            $printer -> text("POS TYPE: DININ TABLE\n");
            $printer -> text("RECEIPT NO.".$orders_no."\n");
            $printer -> text("ORDER NO. ".$orders_no."\n");
            $printer -> text("DATE ".$date.", ".$time."\n");
            $printer -> text("Table ".$table_no."(".$round.")\n");
            $printer->feed(1);
            $printer -> setJustification();

            $size_qty = 3; $size_item = 35; $size_total = 8;

            // Foreach Detail
            foreach($data_detail as $detail){
                $product_name = substr($detail->product_name_en,0,$size_item);
                $qty = $detail->qty;
                $price = $detail->price;
                $sub_price = $qty * $price;
                if($type == 01){
                    $printer -> text(str_pad($qty,$size_qty,' ',STR_PAD_RIGHT).' '.str_pad($product_name,$size_item,' ',STR_PAD_RIGHT).' '.str_pad(number_format($sub_price,2),$size_total, ' ',STR_PAD_LEFT)."\n");
                }
                $total_price += $sub_price;
                $total_qty += $qty;
            }

            // Cal Before VAT 7%
            $before_vat = round($total_price * 100 / 107,2);
            $vat = $total_price - $before_vat;

            if($type == '02'){
                $printer -> text(str_pad("",$size_qty,' ',STR_PAD_RIGHT).' '.str_pad(substr("Food and Beverages",0,$size_item),$size_item,' ',STR_PAD_RIGHT).' '.str_pad(number_format($total_price,2),$size_total, ' ',STR_PAD_LEFT)."\n");
            }

            $printer -> feed(1);
            $printer -> text(str_pad("",$size_qty,' ',STR_PAD_RIGHT).' '.str_pad("Total Qty. ".$total_qty,$size_item,' ',STR_PAD_RIGHT).' '.str_pad("",$size_total, ' ',STR_PAD_LEFT)."\n");

            $printer -> text(str_pad("",$size_qty,' ',STR_PAD_RIGHT).' '.str_pad(substr("Total",0,$size_item),$size_item,' ',STR_PAD_RIGHT).' '.str_pad(number_format($total_price,2),$size_total, ' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("",$size_qty,' ',STR_PAD_RIGHT).' '.str_pad(substr("Before VAT",0,$size_item),$size_item,' ',STR_PAD_RIGHT).' '.str_pad(number_format($before_vat,2),$size_total, ' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad("",$size_qty,' ',STR_PAD_RIGHT).' '.str_pad(substr("VAT 7%",0,$size_item),$size_item,' ',STR_PAD_RIGHT).' '.str_pad(number_format($vat,2),$size_total, ' ',STR_PAD_LEFT)."\n");

            $printer->feed(1);
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer -> setBarcodeHeight(50);
            $printer -> setBarcodeWidth(2);

            $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_BELOW);
            $printer -> barcode($orders_no);
            $printer -> setJustification();

            $printer->feed();
//            $printer -> pulse();

            /* Printer shutdown */
            $printer -> cut();
            $printer -> close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }

    public function printReceipt02($ip = '192.168.1.102', $receipt_id){
        /* Justification */
        $logo = EscposImage::load(public_path().'/images/logo-b.png');

        $data_receipt = Receipt::find($receipt_id);
        $list_orders_id = $data_receipt->orders_id;
        $arr_orders_id = explode(',',$list_orders_id);
        $receipt_no = $data_receipt->receipt_no;

        $data_orders = Orders::whereIn('orders_id',$arr_orders_id)->get();
        $grand_price = 0;
        $total_vat = 0;
        $total_before_vat = 0;
        $total_discount = 0;

        $arr_receipt_dt = explode(' ',$data_receipt->created_at);
        $arr_date = explode('-',$arr_receipt_dt[0]);
        $date = $arr_date[2].'/'.$arr_date[1].'/'.$arr_date[0];
        $time = $arr_receipt_dt[1];
        try {
            // Connect Printer
            $connector = new NetworkPrintConnector($ip, 9100);
            $printer = new Printer($connector);
            /* Initialize */
            $printer -> initialize();
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer -> graphics($logo);
            $printer -> feed(1);
            $printer -> setEmphasis(true);
            $printer -> text("TAX INVOICE\n");
            $printer -> setEmphasis(false);
            $printer -> text("WELCOME TO MY KITCHEN\n");
            $printer -> text("SIAM PIWAT CO., LTD.\n");
            $printer -> text("TEL.+6626581000 Branch SIAM DISCOVERY\n");
            $printer -> text("TAX ID.0029092840300\n");
            $printer -> text("POS SERIAL NO.E3451286090014\n");
            $printer -> text("POS TYPE: DININ TABLE\n");
            $printer -> text("RECEIPT NO.".$receipt_no."\n");
            $printer -> text("DATE ".$date.", ".$time."\n");
            $printer->feed(1);
            $printer -> setJustification();

            $size_qty = 3; $size_order = 39; $size_item = 35; $size_total = 8;

            $printer -> text("Customer Information\n");
            $printer -> text($data_receipt->company_name." ".$data_receipt->branch."\n");
            $printer -> text($data_receipt->address."\n");
            $printer -> text("Tax ID.".$data_receipt->tax_id."\n");
            $printer -> feed(1);
            // Foreach Detail
            foreach($data_orders as $order){
                $orders_no = substr($order->orders_no,0,$size_order);
                $total_price = $order->total_price;
                $discount_price = $order->discount_price;
                $sub_price = $total_price - $discount_price;

                $before_vat = round($total_price * 100 / 107,2);
                $vat = $total_price - $before_vat;

                $total_discount += $discount_price;
                $total_before_vat += $before_vat;
                $total_vat += $vat;

                $data_detail = OrdersDetail::where('orders_id',$order->orders_id)->get();
                // Foreach Detail
                foreach($data_detail as $detail){
                    $product_name = substr($detail->product_name_en,0,$size_item);
                    $qty = $detail->qty;
                    $price = $detail->price;
                    $sub_price2 = $qty * $price;

                    $printer -> text(str_pad($qty,$size_qty,' ',STR_PAD_RIGHT).' '.str_pad($product_name,$size_item,' ',STR_PAD_RIGHT).' '.str_pad(number_format($sub_price2,2),$size_total, ' ',STR_PAD_LEFT)."\n");
                }

                if($discount_price > 0){
                    $printer -> text(str_pad("",$size_qty,' ',STR_PAD_RIGHT).' '.str_pad("Discount",$size_item,' ',STR_PAD_RIGHT).' '.str_pad("-".number_format($discount_price,2),$size_total, ' ',STR_PAD_LEFT)."\n");
                }

                $grand_price += $sub_price;
            }

            $printer -> text(str_pad(substr("Total",0,$size_order),$size_order,' ',STR_PAD_RIGHT).' '.str_pad(number_format($grand_price,2),$size_total, ' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad(substr("Total VAT",0,$size_order),$size_order,' ',STR_PAD_RIGHT).' '.str_pad(number_format($total_vat,2),$size_total, ' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad(substr("Total Before VAT",0,$size_order),$size_order,' ',STR_PAD_RIGHT).' '.str_pad(number_format($total_before_vat,2),$size_total, ' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad(substr("Total Discount",0,$size_order),$size_order,' ',STR_PAD_RIGHT).' '.str_pad(number_format($total_discount,2),$size_total, ' ',STR_PAD_LEFT)."\n");
            $printer -> text(str_pad(substr("Total Price",0,$size_order),$size_order,' ',STR_PAD_RIGHT).' '.str_pad(number_format($grand_price,2),$size_total, ' ',STR_PAD_LEFT)."\n");

            $printer->feed(1);

            /* Printer shutdown */
            $printer -> cut();
            $printer -> close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }

    public function testPrint(){
        /* Justification */
        $justification = array(
            Printer::JUSTIFY_LEFT,
            Printer::JUSTIFY_CENTER,
            Printer::JUSTIFY_RIGHT);
        try {
            // Connect Printer
            $connector = new NetworkPrintConnector('192.168.1.102', 9100);
            $printer = new Printer($connector);
            /* Initialize */
            $printer -> initialize();

            $printer -> setJustification($justification[1]);
            $printer -> text("เลขที่ 1000200323\n");
            $printer -> text("วันที่ 30-09-2016, 18:05:18\n");

            $printer -> setTextSize(2, 2);
            $printer -> text("โต๊ะที่ 1 (2)\n");

            $printer -> setTextSize(1, 1);
            $printer -> text("ขนม นม เนย\n");
            $printer -> text("ข้าวผัดไข่ ไม่ใส่ไข่\n");
            $printer -> text("โจ๊กไม่ใส่ขิง ไม่กิน ไม่จ่าย\n");
            $printer -> text("น้ำเปล่า\n");

            $printer -> setBarcodeHeight(50);
            $printer -> setBarcodeWidth(2);
            $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_NONE);
            $printer -> barcode('1000200323');
            $printer -> setJustification();

            $printer -> feed();
            $printer -> cut();
            $printer -> close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }

    }


}
?>
