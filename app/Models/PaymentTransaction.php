<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentTransaction extends Model
{
    use SoftDeletes;
    public $table = 'payment_transaction';
    public $primaryKey = 'payment_transaction_id';
    public $fillable = ['orders_id','transaction'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
