<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberTransactionLog extends Model
{
    use SoftDeletes;
    public $table = 'member_transaction_log';
    public $primaryKey = 'transaction_id';
    public $fillable = ['telephone','member_id','otp','timeout'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
