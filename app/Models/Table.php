<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Table extends Model
{
    use SoftDeletes;
    public $table = 'table';
    public $primaryKey = 'table_id';
    public $fillable = ['zone_id','table_no','coordinate_x','coordinate_y','is_available'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Zone(){
        return $this->belongsTo('App\Models\Zone','zone_id');
    }

}
