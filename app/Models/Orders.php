<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Orders extends Model
{
    use SoftDeletes;
    public $table = 'orders';
    public $primaryKey = 'orders_id';
    public $fillable = ['orders_no','table_no','round', 'status','payment','is_printed','total_price','discount_price','discount_card','discount_code_id','code'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function StatusOrder($id)
    {
        $s = "";
        $data = DB::table('orders_detail')->where('orders_id',$id)
            ->select('status')
            ->get();

        foreach ($data as $value){
            $status = $value->status;
            if($status == 1){
                $s = "Complete";
            }else{
                $s = "Not Complete";
            }
        }
        return $s;
    }

    public function ordersDetail()
    {
        return $this->hasMany('App\Models\OrdersDetail');
    }
}
