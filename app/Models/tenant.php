<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tenant extends Model
{
    use SoftDeletes;
    public $table = 'tenant';
    public $primaryKey = 'tenant_id';
    public $fillable = ['tenant_name_en','tenant_name_th','number_of_persons'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
