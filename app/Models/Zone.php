<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zone extends Model
{
    use SoftDeletes;
    public $table = 'zone';
    public $primaryKey = 'zone_id';
    public $fillable = ['zone_name'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
