<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentLog extends Model
{
    use SoftDeletes;
    public $table = 'payment_log';
    public $primaryKey = 'payment_log_id';
    public $fillable = ['orders_id','payment','price','payment_other'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
