<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductOption extends Model
{
    use SoftDeletes;
    public $table = 'product_option';
    public $primaryKey = 'product_option_id';
    public $fillable = ['product_id','label_en','label_th','value_en','value_th','tags','type'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Product(){
        return $this->belongsTo('App\Models\Product','product_id');
    }
}
