<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceiptFooter extends Model
{
    use SoftDeletes;
    public $table = 'receipt_footer';
    public $primaryKey = 'receipt_footer_id';
    public $fillable = ['receipt_footer_name_th','receipt_footer_name_en'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
