<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;
    protected $table = 'tag';
    protected $primaryKey = 'tag_id';
    public $fillable = ['tag_name_th','tag_name_en'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function ProductTag(){
        return $this->hasMany('App\Model\ProductTag','product_id');
    }

}


?>