<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    use SoftDeletes;
    public $table = 'permission';
    public $primaryKey = 'permission_id';
    public $fillable = ['admin_role_id','page_id','c','r','u','d'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
