<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receipt extends Model
{
    use SoftDeletes;
    public $table = 'receipt';
    public $primaryKey = 'receipt_id';
    public $fillable = ['receipt_no','orders_id','print_type','value','company_name','branch','address','tax_id'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
