<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    public $table = 'product';
    public $primaryKey = 'product_id';
    public $fillable = ['store_id','collection_id','category_id','product_name_en','product_name_th','product_code','description_en','description_th','price','special_price','cost','barcode','gen_barcode','is_recommended','is_available','tags','img_name','vat'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Store(){
        return $this->belongsTo('App\Models\Store','store_id');
    }

    public function Category(){
        return $this->belongsTo('App\Models\Category','category_id');
    }

    public function Collection(){
        return $this->belongsTo('App\Models\Collection','collection_id');
    }
}
