<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use SoftDeletes;
    public $table = 'store';
    public $primaryKey = 'store_id';
    public $fillable = ['store_name_en', 'store_name_th', 'company_name', 'address', 'phone', 'fax', 'email', 'website', 'company_regis_number', 'company_taxid', 'branch','gp_type','rent','passcode'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
