<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TenantPerson extends Model
{
    use SoftDeletes;
    public $table = 'tenant_person';
    public $primaryKey = 'tenant_person_id';
    public $fillable = ['tenant_id','firstname_en','firstname_th','lastname_en','lastname_th','barcode'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
