<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceMan extends Model
{
    use SoftDeletes;
    public $table = 'serviceman';
    public $primaryKey = 'serviceman_id';
    public $fillable = ['zone_id','firstname', 'lastname','email','mobile','address'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Zone(){
        return $this->belongsTo('App\Models\Zone','zone_id');
    }
}
