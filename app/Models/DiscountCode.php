<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountCode extends Model
{
    use SoftDeletes;
    public $table = 'discount_code';
    public $primaryKey = 'discount_code_id';
    public $fillable = ['code_name_th','code_name_en','category_code','product_code','code','allot', 'reserved', 'used','per_use','min_price','max_price','discount_type','discount','discount_max','year','dayofweek','month','day','hour','minute','show_date','start_date','end_date','is_available','payment_method','normal_price'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];




}
?>
