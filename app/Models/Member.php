<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use SoftDeletes;
    public $table = 'member';
    public $primaryKey = 'member_id';
    public $fillable = ['viz_cardmember_id','tenant_person_id','member_name','member_preference','member_allergic','birthday','mobile','email','expired_date','member_name_tenant'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function TenantPerson(){
        return $this->belongsTo('App\Models\TenantPerson','tenant_person_id');
    }

    // public function Category(){
    //     return $this->belongsTo('App\Models\Category','category_id');
    // }
}
