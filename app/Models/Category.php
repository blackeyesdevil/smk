<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    public $table = 'category';
    public $primaryKey = 'category_id';
    public $fillable = ['store_id', 'category_name_en','category_name_th'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];


}
