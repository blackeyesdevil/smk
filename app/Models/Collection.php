<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Collection extends Model
{
    use SoftDeletes;
    public $table = 'collection';
    public $primaryKey = 'collection_id';
    public $fillable = ['collection_name_th','collection_name_en'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
