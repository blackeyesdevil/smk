<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuSet extends Model
{
    use SoftDeletes;
    public $table = 'menu_set';
    public $primaryKey = 'menu_set_id';
    public $fillable = ['menu_set_name_th','menu_set_name_en'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
