<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersDetail extends Model
{
    use SoftDeletes;
    public $table = 'orders_detail';
    public $primaryKey = 'orders_detail_id';
    public $fillable = ['orders_id', 'product_id','product_name_en','product_name_th','option_en','option_th','qty','price','is_canceled'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];


}
