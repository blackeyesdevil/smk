<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
  'as' => 'home',
  function () {
    return view('frontend.home');

  }
]);

Route::group(['prefix' => 'cashier'], function () {
    Route::get('cashier-table',function(){
        return view('frontend.cashier-table');
    });
    Route::get('customer-service',function(){
        return view('frontend.customer-service');
    });
    Route::get('grab-go',function(){
        return view('frontend.grab-go');
    });
});
/*butler-tablet*/
Route::get('butler-home',function(){
    return view('frontend.butler-home');
});
Route::get('butler-listorder',[
    // return view('frontend.butler-listorder');
    'as' => 'butler-listorder',
    'uses' => 'Frontend\ButlerController@getIndex',

]);
Route::post('butler-listorder',[
    // return view('frontend.butler-listorder');
    'as' => 'butler-listorder',
    'uses' => 'Frontend\ButlerController@getButlerSearch',

]);
Route::get('butler-listorder-detail/{orders_id}/{orders_no}',[
    // return view('frontend.butler-listorder-detail');
    'as' => 'butler-listorder-detail',
    'uses' => 'Frontend\ButlerController@getOrdersDetail',
]);
/*-----------*/
//UI
Route::get('first-page',function(){
    return view('frontend.first-page');
});
Route::get('special-set',function(){
    return view('frontend.special-set');
});
Route::get('promotion',function(){
    return view('frontend.promotion');
});
Route::get('grab',function(){
    return view('frontend.grab');
});
//

Route::get('checkout',function(){
    return view('frontend.checkout');
});

Route::get('callPaymentApp', function(){
    return view('frontend.call-payment');
});

Route::get('nfc', function(){
    return view('frontend.nfc');
});

Route::get('collection-product',function(){
    return view('frontend.collection-product');
});

Route::get('collection-list',function(){
    return view('frontend.collection-list');
});

Route::get('my-room',function(){
    return view('frontend.my-room');
});

Route::get('order-history', 'Frontend\OrderHistoryController@getIndex');

Route::get('before-home',function(){
    return view('frontend.before_home');
});

Route::get('first-store',function(){
    return view('frontend.first-store');
});

Route::get('product-list',function(){
    return view('frontend.product-list');
});

Route::get('product-list-2',function(){
    return view('frontend.product-list-2');
});

Route::get('cart',function(){
    return view('frontend.cart');
});


Route::get('payment-result/{status}/{token}/{string}','Frontend\PaymentResultController@main');
Route::get('thankyou','Frontend\PaymentResultController@thankyou');
Route::get('fail/{order_id}/{token}','Frontend\PaymentResultController@paymentFail');
// -------- API
Route::group(['prefix' => 'api'], function () {
    Route::get('locale/{lang}','API\LocaleController@main');
    Route::controller('collection','API\CollectionController');
    Route::controller('store','API\StoreController');
    Route::controller('product','API\ProductController');
    Route::controller('category','API\CategoryController');
    Route::controller('discount','API\DiscountController');
    Route::controller('session','API\SessionController');
    Route::controller('order','API\OrderController');
    Route::controller('cashier','API\CashierController');
    Route::controller('grab','API\GrabAndGoController');
    Route::controller('otp','API\OTPController');

});
// -------- Kitchen
Route::group(['prefix' => 'kitchen'], function () {
    Route::get('/', function(){
        return view('frontend.kitchen');
    });
    Route::controller('check-order','Frontend\CheckOrderController');
});
// -------- Receipt ใบกำกับภาษีแบบเต็ม
Route::group(['prefix' => 'receipt'], function () {
    Route::get('/','Frontend\ReceiptController@main');
    Route::post('barcode','Frontend\ReceiptController@barcode');
    Route::post('send','Frontend\ReceiptController@send');
    Route::get('print-paper/{gen_token}','Frontend\ReceiptController@printPaper');
});
// -------- Report Cashier
Route::group(['prefix' => 'report'], function () {
    Route::get('/','Frontend\ReportController@main');
    Route::get('print-eod','Frontend\ReportController@printEOD');
    Route::get('print-eod_till','Frontend\ReportController@printEODTill');
    Route::get('print-z_out','Frontend\ReportController@printZOut');
    Route::get('print-transaction','Frontend\ReportController@printTransaction');
    Route::get('print-abb','Frontend\ReportController@printABB');
    Route::get('print-tax','Frontend\ReportController@printTaxInvoice');
});


/* --------------------- Admin ------------------- */
Route::group(['prefix' => '_admin'], function () {
    Route::controller('login', 'Backend\LoginController');
    Route::get('logout', 'Backend\LoginController@logout');

    Route::post('api-active','Backend\ApiController@changeActive');
    Route::post('api-category','Backend\ApiController@changeCategory');
    Route::post('api-product','Backend\ApiController@changeProduct');
});

use App\Models\TenantPerson;
Route::group(['middleware'=>'admin','prefix' => '_admin'], function () {
    // Route::get('/', function () { return view('backend.index'); });
    Route::get('/','Backend\DashboardController@index');

    Route::get('report',function() { return view('backend.index_report'); });
    Route::resource('user-management','Backend\AdminController');
    Route::resource('role','Backend\AdminRoleController');
    Route::resource('page','Backend\PageController');
    Route::resource('permission','Backend\PermissionController');

    Route::resource('store','Backend\StoreController');

    Route::resource('category','Backend\CategoryController');
    Route::resource('collection','Backend\CollectionController');
    Route::resource('product', 'Backend\ProductController');
    Route::resource('product_option', 'Backend\ProductOptionController');
    Route::resource('orders', 'Backend\OrdersController');
    Route::resource('orders_detail', 'Backend\OrdersDetailController');

    Route::resource('discount_code', 'Backend\DiscountCodeController');
    Route::resource('serviceman', 'Backend\ServicemanController');
    Route::resource('zone', 'Backend\ZoneController');
    Route::resource('table', 'Backend\TableController');


    Route::resource('tenant','Backend\TenantController');
    Route::resource('tenant_person','Backend\TenantPersonController');
    Route::resource('member','Backend\MemberController');
    Route::resource('menu_set','Backend\MenuSetController');
    Route::resource('promotion','Backend\PromotionController');
    Route::resource('combo_set','Backend\ComboSetController');

    Route::post('check-username','Backend\CheckUsernameController@checkuser');
    Route::resource('report_store','Backend\ReportStoreController');
    Route::resource('sales_by_date','Backend\SalesByDateController');
    Route::resource('sales_by_hour','Backend\SalesByHourController');
    Route::resource('report_category','Backend\ReportCategoryController');
    Route::resource('report_product','Backend\ReportProductController');
    Route::resource('report_member','Backend\ReportMemberController');
    Route::resource('top_category','Backend\TopCategoryController');
    Route::resource('top_product','Backend\TopProductController');
    Route::post('tag_product','Backend\ProductController@postTagProduct');
    Route::resource('orders_history','Backend\OrdersHistoryController');
    Route::resource('receipt_footer','Backend\ReceiptFooterController');
    Route::resource('product/import','Backend\ProductController@postImport');

});




/* --------------------- Theme ------------------- */
Route::group(['prefix' => '_theme'], function () {
    Route::get('blank',function(){
        return view('backend.theme_component.blank');
    });
    Route::get('form',function(){
        return view('backend.theme_component.form');
    });
    Route::get('list',function(){
        return view('backend.theme_component.list');
    });
});
