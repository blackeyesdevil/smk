<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\PrintForm;

use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\Store;
use App\Models\Product;
use App\Models\DiscountCode;
use App\Models\PaymentTransaction;

use Crypt;

class PaymentResultController extends Controller
{
    public function main($payment_status,$token,$result_string){
        $decrypted = Crypt::decrypt($token);
        $key = explode('-',$decrypted);
        $orders_id = $key[0];
        session()->flash('s_orders_id',$orders_id);
        $orders_no = $key[1];

//        $orders_id = $token;

        // insert payment transaction
        PaymentTransaction::create(['orders_id' => $orders_id, 'transaction' => $result_string]);

        if($payment_status == '1'){
            // ------------------ Change Order Status
            $data_order = Orders::find($orders_id);
            $data_order->status = 1;
            $data_order->save();

            // ------------------ ตัดโค้ด
            if($data_order->discount_code_id != 0){
                $data_discount = DiscountCode::find($data_order->discount_code_id);
                $data_discount->reserved = $data_discount->reserved -1;
                $data_discount->used = $data_discount->used + 1;
            }
            // ------------------ Print order ไปที่ครัว ตามจาน
            $printForm = new PrintForm();
            $printForm->printOrder($orders_id);
            $printForm->printReceipt01('192.168.1.102',$orders_id,'01');

            return redirect()->to('thankyou');
        }else{
            return redirect()->to('fail/'.$orders_id.'/'.$token);
        }
    }

    public function thankyou(){

//        return session()->get('s_orders_id');
        if(session()->has('s_orders_id')){
            $orders_id = session()->get('s_orders_id');
            $order = Orders::find($orders_id);
            $order_detail = OrdersDetail::where('orders_id',$orders_id);

            return view('frontend.thankyou',compact('order','order_detail'));
        }else{
            return redirect()->to('/');
        }

    }

    public function paymentFail($orders_id, $token){

        return 'payment fail';
    }
}
