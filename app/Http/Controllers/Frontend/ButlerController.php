<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Orders;
use App\Models\OrdersDetail;

use DB;

class ButlerController extends Controller
{
    public function getIndex(){
        // $table_no = $request->table_no;
        $orders = Orders::where('created_at', '>=', date('Y-m-d') . ' 00:00:00')
                          ->orderBy('orders_id', 'desc')
                          ->get();

        return view('frontend/butler-listorder', compact('orders'));
    }

    public function getButlerSearch(Request $request){
        $table_no = $request->tb_search;
        if($table_no != null){
          $orders = Orders::where('table_no', $table_no)
                            ->where('created_at', '>=', date('Y-m-d') . ' 00:00:00')
                            ->orderBy('orders_id', 'desc')
                            ->get();
          // return $table_no;
          return view('frontend/butler-listorder', compact('orders'));
        }else{
          $orders = Orders::where('created_at', '>=', date('Y-m-d') . ' 00:00:00')
                            ->orderBy('orders_id', 'desc')
                            ->get();
          // return $table_no;
          return view('frontend/butler-listorder', compact('orders'));
        }

    }

    public function getOrdersDetail($orders_id, $orders_no){
        // $table_no = $request->tb_search;
        $orders = Orders::where('orders_no', $orders_no)->whereNull('deleted_at')->first();

        if(count($orders) > 0){
          // foreach($orders as $order){
              $orders_detail = OrdersDetail::where('orders_id', $orders['orders_id'])->get();
              $orders->orders_detail = $orders_detail;

          // }
          // return $orders;
        }

        // $orders_detail = OrdersDetail::where('orders_id', $orders_id)
        //                  ->whereNUll('deleted_at')
        //                  ->get();
        // $orders_detail = $orders_detail->
        return view('frontend/butler-listorder-detail', compact('orders'));

    }
}
