<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Orders;
use App\Models\OrdersDetail;

use DB;

class OrderHistoryController extends Controller
{
    public function getIndex(Request $request){
        $table_no = $request->table_no;

        $orders = Orders::where('table_no', $table_no)
                          ->where('created_at', '>=', date('Y-m-d') . ' 00:00:00')
                          ->orderBy('orders_id', 'desc')
                          ->get();

        return view('frontend/order-history', compact('orders'));
    }
}
