<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Orders;
use App\Models\OrdersDetail;

use DB;

class CheckOrderController extends Controller
{
    public function getIndex(){


        return view('frontend/kitchen/check-order');
    }

    public function postCheck(Request $request){
        $barcode = explode('-',$request->barcode);
        $orders_id = $barcode[0];
        $orders_detail_id = $barcode[1];

        $detail = OrdersDetail::find($orders_detail_id);
        $qty = $detail->qty;
        $sent = $detail->sent;
        $status = $detail->status;

        // Change Detail Status
        if($status == 0)
            $detail->status = 1;

        // Update Sent
        if($qty > $sent){
            $detail->sent = $sent + 1;
        }
        $detail->save();

        // Update Order Status = 99
        $count_detail = OrdersDetail::select(DB::raw('sum(qty) as total_qty , sum(sent) as total_sent'))->where('orders_id',$orders_id)->first();

        if($count_detail->total_qty == $count_detail->total_sent){
            Orders::find($orders_id)->update(['status' => '99']);
        }

        return redirect()->back();
    }
}
