<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Library\PrintReport;

use App\Models\Orders;
use App\Models\Receipt;

use PDF;
use Crypt;

class ReportController extends Controller
{
    public function main(){
        return view('frontend/report/report');
    }

    public function printEOD(){
        $objPrint = new PrintReport();
        $objPrint->printEOD('192.168.1.102');
        $till = ' ';

        // return view('frontend.report.eod-print',compact('till'));
        return 'success';
    }

    public function printEODTill(){
        $objPrint = new PrintReport();
        $objPrint->printEODTill('192.168.1.102');
        $till = 100;

        return view('frontend.report.eod-print',compact('till'));
    }

    public function printZOut(){
        $objPrint = new PrintReport();
        $objPrint->printZOut('192.168.1.102','James T.');

        // $pdf = PDF::loadView('frontend.receipt.receipt-print', $result);
        // return $pdf->stream();

        $cashier_name_en = "James T.";
        return view('frontend.report.z-out-print',compact('cashier_name_en'));

    }

    public function printTransaction(){
        $objPrint = new PrintReport();
        $objPrint->printTransaction('192.168.1.102');

        // $pdf = PDF::loadView('frontend.receipt.receipt-print', $result);
        // return $pdf->stream();

        $cashier_name_en = "James T.";
        return view('frontend.report.z-out-print',compact('cashier_name_en'));

    }

    public function printABB(){
        $objPrint = new PrintReport();
        $objPrint->printABB('192.168.1.102');

        // $pdf = PDF::loadView('frontend.receipt.receipt-print', $result);
        // return $pdf->stream();

        $cashier_name_en = "James T.";
        return view('frontend.report.z-out-print',compact('cashier_name_en'));

    }

    public function printTaxInvoice(){
        $objPrint = new PrintReport();
        $objPrint->printTaxInvoice('192.168.1.102');

        // $pdf = PDF::loadView('frontend.receipt.receipt-print', $result);
        // return $pdf->stream();

        $cashier_name_en = "James T.";
        return view('frontend.report.z-out-print',compact('cashier_name_en'));

    }
}
