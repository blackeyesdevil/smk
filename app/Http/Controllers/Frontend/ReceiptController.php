<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Library\PrintForm;
use App\Library\ThSms;

use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\Receipt;

use PDF;
use Crypt;

class ReceiptController extends Controller
{
    public function main(){
        return view('frontend/receipt/receipt');
    }
    public function send(Request $request){
        $objPrint = new PrintForm();
        $list_orders_id = '';
        $arr_orders_id = $request->arr_select;
        $print_type = $request->print_type;
        $value = '';
        $company_name = $request->company_name;
        $branch = $request->branch;
        $address = $request->address;
        $tax_id = $request->tax_id;

        foreach($arr_orders_id as $orders_id){
            $list_orders_id .= ','.$orders_id;
            // change is_printed
//            Orders::find($orders_id)->update(['is_printed'=>'1']);
        }
        $list_orders_id = substr($list_orders_id,1);

        $objFn = new MainFunction();
        $gen_receipt_no = $objFn->gen_receipt_no();
        $receipt = Receipt::create([
            'receipt_no' =>  $gen_receipt_no,
            'orders_id'  =>  $list_orders_id,
            'print_type'    =>  $print_type,
            'value'     =>  $value,
            'company_name' => $company_name,
            'branch'    => $branch,
            'address'   => $address,
            'tax_id'    => $tax_id
        ]);
        $receipt_id = $receipt->receipt_id;

        $gen_token = Crypt::encrypt($gen_receipt_no);
        if($print_type == '1'){
            $value = $request->mobile;
            // send otp
            $sms = new thsms();
            $sms->username = 'eakarin';
            $sms->password = '6c4d0c';
            $a = $sms->getCredit();
            // var_dump( $a);
            // $number = "0965199914";
            $number = $value;
            $otp_number = mt_rand(100000,999999);
            $message = "Your OTP is " . $otp_number;
            $b = $sms->send( 'OTP', $number, $message);
            // var_dump( $b);
            // send sms
        }
        if($print_type == '2'){
            $value = $request->email;
            // send mail

        }
        if($print_type == 3){
            // Auto Print
            $objPrint->printReceipt02('192.168.1.102',$receipt_id);
        }
        $result = [ 'status' => '1', 'alertMsg' =>'Success!' ];
        return response()->json($result);
    }
    public function barcode(Request $request){
        $orders_no = $request->barcode;
        $order = Orders::where('orders_no',$orders_no)->first();
        if(!empty($order)){
            if($order->is_printed == '0'){
                $result = ['status'=>'1', 'alertMsg' => 'Success', 'order' => $order];
                return response()->json($result);
            }else{
                $result = ['status'=>'2', 'alertMsg' => 'Is Printed!'];
                return response()->json($result);
            }
        }else{
            $result = ['status'=>'0', 'alertMsg' => 'No Result'];
            return response()->json($result);
        }

    }
    public function printPaper($gen_token){
//        $objPrint = new PrintForm();
//        $objPrint->printReceipt02('192.168.1.102','18');
        $gen_token = Crypt::encrypt('16092900003');
        $receipt_no = Crypt::decrypt($gen_token);
        $receipt = Receipt::where('receipt_no',$receipt_no)->first();
        if(empty($receipt))
            return abort(404);

        $l_orders_id = $receipt->orders_id;
        $arr_orders_id = explode(',',$l_orders_id);

        $orders = Orders::whereIn('orders_id',$arr_orders_id)->orderBy('orders_id','desc')->get();
        $order_details = OrdersDetail::whereIn('orders_id',$arr_orders_id)->orderBy('orders_id','desc')->get();

        $result = ['receipt' => $receipt,'orders' => $orders,'order_details' => $order_details];
//        $pdf = PDF::loadView('frontend.receipt.receipt-print', $result);
//        return $pdf->stream();

        return view('frontend.receipt.receipt-print',compact('receipt', 'orders', 'order_details'));
    }

}
