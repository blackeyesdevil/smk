<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Store;

use DB;
use URL;

class StoreController extends Controller
{
    public function __construct(){
        $this->lang = session()->get('locale');
    }

    public function getIndex(){
        $lang = $this->lang;

        $img_path = URL::asset('images/');
        $stores = Store::select(DB::raw("
                        store_id,
                        store_name_en,
                        store_name_th,
                        CONCAT('" . $img_path . "/', store_logo_normal) as logo_normal,
                        CONCAT('" . $img_path . "/', store_logo_active) as logo_active"
                    ))
            ->where('is_available','1')
            ->orderBy('store_id', 'asc')
            ->take(6)
            ->get();

        $result = [ 'stores' => $stores ];
        return response()->json($result);
    }


}
