<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\ProductOption;
use App\Models\Orders;
use App\Models\OrdersDetail;

class GrabAndGoController extends Controller
{
    public function __construct(){
        $this->lang = session()->get('locale');
    }

    public function getIndex(){
        $lang = $this->lang;

        $result = [];
        return response()->json($result);
    }
    public function getBarcode($barcode){
//        $barcode = $request->barcode
        $status = 0;
        $alertMsg = 'No Result';

        $product = Product::where('barcode', $barcode)->first();
        if(!empty($product)){
            $status = 1;
            $alertMsg = 'Success';
        }
        $result = [ 'status' => $status, 'alertMsg' => $alertMsg, 'product' => $product ];
        return response()->json($result);
    }
}
