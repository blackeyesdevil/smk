<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\PrintForm;

use App\Models\Product;
use App\Models\ProductOption;
use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\PaymentLog;


class CashierController extends Controller
{
    public function __construct(){
        $this->lang = session()->get('locale');
    }

    public function getIndex(){
        $lang = $this->lang;
        $orders = Orders::whereIn('status',[0,1])
            ->orderBy('orders_id','desc')
            ->get();

        if(count($orders) > 0){
            foreach($orders as $key => $order){
                $orders_detail = OrdersDetail::where('orders_id',$order->orders_id)->get();
                $order->orders_detail = $orders_detail;
            }
        }
        $result = [ 'orders' => $orders ];
        return response()->json($result);
    }
    public function getChangeStatus($orders_id,$status){
//        $orders_id = $request->orders_id;
//        $status = $request->status;
        Orders::find($orders_id)->update(['status' => $status]);
        if($status == 1){
            // print
            $printForm = new PrintForm();
            $printForm->printOrder($orders_id);
            $printForm->printReceipt01('192.168.1.102',$orders_id,'01');
        }

        $result = [ 'status' => '1', 'alertMsg' => 'Success' ];
        return response()->json($result);
    }
    public function getDrawer()
    {
        $printForm = new PrintForm();
        $printForm->drawer('192.168.1.102');
    }
    public function getPaymentLog($orders_id){
        $payment = PaymentLog::where($orders_id)->get();
        if(count($payment) > 0){
            $result = [ 'status' => '1', 'alertMsg' => 'Success', 'payment_log' => $payment ];
            return response()->json($result);
        }else{
            $result = [ 'status' => '0', 'alertMsg' => 'No Result' ];
            return response()->json($result);
        }

    }
    public function getTestPrint($orders_id){
        $printForm = new PrintForm();
        $printForm->printOrder($orders_id);
        $printForm->printReceipt01('192.168.1.102',$orders_id,'01');
        $printForm->printReceipt01('192.168.1.102',$orders_id,'02');
        $printForm->printReceipt02('192.168.1.102','27');
    }

}
