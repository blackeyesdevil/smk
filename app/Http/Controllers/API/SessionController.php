<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class SessionController extends Controller
{
    public function __construct(){
        $this->lang = session()->get('locale');
    }

    public function getStore($store_id){
        session()->put('s_store_id',$store_id);

        $result = [ 'status' => '1', 'alertMsg' => 'Success!'];
        return response()->json($result);
    }
    public function getTable($table_no){
        session()->put('s_table_no',$table_no);

        $result = [ 'status' => '1', 'alertMsg' => 'Success!'];
        return response()->json($result);
    }


}
