<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct(){
        $this->lang = session()->get('locale');
    }
    public function getAll(){
        $categories = $this->categoryList(null);
        return response()->json($categories);
    }

    public function getByStore($store_id){
        $categories = $this->categoryList($store_id);
        return response()->json($categories);
    }

    public function categoryList($store_id = null){
        $lang = $this->lang;
        $categories = Category::orderBy('category_name_'.$lang, 'asc');
        if(!empty($store_id)){
            $categories = $categories->where('store_id',$store_id);
        }
        $categories = $categories->get();

        $result = ['categories' => $categories];
        return $result;
    }


}
