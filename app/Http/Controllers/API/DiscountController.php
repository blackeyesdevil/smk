<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\DiscountCode;
use Carbon\Carbon;

class DiscountController extends Controller
{
    public function __construct(){
        $this->lang = session()->get('locale');
    }
    public function getIndex(){

    }
    public function postCheckCode(Request $request){
        $lang = $this->lang;

        $code = $request->code;
        $total_price = $request->total_price;

        $data = DiscountCode::where('code',$code)->where('is_available','1')->first();
        // เช็คมีโค้ดมั๊ย
        if(empty($data)){
            $result = [ 'status'=>'0', 'alertMsg'=> trans('message.discount-not-found')];
            return response()->json($result);
        }

        $currentDate = Carbon::now();
        $codeId = $data->discount_code_id;
        $allot = $data->allot;
        $reserved = $data->reserved;
        $used = $data->used;
        $startDate = $data->start_date;
        $endDate = $data->end_date;
        $minPrice = $data->min_price;
        $maxPrice = $data->max_price;
        $type = $data->discount_type;
        $discount = $data->discount;
        $discountMax = $data->discount_max;
        // เช็คโค้ดหมดอายุยัง
        if($currentDate >= $startDate && $currentDate <= $endDate){
            // เช็คโค้ดหมดยัง
            if($allot == $reserved+$used){
                $result = [ 'status'=>'0', 'alertMsg'=> trans('message.discount-empty')];
                return response()->json($result);
            }

            // เช็คยอดการสั่งซื้อ
            if($total_price < $minPrice){
                $result = [ 'status'=>'0', 'alertMsg'=>trans('message.discount-min-price')];
                return response()->json($result);
            }
            if($total_price > $maxPrice){
                $result = [ 'status'=>'0', 'alertMsg'=>trans('message.discount-max-price')];
                return response()->json($result);
            }
            // Cal discount price
            if($type == 'fix'){
                $discount_price = $discount;
            }else{
                $discount_price = $total_price*$discount/100;
            }
            if($discount_price > $total_price) $discount_price = $total_price;
            if($discount_price > $discountMax ) $discount_price = $discountMax;
        }else{
            $result = [ 'status'=>'0', 'alertMsg'=>trans('message.discount-expire')];
            return response()->json($result);
        }

        $discount_price = round($discount_price,2);
        // คำนวณราคารวมทั้งหมด
        $grand_total = $total_price - $discount_price;

        $result = [ 'status'=>'1', 'alertMsg'=>trans('message.discount-correct'), 'discount_code_id' => $codeId, 'discount_price'=>$discount_price, 'grand_total'=>$grand_total];
        return response()->json($result);
    }


}
