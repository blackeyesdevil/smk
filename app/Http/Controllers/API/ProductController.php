<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;

use App\Models\ProductOption;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct(){
        $this->lang = session()->get('locale');
    }


    public function getAll(){
        $products = $this->productList(null,null,null);
        return response()->json($products);
    }
    public function getByCollection($collection_id){
        $products = $this->productList($collection_id,null,null);
        return response()->json($products);
    }
    public function getByStore($store_id){
        $products = $this->productList(null,$store_id,null);
        return response()->json($products);
    }
    public function getByCategory($category_id){
        $products = $this->productList(null,null,$category_id);
        return response()->json($products);
    }

    public function getDetail($product_id){
        $lang = $this->lang;
        $product = Product::find($product_id);
        $options = null;
        if(!empty($product)){
            // image path
            $img_path = config()->get('constants.NO_PIC');
            $img_dir = config()->get('constants.PATH_IMG_PRODUCT');
            $img_name = $product->img_name;
            $file = $img_dir.$img_name;
            if(!empty($img_name) && file_exists($file)){
                $img_path = url()->asset($file);
            }
            $product['image'] = $img_path;

            // product option
            $options = ProductOption::where('product_id', $product_id)
                ->orderBy('label_'.$lang, 'asc')
                ->get();
        }

        $result = ['product' => $product, 'options' => $options];
        return response()->json($result);

    }
    public function productList($collection_id = null, $store_id = null, $category_id = null ){
        $lang = $this->lang;
        $products = Product::where('is_available','1');
        if(!empty($collection_id))
            $products = $products->where('collection_id',$collection_id);
        if(!empty($store_id))
            $products = $products->where('store_id',$store_id);
        if(!empty($category_id))
            $products = $products->where('category_id',$category_id);

        $products = $products->get();

        if(count($products) > 0){
            foreach($products as $product){
                $img_path = config()->get('constants.NO_PIC');
                $img_dir = config()->get('constants.PATH_IMG_PRODUCT');
                $img_name = $product->img_name;
                $file = $img_dir.$img_name;
                if(!empty($img_name) && file_exists($file)){
                    $img_path = url()->asset($file);
                }
                $product['image'] = $img_path;
            }
        }

        $result = ['products' => $products];
        return $result;
    }


}
