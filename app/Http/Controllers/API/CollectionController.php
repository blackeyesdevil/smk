<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Collection;
use App\Models\Product;

class CollectionController extends Controller
{
    public function __construct(){
        $this->lang = session()->get('locale');
    }
    public function getIndex(){
        $lang = $this->lang;
        $collections = Collection::orderBy('collection_name_'.$lang,'asc')->get();
        if(count($collections) > 0){
            foreach($collections as $collection){
                $product = Product::select('img_name')
                    ->where('collection_id',$collection->collection_id)
                    ->where('is_available','1')
                    ->orderByRaw('RAND()')
                    ->first();
                $img_path = config()->get('constants.NO_PIC');
                if(!empty($product)) {
                    $img_dir = config()->get('constants.PATH_IMG_PRODUCT');
                    $img_name = $product->img_name;
                    $file = $img_dir.$img_name;
                    if(!empty($img_name) && file_exists($file)){
                        $img_path = url()->asset($file);
                    }
                }
                $collection['image'] = $img_path;
            }
        }

        $result = ['collections' => $collections];
        return response()->json($result);
    }

    public function getCollectionBar(){
        $lang = $this->lang;
        $collections = Collection::orderBy('collection_name_'.$lang,'asc')->get();
        $result = ['collections' => $collections];
        return response()->json($result);
    }
  
}
