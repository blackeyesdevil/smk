<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Library\PrintForm;

use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\Store;
use App\Models\Product;
use App\Models\DiscountCode;

use Crypt;
use PDF;
use DNS1D;
use DNS2D;

class OrderController extends Controller
{
    public function __construct(){
        $this->lang = session()->get('locale');
    }

    public function getIndex(){
        $lang = $this->lang;
        $objFn = new MainFunction();
        $gen_result = $objFn->gen_order_no(200);

        return $gen_result;
    }
    public function postConfirm(Request $request){
        $objFn = new MainFunction();
        $objPrint = new PrintForm();

        $order_details = json_decode($request->order_details,true);

//        return $order_details;
        // ----------------- Gen order number and round
        $gen_result = $objFn->gen_order_no($request->table_no);
        $orders_no = $gen_result['orders_no'];
        $round = $gen_result['round'];

        // ----------------- Insert Order
        $input_order = $request->except(['order_details','_token']);
        $input_order['orders_no'] = $orders_no; // orders number
        $input_order['round'] = $round; // จำนวนรอบ ต่อโต๊ะ ต่อวัน

        $ins_order = Orders::create($input_order);
        $orders_id = $ins_order->orders_id;
        session()->flash('s_orders_id',$orders_id);

        // ----------------- Insert Order Detail
        foreach($order_details as $detail){

            $option_list_en = '';
            $option_list_th = '';
            foreach($detail['option'] as $option){
                $option_list_en .= ','.str_replace(['(:',':)'], ['(+',')'], $option['option_value_en']);
                $option_list_th .= ','.str_replace(['(:',':)'], ['(+',')'], $option['option_value_th']);
            }
            if(!empty($option_list_en) && !empty($option_list_th)){
                $option_list_en = substr($option_list_en,1);
                $option_list_th = substr($option_list_th,1);
            }

            $input_detail = [
                'orders_id' => $orders_id,
                'product_id' => $detail['product_id'],
                'product_name_en' => $detail['product_name_en'],
                'product_name_th' => $detail['product_name_th'],
                'option_en' => $option_list_en,
                'option_th' => $option_list_th,
                'qty' => $detail['qty'],
                'price' => $detail['price']
            ];
            $ins_detail = OrdersDetail::create($input_detail);
            $orders_detail_id = $ins_detail->orders_detail_id;

            // ----------------- Get Store
            $data_product = Product::find($detail['product_id']);
            $data_store = Store::find($data_product->store_id);

        }

        // ----------------- Discount Code
        if(!empty($request->discount_id)){
            $data_discount = DiscountCode::find($request->discount_id);
            if($data_discount->allot < $data_discount->reserved + $data_discount->used){
                $result = ['status' => '0', 'alertMsg' => 'Cannot use this discount code.'];
                return response()->json($result);
            }
            $new_rev = $data_discount->reserved + 1;
            DiscountCode::find($request->discount_id)->update(['reserved' => $new_rev]);
        }
        // ----------------- Print ใบสั่งอาหาร
        $objPrint->printOrder02('192.168.1.102',$orders_id);

        $encrypted = Crypt::encrypt($orders_id.'-'.$orders_no);
        $result = ['status' => '1', 'alertMsg' => 'Success!', 'order' => $ins_order, 'encrypted' => $encrypted ];
        return response()->json($result);
    }

    public function getOrderQueue(){
        $objFn = new MainFunction();
        $orderQueue = OrdersDetail::join('orders', 'orders.orders_id', '=', 'orders_detail.orders_id');
        $orderQueue = $orderQueue->select('orders_detail.orders_detail_id','orders_detail.orders_id','orders_detail.product_id','orders_detail.product_name_en','orders_detail.option_en','orders_detail.product_name_en','orders_detail.qty','orders_detail.sent','orders_detail.price','orders_detail.status','orders.orders_no','orders.table_no','orders.orders_no','orders.status AS status_payment','orders_detail.created_at','orders_detail.deleted_at');
        $orderQueue = $orderQueue->whereNull('orders_detail.deleted_at');
        $orderQueue = $orderQueue->whereIn('orders.status', ['1', '99']);
        $orderQueue = $orderQueue->where('orders_detail.created_at', '>=', date('Y-m-d').' 00:00:00');
        $orderQueue = $orderQueue->get();

        foreach ($orderQueue as $order){
            $order->orders_no = substr($order->orders_no, 6);
            $order->time_ago = MainFunction::get_time_ago(strtotime($order->created_at));
        }

        return $orderQueue;
    }

    public function getMemberOrderHistory($member_id){
        $orders = Orders::where('member_id',$member_id);
        $orders = $orders->whereNull('deleted_at');
        $orders = $orders->get();

        if(count($orders) > 0 ){
            foreach($orders as $order){
                $orders_detail = OrdersDetail::where('orders_id', $order['orders_id'])->get();
                $order->orders_detail = $orders_detail;

            }
            return $orders;
          }

    }

}
