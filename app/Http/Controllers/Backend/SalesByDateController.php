<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\Page;

use Input;
use DB;

class SalesByDateController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\OrdersDetail'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function
        $this->page_title = 'Sales by Date'; // Page Title
        $this->a_search = ['']; // Array Search
        $this->path = '_admin/sales_by_date'; // Url Path
        $this->view_path = 'backend.sales_by_date.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {

        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = 'date';
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'asc';

        $data = $obj_model::select(DB::raw('DATE(orders.created_at) as date,
                                            COUNT(DISTINCT(orders.orders_id)) as count_order,
                                            SUM(orders_detail.qty) as sum_qty,
                                            SUM(orders_detail.price * orders_detail.qty) as sum_total'))
                        ->join('orders','orders_detail.orders_id','=','orders.orders_id')
                        ->where('orders.status', '=', '1')
                        ->where('orders_detail.status', '!=', '2');

        //Search
        $from_date = Input::get('from_date');
        if(!empty($from_date)){
            $data = $data->where('orders.created_at','>=',$from_date.' 00:00:00');
        }
        $to_date = Input::get('to_date');
        if(!empty($to_date)){
            $data = $data->where('orders.created_at','<=',$to_date.' 23:59:59');
        }

        $data = $data->groupBy(DB::raw('DATE(orders.created_at)'));

        //Header Report
        $report_title = $page_title;
        if(empty($from_date)) $from = '...';
        else $from = $obj_fn->format_date_en($from_date.' 00:00:00', 8);
        if(empty($to_date)) $through = '...';
        else $through = $obj_fn->format_date_en($to_date.' 23:59:59', 8);

        //Print & Export
        if(!empty(Input::get('mode'))){

            $count_data = $data->get()->count();
            $data = $data->orderBy($order_by,$sort_by);
            $data = $data->paginate($per_page);

            return view($this->view_path.'/report',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','report_title','from','through'));
        }

        $count_data = $data->get()->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','report_title','from','through'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {

    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {

    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
    }

}
