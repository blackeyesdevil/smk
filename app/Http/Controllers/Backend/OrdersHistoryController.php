<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\Member;
use App\Models\Page;

use Input;
use DB;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Mail;

class OrdersHistoryController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Orders'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function
        $this->page_title = 'Orders History'; // Page Title
        $this->path = '_admin/orders_history'; // Url Path
        $this->view_path = 'backend.orders_history.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {

        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $member_id = Input::get('member_id');
        $orders_detail_id = Input::get('orders_detail_id');


        $data = $obj_model;
        $data = $data->leftJoin('member', 'orders.member_id', '=', 'member.member_id')
                ->leftJoin('orders_detail', 'orders.orders_id', '=', 'orders_detail.orders_id')
                ->select(DB::raw('SUM(orders_detail.qty) as qty,member.member_name, orders_detail.orders_id, orders.created_at, orders_detail.price, orders_detail.product_name_en, orders_detail.product_name_th'))
                ->where('orders.member_id', $member_id)
                ->groupBy('orders_detail.product_id');
        $count_data = $data->count();
        $data = $data->orderBy('qty','desc')->take(5)->get();

        $member = Member::where('member_id', $member_id)
                    ->select('member_name')
                    ->first();

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','member'));

    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {

    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        
    }

}
