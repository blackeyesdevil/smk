<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\OrdersDetail;
use App\Models\Orders;
use App\Models\Member;
use App\Models\Page;

use Input;
use DB;

class ReportMemberController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Orders'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->tbName = 'orders';
        $this->obj_fn = new MainFunction(); // Obj Function
        $this->page_title = 'Sales by Member'; // Page Title
        $this->a_search = array('member.customer_name'); // Array Search
        $this->path = '_admin/report_member'; // Url Path
        $this->view_path = 'backend.report_member.'; // View Path

        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID

    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = "orders_detail.".$obj_model->primaryKey;
        // if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';
        $search1 = Input::get('search1');
        $date = Input::get('date');

        $member = Member::all();
        $data = $obj_model;
        $total = Orders::sum('total_price');
        $data = $data::leftjoin('member','member.member_id','=','orders.member_id')
                    ->leftJoin('orders_detail','orders_detail.orders_id','=','orders.orders_id')
                    ->where('orders.status', '=', '1')
                    ->select(DB::raw('COUNT(orders_detail.orders_id) as count_order, 
                                    SUM(orders_detail.qty) as sum_qty, 
                                    SUM(orders_detail.price * orders_detail.qty) as sum_price,
                                    SUM((orders_detail.price * orders_detail.qty) - ((orders.discount_price / orders.total_price) * (orders_detail.price * orders_detail.qty))) as net_sales,
                                    SUM((orders.discount_price / orders.total_price) * (orders_detail.price * orders_detail.qty)) as discount_price,
                                    orders.member_id, member.member_name'))
                    ->groupBy('orders.member_id');

        if(!empty($search1))
        {
            $data = $data->where(function($query) use ($search1){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field,'like','%'.$search1.'%');
                }
            });
        }
        
        $from_date = Input::get('from_date');
        if(!empty($from_date)){
            $data = $data->where('orders.created_at','>=',$date.' '.$from_date.':00:00');
        }

        $to_date = Input::get('to_date');
        if(!empty($to_date)){
            $data = $data->where('orders.created_at','<=',$date.' '.$to_date.':59:59');
        } 

        $report_title = $page_title;
        if(empty($from_date)) $from = '...';
        else $from = $obj_fn->format_date_en($from_date.' 00:00:00', 8);
        if(empty($to_date)) $through = '...';
        else $through = $obj_fn->format_date_en($to_date.' 23:59:59', 8);
        
        if(!empty(Input::get('mode'))){
            $count_data = $data->get()->count();
            $data = $data->orderBy($order_by,$sort_by);
            $data = $data->paginate($per_page);

            return view($this->view_path.'/report',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','total','member','from','through','report_title'));
        }
        $count_data = $data->get()->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','total','member','from','through','report_title'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {

    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {

    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {

    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {

    }

}
