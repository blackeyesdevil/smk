<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Store;
use App\Models\Page;
use Input;
use Hash;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Category'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Category'; // Page Title
        $this->a_search = ['category_name_en','category_name_th']; // Array Search
        $this->path = '_admin/category'; // Url Path
        $this->view_path = 'backend.category.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()

    {
        if (session()->get('s_admin_store_id') <> 0) {
            
            if (session()->get('s_admin_store_id') <> Input::get('store_id')){
                        return abort(403);

                }
        }


        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'r');

        //Store Admin
        if (session()->get('s_admin_store_id') <> 0) {
            if (session()->get('s_admin_store_id') <> Input::get('store_id')) {
                return abort(403);
            }
            //return redirect()->to($this->path.'?store_id='.session()->get('s_admin_store_id'));
        }

        $path = $this->path;
        $page_title = $this->page_title;


        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $store_id = Input::get('store_id');

        // $data = $obj_model->join('admin_role','admin.admin_role_id','=','admin_role.admin_role_id');
        $data = $obj_model::where('store_id',$store_id);
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere($field,'like','%'.$search.'%');
               }
            });
        }
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        $store = Store::where('store_id',$store_id)
            ->select('store_name_en','store_name_th')
            ->first();

        if(!empty($store_id)){
            return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','store','permission'));
        }else{
            return redirect()->to('_admin/store');
        }

    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
if (session()->get('s_admin_store_id') <> 0) {
         if (session()->get('s_admin_store_id') <> Input::get('store_id')){
                        return abort(403);

                }
            }
        $permission = $obj_fn->permission($this->page_id,'c');
        $store_id = Input::get('store_id');

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        if(!empty($store_id)){
            return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn'));
        }else{
            return redirect()->to('_admin/store');
        }
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
$obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');

        $input = $request->all(); // Get all post from form
        $data = $obj_model->create($input);

        return redirect()->to($this->path."?store_id=".input::get('store_id'));
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
if (session()->get('s_admin_store_id') <> 0) {
         if (session()->get('s_admin_store_id') <> Input::get('store_id')){
                        return abort(403);

                }
            }
        $permission = $obj_fn->permission($this->page_id,'u');
        $store_id = Input::get('store_id');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';

        $data = $obj_model->find($id);

        //$roles = AdminRole::all();

        if(!empty($store_id)){
            return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn'));
        }else{
            return redirect()->to('_admin/store');
        }

    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
if (session()->get('s_admin_store_id') <> 0) {
         if (session()->get('s_admin_store_id') <> Input::get('store_id')){
                        return abort(403);

                }
            }
        $permission = $obj_fn->permission($this->page_id,'u');

        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $data = $obj_model->find($id)->update($input);

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

      
        $permission = $obj_fn->permission($this->page_id,'d');
        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
