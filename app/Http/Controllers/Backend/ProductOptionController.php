<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Product;
use App\Models\Page;

use Input;
use Hash;

class ProductOptionController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\ProductOption'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Product Option'; // Page Title
        $this->a_search = ['label_en','label_th','value_en','value_th']; // Array Search
        $this->path = '_admin/product_option'; // Url Path
        $this->view_path = 'backend.product_option.'; // View Path

        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'r');


        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $product_id = Input::get('product_id');

        //Store Admin
        if (session()->get('s_admin_store_id') != 0) {
            $store_id = Product::where('product_id',Input::get('product_id'))->first()->store_id;
            if (session()->get('s_admin_store_id') <> $store_id) {
                return abort(403);
            }
        }

        $data = $obj_model::where('product_id','=',$product_id);

        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field,'like','%'.$search.'%');
                }
            });
        }
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        $product = Product::where('product_id',$product_id)
                    ->select('product_name_en','product_name_th')
                    ->first();
    // print_r($data);
        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','product','permission'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'c');

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $product_id = input::get('product_id');

        $product_name = Product::where('product_id',$product_id)
            ->select('product_name_th','product_name_en')
            ->first();

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn','product_name'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $str_param = $request->str_param;
        $permission = $obj_fn->permission($this->page_id,'c');

        $input = $request->all(); // Get all post from form

        $data = $obj_model->create($input);

        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';

        $data = $obj_model->find($id);
        $product_id = input::get('product_id');

        $product_name = Product::where('product_id',$product_id)
            ->select('product_name_th','product_name_en')
            ->first();

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','product_name'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_model = $this->obj_model;

        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $data = $obj_model->find($id)->update($input);

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_model = $this->obj_model;
        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
