<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Page;
use App\Models\TenantPerson;
use App\Models\Tenant;

use Input;
use Hash;

class TenantPersonController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\TenantPerson'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Tenant Person'; // Page Title
        $this->a_search = ['firstname_en','firstname_th']; // Array Search
        $this->path = '_admin/tenant_person'; // Url Path
        $this->view_path = 'backend.tenant_person.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID

    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $tenant_id = Input::get('tenant_id');

        $data = $obj_model::where('tenant_id',$tenant_id);

        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
               foreach($this->a_search as $field)
               {
                   $query = $query->orWhere($field,'like','%'.$search.'%');
               }
            });
        }
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        //Check limit
        $limit = Tenant::where('tenant_id',$tenant_id)->first()->number_of_persons;
        $count_tenant_person = TenantPerson::where('tenant_id',$tenant_id)->count();
        if ($limit <= $count_tenant_person) {
            $permission['c'] = 0;
        }

        //Get Tenant
        $tenant = Tenant::where('tenant_id',$tenant_id)->first();

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','tenant'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'c');

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $tenant_id = Input::get('tenant_id');

        //Check limit
        $limit = Tenant::where('tenant_id',$tenant_id)->first()->number_of_persons;
        $count_tenant_person = TenantPerson::where('tenant_id',$tenant_id)->count();
        if ($limit <= $count_tenant_person) {
            return abort(403);
        }

        //Gen Barcode
        $num_row = 1;
        while ($num_row <> 0) {
            $gen_barcode = $obj_fn->gen_random(10,2);
            $num_row = TenantPerson::where('barcode','=',$gen_barcode)->count();
        }

        //Get Tenant
        $tenant = Tenant::where('tenant_id',$tenant_id)->first();

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn','gen_barcode','tenant'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'c');

        $str_param = $request->str_param;
        $input = $request->all(); // Get all post from form
        $data = $obj_model->create($input);

        return redirect()->to($this->path.'?'.$str_param);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';

        $data = $obj_model->find($id);

        $tenant_id = Input::get('tenant_id');

        //Gen Barcode
        $num_row = 1;
        while ($num_row <> 0) {
            $gen_barcode = $obj_fn->gen_random(10,2);
            $num_row = TenantPerson::where('barcode','=',$gen_barcode)->count();
        }

        //Get Tenant
        $tenant = Tenant::where('tenant_id',$tenant_id)->first();

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','gen_barcode','tenant'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');

        $input = $request->except(['_token','_method','str_param']); // Get all post from form

        $data = $obj_model->find($id)->update($input);

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'d');

        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }

}
