<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\Store;

use Input;
use Hash;
use DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Orders'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Dashboard'; // Page Title
        $this->a_search = ['']; // Array Search
        $this->path = '_admin/orders'; // Url Path
        $this->view_path = 'backend'; // View Path
    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $path = $this->path;
        $page_title = $this->page_title;

        $scope_data = array('monthly'=>'Y-m-', 'today'=>'Y-m-d');

        foreach ($scope_data as $key => $value) {
            $data = array();

            //---All Stores---
            $total = OrdersDetail::join('orders', 'orders.orders_id', '=', 'orders_detail.orders_id')
                    ->where('orders.created_at', 'like', date($value).'%')
                    ->where('orders.status', '=', '1')
                    ->select(DB::raw('CONCAT(orders_detail.qty * orders_detail.price) as sum_total'))
                    ->get()
                    ->sum('sum_total');

            $discount = OrdersDetail::join('orders', 'orders.orders_id', '=', 'orders_detail.orders_id')
                                ->where('orders.created_at', 'like', date($value).'%')
                                ->where('orders.status', '=', '1')
                                ->select(DB::raw('CONCAT((orders.discount_price / orders.total_price) * (orders_detail.qty * orders_detail.price)) as sum_discount'))
                                ->get()
                                ->sum('sum_discount');

            $count = Orders::where('orders.created_at', 'like', date($value).'%')
                            ->where('orders.status', '=', '1')
                            ->count();


            $total_price = $total - $discount;
            if ($count == 0) {$aos = 0;}
            else{$aos = $total_price / $count;}

            $data['all_stores'] = ['count'=>$count, 'total_price'=>$total_price, 'aos'=>$aos];

            //---Each Store---
            $stores = Store::orderBy('store_name_th', 'asc')->get();
            foreach ($stores as $store) {
                $total = OrdersDetail::join('orders', 'orders.orders_id', '=', 'orders_detail.orders_id')
                                    ->join('product', 'product.product_id', '=', 'orders_detail.product_id')
                                    ->where('orders.created_at', 'like', date($value).'%')
                                    ->where('orders.status', '=', '1')
                                    ->where('product.store_id', '=', $store->store_id)
                                    ->select(DB::raw('CONCAT(orders_detail.qty * orders_detail.price) as sum_total'))
                                    ->get()
                                    ->sum('sum_total');

                $discount = OrdersDetail::join('orders', 'orders.orders_id', '=', 'orders_detail.orders_id')
                                    ->join('product', 'product.product_id', '=', 'orders_detail.product_id')
                                    ->where('orders.created_at', 'like', date($value).'%')
                                    ->where('orders.status', '=', '1')
                                    ->where('product.store_id', '=', $store->store_id)
                                    ->select(DB::raw('CONCAT(orders.discount_price / orders.total_price * orders_detail.qty * orders_detail.price) as sum_discount'))
                                    ->get()
                                    ->sum('sum_discount');

                $count = OrdersDetail::join('orders', 'orders.orders_id', '=', 'orders_detail.orders_id')
                                    ->join('product', 'product.product_id', '=', 'orders_detail.product_id')
                                    ->where('orders.created_at', 'like', date($value).'%')
                                    ->where('orders.status', '=', '1')
                                    ->where('product.store_id', '=', $store->store_id)
                                    ->groupBy('orders_detail.orders_id')
                                    ->get()
                                    ->count();

                $total_price = $total - $discount;
                if ($count == 0) {$aos = 0;}
                else{$aos = $total_price / $count;}
                $data[$store->store_name_en] = ['count'=>$count, 'total_price'=>$total_price, 'aos'=>$aos];

                if ($key == 'monthly') {
                    $monthly_data = $data;
                }else{
                    $today_data = $data;
                }

            }
        }

        return view($this->view_path.'/index',compact('page_title','path','obj_model','obj_fn','stores','monthly_data','today_data'));
    }
}
