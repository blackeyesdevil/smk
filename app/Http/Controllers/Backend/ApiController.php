<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use DB;
use Input;
use Hash;
use Validator;

class ApiController extends Controller
{

    // ----------------------------------------- Show All List Page

    public function changeActive()
    {
        $rules = array('pk_field'=>'required','v_pk_field'=>'required','change_field'=>'required','value'=>'required','tb_name'=>'required');
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {


            if(Input::get('value') == '1')
            {
                $Obj = DB::table(Input::get('tb_name'))
                    ->where(Input::get('pk_field'),Input::get('v_pk_field'))
                    ->update(array(
                        Input::get('change_field') => 0,
                        'updated_at' => date('Y-m-d H:i:s')

                    ));

                echo "0";
            }else{

                $Obj = DB::table(Input::get('tb_name'))
                    ->where(Input::get('pk_field'),Input::get('v_pk_field'))
                    ->update(array(
                        Input::get('change_field') => 1,
                        'updated_at' => date('Y-m-d H:i:s')

                    ));

                echo "1";
            }
        }
    }

    public function delProduct()
    {
        $rules = array('value' => 'required', 'blog_id' => 'required', 'tb_name' => 'required');
        $validator = Validator::make(Input::all(),$rules);
        $value = Input::get('value');
        $blog_id = Input::get('blog_id');
        $tb_name = Input::get('tb_name');

        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {

            DB::table($tb_name)->where('blog_id', $blog_id)->where('product_id', $value)->delete();

            echo "0";


        }
    }


    public function delCategory()
    {
        $rules = array('value' => 'required', 'product_id' => 'required', 'tb_name' => 'required');
        $validator = Validator::make(Input::all(),$rules);
        $value = Input::get('value');
        $product_id = Input::get('product_id');
        $tb_name = Input::get('tb_name');

        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {

            $data_cat = DB::table('category')->select('count','category_id')->where('category_id',$value)->first();

            $count = $data_cat->count-1;
            DB::table('category')->where('category_id',$data_cat->category_id)->update(['count' => $count]);

            DB::table('product_category')->where('product_id', $product_id)->where('category_id', $value)->delete();

            echo "0";


        }
    }



    public function Province(Request $request)
    {
        $province_id = $request->province_id;


        if ($province_id != '') {

            $amphurs = Amphur::where('province_id',$province_id)->select('amphur_id','amphur_name_th')->get();

            $a_amphur = "";

            if (count($amphurs) == 0) {

                $a_amphur = "<option value='0' class='form-control select2-container select2me'>Select Amphur</option>";

            }else{

                foreach ($amphurs as $amphur) {
                    $amphur_name_th = $amphur->amphur_name_th;
                    $amphur_id = $amphur->amphur_id;
                    $a_amphur .= "<option value='$amphur_id' class='form-control select2-container select2me'>$amphur_name_th</option>";

                }
            }


            return $a_amphur;

        }else{

            echo "error";

        }

    }
    public function Amphur(Request $request){

        $amphur_id = $request->amphur_id;

        if ($amphur_id != '') {

            $districts = District::where('amphur_id',$amphur_id)->select('district_id','district_name_th')->get();

            $a_district = "";

            if (count($districts) == 0) {

                $a_district = "<option value='0' class='form-control select2-container select2me'>Select District</option>";

            }else{

                foreach ($districts as $district) {
                    $district_name_th = $district->district_name_th;
                    $district_id = $district->district_id;
                    $a_district .= "<option value='$district_id' class='form-control select2-container select2me'>$district_name_th</option>";

                }
            }


            return $a_district;

        }else{

            echo "error";

        }


    }


    public function changeCategory()
    {
        $rules = array('store_id' => 'required');
        $validator = Validator::make(Input::all(),$rules);
        $value = Input::get('store_id');
        $value2 = Input::get('value2');

        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {

            $data = DB::table('category')
                ->where('store_id', $value)
                ->whereNull('deleted_at')
                ->select('category_id','category_name_en')
                ->get();

            $option = '<option value="">Select Category</option>';
            foreach ($data as $value){
                $selected = '';
                if($value->category_id == $value2){
                    $selected = 'selected';
                }
                $option .='<option value='.$value->category_id.' '.$selected.'>'.$value->category_name_en.'</option>';
            }

            return $option;


        }
    }

    public function changeCategory2()
    {
        $rules = array('value' => 'required');
        $validator = Validator::make(Input::all(),$rules);
        $value = Input::get('value');
        $value2 = Input::get('value2');

        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {

            $data = DB::table('category')
                ->where('store_id', $value)
                ->whereNull('deleted_at')
                ->select('category_id','category_name_en')
                ->get();

            $option = '<option value="">Select Category</option>';
            foreach ($data as $value){
                $selected = '';
                if($value->category_id == $value2){
                    $selected = 'selected';
                }
                $option .='<option value='.$value->category_id.' '.$selected.'>'.$value->category_name_en.'</option>';
            }

            return $option;


        }
    }

    public function changeProduct()
    {
        $rules = array('category_id' => 'required');
        $validator = Validator::make(Input::all(),$rules);
        $value = Input::get('category_id');
        $value2 = Input::get('value2');

        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {

            $data = DB::table('product')
                ->where('category_id', $value)
                ->whereNull('deleted_at')
                ->select('product_id','price','product_name_en')
                ->get();

            $option = '';
            foreach ($data as $value){
                $selected = '';
                if($value->product_id == $value2){
                    $selected = 'selected';
                }

                $val = $value->product_id.':'.$value->price.':'.$value->product_name_en;
                $option .='<option value='.$val.'>'.$value->product_name_en.'</option>';
//                $option .='<option value='.$value->product_name_en.' '.$selected.'>'.$value->product_name_en.'</option>';
            }

            return $option;
        }
    }



}
