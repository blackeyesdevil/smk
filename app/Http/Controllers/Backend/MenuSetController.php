<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Page;
use App\Models\Product;
use App\Models\Category;
use App\Models\Store;

use Input;
use DB;

class MenuSetController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\MenuSet'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        $this->page_title = 'Menu Set'; // Page Title
        $this->a_search = ['menu_set_name_th','menu_set_name_en']; // Array Search
        $this->path = '_admin/menu_set'; // Url Path
        $this->view_path = 'backend.menu_set.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID

    }

    // ------------------------------------ Show All List Page
    public function index()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');

        // $data = $obj_model->join('admin_role','admin.admin_role_id','=','admin_role.admin_role_id');
        $data = $obj_model;
        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field,'like','%'.$search.'%');
                }
            });
        }
        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission'));
    }
    // ------------------------------------ View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'c');

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $product = Product::all();
        $category = Category::all();
        $store = Store::all();

        return view($this->view_path.'update',compact('page_title','url_to','method','txt_manage','obj_model','obj_fn','product','category','store'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'c');
        $input = $request->all(); // Get all post from form
        $data = $obj_model->create($input);

        for($i=1;$i<=5;$i++){
            $product_name = '';

            if(!empty($request->product_name_set[$i]) and !empty($request->set_name_detail_th[$i])){

                $product_name_set = explode(',',$request->product_name_set[$i]);
                foreach ($product_name_set as $key => $value){
                    $sub_product[$i][$key] = explode(':',$value);
                }

                foreach ($sub_product[$i] as $key => $value2){
                    $product_name .= ",".$value2[0].":".$value2[1];
                }

                DB::table('menu_set_detail')
                    ->insert(['menu_set_id' => $data->menu_set_id ,
                        'set_name_detail_th' => $request->set_name_detail_th[$i] ,
                        "set_name_detail_en" => $request->set_name_detail_th[$i] ,
                        "product_name_set" => substr($product_name,1) ,
                        "qty" => $request->qty[$i] ,
                        "deleted_at" => NULL ]);

            }
        }

        return redirect()->to($this->path);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $path = $this->path;
        $page_title = $this->page_title;

        $data = $obj_model->join('menu_set_detail','menu_set_detail.menu_set_id','=','menu_set.menu_set_id')
            ->where('menu_set.menu_set_id',$id)
            ->get();

        $menu_set_name = $data[0]->menu_set_name_th." (".$data[0]->menu_set_name_en.")";

        foreach ($data as $key => $value) {
            $product_name_set = explode(',', $value->product_name_set);
            foreach ($product_name_set as $key2 => $value2) {
                $sub_product[$value->menu_set_detail_id][$key2] = explode(':', $value2);

                $product_id = $sub_product[$value->menu_set_detail_id][$key2][0];
                $product = DB::table('product')->select('product_id','product_name_th','product_name_en','price','img_name')
                    ->where('product_id',$product_id)
                    ->first();
                $data_product[$value->menu_set_detail_id][$key2] = $product;
            }
        }

//            return $data_product;

        return view($this->view_path.'menu_set_detail',compact('page_title','data','path','obj_model','obj_fn','data_product','sub_product','menu_set_name'));
    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';

        $data = $obj_model->find($id);
        $product = Product::all();
        $category = Category::all();
        $store = Store::all();

        //$roles = AdminRole::all();

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','product','category','store'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'u');

        for($i=1;$i<=5;$i++){
            $product_name = '';

            if(!empty($request->product_name_set[$i]) and !empty($request->set_name_detail_th[$i])){

                $product_name_set = explode(',',$request->product_name_set[$i]);
                foreach ($product_name_set as $key => $value){
                    $sub_product[$i][$key] = explode(':',$value);
                }

                foreach ($sub_product[$i] as $key => $value2){
                    $product_name .= ",".$value2[0].":".$value2[1];
                }

                $count = DB::table('menu_set_detail')
                    ->where('menu_set_detail_id',$request->menu_set_detail_id[$i])
                    ->count();

                if($count == 1) {

                    DB::table('menu_set_detail')
                        ->where('menu_set_detail_id',$request->menu_set_detail_id[$i])
                        ->update(['set_name_detail_th' => $request->set_name_detail_th[$i] ,
                            "set_name_detail_en" => $request->set_name_detail_th[$i] ,
                            "product_name_set" => trim(substr($product_name,1)) ,
                            "qty" => $request->qty[$i]]);

                }else{

                    DB::table('menu_set_detail')
                        ->insert(['menu_set_id' => $id ,
                            'set_name_detail_th' => $request->set_name_detail_th[$i] ,
                            "set_name_detail_en" => $request->set_name_detail_th[$i] ,
                            "product_name_set" => trim(substr($product_name,1)) ,
                            "qty" => $request->qty[$i] ,
                            "deleted_at" => NULL ]);
                    }
            }else{
                DB::table('menu_set_detail')->where('menu_set_detail_id', $request->menu_set_detail_id[$i])->delete();
            }
        }


        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $data = $obj_model->find($id)->update($input);

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?1'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'d');

        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }
}
