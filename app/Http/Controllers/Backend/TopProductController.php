<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\OrdersDetail;
use App\Models\Orders;
use App\Models\Product;
use App\Models\Category;
use App\Models\Store;
use App\Models\Page;

use Input;
use DB;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Mail;

class TopProductController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\OrdersDetail'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->tbName = 'orders_detail';
        $this->fieldList = array('orders_id','product_id','product_name_en','product_name_th','qty','price');
        $this->obj_fn = new MainFunction(); // Obj Function
        $this->page_title = 'Top Selling Product'; // Page Title
        $this->a_search = array('product.category_id'); // Array Search
        $this->path = '_admin/top_product'; // Url Path
        $this->view_path = 'backend.top_product.'; // View Path

        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID

    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');
        $order_by = Input::get('order_by');
        // if(empty($order_by)) $order_by = "orders_detail.".$search;
        // if(empty($order_by)) $order_by = "orders_detail.".$obj_model->primaryKey;
        if(empty($order_by)) $order_by = "orders_detail.".$obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';
        $search = Input::get('search');
        $date = Input::get('date');
        $categories = Category::all();
        foreach ($categories as $category) {
            $a_data[$category->category_id] = $obj_model;
            $a_data[$category->category_id] = $a_data[$category->category_id]::leftjoin('orders','orders_detail.orders_id','=','orders.orders_id')
                                                                            ->leftjoin('product','product.product_id','=','orders_detail.product_id')
                                                                            ->leftjoin('category','product.category_id','=','category.category_id')
                                                                            ->where('orders.status','=','1')
                                                                            ->where('category.category_id','=',$category->category_id)
                                                                            ->select(DB::raw('COUNT(orders_detail.orders_id) as count_order,
                                                                                            SUM(orders_detail.qty) as sum_qty,
                                                                                            SUM(orders_detail.price * orders_detail.qty) as net_sales,
                                                                                            SUM((orders_detail.price * orders_detail.qty) - product.cost) as profit,
                                                                                            SUM(((orders_detail.price * orders_detail.qty) / ((orders_detail.price * orders_detail.qty) - product.cost)) * 100) as profit_ratio,
                                                                                            orders_detail.product_name_en,
                                                                                            orders_detail.product_name_th,
                                                                                            orders_detail.created_at,
                                                                                            orders_detail.qty,
                                                                                            product.cost'))
                                                                            ->groupBy('orders_detail.product_id');
            $from_date = Input::get('from_date');
            if(!empty($from_date)){
                $a_data[$category->category_id] = $a_data[$category->category_id]->where('orders.created_at','>=',$date.' '.$from_date.':00:00');
            }

            $to_date = Input::get('to_date'); 
            if(!empty($to_date)){
                $a_data[$category->category_id] = $a_data[$category->category_id]->where('orders.created_at','<=',$date.' '.$to_date.':59:59');
            }
            // $a_count_data[$category->category_id] = $a_data[$category->category_id]->whereBetween('orders.created_at',[Input::get('from_date').' 00:00:00',Input::get('to_date').' 23:59:59']);
            // $a_data[$category->category_id] = $a_data[$category->category_id]->where('orders.created_at','>=',$date.' '.$from_date.':00:00')
            //                                                                 ->where('orders.created_at','<=',$date.' '.$to_date.':59:59');
            $a_count_data[$category->category_id] = $a_data[$category->category_id]->get()->count();

            if($search == 'qty'){
                $a_data[$category->category_id] = $a_data[$category->category_id]->orderBy('sum_qty',$sort_by)->take(10)->get();
            }elseif($search == 'price'){
                $a_data[$category->category_id] = $a_data[$category->category_id]->orderBy('net_sales',$sort_by)->take(10)->get();
            }
        }

        $report_title = $page_title;
        if(empty($from_date)) $from = '...';
        else $from = $obj_fn->format_date_en($from_date.' 00:00:00', 8);
        if(empty($to_date)) $through = '...';
        else $through = $obj_fn->format_date_en($to_date.' 23:59:59', 8);


        if(!empty(Input::get('mode'))){
            return view($this->view_path.'/report',compact('page_title','a_count_data','a_data','path','obj_model','obj_fn','permission','from','through','report_title'));
        }


        return view($this->view_path.'index',compact('page_title','a_count_data','a_data','path','obj_model','obj_fn','permission','from','through','report_title'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {

    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {

    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {

    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {

    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {

    }

}
