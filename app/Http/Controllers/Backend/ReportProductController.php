<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\OrdersDetail;
use App\Models\Orders;
use App\Models\Store;
use App\Models\Product;
use App\Models\Category;
use App\Models\Page;

use Input;
use DB;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class ReportProductController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\OrdersDetail'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->tbName = 'orders_detail';
        $this->fieldList = array('orders_id','product_id','product_name_en','product_name_th','qty','price');
        $this->obj_fn = new MainFunction(); // Obj Function
        $this->page_title = 'Sales by Product'; // Page Title
        $this->a_search = array('orders_detail.product_name_en','orders_detail.product_name_th'); // Array Search
        $this->path = '_admin/report_product'; // Url Path
        $this->view_path = 'backend.report_product.'; // View Path

        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID

    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = "orders_detail.".$obj_model->primaryKey;
        // if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');
        $date = Input::get('date');
        $store = Store::where('is_available',1)->get();
        $product = Product::all();
        $category = Category::all();
        $total = Orders::sum('total_price');
        $data = $obj_model;
        $data = $data::leftJoin('orders','orders_detail.orders_id','=','orders.orders_id')
                    ->leftJoin('product','orders_detail.product_id','=','product.product_id')
                    ->leftJoin('store','product.store_id','=','store.store_id')
                    ->leftJoin('category','product.category_id','=','category.category_id')
                    ->leftJoin('member','orders.member_id','=','member.member_id')
                    ->where('orders.status', '=', '1')
                    ->select(DB::raw('COUNT(orders_detail.orders_id) as count_order, 
                                    SUM(orders_detail.qty) as sum_qty, 
                                    SUM(orders_detail.price * orders_detail.qty) as sum_price,
                                    SUM((orders_detail.price * orders_detail.qty) - ((orders.discount_price / orders.total_price) * (orders_detail.price * orders_detail.qty))) as net_sales,
                                    SUM((orders.discount_price / orders.total_price) * (orders_detail.price * orders_detail.qty)) as discount_price, 
                                    orders_detail.product_name_en, 
                                    orders_detail.product_name_th,
                                    orders_detail.created_at,
                                    store.store_name_en,
                                    product.store_id, 
                                    store.address'))
                    ->groupBy('orders_detail.product_id');

        $from_date = Input::get('from_date');
        if(!empty($from_date)){
            $data = $data->where('orders.created_at','>=',$date.' '.$from_date.':00:00');
        }

        $to_date = Input::get('to_date');
        if(!empty($to_date)){
            $data = $data->where('orders.created_at','<=',$date.' '.$to_date.':59:59');
        }

        $report_title = $page_title;
        if(empty($from_date)) $from = '...';
        else $from = $obj_fn->format_date_en($from_date.' 00:00:00', 8);
        if(empty($to_date)) $through = '...';
        else $through = $obj_fn->format_date_en($to_date.' 23:59:59', 8);

        $store_id = Input::get('store_id');
        if(!empty($store_id)){
            $data = $data->where('product.store_id',$store_id);
        }

        $category_id = Input::get('category_id');
        if(!empty($category_id)){
            $data = $data->where('product.category_id',$category_id);
        }

        $product_id = Input::get('product_id');
        if(!empty($product_id)){
            $data = $data->where('product.product_id',$product_id);
        }

        if(!empty(Input::get('mode'))){
            // $data = $data->whereBetween('orders_detail.created_at',[Input::get('from_date').' 00:00:00',Input::get('to_date').' 23:59:59']);
            $count_data = $data->get()->count();
            $data = $data->orderBy($order_by,$sort_by);
            $data = $data->paginate($per_page);

            return view($this->view_path.'/report',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','total', 'store', 'product','category','from','through','report_title'));
        }
        // $data = $data->whereBetween('orders_detail.created_at',[Input::get('from_date').' 00:00:00',Input::get('to_date').' 23:59:59']);
        $count_data = $data->get()->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','total', 'store', 'product','category','from','through','report_title'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {

    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {

    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {

    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {

    }

}
