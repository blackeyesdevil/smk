<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Product;
use App\Models\Store;
use App\Models\Collection;
use App\Models\Category;
use App\Models\Page;

use Input;
use DB;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\Product'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function
        $this->page_title = 'Product'; // Page Title
        $this->a_search = ['product_name_th', 'product_name_en']; // Array Search
        $this->path = '_admin/product'; // Url Path
        $this->view_path = 'backend.product.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID

    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {

        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'r');

        $path = $this->path;
        $page_title = $this->page_title;
        $per_page = config()->get('constants.PER_PAGE');

        $order_by = Input::get('order_by');
        if(empty($order_by)) $order_by = $obj_model->primaryKey;
        $sort_by = Input::get('sort_by');
        if(empty($sort_by)) $sort_by = 'desc';

        $search = Input::get('search');

        if (session()->get('s_admin_store_id') != 0) {
            $data = $obj_model->where('product.store_id','=', session()->get('s_admin_store_id') );

        }else $data = $obj_model;

        if(!empty($search))
        {
            $data = $data->where(function($query) use ($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field,'like','%'.$search.'%');
                }
            });
        }

        $count_data = $data->count();
        $data = $data->orderBy($order_by,$sort_by);
        $data = $data->paginate($per_page);

//        $data2 = $obj_model->find($id);
//        if (session()->get('s_admin_store_id') != 0) {
//            $store = Store::where('store_id','=', session()->get('s_admin_store_id') )->get();
//        }else $store = Store::where('is_available',1)->get();
//        $collection = Collection::all();
//        if (session()->get('s_admin_store_id') != 0) {
//            $category = Category::where('store_id','=', session()->get('s_admin_store_id') )->get();
//        }else $category = Category::all();

        $categorys = Category::all();

        return view($this->view_path.'index',compact('page_title','count_data','data','path','obj_model','obj_fn','permission','categorys'));

    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'c');

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";



        if (session()->get('s_admin_store_id') != 0) {
            $store = Store::where('store_id','=', session()->get('s_admin_store_id') )->get();
        }else $store = Store::where('is_available',1)->get();
        $collection = Collection::all();
        if (session()->get('s_admin_store_id') != 0) {
            $category = Category::where('store_id','=', session()->get('s_admin_store_id') )->get();
        }else $category = Category::all();

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','store','collection','category'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'c');

        $input = $request->all(); // Get all post from form
        $data = $obj_model->create($input);


        if (Input::hasFile('img_name')) { // เพิ่มตรงนี้
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/product');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;

            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   //
            $obj_fn->image_resize($photo, $destinationPath,100, $filename);   // resize image

            $data = $obj_model::find($data->product_id);
            $data->img_name = $filename;
            $data->save();

        }


        return redirect()->to($this->path);
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';

        $data = $obj_model->find($id);
        if (session()->get('s_admin_store_id') != 0) {
            $store = Store::where('store_id','=', session()->get('s_admin_store_id') )->get();
        }else $store = Store::where('is_available',1)->get();
        $collection = Collection::all();
        if (session()->get('s_admin_store_id') != 0) {
            $category = Category::where('store_id','=', session()->get('s_admin_store_id') )->get();
        }else $category = Category::all();
        
        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','store','collection','category'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
//        return $request->category_id;
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'u');

        $data = $obj_model->find($id);

        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $obj_model->find($id)->update($input);

        if (Input::hasFile('img_name')) { // test P.
            $photo = $request->file('img_name');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/product');           // set path
            $obj_fn->del_storage($path,$old_name);                   // delete old picture in storage
            $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $obj_fn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $obj_fn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
            $data = $obj_model::find($id);
            $data->img_name = $filename;
            $data->save();
        }
        else if ($request->img_del == 'y'){
            $old_name = $data->img_name;                            // get old name
            $path = public_path('uploads/product');           // set path
            $obj_fn->del_storage($path,$old_name);                   // delete old picture in storage
            $obj_fn->del_storage($path.'/100',$old_name);                   // delete old picture in storage

            $data->img_name = '';
            $data->save();
        }

        $str_param = $request->str_param;
        return redirect()->to($this->path.'?'.$str_param);
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        session()->put('ref_url',url()->previous());

        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;

        $permission = $obj_fn->permission($this->page_id,'d');

        $obj_model->find($id)->delete();

        return redirect()->to(session()->get('ref_url'));
    }

    public function postImport(Request $request) {
        
        $results  = \Excel::load(Input::file('product'))->get();
        $objFn = new MainFunction();
        $store_id = session()->get('s_admin_store_id');

        foreach ($results as $key => $row) {
                // Create New Product
                Product::insert([
                    'store_id' => $store_id,
                    'collection_id' => $row->collection_id,
                    'category_id' => $row->category_id,
                    'product_name_en' => $row->product_name_en,
                    'product_name_th' => $row->product_name_th,
                    'description_en' => $row->description_en,
                    'description_th' => $row->description_th,
                    'price' => $row->price,
                    'special_price' => $row->special_price,
                    'cost' => $row->cost,
                    'is_recommended' => $row->is_recommended,
                    'is_available' => $row->is_available,
                    'img_name' => $row->img_name,
                    'tags' => $row->tags,
                ]);
        }

        return back();
    }

}
