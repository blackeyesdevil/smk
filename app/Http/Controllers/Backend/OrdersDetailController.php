<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\OrdersDetail;
use App\Models\Orders;
use App\Models\Member;

use Input;

class OrdersDetailController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\OrdersDetail'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->tbName = 'orders_detail';
        $this->fieldList = array('orders_id','product_id','product_name_en','product_name_th','qty','price');
        $this->obj_fn = new MainFunction(); // Obj Function
        $this->page_title = 'Orders Detail'; // Page Title
        $this->a_search = ['']; // Array Search
        $this->path = '_admin/orders_detail'; // Url Path
        $this->view_path = 'backend.orders_detail.'; // View Path
    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {


    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $orders_id = input::get('orders_id');

        $page_title = $this->page_title;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        
//        $orders_detail = OrdersDetail::where('orders_id',$orders_id)->get();
        $order = Orders::Leftjoin('orders_detail','orders_detail.orders_id','=','orders.orders_id')
//                        ->select('orders_detail.product_name_th','orders_detail')
                        ->where('orders.orders_id',$orders_id)->get();

//        $order = Orders::leftjoin('member', 'member.member_id', '=', 'orders.member_id')
//                ->select('member.customer_name')
//                ->where('orders.orders_id',$orders_id)
//                ->first();

        return view($this->view_path.'/update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn','orders','orders_detail', 'order'));
    }
    // ------------------------------------ Record Data
    public function store(Request $request)
    {
        
    }
    // ------------------------------------ Show Data : ID
    public function show($id)
    {

    }
    // ------------------------------------ View Update Page
    public function edit($id)
    {

    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        
    }
    // ------------------------------------ Delete Data
    public function destroy($id)
    {
        
    }

}
