<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Models\Page;
use Input;
use DB;

class ReceiptFooterController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\Models\ReceiptFooter'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function
        $this->tbName = 'receipt_footer';
        $this->page_title = 'Receipt Footer'; // Page Title
        $this->a_search = ['']; // Array Search
        $this->path = '_admin/receipt_footer'; // Url Path
        $this->view_path = 'backend.receipt_footer.'; // View Path
        $this->page_id = Page::where('page_name',$this->page_title)->first()->page_id; // Page ID
        
    }

    // ------------------------------------ View Update Page
    public function edit($id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');

        $page_title = $this->page_title;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = 'Update';

        $data = $obj_model->find($id);

        return view($this->view_path.'update',compact('page_title','data','url_to','method','txt_manage','obj_model','obj_fn'));
    }
    // ------------------------------------ Record Update Data
    public function update(Request $request,$id)
    {
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $permission = $obj_fn->permission($this->page_id,'u');

        $input = $request->except(['_token','_method','str_param']); // Get all post from form
        $data = $obj_model->find($id)->update($input);

        $str_param = $request->str_param;
        return redirect()->back();
    }
}
