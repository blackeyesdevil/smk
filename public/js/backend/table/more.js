$(function(){

    //Element
    image = $('#table_map');
    imgPos = [];
    pin = $('#pin');
    coords = $('#coords');
    txt_coords_x = $('#coordinate_x');
    txt_coords_y = $('#coordinate_y');

    //Value for Reset
    var pre_x = txt_coords_x.val();
    var pre_y = txt_coords_y.val();

    image.height( image.width() * 1140 / 1440 );
    imgPos = [image.offset().left, image.offset().top];

    pin.css('left', (txt_coords_x.val() * image.width())  - (pin.width()/2));
    pin.css('top', (txt_coords_y.val() * image.height()) - pin.height());
    pin.appendTo('#table_map');
    if ( (txt_coords_x.val()<=0) && (txt_coords_y.val()<=0) ) {
        pin.hide();
    }

    coords.css('left', image.width() -  coords.width());
    coords.css('top', image.height() -  coords.height());

    $(window).on({
        resize: function() {
            image.height( image.width()*1140/1440 );
            imgPos = [image.offset().left, image.offset().top];

            pin.css('left', (txt_coords_x.val() * image.width()) - (pin.width()/2));
            pin.css('top', (txt_coords_y.val() * image.height()) - pin.height());
            pin.appendTo('#table_map');
            if ( (txt_coords_x.val()<=0) && (txt_coords_y.val()<=0) ) {
                pin.hide();
            }

            coords.css('left', image.width() -  coords.width());
            coords.css('top', image.height() -  coords.height());
        },
        scroll: function() {
            imgPos = [image.offset().left, image.offset().top];
        }
    });

    image.on({
        mousemove: function(e) {
            //e.preventDefault();
            var x = e.pageX-imgPos[0];
            var y = e.pageY-imgPos[1];
            coords.html(x + ', ' + y);
        },
        mouseleave: function() {
            coords.html('0, 0');
        },
        click: function(e) {
            //e.preventDefault();
            var x = e.pageX-imgPos[0];
            var y = e.pageY-imgPos[1];

            pin.css('left', x - (pin.width()/2));
            pin.css('top', y - pin.height());
            pin.appendTo('#table_map');
            pin.show();

            txt_coords_x.val(x/image.width());
            txt_coords_y.val(y/image.height());
            console.log('click ' + x/image.width() + ', ' + y/image.height());
        }
    });

    $('#reset').on({
        click: function() {
            if (pre_x=="" && pre_y=="") {
                pin.hide();
            }else{
                pin.css('left', (pre_x * image.width()) - (pin.width()/2));
                pin.css('top', (pre_y * image.height()) - pin.height());
            }
        }
    });

});
