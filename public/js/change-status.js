jQuery(document).ready(function(){
    ChangeActive(jQuery('.change-active'));
    ChangeCRUDActive(jQuery('.change-crud-active'));
    ChangeReportStatus(jQuery('.change-report'));


});


function ChangeActive(element)
{
    element.on('click',function(event){

        var tb_name = $(this).data('tb_name');
        var pk_field = $(this).data('pk_field');
        var v_pk_field = $(this).data('v_pk_field');
        var change_field = $(this).data('change_field');
        var value = $(this).data('value');
        var _token = $('input[name=_token]').val();
        var FormData = {
            'tb_name' : tb_name,
            'pk_field' : pk_field,
            'v_pk_field' : v_pk_field,
            'change_field' : change_field,
            'value' : value,
            '_token' : _token
        };

        //console.log('tb_name: '+tb_name+' pk_field: '+pk_field+' v_pk_field: '+v_pk_field+' change_field: '+change_field+' value: '+value);

        $.ajax({
            type : "POST",
            url : 'api-active',
            data : FormData,
            dataType : 'json',
            success: function(data){
                $("#zip_id").append(data);
                if(data==1)
                {
                    //console.log(data);
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('text-red').addClass('text-green');
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('fa-close').addClass('fa-check');
                    $('a[id='+change_field+'-'+v_pk_field+']').data('value','1');
                    value = 1 ;
                    //$().empty().append();
                }else if(data==0)
                {
                    //console.log(data);
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('text-green').addClass('text-red');
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('fa-check').addClass('fa-close');
                    $('a[id='+change_field+'-'+v_pk_field+']').data('value','0');
                    value = 0 ;
                }




            }
        });


    });
}

function ChangeCRUDActive(element)
{
    element.on('click',function(event){

        var tb_name = $(this).data('tb_name');
        var pk_field = $(this).data('pk_field');
        var v_pk_field = $(this).data('v_pk_field');
        var change_field = [$(this).data('change_field_c'),$(this).data('change_field_r'),$(this).data('change_field_u'),$(this).data('change_field_d')];
        var value = $(this).data('value');
        var _token = $('input[name=_token]').val();
        change_field.forEach(function(entry) {
            // console.log("entry = "+entry);

            var FormData = {
                'tb_name' : tb_name,
                'pk_field' : pk_field,
                'v_pk_field' : v_pk_field,
                'change_field' : entry,
                'value' : value,
                '_token' : _token
            };

            $.ajax({
                type : "POST",
                url : 'api-active',
                data : FormData,
                dataType : 'json',
                success: function(data){
                    $("#zip_id").append(data);
                    if(data==1)
                    {
                        //console.log(data);
                        // console.log("change_field = "+entry);

                        $('a[id='+entry+'-'+v_pk_field+']').removeClass('text-red').addClass('text-green');
                        $('a[id='+entry+'-'+v_pk_field+']').removeClass('fa-close').addClass('fa-check');
                        $('a[id='+entry+'-'+v_pk_field+']').data('value','1');
                        value = 1 ;
                        //$().empty().append();
                    }else if(data==0)
                    {
                        //console.log(data);
                        $('a[id='+entry+'-'+v_pk_field+']').removeClass('text-green').addClass('text-red');
                        $('a[id='+entry+'-'+v_pk_field+']').removeClass('fa-check').addClass('fa-close');
                        $('a[id='+entry+'-'+v_pk_field+']').data('value','0');
                        value = 0 ;
                    }
                }
            });
        });


        //console.log('tb_name: '+tb_name+' pk_field: '+pk_field+' v_pk_field: '+v_pk_field+' change_field: '+change_field+' value: '+value);




    });
}

function ChangeReportStatus(element)
{
    element.on('click',function(event){

        var tb_name = $(this).data('tb_name');
        var pk_field = $(this).data('pk_field');
        var v_pk_field = $(this).data('v_pk_field');
        var change_field = $(this).data('change_field');
        var value = $(this).data('value');
        var _token = $('input[name=_token]').val();
        var FormData = {
            'tb_name' : tb_name,
            'pk_field' : pk_field,
            'v_pk_field' : v_pk_field,
            'change_field' : change_field,
            'value' : value,
            '_token' : _token
        };

        //console.log('tb_name: '+tb_name+' pk_field: '+pk_field+' v_pk_field: '+v_pk_field+' change_field: '+change_field+' value: '+value);

        $.ajax({
            type : "POST",
            url : '/api-report',
            data : FormData,
            success: function(data){
                if(data=='N')
                {
                    //console.log(data);

                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('text-green').addClass('text-red');
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('fa-check').addClass('fa-close');
                    $('a[id='+change_field+'-'+v_pk_field+']').data('value','N');
                    value = 'N' ;
                    //$().empty().append();
                }else if(data=='Y')
                {
                    //console.log(data);

                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('text-red').addClass('text-green');
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('fa-close').addClass('fa-check');
                    $('a[id='+change_field+'-'+v_pk_field+']').data('value','Y');
                    value = 'Y' ;
                }




            }
        });


    });


}
