function setLayout(topic, items){
    var container, itemClassName;

    if (topic == 'collection'){
        container = $('#collection-list');
        itemClassName = 'collection-item';
    }
    
    container.hide().html('');
    for (var i=0; i <= Math.floor(items.length / 6); i++){
        var resultHtml = '';

        if (i != Math.floor(items.length / 6)){
            var item0 = items[(i*6)+0];
            var item1 = items[(i*6)+1];
            var item2 = items[(i*6)+2];
            var item3 = items[(i*6)+3];
            var item4 = items[(i*6)+4];
            var item5 = items[(i*6)+5];

            resultHtml = '<div class="col-xs-8">'
                            + '<div class="' + itemClassName + '" data-collection-id="'+ item0.collection_id +'">'
                                + '<img src="' + item0.image + '">'
                                + '<div class="cover-div">'
                                    + '<span class="en">' + item0.collection_name_en + '</span>'
                                    + '<span class="th">' + item0.collection_name_th + '</span>'
                                + '</div>'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-xs-4">'
                            + '<div class="' + itemClassName + '" data-collection-id="'+ item1.collection_id +'">'
                                + '<img src="' + item1.image + '">'
                                + '<div class="cover-div">'
                                    + '<span class="en">' + item1.collection_name_en + '</span>'
                                    + '<span class="th">' + item1.collection_name_th + '</span>'
                                + '</div>'
                            + '</div>'
                            + '<div class="' + itemClassName + '" data-collection-id="'+ item2.collection_id +'">'
                                + '<img src="' + item2.image + '">'
                                + '<div class="cover-div">'
                                    + '<span class="en">' + item2.collection_name_en + '</span>'
                                    + '<span class="th">' + item2.collection_name_th + '</span>'
                                + '</div>'
                            + '</div>'
                        + '</div>'
                        + '<div class="clearfix"></div>'
                        + '<div class="bottom-row">'
                            + '<div class="col-xs-4">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item3.collection_id +'">'
                                    + '<img src="' + item3.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item3.collection_name_en + '</span>'
                                        + '<span class="th">' + item3.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="col-xs-4">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item4.collection_id +'">'
                                    + '<img src="' + item4.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item4.collection_name_en + '</span>'
                                        + '<span class="th">' + item4.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="col-xs-4">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item5.collection_id +'">'
                                    + '<img src="' + item5.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item5.collection_name_en + '</span>'
                                        + '<span class="th">' + item5.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="clearfix"></div>'
                        + '</div>';
        }
        else{
            if ((items.length % 6) == 1){
                var item0 = items[(i*6)+0];

                resultHtml = '<div class="col-xs-12">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item0.collection_id +'">'
                                    + '<img src="' + item0.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item0.collection_name_en + '</span>'
                                        + '<span class="th">' + item0.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>';
            }
            else if ((items.length % 6) == 2){
                var item0 = items[(i*6)+0];
                var item1 = items[(i*6)+1];

                resultHtml = '<div class="col-xs-6">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item0.collection_id +'">'
                                    + '<img src="' + item0.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item0.collection_name_en + '</span>'
                                        + '<span class="th">' + item0.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="col-xs-6">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item1.collection_id +'">'
                                    + '<img src="' + item1.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item1.collection_name_en + '</span>'
                                        + '<span class="th">' + item1.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>';
            }
            else if ((items.length % 6) == 3){
                var item0 = items[(i*6)+0];
                var item1 = items[(i*6)+1];
                var item2 = items[(i*6)+2];

                resultHtml = '<div class="col-xs-4">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item0.collection_id +'">'
                                    + '<img src="' + item0.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item0.collection_name_en + '</span>'
                                        + '<span class="th">' + item0.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="col-xs-4">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item1.collection_id +'">'
                                    + '<img src="' + item1.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item1.collection_name_en + '</span>'
                                        + '<span class="th">' + item1.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="col-xs-4">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item2.collection_id +'">'
                                    + '<img src="' + item2.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item2.collection_name_en + '</span>'
                                        + '<span class="th">' + item2.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>';
            }

            else if ((items.length % 6) == 4){
                var item0 = items[(i*6)+0];
                var item1 = items[(i*6)+1];
                var item2 = items[(i*6)+2];
                var item3 = items[(i*6)+3];

                resultHtml = '<div class="col-xs-6">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item0.collection_id +'">'
                                    + '<img src="' + item0.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item0.collection_name_en + '</span>'
                                        + '<span class="th">' + item0.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="col-xs-6">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item1.collection_id +'">'
                                    + '<img src="' + item1.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item1.collection_name_en + '</span>'
                                        + '<span class="th">' + item1.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="clearfix"></div>'
                            + '<div class="col-xs-6">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item2.collection_id +'">'
                                    + '<img src="' + item2.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item2.collection_name_en + '</span>'
                                        + '<span class="th">' + item2.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="col-xs-6">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item3.collection_id +'">'
                                    + '<img src="' + item3.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item3.collection_name_en + '</span>'
                                        + '<span class="th">' + item3.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>';
            }
            else if (items.length % 7 == 5){
                var item0 = items[(i*6)+0];
                var item1 = items[(i*6)+1];
                var item2 = items[(i*6)+2];
                var item3 = items[(i*6)+3];
                var item4 = items[(i*6)+4];

                resultHtml = '<div class="col-xs-6">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item0.collection_id +'">'
                                    + '<img src="' + item0.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item0.collection_name_en + '</span>'
                                        + '<span class="th">' + item0.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="col-xs-6">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item1.collection_id +'">'
                                    + '<img src="' + item1.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item1.collection_name_en + '</span>'
                                        + '<span class="th">' + item1.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="clearfix">'
                            + '<div class="col-xs-4">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item2.collection_id +'">'
                                    + '<img src="' + item2.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item2.collection_name_en + '</span>'
                                        + '<span class="th">' + item2.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="col-xs-4">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item3.collection_id +'">'
                                    + '<img src="' + item3.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item3.collection_name_en + '</span>'
                                        + '<span class="th">' + item3.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="col-xs-4">'
                                + '<div class="' + itemClassName + '" data-collection-id="'+ item4.collection_id +'">'
                                    + '<img src="' + item4.image + '">'
                                    + '<div class="cover-div">'
                                        + '<span class="en">' + item4.collection_name_en + '</span>'
                                        + '<span class="th">' + item4.collection_name_th + '</span>'
                                    + '</div>'
                                + '</div>'
                            + '</div>'
            }
        }

        resultHtml = filterLang(resultHtml, container);
    }

    hideLoading();
    container.fadeIn();
}
