$('#discount-code-normal').on('click', function(){
    $('#cover-box-discount').fadeIn(200);
    $('#discount-code').fadeIn(200);
});

// $('#viz-btn').on('click', function(){
//     $('#cover-box-discount').fadeIn(200);
//     $('#viz-card').fadeIn(200);
// });

$('#cover-box-discount, .discount-popup .btn-cancel').on('click', function(){
    $('.discount-popup, #cover-box-discount').fadeOut(function(){
      $('.discount-alert').text('').hide();
      $('#discount-input').val('');
    });
});

$('#pay-cash').on('click', function(){
    $('#cover').fadeIn(200);
    $('#confirm-pay-cash').fadeIn(200);
});

$('#cover, .btn-finish').on('click', function(){
    $('#cover').fadeOut(200);
    $('#confirm-pay-cash').fadeOut(200);
});

var discount_id;
var discount_code;
var discount_price = 0;
var discount_grand_total = 0;
// ----------- DIscount Code ----------
var token  = $('meta[name="token"]').attr('content');
$('#discount-input').on('keyup', function(e){
  clearTimeout($.data(this, 'timer'));

  if (e.keyCode == 13)
      checkDiscount();
  else
      $(this).data('timer', setTimeout(checkDiscount, 500));
});

function checkDiscount(){
  var checkout_total = $('.checkout-total-price .total').text();
      checkout_total = checkout_total.split(" ");
  var discount_input = $('#discount-input').val();
      discount_code = discount_input;
      console.log(checkout_total);
      console.log("discount_code: "+discount_input);

      $('#discount-input').attr('disabled', 'disabled');
      $('#discount-code .loading').show();

      $.ajax({
        url: api_url+'/discount/check-code',
        headers: {'X-CSRF-TOKEN': token},
        data: {"code": discount_input, "total_price": checkout_total[0]},
        type: "POST",
        success: function(data){
          // console.log('post to controller');
          // console.log(data);

          if("0" === data.status){
            discount_price = 0;
            discount_code = '';
            discount_id = 0;

            $('.discount-alert').text(data.alertMsg).removeClass('success').addClass('fail').fadeIn();
          }else if ("1" === data.status){
            discount_price = data.discount_price;
            discount_id = data.discount_code_id;
            discount_grand_total = data.grand_total;

            $('.discount-alert').text(data.alertMsg).removeClass('fail').addClass('success').fadeIn();
          }

          $('#discount-input').removeAttr('disabled').focus();
          $('#discount-code .loading').hide();
        },
      });
}

$('#discount-apply-btn').on('click', function(){
  if (discount_code != ''){
    $('.checkout-total-discount-price').html('<div class="col-xs-8">Discount</div><div class="col-xs-4">- '+discount_price+' THB</div>');
  }
  else{
    $('.checkout-total-discount-price').text('');
  }

  $('.checkout-total-discount').html('<div class="col-xs-8">Grand Total</div><div class="col-xs-4">'+discount_grand_total+' THB</div>');

  $('#discount-code, #cover-box-discount').fadeOut(function(){
    $('.discount-alert').text('').hide();
    $('#discount-input').val('');
  });
});

// ----------- Pay By ----------
$('.button-pay-by').on('click', function(){
  $('#content-checkout').fadeOut();
  console.log("checkout total price: ......... "+ $('.checkout-total-price').text());
  var checkout_total = $('.checkout-total-price').text();
      checkout_total = checkout_total.split(" ");
      console.log(checkout_total);
  var str_payment = $(this).text();
  // var url_category_des = '/product/by-category/'+$(this).attr('data-id');
  var url_pay = '/order/confirm';
  var table_no = 1;
  var order_details = JSON.stringify(carts);
  var sumPrice = checkout_total[1];
      sumPrice = sumPrice.replace(/[A-Za-z]/g,"");

  // var discount_price = 10;
  // var discount_id = 1;
  // var discount_code = 123456789;

  console.log(JSON.stringify(carts));


  if( str_payment == 'PAY BY CASH'){
    var payment = 2; // by CASH
    var postPayByCash = { "table_no":table_no, "payment":payment, "order_details": order_details, "total_price":sumPrice, "discount_price": discount_price, "discount_id": discount_id , "code": discount_code };

    console.log(postPayByCash);
    $.ajax({
              url: api_url+url_pay,
              headers: {'X-CSRF-TOKEN': token},
              data: postPayByCash,
              type: "POST",
              success: function(data){
              //  console.log('post to controller');
              //  console.log(data.status);
                if('1' == data.status){
                    // console.log(data);

                     window.location=base_url+'/thankyou';
                    //  $('#thank-order-id').text(data);
                }else if('0' == data.status){
                  // console.log(data);
                      window.location=base_url+'/thankyou';

                }
              },
            });


  }else if( str_payment == 'PAY BY CREDIT CARD'){
    alert(str_payment);

    var payment = 1; // by Credit
    var postPayByCredit = { "table_no":table_no, "payment":payment, "order_details": order_details, "total_price":sumPrice, "discount_price": discount_price, "discount_id": discount_id , "code": discount_code };
    // console.log(postPayByCash);
    $.ajax({
              url: api_url+url_pay,
              headers: {'X-CSRF-TOKEN': token},
              data: postPayByCredit,
              type: "POST",
              success: function(data){

                if('1' == data.status){
                  var amount = data.order.total_price - data.order.discount_price; // total_price - discount_price at ordercontroller
                  var orderId = data.order.orders_id;
                  var token = data.encrypted;

                     window.location='intent:#Intent;action=com.smk.NK_CUSTOM_ACTION;S.amount=' + amount + ';S.orderid=' + orderId + ';S.token=' + token + ';end';

                }else if('0' == data.status){
                      window.location=base_url+'/thankyou';

                }
              },
            });

  }

});
