var payment_log =[];
var payment = [];
$( document ).ready(function() {
    var orders= [];

    console.log(api_url+'/cashier');
    console.log('loading order');
    $.getJSON(api_url + '/cashier', null , function (data) {
    //     console.log(data);
        orders = data.orders;
    //     console.log(orders);
        $.each(orders,function( key,value){
            if(value.status=='1'){

            }
            else {
                order = '<tr id="'+value.orders_no+'" data-amount="'+value.total_price+'" data-table="'+value.table_no+'"" data-id="'+value.orders_id+'">'+
                  '<td>'+value.orders_no+'</td>'+
                  '<td>'+value.table_no+'</td>'+
                  '<td>'+value.total_price+'</td>'+
                '</tr>';
                $('#table-order-tb').append(order);
            }

        });
        console.log(orders);
        console.log('loaded order');
    });
});


$('#table-order-tb').on('click','td',function(){

    data_amount =$(this).parent().attr('data-amount');
    data_id = $(this).parent().attr('data-id');
    console.log($(this).parent());
    getSetAmount(data_amount);
    getSetOrderId(data_id);
    console.log(getSetOrderId());
    $('#table-order-tb tr.active').removeClass('active');
    $(this).parent().addClass('active');
    if($('#table-payment').css('display') == 'none')
    {
        $('#table-order').animate({"height": "60%"}, "medium");
        $('#table-payment').slideDown();
    }else{
        $('#table-payment').slideDown();
    }
    setHeadPayText(data_amount);
    updatePaidLeft(data_amount);
    // getSetPaymentLog();
    setChange();
});

$('#btn-number').on('click','td',function(){
    paytext = $('#pay-text').text();
    value = $(this).text();
    data = getValueInputPanel(paytext,value);
    $('#pay-text').text(data);
    console.log('click!');
});

$('#bank').on('click','img',function(){
    paytext = $('#pay-text').text();
    var bank = $(this).attr('id');
    bank =bank.substring(1,bank.length);
    console.log(paytext);
    data = getValueInputPanel(paytext,bank,'bank');
    $('#pay-text').text(data);
});

$('#payment-method').on('click','td',function(){
    $('#payment-method td.active').removeClass('active');
    $(this).addClass('active');
});

$('#table-payment-tb').on('click','#remove-payment',function(){
    console.log('click on remove');
    $(this).closest("tr").remove();
});

function getValueInputPanel(total,value,type='number'){
    var value_int = parseInt(value);
    if(type=='number'){
        if(value=='C'){
            total ='0';
            checkDot(total,1);
        }
        else if(value=='x'){
            if(total.charAt(total.length-1)=='.'){
                checkDot(total,1);
            }
            total = total.substring(0,total.length - 1);
        }
        else if(value=='ENTER'){
            if(getSetPrintReady()!='0'){
                updateStatus(getSetOrderId(),'1');
                getSetPrintReady('0');
                return 0 ;
            }
            else{
                method = getPaymentMethod();
                amount = getSetAmount();
                pay_text = $('#pay-text').text();
                order_id = getSetOrderId();
                console.log($('#pay-text').text());
                console.log($('#payment-method').children().children().children());
                $('#payment-method td.active').removeClass('active');
                paymentToTablePayment(method,order_id,amount,pay_text);
                return 0 ;

            }

        }
        else if(value=='.'){
            total = checkDot(total);
        }
        else{
            console.log('num button');
            console.log(total);
            if(total=='0.'){
                console.log(total);
                total = total + value;
            }
            else if(total =='0'|| total =='00'){
                total = parseInt(value);
            }
            else{
                console.log(total);
                console.log(value);
                total = total + value;
            }
        }
    }
    else{
        total = parseFloat(total) + value_int;
    }
    return total ;
}

function checkDot(total,is_tool=0){
    if(typeof dot == 'undefined'){
        dot = 0 ;
    }
    if(is_tool==1){
        dot = 0 ;
    }
    else{
        if(dot==0){
            dot = 1;
            total = total + '.';
            return total;
        }
        else if(dot==1){
            return total;
        }
    }
}
function getPaymentMethod(){
    console.log($('#payment-method .active').text());
    return $('#payment-method .active').text();
}

function paymentToTablePayment(payment_method='',order_id,amount,pay_text){
    console.log('in paymentToTablePayment');
    if(payment_method==''){

    }
    else{
        console.log(pay_text);
        paidleft = updatePaidLeft(data_amount,pay_text);
        paid = paidleft.substring(0,paidleft.indexOf(' '));
        left = paidleft.substring(paidleft.indexOf(' '),paidleft.length);
        var table =
                    '<tr>'+
                      '<td>'+payment_method+'</td>'+
                      '<td>'+paid+'</td>'+
                      '<td><span id="remove-payment" data-amount="'+pay_text+'"><i class="fi-x"></i></span></td>'+
                    '</tr>';
        $('#table-payment-tb').append(table);
        $('#paid-text-p-input').text(paid);
        $('#paid-text-l-input').text(left);
        setHeadPayText(left);
        // console.log(total_cal);
        // updatePaidLeft(total_cal,pay_order);

    }
}

function setHeadPayText(value='0'){
    $('#head-pay-text').text(value);
}

function updatePaidLeft(data_amount,pay_text=''){
    console.log(pay_text);
    console.log(data_amount);
    data_amount_float = parseFloat(data_amount);
    console.log(data_amount_float);
    pay_text_float = parseFloat(pay_text);
    console.log(pay_text_float);
    if(pay_text=''){
        $('#paid-text-l-input').text(data_amount);
    }
    else{
        paid = pay_text_float;
        if(paid>data_amount){
            change = paid - data_amount_float;
            paid=data_amount;
            setChange(change);
        }
        else{
            paid = pay_text_float;
        }
        left = data_amount_float - paid;
        if(left=='0'){
            console.log("left = 0");
            console.log('update status');
            getSetPrintReady('1');
            updateStatus(getSetOrderId(),'1');
        }
        return paid.toString() +' '+ left.toString() ;
    }
}

function setChange(change=0){
    $('#change-pay-text').text(change);
}
function updateStatus(order_id,status){
    $.ajax({
              url: api_url+'/cashier/change-status'+'/'+order_id+'/'+status,
              type: "GET",
              success: function(data){
              //  console.log('post to controller');
              //  console.log(data.status);
                console.log(data);
                if('1' == data.status){
                    // console.log(data);
                    console.log("Done");
                    //  $('#thank-order-id').text(data);
                }else if('0' == data.status){
                  // console.log(data);
                      console.log('fail');
                }
              },
            });
    $.ajax({
              url: api_url+'/cashier/drawer',
              type: "GET",
              success: function(data){
              //  console.log('post to controller');
              //  console.log(data.status);
                console.log(data);
                if('1' == data.status){
                    // console.log(data);
                    console.log("Done");
                    //  $('#thank-order-id').text(data);
                }else if('0' == data.status){
                  // console.log(data);
                      console.log('fail');

                }
              },
            });
}

function getSetAmount(method='get'){
    if(typeof amount_order == 'undefined'){
        amount_order = 0 ;
    }
    if(method!='get'){
        amount_order =method;
        console.log(amount_order);
    }
    else{
        return amount_order;
    }
}

function getSetOrderId(method='get'){
    if(typeof global_order_id == 'undefined'){
        global_order_id = 0 ;
    }
    if(method!='get'){
        global_order_id =method;
    }
    else{
        return global_order_id;
    }
}

function getSetPrintReady(method='get'){
    if(typeof ready == 'undefined'){
        ready = 0 ;
    }
    if(method!='get'){
        ready = method;
    }
    else{
        return ready;
    }
}

function getSetPaymentLog(method='get'){
    if(method!='get'){

    }
    else{
        console.log('set payment log');
        $('#table-payment-tb').html('');
        var table =
                    '<tr>'+
                      '<td>'+'</td>'+
                      '<td>'+' Empty'+'</td>'+
                      '<td></td>'+
                    '</tr>';
        $('#table-payment-tb').append(table);
    }
}
