var url = '/smk/public/report/';

$(function(){

    $('#eod').on('click',function(e){
        e.preventDefault();
        $.ajax({
            url: url+'print-eod_till',
            type: 'GET',
            success: function (data) {
                if(data != ''){
                    console.log(data);
                }
            }
        });
    });

    $('#eod').on('click',function(e){
        e.preventDefault();
        $.ajax({
            url: url+'print-eod',
            type: 'GET',
            success: function (data) {
                if(data != ''){
                    console.log(data);
                }
            }
        });
    });

    $('#z_out').on('click',function(e){
        e.preventDefault();
        $.ajax({
            url: url+'print-z_out',
            type: 'GET',
            success: function (data) {
                if(data != ''){
                    console.log(data);
                }
            }
        });
    });

    $('#transaction').on('click',function(e){
        e.preventDefault();
        $.ajax({
            url: url+'print-transaction',
            type: 'GET',
            success: function (data) {
                if(data != ''){
                    console.log(data);
                }
            }
        });
    });

    $('#abb').on('click',function(e){
        e.preventDefault();
        $.ajax({
            url: url+'print-abb',
            type: 'GET',
            success: function (data) {
                if(data != ''){
                    console.log(data);
                }
            }
        });
    });

});
