$(function () {
    function toggleLang(){
        $('.' + lang).removeClass('show').addClass('hide');
        lang = (lang == 'en')?'th':'en';
        $('.' + lang).removeClass('hide').addClass('show');

        $('#lang-bar .switch-icon').css('float', (lang=='en')?'left':'right');

        $.get(api_url + '/locale/' + lang);
    }

    $('#lang-bar .switch-icon').css('float', (lang=='en')?'left':'right');

    $('#lang-bar').on('click', '.switch-slider', function(){
        toggleLang();
    });

    // Get All Stores
    $.getJSON(api_url + '/store', null, function (data) {
        var stores = data.stores;

        $('#store-list').html('');
        $.each(stores, function (key, store) {
            var storeItem = '<a href="#" class="store-item" id="store-key" data-id="'+store.store_id+'">'
                                + '<img src="' + store.logo_normal + '" class="img-normal">' + '<img src="' + store.logo_active + '" class="img-active">';
                            +'</a>';

            $('#store-list').append(storeItem);
        });
    });

    // Assign an onClick listener to each store item
    $('#store-list').on('click', '.store-item', function (e) {
        e.preventDefault();
        showLoading();

        var store_id = $(this).attr('data-id');
        $('#store-list .store-item.active').removeClass('active');
        $(this).addClass('active');

        $('#content .container').fadeOut(function(){
            $.get(base_url + '/product-list', function(data){
                $('#content .container').html(data).fadeIn();

                var isCatLoading = true;
                var isProductLoading = true;

                // Get A List of Categories by store_id
                var url_cat_des = '/category/by-store/' + store_id;
                $.getJSON(api_url + url_cat_des, null , function (data) {
                      var categories = data.categories;
                      var category_list = '';
                      $('#category-bar').html('');
                      all_category =   '<div class="category-item promotion ">'
                                          + '<span class="th">โปรโมชั่น</span>'
                                          + '<span class="en">Promotion</span>'
                                      + '</div>'+
                                      '<div class="category-item set ">'
                                          + '<span class="th">เซ็ต</span>'
                                          + '<span class="en">Set</span>'
                                      + '</div>'+
                                      '<div class="category-item all active">'
                                          + '<span class="th">ทั้งหมด</span>'
                                          + '<span class="en">All</span>'
                                      + '</div>';
                      filterLang(all_category, $('#category-bar'));

                      $.each( categories, function(key, value){
                        category_list = '<div class="category-item" data-id="' + value.category_id + '">'
                                            + '<span class="th">' + value.category_name_th + '</span>'
                                            + '<span class="en">' + value.category_name_en + '</span>'
                                         + '</div>';
                        // category_list = '<div class="category-item col-xs-3">'+
                        //                 value.category_name+
                        //                 '</div>';

                        filterLang(category_list, $('#category-bar'));
                      });

                      isCatLoading = false;
                      if (!isProductLoading)
                          hideLoading();
                  });

                // Get A List of Products by store_id
                var url_product_des = '/product/by-store/' + store_id;
                $.getJSON(api_url + url_product_des , null , function (data) {
                    var products = data.products;
                    var product_list='';
                    $('#product-list').hide();
                    $('#product-list').html('');
                    $.each( products, function( key, value ) {
                          product_list = '<div class="product-item col-xs-4" data-product-id="'
                                              + value.product_id
                                              + '">'
                                             + '<div class="product-image" style="background-image: url(\'' + value.image + '\')"></div>'
                                             + '<div class="product-info">'
                                                 + '<div class="product-name">'
                                                     + '<span class="en">' + value.product_name_en + '</span>'
                                                     + '<span class="th">' + value.product_name_th + '</span>'
                                                 + '</div>'
                                                 + '<div class="product-price">'
                                                     + '<span class="en">' + value.price + ' THB</span>'
                                                     + '<span class="th">' + value.price + ' บาท</span>'
                                                 + '</div>'
                                             + '</div>'
                                         + '</div>';

                        product_list = filterLang(product_list, $('#product-list'));

                        if ((key+1)%3==0)
                            $('#product-list').append($('<div class="clearfix"></div>'));
                    });

                    isProductLoading = false;
                    if (!isCatLoading)
                        hideLoading();

                    $('#product-list').fadeIn();
                });
            });
        });
    });

    // Get Collection List
    showLoading();
    $('#content .container').fadeOut(function(){
        container = $(this);

        $.get(base_url + '/collection-list', function(data){
            container.html(data).fadeIn();

            $.getJSON(api_url + '/collection', null, function (data) {
                setLayout('collection', data.collections);
            });
        });
    });

    $('#ui-main').on('click', '.collection-bar-item', function () {
        showLoading();
        $('#collection-bar .collection-bar-item.active').removeClass('active');
        $(this).addClass('active');

        var collection_bar_id = $(this).data('id');
        $.getJSON(api_url + '/product/by-collection/' + collection_bar_id , null, function (collection_product) {
              var collection_products = collection_product.products;
              $('#collection-product-list').html('');

              $.each( collection_products, function( key, value ) {
                  collection_list = '<div class="product-item col-xs-4" data-product-id="'
                                      + value.product_id
                                      + '">'
                                     + '<div class="product-image" style="background-image: url(\'' + value.image + '\')"></div>'
                                     + '<div class="product-info">'
                                         + '<div class="product-name">'
                                             + '<span class="en">' + value.product_name_en + '</span>'
                                             + '<span class="th">' + value.product_name_th + '</span>'
                                         + '</div>'
                                         + '<div class="product-price">'
                                             + '<span class="en">' + value.price + ' THB</span>'
                                             + '<span class="th">' + value.price + ' บาท</span>'
                                         + '</div>'
                                     + '</div>'
                                 + '</div>';

                  if ((key+1)%3==0)
                    collection_list += '<div class="clearfix"></div>';

                  hideLoading();
                  filterLang(collection_list, $('#collection-product-list'));
              });
        });
    });

    // Assign an onClick listener to each collection-item
    var isCollectionLoading = false;
    var isCollectionProductLoading = false;
    $('#ui-main').on('click', '.collection-item', function(){
      showLoading();

      var collection_item = $(this);

      $.get(base_url + '/collection-product', function(data){
          $('#content .container').fadeOut(function(){
              $(this).html(data);
              $(this).fadeIn();

              var collection_id = collection_item.data('collection-id');

              isCollectionLoading = true;
              $.getJSON(api_url + '/collection/collection-bar', null, function (collectionBar) {
                  $.each(collectionBar.collections, function(key, value){
                    if(collection_id == value.collection_id){
                      collection_list = '<div class="collection-bar-item active" data-id="' + value.collection_id + '">'
                                          + '<span class="th">' + value.collection_name_th + '</span>'
                                          + '<span class="en">' + value.collection_name_en + '</span>'
                                       + '</div>';
                      filterLang(collection_list, $('#collection-bar'));
                    }else{
                      collection_list = '<div class="collection-bar-item" data-id="' + value.collection_id + '">'
                                          + '<span class="th">' + value.collection_name_th + '</span>'
                                          + '<span class="en">' + value.collection_name_en + '</span>'
                                       + '</div>';
                      filterLang(collection_list, $('#collection-bar'));
                    }
                  });

                  isCollectionLoading = false;
                  if (!isCollectionProductLoading)
                    hideLoading();
              });

              // console.log($(this).data('collection-id'));
              // var collection_id = $(this).data('collection-id');
              isCollectionProductLoading = true;
              $.getJSON(api_url + '/product/by-collection/' + collection_id , null, function (collection_product) {

                  var collection_products = collection_product.products;
                  $.each( collection_products, function( key, value ) {
                      collection_list = '<div class="product-item col-xs-4" data-product-id="'
                                            + value.product_id
                                            + '">'
                                           + '<div class="product-image" style="background-image: url(\'' + value.image + '\')"></div>'
                                           + '<div class="product-info">'
                                               + '<div class="product-name">'
                                                   + '<span class="en">' + value.product_name_en + '</span>'
                                                   + '<span class="th">' + value.product_name_th + '</span>'
                                               + '</div>'
                                               + '<div class="product-price">'
                                                   + '<span class="en">' + value.price + ' THB</span>'
                                                   + '<span class="th">' + value.price + ' บาท</span>'
                                               + '</div>'
                                           + '</div>'
                                       + '</div>';

                      if ((key+1)%3==0)
                        collection_list += '<div class="clearfix"></div>';

                      filterLang(collection_list, $('#collection-product-list'));
                  });

                  isCollectionProductLoading = false;
                  if (!isCollectionLoading)
                    hideLoading();
              });
          });
      });
    });

    function loadPage(url){
        $.get(url, function(data){
            $('#content .container').fadeOut(function(){
                $(this).html(data);
                $(this).fadeIn();

                hideLoading();
            });
        });
    }

    $('#home-btn').on('click', function(e){
        e.preventDefault();
        showLoading();
        $('#store-list .active').removeClass('active');

        $.get(base_url + '/collection-list', function(data){
            container.html(data).fadeIn();

            $.getJSON(api_url + '/collection', null, function (data) {
                setLayout('collection', data.collections);
            });
        });
    });

    $('#my-room-btn').on('click', function(e){
        e.preventDefault();
        // showLoading();
        // $('#store-list .active').removeClass('active');
        $("#cover").fadeIn();
        $('#personalize').fadeIn();

        // loadPage(base_url + '/my-room');
    });
    $('.enter-otp').on('click', function(e){
        e.preventDefault();
        showLoading();
        $('#store-list .active').removeClass('active');

        loadPage(base_url + '/my-room');
        $('#personalize').fadeOut();
        $('#cover').fadeOut(function(){
          $('#personalize-otp').hide();
          $('#personalize-login').show();
        });
    });


    // $('.switch-slider').on('click', function () {
    //     var switchIcon = $(this).find('.switch-icon');
    //     var lang ='';
    //     switchIcon.css('float', switchIcon.css('float') == 'left' ? 'right' : 'left');
    //     lang =switchIcon.css('float');
    //     if(lang=='left'){
    //         $('.en').show();
    //         $('.th').hide();
    //         console.log('show en');
    //     }
    //     else if( lang=='right' ){
    //         $('.en').hide();
    //         $('.th').show();
    //         console.log('show th');
    //     }
    //
    // });
    // $('.category-item').on('click', function () {
    //   alert('#category-bar');
    // });
    //  $('#ui-main').on('click', '.category-item', function () {
    //     alert('.category-item');
    //    });
});
