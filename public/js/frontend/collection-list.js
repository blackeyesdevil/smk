// Get All Collections
$.getJSON(api_url + '/collection', null, function (data) {
    setLayout('collection', data.collections);
});

// Get collection bar
$('#ui-main').on('click', '.collection-bar-item', function () {
  showLoading();
    $('#collection-bar .collection-bar-item.active').removeClass('active');
    $(this).addClass('active');

    var collection_bar_id = $(this).data('id');
    $.getJSON(api_url + '/product/by-collection/' + collection_bar_id , null, function (collection_product) {

          var collection_products = collection_product.products;
          $('#collection-product-list').html('');

          $.each( collection_products, function( key, value ) {
              collection_list = '<div class="product-item col-xs-4" data-product-id="'
                                    + value.product_id
                                    + '">'
                                   + '<div class="product-image" style="background-image: url(\'' + value.image + '\')"></div>'
                                   + '<div class="product-info">'
                                       + '<div class="product-name">'
                                           + '<span class="en">' + value.product_name_en + '</span>'
                                           + '<span class="th">' + value.product_name_th + '</span>'
                                       + '</div>'
                                       + '<div class="product-price">'
                                           + '<span class="en">' + value.price + ' THB</span>'
                                           + '<span class="th">' + value.price + ' บาท</span>'
                                       + '</div>'
                                   + '</div>'
                               + '</div>';

              if ((key+1)%3==0)
                collection_list += '<div class="clearfix"></div>';
                  hideLoading();
              filterLang(collection_list, $('#collection-product-list'));
           });


    });

});
