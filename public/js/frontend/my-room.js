$('.enter-mobile').on('click', function(){
    $('#personalize-login').fadeOut(function(){
        var url_otp = '';
        $.getJSON(api_url + )
        $('#personalize-otp').fadeIn();
    });
});
$('#personalize-otp').on('click', '#personalize-close',function(){
    $('#personalize-otp').fadeOut();
    $('#personalize').fadeOut();
    $('#cover').fadeOut();

});
$('#personalize').on('click', '#personalize-close',function(){
    $('#personalize').fadeOut();
    $('#cover').fadeOut(function(){
        $('#personalize-otp').hide();
        $('#personalize-login').show();
    });
});
// my-room-orders-history
$('#content').on('click','#my-orders-history', function(){
  $('.my-room-content').html('');
  var member_id = $(this).data('member');
  var url_mem_history = '/order/member-order-history/'+ member_id;

  $.getJSON(api_url + url_mem_history , null , function (data){
    console.log(JSON.stringify(data));
    var orderHistory = '';
      $.each(data ,function(key, value){
        var orders_detail_order_history = value.orders_detail;

        // console.log(value.status);
         orderHistory = '<div id="order-history" class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">'
                          + '<div id="order-list">'
                          +'<div class="order-item">'
                              +'<div class="order-info">'
                                + '<div class="order-datetime">6 minute</div>';

                                if(value.status == 0 || value.status == 1 ){
                                    orderHistory += '<div class="label label-default">IN PROCESS</div>';
                                }else if(value.status == 2){
                                    orderHistory += '<div class="label label-danger">CANCELED</div>';
                                }else{
                                    orderHistory += '<div class="label label-success">COMPLETED</div>';
                                }



                                orderHistory += '<div class="order-no">ORDER #'+value.orders_no+'</div>'

                                + '<div class="order-total-price">Total Price: &nbsp;&nbsp; '+value.total_price+' THB</div>'

                              + '</div>'
                              + '<div class="order-detail">'
                              $.each(orders_detail_order_history, function(key, detail){
                                    var sumPrice = detail.qty * detail.price;
                                        orderHistory +=  '<div class="order-detail-item">'
                                                + '<div class="row">'
                                                  + '<div class="product-info col-xs-5">'
                                                    + '<div class="product-name">'+detail.product_name_en+'</div>'
                                                    + '<div class="product-option-list">'
                                                        + '<div class="product-option-item">'
                                                            + detail.option_en
                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                                + '<div class="qty col-xs-4"> '+detail.qty+' x '+detail.price+' THB</div>'

                                                + '<div class="subtotal-price col-xs-3">'
                                                    + sumPrice+'THB'
                                                + '</div>'
                                                + '</div>'
                                                + '</div>';



                                              });
                                              orderHistory += '</div>'

                              + '</div>';
                            $('.my-room-content').append(orderHistory);
      });
  });
});
