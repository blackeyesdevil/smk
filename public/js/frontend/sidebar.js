var animationSpeed = 200;


function hideSideBar(){
    // hide #sidebar
    $('#sidebar, #sidebar .sidebar-footer').animate({
        right: '-550px'
    }, animationSpeed, function(){
        $('#cover-box').fadeOut(animationSpeed, function(){
            $('#popup-div').hide();
            $('#sidebar').hide();
            $('#content-checkout').hide();
        });
    });

    unlockScrollBar();
}

function showSideBar(){
    // show #sidebar
    var x = $(document).scrollTop();
    $('#popup-div').css('top',x);
    $('#popup-div').show();
    $('#sidebar').show();
    $('#cover-box').fadeIn(animationSpeed);
    $('#sidebar, #sidebar .sidebar-footer').animate({
        right: '0'
    }, animationSpeed);

    lockScrollBar();
}

function lockScrollBar(){
    // lock scroll position, but retain settings for later
    var scrollPosition = [
      self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
      self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
    ];
    var html = $('html'); // it would make more sense to apply this to body, but IE7 won't have that
    html.data('scroll-position', scrollPosition);
    html.data('previous-overflow', html.css('overflow'));
    html.css('overflow', 'hidden');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);
}

function unlockScrollBar(){
    // Un-lock scroll position
    var html = $('html');
    var scrollPosition = html.data('scroll-position');
    html.css('overflow', html.data('previous-overflow'));
    window.scrollTo(scrollPosition[0], scrollPosition[1]);
}

$(function(){
    // Assign an onClick Listener to the order-list-btn
    $('#order-list-btn').on('click', function(evt){
        evt.preventDefault();
        if ($('#sidebar').is(':visible'))
            hideSideBar();
        else
            showSideBar();
    });

    // Assign an onClick Listener when user try to close the sidebar
    $('#popup-div').on('click', '#cover-box, .close-btn-cart, .close-btn', function(){
        hideSideBar();

        array_product_option = [];
        $('#content-checkout').fadeOut();
        $('#yesno').fadeOut();
        $('#product-option:visible').fadeOut(function(){
            $('#popup-div').hide();
            $('#content-checkout').hide();
        });
    });

    // Assign an onClick Listener to the view-order-history-btn
    $('#popup-div').on('click', '#view-order-history-btn', function(){
        $('#sidebar, #sidebar .sidebar-footer').animate({
            right: '-550px'
        }, animationSpeed, function(){
            $('#cover-box').fadeOut(animationSpeed, function(){
                $('#sidebar').hide();
                $('#content-checkout').hide();
            });
        });

        showLoading();
        unlockScrollBar();
        $.get(base_url + '/order-history', { table_no: '1' }, function(data){
            $('#store-list .active').removeClass('active');

            $('#content .container').fadeOut(function(){
                $(this).html(data);
                $(this).fadeIn();

                hideLoading();
            });
        });
    });
});
