var url = '/smk/public/receipt/';
var token = $('meta[name="token"]').attr('content');
var orders_no = [];
$(function(){
    function scrollTop(){
        $("html, body").animate({scrollTop:0}, '500', 'swing', function() {
            $("#barcode").focus();
        });
    }
    function numberFormat(price){
        price = parseFloat(price).toLocaleString(undefined, {minimumFractionDigits: 2});
        return price;
    }
    function deleteArr(arr,delValue){
        i = arr.indexOf(delValue);
        if(i != -1) {
            arr.splice(i, 1);
        }
    }
    $('#barcode').on('keyup', function(e){
        var barcode = $("#barcode").val();
        $('.error-barcode').hide();
        clearTimeout($.data(this, 'timer'));
        if(barcode != ''){
            if (e.keyCode == 13)
                checkBarcode(barcode);
            else
                $(this).data('timer', setTimeout(checkBarcode(barcode), 500));
        }
    });
    function checkBarcode(barcode){
        $.ajax({
            url: url+'barcode',
            headers: { 'X-CSRF-TOKEN': token },
            data: { 'barcode':barcode },
            type: 'POST',
            success: function (data) {
                if(data.status == '1'){
                    rtnOrder = data.order;
                    ordersId = rtnOrder.orders_id;
                    totalPrice = rtnOrder.total_price + rtnOrder.discount_price;

                    if(orders_no.indexOf(ordersId) <= -1){
                        orders_no.push(ordersId);
                        var body_list = '';
                        $( rtnOrder ).each(function(  key, value ) {
                            body_list += "<tr>";
                                body_list += "<td class='text-center'><input type=\"checkbox\" name='arr_select[]' value=\""+value.orders_id+"\" class='arr_select' checked></td>";
                                body_list += "<td class='text-center'>"+value.created_at+"</td>";
                                body_list += "<td class='text-left'>"+value.orders_no+"</td>";
                                body_list += "<td class='text-right'>"+numberFormat(value.total_price)+"</td>";
                                body_list += "<td class='text-center'><a href='#' class='delete' data-key='"+ordersId+"'><i class=\"glyphicon glyphicon-trash text-danger\" ></i></a></td>";
                            body_list += "</tr>";
                        });
                        $('.orders-list .default-list').hide();
                        $('.orders-list').append(body_list);
                    }
                    console.log(orders_no);
                }else{

                }
                // clear input barcode
                $("#barcode").val('');
            }
        });
    }

    $('.orders-list').on('click','.delete',function(e){
        e.preventDefault();
        var delKey = $(this).data('key');
        $(this).parent().parent().remove();
        deleteArr(orders_no,delKey);
        if($('.orders-list tr').length <= 1) {
            $('.orders-list .default-list').show();
        }
        console.log(orders_no);

    });

    $('.print_type').on('change',function(){
        var print_type = $(this).val();
        $('.input-send .error').hide();
        $('.mobile').val('').attr('disabled','disabled');
        $('.email').val('').attr('disabled','disabled');
        if(print_type == '1'){
            $('.mobile').removeAttr('disabled');
        }else if(print_type == '2'){
            $('.email').removeAttr('disabled');
        }
    });

    $('.btn-submit').on('click',function(e){
        e.preventDefault();
        var result = 1;

        var arr_select = [];
        $('.arr_select:checked').each(function(key, value){
            arr_select.push($(this).val());
        });
        var print_type = $('.print_type:checked').val();
        var mobile = $('input[name=mobile]').val();
        var email = $('input[name=email]').val();
        var company_name = $('input[name=company_name]').val();
        var branch = $('input[name=branch]').val();
        var address = $('textarea[name=address]').val();
        var tax_id = $('input[name=tax_id]').val();

        // select print type
        if($('.print_type').is(':checked') == false){
            $('.error.error-type').show();
            result = 0;
            console.log('Print Type');
        }
        // print type = sms
        if(print_type == '1'){
            if(mobile == ''){
                $('.mobile').focus();
                $('.error.error-mobile').show();
                result = 0;
            }
        }
        // print type = email
        if(print_type == '2'){
            if(email == ''){
                $('.email').focus();
                $('.error.error-email').show();
                result = 0;
            }
        }
        // address
        if(company_name == ''){
            $('input[name=company_name]').parent().find('.error').show();
            result = 0;
        }
        if(address == ''){
            $('textarea[name=address]').parent().find('.error').show();
            result = 0;
        }
        if(tax_id == ''){
            $('input[name=tax_id]').parent().find('.error').show();
            result = 0;
        }

        if($('.arr_select').is(':checked') == false || orders_no.length <= 0){
            scrollTop();
            result = 0;
        }

        console.log('Result '+result);
        if(result == 1){
            $.ajax({
                url: url+'send',
                headers: { 'X-CSRF-TOKEN': token },
                data: {
                    'arr_select'    : arr_select,
                    'print_type'    : print_type,
                    'company_name'  : company_name,
                    'branch'    : branch,
                    'address'   : address,
                    'tax_id'    : tax_id,
                    'mobile'    : mobile,
                    'email'     : email
                },
                type: 'POST',
                success: function (data) {
                    if(data.status == '1'){
                        $('.arr_select:checked').each(function(key, value){
                            delKey = $(this).val();
                            deleteArr(orders_no,delKey);
                        });
                        $('.arr_select:checked').parent().parent().remove();
                        if($('.orders-list tr').length <= 1){
                            location.reload();
                        }else{
                            scrollTop();
                        }
                        console.log(orders_no);
                    }
                    console.log(data);
                }
            });
        }
    });

    $('.input-address input[type=text], textarea').on('keyup',function(){
        $(this).parent().find('.error').hide();
    });
    $('.input-send input[type=text]').on('keyup',function(){
        $('.input-send .error').hide();
    });
});
