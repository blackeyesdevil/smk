function loadPage(url){
    $.get(url, function(data){
        $('#content .container').fadeOut(function(){
            $('#product-list').html('');
            filterLang(data, $('#product-list'));
            $(this).fadeIn(function(){
              hideLoading();
            });
        });
    });
}
 $('#ui-main').on('click', '.category-item', function () {
    showLoading();

    $('#category-bar .category-item.active').removeClass('active');
    $(this).addClass('active');

    if ($(this).hasClass('all')){
        var store_id = $('.store-item.active').data('id');

        // Get A List of Products by store_id
        var url_product_des = '/product/by-store/' + store_id;
        $.getJSON(api_url + url_product_des , null , function (data) {
            var products = data.products;
            var product_list='';

            $('#product-list').fadeOut('fast', function(){
                $('#product-list').html('');

                if (products.length == 0){
                    resultHtml = '<div class="text-center">'
                                      + '<span class="en">There is no products in this store.</span>'
                                      + '<span class="th">ยังไม่มีเมนูสินค้าในร้านนี้</span>'
                                  + '</div>';

                    filterLang(resultHtml, $('#product-list'));
                }
                else{
                    $.each( products, function( key, value ) {
                        product_list = '<div class="product-item col-xs-4" data-product-id="'
                                              + value.product_id
                                              + '">'
                                             + '<div class="product-image" style="background-image: url(\'' + value.image + '\')"></div>'
                                             + '<div class="product-info">'
                                                 + '<div class="product-name">'
                                                     + '<span class="en">' + value.product_name_en + '</span>'
                                                     + '<span class="th">' + value.product_name_th + '</span>'
                                                 + '</div>'
                                                 + '<div class="product-price">'
                                                     + '<span class="en">' + value.price + ' THB</span>'
                                                     + '<span class="th">' + value.price + ' บาท</span>'
                                                 + '</div>'
                                             + '</div>'
                                         + '</div>';

                        if ((key+1)%3==0)
                          product_list += '<div class="clearfix"></div>';

                        filterLang(product_list, $('#product-list'));
                    });
                }

                hideLoading();
                $(this).fadeIn('fast');
            });
        });
    }
    else if($(this).hasClass('promotion')){
        showLoading();
        loadPage(base_url + '/promotion');
    }
    else if($(this).hasClass('set')){
        showLoading();
        loadPage(base_url + '/special-set');
    }
    else{
      console.log('in');
        var url_category_des = '/product/by-category/'+$(this).attr('data-id');
        $.getJSON(api_url + url_category_des , null , function (data) {
            var products = data.products;
            var product_list='';

            $('#product-list').fadeOut(function(){
                $('#product-list').html('');

                if (products.length == 0){
                    resultHtml = '<div class="text-center">'
                                      + '<span class="en">There is no products in this category.</span>'
                                      + '<span class="th">ยังไม่มีเมนูสินค้าในหมวดหมู่นี้</span>'
                                  + '</div>';

                    filterLang(resultHtml, $('#product-list'));
                }
                else{
                    $.each( products, function( key, value ) {
                        product_list = '<div class="product-item col-xs-4" data-product-id="'
                                            + value.product_id
                                            + '">'
                                           + '<div class="product-image" style="background-image: url(\'' + value.image + '\')"></div>'
                                           + '<div class="product-info">'
                                               + '<div class="product-name">'
                                                   + '<span class="en">' + value.product_name_en + '</span>'
                                                   + '<span class="th">' + value.product_name_th + '</span>'
                                               + '</div>'
                                               + '<div class="product-price">'
                                                   + '<span class="en">' + value.price + ' THB</span>'
                                                   + '<span class="th">' + value.price + ' บาท</span>'
                                               + '</div>'
                                           + '</div>'
                                       + '</div>';

                        if ((key+1)%3==0)
                          product_list += '<div class="clearfix"></div>';

                        filterLang(product_list, $('#product-list'));
                    });
                }

                hideLoading();
                $(this).fadeIn('fast');
            });
        });
    }
});

$('#ui-main').on('click', '.product-item', function(){
    showLoading();

    var x = $(document).scrollTop();
    $('#popup-div').css('top',x);
    $('#popup-div').show();
    $('#product-option .product-info').html('');
    $('#product-option .option-div .options').html('');

    $.getJSON(api_url + '/product/detail/' + $(this).data('product-id'), null, function(data){
        var product = data.product;
        var options = data.options;

        var product_res = JSON.stringify(product);
        for(var i=0; i < product_res.length; i++) {
            product_res = product_res.replace("\"", "'");
        }

        var product_info = '<div class="product-name">'
                                + '<span class="en">' + product.product_name_en + '</span>'
                                + '<span class="th">' + product.product_name_th + '</span>'
                                + '<input type="hidden" class="product-description" value="'+product_res+'">'
                            + '</div>'
                            + '<div class="product-description">'
                                + '<span class="en">' + product.description_en + '</span>'
                                + '<span class="th">' + product.description_th + '</span>'
                            + '</div>'
                            + '<div class="product-price">'
                                + '<span class="en">' + product.price + ' THB</span>'
                                + '<span class="th">' + product.price + ' บาท</span>'
                            + '</div>'

        $('#product-option .product-item').attr('style', 'background-image: url(\'' + product.image + '\')');
        $('#product-option .product-info').html(product_info);

        var option_details = '';
        var option_detail = '';
        var values = '';

        $.each(options, function(key, value){

          console.log(value);
          var value_en = value.value_en;
          var value_th = value.value_th;
          var res_split_en = value_en.split(",");
          var res_split_th = value_th.split(",");
          console.log(res_split_en);

          var label_name = value.label_en.replace(/ /g, '');
              // option_detail = '<div class="option-item col-xs-12">'
              //                   + '<div class="topic">'
              //                       + '<span class="en">' + value.label_en + '</span>'
              //                       + '<span class="th">' + value.label_th + '</span>'
              //                   + '</div>'
              //                   + '<div class="values"></div>'
              //               + '</div>';
          if(value.type == "r"){
            option_detail = '<div class="option-item col-xs-12">'
                              + '<div class="topic">'
                                  + '<span class="en">' + value.label_en + '</span>'
                                  + '<span class="th">' + value.label_th + '</span>'
                              + '</div>'
                              + '<div class="values"></div>'
                              + '<div id="alert'+key+'" class="alert col-xs-12"></div>'
                          + '</div>';
                          for(var i = 0 ; i < res_split_en.length ; i++){
                              var option_replace_en = res_split_en[i].replace(/[(:)]/g," ");
                                  option_replace_en = option_replace_en.split(' ');
                              var option_replace_th = res_split_th[i].replace(/[(:)]/g," ");
                                  option_replace_th = option_replace_th.split(' ');

                              // console.log('mystring');
                              // console.log(mystring);
                              values += '<label class="value-item col-xs-4">';
                              values += '<input type="radio" class="" name="option' + key + '" data-name-lang-th="'+ res_split_th[i] +'" data-name-lang-en="'+res_split_en[i]+'">'
                                        +'<span class="en">' + option_replace_en[0] + ' (+' +option_replace_en[2]+ ' THB)</span>'
                                        + '<span class="th">' + option_replace_th[0] + ' (+' + option_replace_th[2]+ ' THB)</span>';
                              values += '</label>';
                          }
          $('.option-div .options').append(option_detail);
          $('.option-div .options .option-item:last .values').html(values);
          option_detail = '';
          values = '';
          }else if(value.type == "c"){
            option_detail = '<div class="option-item col-xs-12">'
                              + '<div class="topic">'
                                  + '<span class="en">' + value.label_en + '</span>'
                                  + '<span class="th">' + value.label_th + '</span>'
                              + '</div>'
                            + '<div class="values"></div>'
                            + '<div id="alert'+key+'" class="alert col-xs-12"></div>'
                            + '</div>';
                          for(var i = 0 ; i < res_split_en.length ; i++){
                              var option_replace_en = res_split_en[i].replace(/[(:)]/g," ");
                                  option_replace_en = option_replace_en.split(' ');
                              var option_replace_th = res_split_th[i].replace(/[(:)]/g," ");
                                  option_replace_th = option_replace_th.split(' ');

                              values += '<label class="value-item col-xs-4">';
                              values += '<input type="checkbox" class="" name="option' + key + '" data-name-lang-th="'+ res_split_th[i] +'" data-name-lang-en="'+ res_split_en[i] +'">'
                                        +'<span class="en">' + option_replace_en[0] + ' (+' +option_replace_en[2]+ ' THB)</span>'
                                        + '<span class="th">' + option_replace_th[0] + ' (+' + option_replace_th[2]+ ' THB)</span>';
                              values += '</label>';
                          }
            $('.option-div .options').append(option_detail);
            $('.option-div .options .option-item:last .values').html(values);
            option_detail = '';
            values = '';
          }

        });

        // $('.option-div .options').append(option_detail);
        var total = '<div class="total">'
                      + 'TOTAL&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'<span id="sum_price">'+product.price+'</span>'+' THB'
                    + '</div>';
        $('.option-div .options').append(total);
        // $('.option-div .options .option-item:last .values').html(values);

        var anotherLang = (lang=='en')?'th':'en';
        $('#product-option').find('.' + anotherLang).removeClass('show').addClass('hide');
        $('#product-option').find('.' + lang).removeClass('hide').addClass('show');

        $('#popup-div').fadeOut(300, function(){
            $('#popup-div #loading').hide();
            $(this).css('background', 'none').show();

            $("#cover-box").fadeIn();
            $("#product-option").fadeIn();
        });

        lockScrollBar();
    });
});



var array_product_option =[];
var array_product_option_push = '';
var flag = 0;
var key_check = 0;

$(document).on('change', 'input:checkbox',function () {

      if($(this).prop('checked') === true){
        var option_val = $(this).val();
        var name_lang_en = $(this).data('name-lang-en');
        var name_lang_th = $(this).data('name-lang-th');
        // console.log(option_val)
        console.log("name_lang_th"+name_lang_th);
        console.log("name_lang_en"+name_lang_en);

        var res_split = name_lang_en.split("(:");
        var price = res_split[1].split(":)");

        console.log(price);
        console.log(price[0]);
        var sum_price = parseInt($('span#sum_price').text());
        var sum   = sum_price + parseInt(price[0]);

        var array_product_option_push = '{ "option_name": "'+ $(this).attr('name')+'" , "option_value_en": "'+name_lang_en+'" ,"option_value_th": "'+ name_lang_th +'", "option_price":'+price[0]+'}';
            array_product_option_push  = JSON.parse(array_product_option_push);
            array_product_option.push(array_product_option_push);

            $('span#sum_price').html('');
            $('span#sum_price').html(sum);

      }else if($(this).prop('checked') === false){
        var option_val = $(this).val();
        var name_lang_en = $(this).data('name-lang-en');
        var name_lang_th = $(this).data('name-lang-th');
        var res_split = name_lang_en.split("(:");
        var price = res_split[1].split(":)");
        var sum_price = parseInt($('span#sum_price').text());
        var sum   = sum_price - parseInt(price[0]);

        array_product_option = array_product_option.filter(function(optionvalue) {

            return optionvalue.option_value_en !== name_lang_en;

        });

        $('span#sum_price').html('');
        $('span#sum_price').html(sum);

      }
});

$(document).on('change', 'input:radio',function () {
    var attr_name = $(this).attr('name');
    var attr_val = $(this).val();
    var name_lang_th = $(this).data('name-lang-th');
    var name_lang_en = $(this).data('name-lang-en');

    if( array_product_option.length > 0 ){

      for(var i = 0 ; i < array_product_option.length ; i++){
        console.log('attr_name='+attr_name+':'+'value option='+array_product_option[i].option_name);
        console.log('name_lang_en='+name_lang_en+':'+'option_value_en='+array_product_option[i].option_value_en);
        if(array_product_option[i].option_name === attr_name){
          //  ถ้า option_name ตรงกัน และ option value ไม่ตรงกัน update ค่าใหม่เข้าไป
            if(array_product_option[i].option_value_en !== name_lang_en){
                console.log('')
                key_check = i;
                flag = 2;
                console.log('flag='+flag);
            }

        }
      }

        if(flag == 2 ){ //เมื่อกด option ตัวเก่า แต่คนละ value กัน
          // var option_val = $(this).val();
          var res_split = name_lang_en.split("(:");
          var price = res_split[1].split(":)");
          console.log('price'+price)
          var sum_price = parseInt($('span#sum_price').text());
          var sum   = sum_price - parseInt(array_product_option[key_check].option_price); // ลบราคาเก่าทิ้ง

          //เอา value ใหม่ แทน value เก่า
          array_product_option[key_check].option_value_en = name_lang_en;
          array_product_option[key_check].option_value_th = name_lang_th;

          array_product_option[key_check].option_price = price[0];
          // console.log('array_product_option_Flag==2===='+JSON.stringify(array_product_option));
          // เพิ่มราคา ตัวใหม่เข้าไป
          sum   = sum + parseInt(array_product_option[key_check].option_price); // บวกราคาใหม่
          $('span#sum_price').html('');
          $('span#sum_price').html(sum);
          flag = 0;

        }else{ // เมื่อกด option ตัวใหม่เข้ามา
          // var option_val = $(this).val();
          var res_split = name_lang_en.split("(:");
          var price = res_split[1].split(":)");

          var array_product_option_push = '{ "option_name": "'+ $(this).attr('name')+'" , "option_value_en": "'+name_lang_en+'" ,"option_value_th": "'+ name_lang_th +'", "option_price":'+price[0]+'}';
          array_product_option_push  = JSON.parse(array_product_option_push);
          array_product_option.push(array_product_option_push);
          // console.log('array_product_option===='+JSON.stringify(array_product_option));
          // บวกราคาใหม่เข้าไป
          var sum_price = parseInt($('span#sum_price').text());
          // var sum   = sum_price + parseInt(array_product_option[array_product_option.length-1].option_price); // บวกราคาใหม่
          var sum   = sum_price + parseInt(price[0]); // บวกราคาใหม่
          $('span#sum_price').html('');
          $('span#sum_price').html(sum);
          flag = 0;

        }


    }else{
      // var name_lang_en = $(this).val();
      var res_split = name_lang_en.split("(:");
      var price = res_split[1].split(":)");
      // var price_spl = price[0].split("d");

      var array_product_option_push = '{ "option_name": "'+ $(this).attr('name')+'" , "option_value_en": "'+name_lang_en+'" ,"option_value_th": "'+ name_lang_th +'", "option_price":'+price[0]+'}';
      array_product_option_push  = JSON.parse(array_product_option_push);
      array_product_option.push(array_product_option_push);

      var sum_price = parseInt($('span#sum_price').text());
      var sum   = sum_price + parseInt(price[0]); // บวกราคาใหม่

      $('span#sum_price').html('');
      $('span#sum_price').html(sum);


    }

});

$('#popup-div').on('click', '.btn-add-cart', function(){

    var option_item = '.option-item';
    var numItems = $(option_item).length;
    console.log('numberItem'+numItems);
    var isOptionCheckSingle = false; // มีการ checked อย่างน้อย 1 ครั้ง
    var isOptionCheckMuti = false;  // เช็คทั้งหมด
    var isOptionCheckProgress = true;  // เช็คทั้งหมด
    for(var j=0 ; j < numItems ; j++){
      var alert_str = '#alert'+j;
      $(alert_str).html('');
      var value_item = '.option-item .value-item input[name=option'+j+']';
      console.log('value_item'+$(value_item).attr('type'));
        if($(value_item).attr('type') == 'radio'){
            if($(value_item).is(':checked') == true){
              isOptionCheckSingle = true;
            }else{
              $(alert_str).text('* Please Select Option ');
              isOptionCheckProgress = false;
            }

        }else if($(value_item).attr('type') == 'checkbox'){
            isOptionCheckProgress = true;
        }
    }


    if( isOptionCheckProgress == true ){
        var product_str = $('.product-description').val();
        for(var i=0; i < product_str.length; i++) {
            product_str = product_str.replace("'", "\"");
        }

        product_str = JSON.parse(product_str);
        // console.log(JSON.stringify(product_str));
        var str_array_product_option =  JSON.stringify(array_product_option);

        var array_product = '{ "product_id": '+ product_str.product_id + ',"product_name_en": "' + product_str.product_name_en + '" ,"product_name_th": "' + product_str.product_name_th +'", "option": '+str_array_product_option+', "price":'+ $('span#sum_price').text() +', "img_name":"'+product_str.image+'","qty":1, "total_price" : '+$('span#sum_price').text()+'}';
            array_product  = JSON.parse(array_product);
        array_product_option = [];

        checkCart(array_product,add_to_cart,lang);

        // add_to_cart.push(array_product);
        console.log("add_to_cart>>>>>>>>"+JSON.stringify(add_to_cart));
        showCart();
        $('#cover-box').fadeOut();
        hideSideBar();
        $('#product-option').fadeOut(function(){
            $('#popup-div').hide();
        });
        $('#noti-add-cart').html('');
        $('#noti-add-cart').hide().css('margin-top', '10px');
        // $('#noti-add-cart').fadeIn(300);

        var noti = '<div id="noti-detail">'
                      + '<div class="product-img"><img src="'+product_str.image+'"></div>'
                      + '<div class="product-info">'
                          + '<div class="product-name">' + product_str.product_name_en + '</div>'
                          + '<div class="product-price">' + array_product.price + ' THB'+'</div>'
                      + '</div>'
                  + '</div>';
        $('#noti-add-cart').append(noti);
        $('#noti-add-cart').show().animate({marginTop: '0px', opacity: '1'}).delay(3000).animate({opacity: '0'});
  }

});

// $('#cover-box, .close-btn').on('click', function(){
//     array_product_option = [];
//     hideSideBar();
//     $('#cover-box').fadeOut();
//     $('#content-checkout').fadeOut();
//     $('#yesno').fadeOut();
//     $('#product-option').fadeOut(function(){
//         $('#popup-div').hide();
//         $('#content-checkout').hide();
//     });
// });

$('#cover-box-popup').on('click', function(){
    // array_product_option = [];
    $('#cover-box-popup').fadeOut();
    $('#content-checkout').fadeOut();
    // $('#content-checkout').fadeOut();
    $('#yesno').fadeOut();
    // hideSideBar();
    // $('#product-option').fadeOut(function(){
    //     $('#popup-div').hide();
    //     // $('#content-checkout').hide();
    // });
});

// ---------- CHECKOUT BTN----------
$('.checkout-btn').on('click', function(){
    $('#content-checkout').fadeIn(200);
    $('#cover-box-popup').css('z-index', '1000').fadeIn(200);
    console.log(add_to_cart);
    showCart();
    function showCart(){
        // carts = add_to_cart;

        var product_item='';
        var total_price = 0;
        console.log("carts: "+JSON.stringify(carts));
        for (var i = 0; i < carts.length; i++) {
            var check_topic ='';
            var data = carts[i];
            var options = carts[i].option;
            total_price += parseInt(carts[i].price);
            if( lang == 'en'){
              var product_name = data.product_name_en;
            }else if( lang == 'th'){
              var product_name = data.product_name_th;
            }
            product_item = product_item +
                            '<div class="product-orders">'+
                                '<div class="row">'+
                                    // '<div class="product-info col-xs-6">'+
                                        '<div class="product-name col-xs-9">'+
                                            product_name+
                                        '</div>'+
                                        '<div class="product-price col-xs-3">'+
                                        data.total_price+'THB'+
                                        '</div>';

                                        for (var j = 0; j < options.length; j++) {
                                          if( lang == 'en'){
                                            var option_val = options[j].option_value_en;
                                                option_val = option_val.split("(");
                                          }else if(lang == 'th'){
                                            var option_val = options[j].option_value_th;
                                                option_val = option_val.split("(");
                                          }
                                        product_item = product_item+     '<div class="product-option-item">'+
                                                // '<div class="topic col-xs-6">'+
                                                // options[j].option_name+':'+
                                                // '</div>'+
                                                '<div class="value col-xs-12">'+
                                                  '<span id="checkout-value-option" style="margin-left: 30px;padding-top: -20px"> -'+option_val[0]+' +'+options[j].option_price+'</span>'+
                                                  // option_val[0]+' +'+options[j].option_price+
                                                '</div>'+
                                        '</div>';

                                        }

            //                                 for (var j = 0; j < options.length; j++) {
            //                                   if( lang == 'en'){
            //                                     var option_val = options[j].option_value_en;
            //                                   }else if( lang == 'th'){
            //                                     var option_val = options[j].option_value_th;
            //                                   }
            // product_item = product_item+     '  <div class="product-option col-xs-9">'+
            //                                         // '<div class="product-option-item col-xs-2">'+
            //                                         // options[j].option_name+':'+
            //                                         // '</div>'+
            //
            //                                         '<div class="value">'+
            //                                          '<span id="checkout-value-option" style="margin-left: 30px;padding-top: -20px"> -'+option_val+'</span>'+
            //                                         '</div>'+
            //                                 '</div>';
            //                                 // }
            //                             }
            product_item = product_item+
                                            '</div>'+
                                        // '</div>'+
                                    // '</div>'+
                                '</div>'+

                        '</div>';


        }
        // TOTal ------------------------
        var checkout_sum_price = 0;
        for (var i = 0; i < add_to_cart.length; i++) {
          var datas = add_to_cart[i];
          checkout_sum_price = checkout_sum_price+parseInt(datas.total_price);

        }

        $('.product-order-list').html('');
        $('.product-order-list').append(product_item);
        $('.checkout-total-price').html('<div class="col-xs-8">Sub Total</div><div class="total col-xs-4">'+checkout_sum_price+' THB</div>');
        $('.checkout-total-discount').html('<div class="col-xs-8">Grand Total</div><div class="col-xs-4">'+(checkout_sum_price - discount_price) + ' THB</div>');

    }

});


$('#content-checkout').on('click', '.close-checkout-btn', function(){
    $('#content-checkout').fadeOut();
    $('#cover-box-popup').fadeOut();
});

function checkCart(product,carts,lang){
    var duplicate = 0;
    for (var i = 0; i < carts.length; i++) {
        var data = carts[i];
        var carts_option = carts[i].option;
        console.log('option length'+carts_option.length);
        // if(lang=='en'){
        //     product_name = product.product_name_en;
        //     carts_name = data.product_name_en;
        //
        // }
        // else(lang=='th'){
        //     product_name = product.product_name_th;
        //     carts_name = data.product_name_th;
        // }
        console.log(data.product_id==product.product_id);
        if(data.product_id==product.product_id){
            console.log('same product id');
            if(product.option.length==0 && data.option.length==0){
                duplicate = 1;
                console.log('no option');
                break;
            }
            else{
                console.log('have option');
                duplicate = 0;
            }
            for (var j = 0; j < carts_option.length; j++) {
                cart_option_name = carts_option[j].option_name;
                product_option_name = product.option[j].option_name;

                if(lang=='en'){
                    // cart_option_name = carts_option[j].option_name_en;
                    cart_option_value = carts_option[j].option_value_en;
                    // product_option_name = product.option[j].option_name_en;
                    product_option_value = product.option[j].option_value_en;
                }
                else{
                    // cart_option_name = carts_option[j].option_name_th;
                    cart_option_value = carts_option[j].option_value_th;
                    // product_option_name = product.option[j].option_name_th;
                    product_option_value = product.option[j].option_value_th;
                }
                if(cart_option_name==product_option_name&&cart_option_value==product_option_value){
                    duplicate = 1;
                }
                else{
                    duplicate = 0;
                }
            }
            if(duplicate==1){
                break;
            }
            else{}
        }
    }
    if(duplicate==1){
        data.qty = data.qty+1;
        data.total_price = data.price*data.qty;
        console.log(data);
        console.log('duplicate');
    }
    else{
        console.log(">>>>><<<<<");
        console.log('add new to cart');
        carts.push(product);
    }
}
