$(function(){
  $('#ui-main').on('click', '.order-item', function(){
    $('#order-history .order-item.active .order-detail').slideUp(function(){
      $('#order-history .order-item.active').removeClass('active');
    });

    if (!$(this).hasClass('active')){
      var item = $(this);
      item.find('.order-detail').slideDown(function(){
        item.addClass('active');
      });
    }
  });
});
