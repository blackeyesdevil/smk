// var carts = [];
//
// item1 = {
//     "product_name":"Bric Box",
//     "price":"230",
//     "qty":"1",
//     "img_name":"no-pic.png",
//     "product_option":[
//         {
//             "topic":"Ice Cream Flavor",
//             "value":"Chocolate",
//             "price":"30"
//         }
//     ],
//     "total_price":"230"
// }
// item2 = {
//     "product_name":"Bric Rice",
//     "price":"400",
//     "qty":"1",
//     "img_name":"no-pic.png",
//     "product_option":[
//         {
//             "topic":"Ice Cream Flavor",
//             "value": "Chocolate",
//             "price":"10"
//         },
//         {
//             "topic":"Topping",
//             "value":"Whip Cream",
//             "price":"20"
//         },
//         {
//             "topic":"Topping",
//             "value":"Banana",
//             "price":"15"
//         },
//         {
//             "topic":"Topping",
//             "value":"Jelly",
//             "price":"20"
//         }
//     ],
//     "total_price":"400"
// }
// carts.push(item1);
// carts.push(item2);

showCart();
function showCart(){
    // console.log(add_to_cart);
    if(add_to_cart!=''){
      $('.total-price').show();
      $('.checkout-btn').show();
      $('.text-des').hide();
    }
    carts = add_to_cart;
    var product_item='';
    var total_price = 0;
    for (var i = 0; i < carts.length; i++) {
        var check_topic ='';
        var data = carts[i];
        var options = carts[i].option;

        total_price += parseInt(carts[i].price);

        if( lang == 'en'){
          var product_name = data.product_name_en;
        }else if(lang == 'th'){
          var product_name = data.product_name_th;
        }
        product_item = product_item +
                        '<div class="order-detail-item ">'+
                            '<div class="row">'+
                                '<div class="product-img col-xs-2">'+
                                    '<img src="'+data.img_name+'" alt=""/>'+
                                '</div>'+
                                '<div class="product-info col-xs-6">'+
                                    '<div class="product-name">'+
                                    '<span class="en">'+data.product_name_en+'</span>'+
                                    '<span class="th hide">'+data.product_name_th+'</span>'+
                                    '</div>'+
                                    '<div class="product-option-list">';
        // product_item = product_item+
        //                                 '<div id="product-option-item">';

          // if(options.length > 0 ){
          //    $.each(options, function(key, value){
          //      product_item = product_item + '<div class="product-option-item">'+
          //                                         '<div class="topic col-xs-6">'+
          //                                         value.option_name+':'+
          //                                         '</div>'+
          //                                         '<div class="value col-xs-6">'+
          //                                         value.option_value+
          //                                         '</div>'+
          //                                       '</div>';
          //    });
          //   }
            // console.log("option value"+ product_item);


                                        for (var j = 0; j < options.length; j++) {
                                          if( lang == 'en'){
                                            var option_val = options[j].option_value_en;
                                                option_val = option_val.split("(");
                                          }else if(lang == 'th'){
                                            var option_val = options[j].option_value_th;
                                                option_val = option_val.split("(");
                                          }
        product_item = product_item+     '<div class="product-option-item">'+
                                                '<div class="topic col-xs-6">'+
                                                options[j].option_name+':'+
                                                '</div>'+
                                                '<div class="value col-xs-6">'+
                                                  option_val[0]+' +'+options[j].option_price+
                                                '</div>'+
                                        '</div>';

                                    }
        product_item = product_item+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="qty col-xs-3"  id="'+i+'">'+
                                        '<span class="control-btn">-</span>'+
                                        '<span class="qty-number">'+data.qty+'</span>'+
                                        '<span class="control-btn">+</span>'+
                                    '</div>'+
                                    '<div class="remove col-xs-1">'+
                                        '<button class="remove-btn" id="'+i+'">'+
                                            '<span>X</span>'+
                                        '</button>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="subtotal-price" id="subtotal'+i+'">'+
                                    data.total_price+' THB'+
                                '</div>'+
                            '</div>'+

                    '</div>';
    }

    $('.order-detail-list').html('');
    $('.order-detail-list').append(product_item);
    $('.total-price .value').text(total_price+' THB');
    $('.control-btn').off();
    $('.remove-btn').off();
    $('#yes').off();
    $('#no').off();
    $('.control-btn').on('click', function(){
        sign = $(this).text();
        var id = $(this).closest("div").attr("id");
        addOrSub(sign,id);
    });
    $('.remove-btn').on('click',function(){
        $('#yesno').fadeIn(200);
        id = $(this).attr('id');
        $('#cover-box-popup').css('z-index', 3000);
        $('#cover-box-popup').fadeIn(200);
        $('#yes').on('click',function(){
            console.log('before remove ');
            console.log(carts);
            carts.splice(id,1);
            console.log('after remove ');
            console.log(carts);
            $('#cover-box-popup').fadeOut();
            $('#yesno').fadeOut();
            showCart();
        });
        $('#no').on('click',function(){
            $('#yesno').fadeOut();
            $('#cover-box-popup').fadeOut();
        })
    });
    updateTotal();
    $('#cover-box-popup').css('z-index', -3000);
}
function addOrSub(sign,id){
    var qty = carts[id].qty;
    var price = carts[id].price;
    var total_price = 0;
    qty = parseInt(qty);
    if(sign=='+'){
        qty+=1;
    }
    else{
        if(qty==1){
            qty=1;
        }
        else{
            qty-=1;
        }
    }
    String(qty);
    carts[id].qty = qty;
    console.log();
    $('#'+id+' .qty-number').text(qty);
    carts[id].total_price = qty * price;
    total_price = carts[id].total_price;
    total_price = String(total_price)+' THB';
    $('#subtotal'+id).text(total_price);
    updateTotal();
}
function updateTotal(){
    var subtotal = 0;
    for (var i = 0; i < carts.length; i++) {
        subtotal += parseInt(carts[i].total_price);
    }
    subtotal = subtotal + ' THB';
    $('.total-price .value').text(subtotal);

    if(add_to_cart==''){
      $('.total-price').hide();
      $('.checkout-btn').hide();
      $('.text-des').show();
    }

    // Check Qty for show notification icon
    total_qty = 0;
    for (var i=0; i<add_to_cart.length; i++){
        total_qty += add_to_cart[i].qty;
    }
    $('#order-list-btn .noti-number').html(total_qty);
    if (total_qty == 0)
        $('#order-list-btn .noti-div').hide();
    else
        $('#order-list-btn .noti-div').show();
}
