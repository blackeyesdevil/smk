$('#category-bar').on('click', '.category-item', function () {
    $('#category-bar .category-item.active').removeClass('active');
    $(this).addClass('active');

    if ($(this).hasClass('all')){
        var store_id = $('.store-item.active').data('id');

        // Get A List of Products by store_id
        var url_product_des = '/product/by-store/' + store_id;
        $.getJSON(api_url + url_product_des , null , function (data) {
            var products = data.products;
            var product_list='';

            $('#product-list').fadeOut(function(){
                $('#product-list').html('');

                if (products.length == 0){
                    resultHtml = '<div class="text-center">'
                                      + '<span class="en">There is no products in this store.</span>'
                                      + '<span class="th">ยังไม่มีเมนูสินค้าในร้านนี้</span>'
                                  + '</div>';

                    filterLang(resultHtml, $('#product-list'));
                }
                else{
                    $.each( products, function( key, value ) {
                        product_list = '<div class="product-item col-xs-4" data-product-id="'
                                              + value.product_id
                                              + '">'
                                             + '<div class="product-image">'
                                                 + '<img src=' + value.image + '>'
                                             + '</div>'
                                             + '<div class="product-info">'
                                                 + '<div class="product-name">'
                                                     + '<span class="en">' + value.product_name_en + '</span>'
                                                     + '<span class="th">' + value.product_name_th + '</span>'
                                                 + '</div>'
                                                 + '<div class="product-price">' + value.price + ' THB</div>'
                                             + '</div>'
                                         + '</div>';

                        filterLang(product_list, $('#product-list'));
                    });
                }

                $(this).fadeIn();
            });
        });
    }
    else{
        var url_category_des = '/product/by-category/'+$(this).attr('data-id');
        $.getJSON(api_url + url_category_des , null , function (data) {
            var products = data.products;
            var product_list='';

            $('#product-list').fadeOut(function(){
                $('#product-list').html('');

                if (products.length == 0){
                    resultHtml = '<div class="text-center">'
                                      + '<span class="en">There is no products in this category.</span>'
                                      + '<span class="th">ยังไม่มีเมนูสินค้าในหมวดหมู่นี้</span>'
                                  + '</div>';

                    filterLang(resultHtml, $('#product-list'));
                }
                else{
                    $.each( products, function( key, value ) {
                        product_list = '<div class="product-item col-xs-4" data-product-id="'
                                            + value.product_id
                                            + '">'
                                           + '<div class="product-image">'
                                               + '<img src=' + value.image + '>'
                                           + '</div>'
                                           + '<div class="product-info">'
                                               + '<div class="product-name">'
                                                   + '<span class="en">' + value.product_name_en + '</span>'
                                                   + '<span class="th">' + value.product_name_th + '</span>'
                                               + '</div>'
                                               + '<div class="product-price">' + value.price + ' THB</div>'
                                           + '</div>'
                                       + '</div>';

                        product_list = filterLang(product_list, $('#product-list'));
                    });
                }

                $(this).fadeIn();
            });
        });
    }
});

$('#product-list').on('click','.product-item',function () {
    var array_product_option =[];
    var array_product_option_push = '';
    $('.product-detail-in').html('');
    $('.product-option').html('');
    var url_detail_des = '/product/detail/'+$(this).attr('data-product-id');

    $.getJSON(api_url + url_detail_des, null ,function(data){
      // console.log(data);
      var product = data.product;
      var options = data.options;
      console.log(data);
      console.log(product);
      console.log(options);

      var detail = '<div class="head-text">'
                        + product.product_name_en
                    +'</div>'
                    +'<div class="detail-text">'
                        + product.description_en
                    +'</div>'
                    +'<div class="head-text">'
                        + product.price +'THB.'
                    +'</div>';

      $('.product-detail-in').html(detail);
      var option_details = '';
      var option_detail = '';
      // alert(JSON.stringify(options).lenght);
      $.each(options, function(key, value){

          if(key == 0){
               option_detail = '<div class="option">'
                                      + '<div class="head-text">'
                                          + value.label_en
                                      + '</div>'
                                      + '<div class="radio-item">'
                                          + '<input type="radio" id="'+value.label_en+key+'" name="'+value.label_en+'" value="'+ value.value_en + '">'
                                          + '<label for="'+value.label_en+key+'">'
                                              + value.value_en
                                          +'</label>'
                                      + '</div>';
                                    // + '</div>';
              option_details = option_details.concat(option_detail);
              // $('.product-option').append(option_detail);
          }else{
              var label_prev = options[key-1].label_en;
              var label_current = options[key].label_en;
              if( label_prev === label_current){
                   option_detail = '<div class="radio-item">'
                                        + '<input type="radio" id="'+value.label_en+key+'" name="'+value.label_en+'" value="'+ value.value_en + '">'
                                        + '<label for="'+value.label_en+key+'">'
                                            + value.value_en
                                        +'</label>'
                                    // + '</div>'
                                    + '</div>';
                  // $('.product-option').append(option_detail);
                  option_details = option_details.concat(option_detail);

              }
              else{
                 option_detail = '</div>'
                                    +'<div class="option">'
                                      + '<div class="head-text">'
                                          + value.label_en
                                      + '</div>'
                                      + '<div class="radio-item">'
                                        + '<input type="radio" id="'+value.label_en+key+'" name="'+value.label_en+'" value="'+ value.value_en + '">'
                                        + '<label for="'+value.label_en+key+'">'
                                            + value.value_en
                                        +'</label>'
                                      + '</div>';
              option_details = option_details.concat(option_detail);
              }
          }
      });
      var div = '</div>';
      option_details = option_details.concat(div);
      var total = '<div class="total">'
                    + 'TOTAL&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'<span id="sum_price">'+product.price+'</span>'+' THB'
                  + '</div>';
      option_details = option_details.concat(total);
      // console.log(option_details);
      $('.product-option').html(option_details);


      $("#product-detail").fadeIn(200 );
      $("#cover-box").fadeIn(200 );
    });


});

// var array_product_option ='[{"option" : [{ "option_name": "name"}, { "option_name": "name"}, { "option_name": "name"}, { "option_name": "name"}]}]';
var array_product_option =[];
var array_product_option_push = '';
var flag = 0;
var key_check = 0;
$(document).on('change', 'input:radio',function () {
    var attr_name = $(this).attr('name');
    var attr_val = $(this).val();
    console.log(attr_name);
    console.log(attr_val);
    if( array_product_option.length > 0 ){

      for(var i = 0 ; i < array_product_option.length ; i++){
        console.log('attr_name'+attr_name+':'+'value option'+array_product_option[i].option_name);
        if(array_product_option[i].option_name === attr_name){
          //  ถ้า option_name ตรงกัน และ option value ไม่ตรงกัน update ค่าใหม่เข้าไป
            if(array_product_option[i].option_value !== attr_val){
              key_check = i;
               flag = 2;
              //  flag_g = 2;
               console.log('2');
              // return false;
            }

        }
      }
        // $.each(array_product_option, function(key, value){
        // console.log('attr_name'+attr_name+':'+'value option'+value.option_name);
        // if(value.option_name === attr_name){
        //   //  ถ้า option_name ตรงกัน และ option value ไม่ตรงกัน update ค่าใหม่เข้าไป
        //     if(value.option_value !== attr_val){
        //       key_check = key;
        //        flag = 2;
        //        console.log('2');
        //       return false;
        //     }else{
        //       // flag = 3;
        //       // console.log('3');
        //       return false;
        //     }
        //
        // }else{
        //   flag = 0;
        //   console.log('0');
        // }

      // });
      // if(flag == 0){ // เมื่อกด option ตัวใหม่เข้ามา
      //
      // }
      if(flag == 2 ){ //เมื่อกด option ตัวเก่า แต่คนละ value กัน
        var sum_price = parseInt($('span#sum_price').text());
        var sum   = sum_price - parseInt(array_product_option[key_check].option_price); // ลบราคาเก่าทิ้ง
        // console.log('array_product_option'+JSON.stringify(array_product_option));
        console.log('old'+sum);
        //เอา value ใหม่ แทน value เก่า
        array_product_option[key_check].option_value = attr_val;
        array_product_option[key_check].option_price = 20;
        // เพิ่มราคา ตัวใหม่เข้าไป
        sum   = sum + parseInt(array_product_option[key_check].option_price); // บวกราคาใหม่
        $('span#sum_price').html('');
        $('span#sum_price').html(sum);
        flag = 0;
        console.log('new'+sum);
        console.log('array_product_option'+JSON.stringify(array_product_option));
      }else{ // เมื่อกด option ตัวใหม่เข้ามา
        array_product_option_push ='{ "option_name": "'+$(this).attr('name')+'" , "option_value": "'+$(this).val()+'" , "option_price": 50}';
        array_product_option_push  = JSON.parse(array_product_option_push);
        array_product_option.push(array_product_option_push);

        // บวกราคาใหม่เข้าไป
        var sum_price = parseInt($('span#sum_price').text());
        var sum   = sum_price + parseInt(array_product_option[array_product_option.length-1].option_price); // บวกราคาใหม่
        $('span#sum_price').html('');
        $('span#sum_price').html(sum);
        flag = 0;
        // console.log(array_product_option.length-1);
        console.log('newnew'+sum);
        console.log('array_product_option'+JSON.stringify(array_product_option));
      }


    }else{
      array_product_option_push ='{ "option_name": "'+ $(this).attr('name')+'" , "option_value": "'+$(this).val()+'" , "option_price": "10"}';
      array_product_option_push  = JSON.parse(array_product_option_push);
      array_product_option.push(array_product_option_push);
      var sum_price = parseInt($('span#sum_price').text());
      var sum   = sum_price + parseInt(array_product_option[0].option_price); // บวกราคาใหม่
          // console.log(sum);
      $('span#sum_price').html('');
      $('span#sum_price').html(sum);
      console.log(sum);
      console.log('array_product_option'+JSON.stringify(array_product_option));

    }

});
$("#remove, #cover-box").click(function () {
    $("#product-detail").fadeOut(200);
    $("#cover-box").fadeOut(200);
     array_product_option = [];
    //  array_product_option_push = null;
});
